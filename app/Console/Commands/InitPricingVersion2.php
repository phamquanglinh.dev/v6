<?php

namespace App\Console\Commands;

use App\Models\V2Package;
use App\Models\V2Pricing;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitPricingVersion2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:init-pricing-version2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $pricingInitData = [
            [
                'business_size' => V2Pricing::SMALL_BUSINESS,
                'number_of_user' => 3,
                'production_fee' => 5760000,
                'setup_fee' => 500000,
                'setup_number' => 1,
                'data_limit' => 5000,
                'upgrade_fee' => '200,000đ/user/tháng',
                'payment_cycle' => '12 tháng'
            ],
            [
                'business_size' => V2Pricing::MEDIUM_BUSINESS,
                'number_of_user' => json_encode([10, 30, 50]),
                'production_fee' => json_encode([7200000, 12816000, 18816000]),
                'setup_fee' => json_encode([500000, 1000000, 1000000]),
                'setup_number' => json_encode([1, 2, 2]),
                'upgrade_fee' => json_encode([100000, 50000, 35000]),
                'data_limit' => json_encode([10000, 30000, 30000]),
                'payment_cycle' => json_encode([12, 12, 12]),
            ],
            [
                'business_size' => V2Pricing::BIG_BUSINESS,
                'production_fee' => 'Nhận tư vấn',
                'setup_fee' => 'Nhận tư vấn',
                'setup_number' => 'Nhận tư vấn',
                'number_of_user' => 'Nhận tư vấn',
                'data_limit' => 'Nhận tư vấn',
                'payment_cycle' => 'Nhận tư vấn',
                'upgrade_fee' => 'Nhận tư vấn'
            ],
        ];

        if (Schema::hasTable('tbl_v2_pricings')) {
            DB::table('tbl_v2_pricings')->truncate();
            DB::table('tbl_v2_pricings')->insert($pricingInitData);
        }

        $packageInitData = [
            [
                'name' => 'Startup',
                'description' => 'Tất cả trong một',
                'amount_per_month' => '480.000 đ / Tháng',
                'user' => '03 người dùng',
                'data' => '5,000 data',
                'benefit_text' => 'Giải pháp tích hợp các <b> tính năng CRM cho mọi hoạt động </b> của doanh nghiệp.',
                'type' => V2Package::ON_CLOUD_PACKAGE
            ],
            [
                'name' => 'Professional',
                'description' => 'Tiết kiệm chi phí',
                'amount_per_month' => '600.000 đ / Tháng',
                'user' => '10 người dùng',
                'data' => '10,000 data',
                'benefit_text' => 'Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu',
                'type' => V2Package::ON_CLOUD_PACKAGE
            ],
            [
                'name' => 'Enterprise',
                'description' => 'Mạnh mẽ - Linh hoạt',
                'amount_per_month' => 'Liên hệ ngay',
                'user' => 'Liên hệ ngay',
                'data' => 'Liên hệ ngay',
                'benefit_text' => 'Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng',
                'type' => V2Package::ON_CLOUD_PACKAGE
            ],
            [
                'name' => 'Master',
                'description' => 'Tối ưu chi phí',
                'amount_per_month' => '40.000.000 đ',
                'user' => '20 người dùng',
                'data' => 'Không giới hạn data',
                'benefit_text' => 'Tư vấn tuỳ chỉnh theo quy mô doanh nghiệp',
                'type' => V2Package::ON_PREMISE_PACKAGE
            ],
            [
                'name' => 'Enterprise',
                'description' => 'Không giới hạn sức mạnh',
                'amount_per_month' => 'Liên hệ ngay',
                'user' => 'Không giới hạn user',
                'data' => 'Không giới hạn data',
                'benefit_text' => 'Giải pháp chuyển đổi số vượt bậc cho doanh nghiệp lớn',
                'type' => V2Package::ON_PREMISE_PACKAGE
            ],
        ];

        if (Schema::hasTable('tbl_v2_packages')) {
            DB::table('tbl_v2_packages')->truncate();
            DB::table('tbl_v2_packages')->insert($packageInitData);
        }

        $featureInitData = [
            [
                'feature_name' => 'Giao và giám sát mục tiêu KPI',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Quản lý hành trình khách hàng',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Automation MKT (Email, SMS,...)',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Workstream: MXH nội bộ doanh nghiệp',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Landing pages',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Quản lý phản hồi khách hàng',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Quản lý dự án công việc',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1,
            ],
            [
                'feature_name' => 'Tự động phân lead',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1
            ],
            [
                'feature_name' => 'Quản lý chiến dịch marketing',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1
            ],
            [
                'feature_name' => 'Hỗ trợ kết nối API với phần mềm khác',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 1,
                'enterprise_available' => 1,
                'tab' => 1
            ],
            [
                'feature_name' => 'Quản lý tổng đài chăm sóc KH',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Quản lý nhân sự HRM',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Nhắn tin & gọi điện liên phòng ban',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2,
            ],
            [
                'feature_name' => 'Chấm công GPS hoặc IP tĩnh',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Kết nối Facebook, Zalo ghi nhận tiềm năng',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Quản lý Kho & bán lẻ',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Quản lý tích điểm',
                'feature_tooltip' => '#',
                'startup_available' => 1,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Quản lý tài chính công nợ khách hàng',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Thiết lập quy trình bán hàng - sản phẩm',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Sàn TMĐT (Shopee)',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Bảo mật nâng cao theo nhu cầu',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Theo dõi lịch đi tuyến',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
            [
                'feature_name' => 'Theo dõi bảo hành, bảo trì',
                'feature_tooltip' => '#',
                'startup_available' => 0,
                'professional_available' => 0,
                'enterprise_available' => 0,
                'tab' => 2
            ],
        ];

        if (Schema::hasTable('tbl_v2_feature')) {
            DB::table('tbl_v2_feature')->truncate();
            DB::table('tbl_v2_feature')->insert($featureInitData);
        }

        $topPartnerData = [
            [
                'partner_name' => 'Ksc',
                'partner_logo' => v2_url('images/part-1.png'),
                'place_in_top_home' => 1,
                'place_in_bottom_home' => 0,
            ],
            [
                'partner_name' => 'Silicon',
                'partner_logo' => v2_url('images/part-2.png'),
                'place_in_top_home' => 1,
                'place_in_bottom_home' => 0,
            ],
            [
                'partner_name' => 'Road',
                'partner_logo' => v2_url('images/part-3.png'),
                'place_in_top_home' => 1,
                'place_in_bottom_home' => 0,
            ],
            [
                'partner_name' => 'Karakas',
                'partner_logo' => v2_url('images/part-4.png'),
                'place_in_top_home' => 1,
                'place_in_bottom_home' => 0,
            ],
            [
                'partner_name' => 'Esms',
                'partner_logo' => v2_url('images/part-5.png'),
                'place_in_top_home' => 1,
                'place_in_bottom_home' => 0,
            ],
        ];

        $bottomPartnerData = [
            [
                'partner_name' => 'Coach English',
                'partner_logo' => v2_url('images/logo-doitac1.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Amslink',
                'partner_logo' => v2_url('images/logo-doitac2.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'EDUPHIL',
                'partner_logo' => v2_url('images/logo-doitac3.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Mỹ thuật Bụi',
                'partner_logo' => v2_url('images/logo-doitac4.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Mobifone',
                'partner_logo' => v2_url('images/logo-doitac5.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'AustDoor',
                'partner_logo' => v2_url('images/logo-doitac6.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Hoà Phát',
                'partner_logo' => v2_url('images/logo-doitac7.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Mekong',
                'partner_logo' => v2_url('images/logo-doitac8.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Times Pro',
                'partner_logo' => v2_url('images/logo-doitac9.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'CCV',
                'partner_logo' => v2_url('images/logo-doitac10.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Paper World',
                'partner_logo' => v2_url('images/logo-doitac11.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
            [
                'partner_name' => 'Neva Land',
                'partner_logo' => v2_url('images/logo-doitac12.png'),
                'place_in_top_home' => 0,
                'place_in_bottom_home' => 1,
            ],
        ];

        if (Schema::hasTable('tbl_v2_partners')) {
            DB::table('tbl_v2_partners')->truncate();
            DB::table('tbl_v2_partners')->insert($topPartnerData);
            DB::table('tbl_v2_partners')->insert($bottomPartnerData);
        }
    }
}
