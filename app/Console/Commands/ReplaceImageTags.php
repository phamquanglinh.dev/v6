<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ReplaceImageTags extends Command
{
    protected $signature = 'replace:images';
    protected $description = 'Replace <img> tags with asset() in Blade files';

    public function handle()
    {
        $directory = base_path('resources/views');
        $this->replaceInFiles($directory);
        $this->info('Images replaced successfully!');
    }

    protected function replaceInFiles($directory)
    {
        $files = File::allFiles($directory);

        foreach ($files as $file) {
            $content = file_get_contents($file);
            $content = str_replace('<img src="', '<img src="{{ asset("', $content);
            file_put_contents($file, $content);
        }
    }
}
