<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetNewspaperPriority extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newspaper:priority';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Newspaper Priority';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::table("tbl_news")->where("order", 999)->update([
            "order" => 0
        ]);
    }
}
