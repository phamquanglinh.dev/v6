<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetOptInForm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'form:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::table("tbl_form")->delete();
        $tbl_form = array(
            array(
                "id" => 1,
                "name" => "Form đăng ký dùng thử(HN)",
                "link" => "<script\r\n                        type=\"text/javascript\"> (function () {\r\n                        var r = window.document.referrer != \"\" ? window.document.referrer : window.location.origin;\r\n                        var f = document.createElement(\"iframe\");\r\n                        f.setAttribute(\"src\", \"https://1.getfly.vn/api/forms/viewform/?key=f9myphxCNi6I2G3oaC7er9nhIetDyUXsA5TP8AZkCnig9bnWcg&referrer=\" + r);\r\n                        f.style.width = \"100%\";\r\n                        f.style.height = \"500px\";\r\n                        f.setAttribute(\"frameborder\", \"0\");\r\n                        f.setAttribute(\"marginheight\", \"0\");\r\n                        f.setAttribute(\"marginwidth\", \"0\");\r\n                        var s = document.getElementById(\"getfly_hn\");\r\n                        s.appendChild(f);\r\n                    })(); </script>",
                "position" => "dang_ky_hn",
                "created_at" => NULL,
                "updated_at" => "2023-07-03 07:41:39",
            ),
            array(
                "id" => 2,
                "name" => "Form đăng ký dùng thử(TPHCM)",
                "link" => "                <script\r\n                        type=\"text/javascript\"> (function () {\r\n                        var r = window.document.referrer != \"\" ? window.document.referrer : window.location.origin;\r\n                        var f = document.createElement(\"iframe\");\r\n                        f.setAttribute(\"src\", \"https://1.getfly.vn/api/forms/viewform/?key=lTdXfl9DPaesn4AkCmms2W1I3VVeHClaCZfwnJkoMP01shCrs2&referrer=\" + r);\r\n                        f.style.width = \"100%\";\r\n                        f.style.height = \"500px\";\r\n                        f.setAttribute(\"frameborder\", \"0\");\r\n                        f.setAttribute(\"marginheight\", \"0\");\r\n                        f.setAttribute(\"marginwidth\", \"0\");\r\n                        var s = document.getElementById(\"getfly_hcm\");\r\n                        s.appendChild(f);\r\n                    })(); </script>",
                "position" => "dang_ky_tphcm",
                "created_at" => NULL,
                "updated_at" => "2023-07-03 07:41:39",
            ),
            array(
                "id" => 3,
                "name" => "Form liên hệ tư vấn",
                "link" => "<div id=\"getfly-optin-form-iframe-1684912451\"></div>\r\n                    <script\r\n                            type=\"text/javascript\">            (function () {\r\n                            var r = window.document.referrer != \"\" ? window.document.referrer : window.location.origin;\r\n                            var regex = /(https?:\\/\\/.*?)\\//g;\r\n                            var furl = regex.exec(r);\r\n                            r = furl ? furl[0] : r;\r\n                            var f = document.createElement(\"iframe\");\r\n                            const url_string = new URLSearchParams(window.location.search);\r\n                            var utm_source, utm_campaign, utm_medium, utm_content, utm_term, utm_user, utm_account;\r\n                            if ((!url_string.has('utm_source') || url_string.get('utm_source') == '') && document.cookie.match(new RegExp('utm_source' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_source' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_source') != null ? \"&utm_source=\" + url_string.get('utm_source') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_campaign') || url_string.get('utm_campaign') == '') && document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_campaign') != null ? \"&utm_campaign=\" + url_string.get('utm_campaign') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_medium') || url_string.get('utm_medium') == '') && document.cookie.match(new RegExp('utm_medium' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_medium' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_medium') != null ? \"&utm_medium=\" + url_string.get('utm_medium') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_content') || url_string.get('utm_content') == '') && document.cookie.match(new RegExp('utm_content' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_content' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_content') != null ? \"&utm_content=\" + url_string.get('utm_content') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_term') || url_string.get('utm_term') == '') && document.cookie.match(new RegExp('utm_term' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_term' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_term') != null ? \"&utm_term=\" + url_string.get('utm_term') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_user') || url_string.get('utm_user') == '') && document.cookie.match(new RegExp('utm_user' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_user' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_user') != null ? \"&utm_user=\" + url_string.get('utm_user') : \"\";\r\n                            }\r\n                            if ((!url_string.has('utm_account') || url_string.get('utm_account') == '') && document.cookie.match(new RegExp('utm_account' + '=([^;]+)')) != null) {\r\n                                r += \"&\" + document.cookie.match(new RegExp('utm_account' + '=([^;]+)'))[0];\r\n                            } else {\r\n                                r += url_string.get('utm_account') != null ? \"&utm_account=\" + url_string.get('utm_account') : \"\";\r\n                            }\r\n                            f.setAttribute(\"src\", \"https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082&referrer=\" + r);\r\n                            f.style.width = \"100%\";\r\n                            f.style.height = \"480px\";\r\n                            f.setAttribute(\"frameborder\", \"0\");\r\n                            f.setAttribute(\"marginheight\", \"0\");\r\n                            f.setAttribute(\"marginwidth\", \"0\");\r\n                            var s = document.getElementById(\"getfly-optin-form-iframe-1684912451\");\r\n                            s.appendChild(f);\r\n                        })();        </script>",
                "position" => "lien-he.html",
                "created_at" => NULL,
                "updated_at" => "2023-07-03 07:39:15",
            ),
            array(
                "id" => 4,
                "name" => "Form đăng ký tuyển dụng",
                "link" => "<iframe src=\"https://icdt.getflycrm.com/api/forms/viewform?key=jDmOIGJOZDdAb0v5lybSAguArYOM19jFhTxE4QnZ5uqCxZMkxN\" width=\"100%\" height=\"350px\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Loading ...</iframe>",
                "position" => "tuyen-dung.html",
                "created_at" => NULL,
                "updated_at" => "2023-07-03 07:32:13",
            ),
            array(
                "id" => 5,
                "name" => "Form đăng ký đối tác",
                "link" => "<div id=\"getfly-optin-form-iframe-1687235714\"></div>\r\n        <script\r\n                type=\"text/javascript\">  \r\n (function () {\r\n                var r = window.document.referrer != \"\" ? window.document.referrer : window.location.origin;\r\n                var regex = /(https?:\\/\\/.*?)\\//g;\r\n                var furl = regex.exec(r);\r\n                r = furl ? furl[0] : r;\r\n                var f = document.createElement(\"iframe\");\r\n                const url_string = new URLSearchParams(window.location.search);\r\n                var utm_source, utm_campaign, utm_medium, utm_content, utm_term, utm_user, utm_account;\r\n                if ((!url_string.has('utm_source') || url_string.get('utm_source') == '') && document.cookie.match(new RegExp('utm_source' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_source' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_source') != null ? \"&utm_source=\" + url_string.get('utm_source') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_campaign') || url_string.get('utm_campaign') == '') && document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_campaign') != null ? \"&utm_campaign=\" + url_string.get('utm_campaign') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_medium') || url_string.get('utm_medium') == '') && document.cookie.match(new RegExp('utm_medium' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_medium' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_medium') != null ? \"&utm_medium=\" + url_string.get('utm_medium') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_content') || url_string.get('utm_content') == '') && document.cookie.match(new RegExp('utm_content' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_content' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_content') != null ? \"&utm_content=\" + url_string.get('utm_content') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_term') || url_string.get('utm_term') == '') && document.cookie.match(new RegExp('utm_term' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_term' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_term') != null ? \"&utm_term=\" + url_string.get('utm_term') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_user') || url_string.get('utm_user') == '') && document.cookie.match(new RegExp('utm_user' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_user' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_user') != null ? \"&utm_user=\" + url_string.get('utm_user') : \"\";\r\n                }\r\n                if ((!url_string.has('utm_account') || url_string.get('utm_account') == '') && document.cookie.match(new RegExp('utm_account' + '=([^;]+)')) != null) {\r\n                    r += \"&\" + document.cookie.match(new RegExp('utm_account' + '=([^;]+)'))[0];\r\n                } else {\r\n                    r += url_string.get('utm_account') != null ? \"&utm_account=\" + url_string.get('utm_account') : \"\";\r\n                }\r\n                f.setAttribute(\"src\", \"https://icdt.getflycrm.com/api/forms/viewform?key=Pwu2bPjwbj6lP7TZiRFWzsyTNgkHf2LcaRyZhO9vEBjoWCvWXR&referrer=\" + r);\r\n                f.style.width = \"100%\";\r\n                f.style.height = \"460px\";\r\n                f.setAttribute(\"frameborder\", \"0\");\r\n                f.setAttribute(\"marginheight\", \"0\");\r\n                f.setAttribute(\"marginwidth\", \"0\");\r\n                var s = document.getElementById(\"getfly-optin-form-iframe-1687235714\");\r\n                s.appendChild(f);\r\n            })();        </script>",
                "position" => "thu-moi-hop-tac.html",
                "created_at" => NULL,
                "updated_at" => "2023-07-03 07:19:56",
            ),
        );
        foreach ($tbl_form as $form) {
            DB::table("tbl_form")->insert($form);
        }

    }
}
