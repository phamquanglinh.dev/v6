<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PartnerType extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_partner_type";
    protected $guarded = ["id"];

    public function Partners(): HasMany
    {
        return $this->hasMany(Partner::class, "partner_type", "id")->orderBy("id");
    }
}
