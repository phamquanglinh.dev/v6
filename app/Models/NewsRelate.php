<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NewsRelate extends Model
{
    use HasFactory;

    protected $table = 'tbl_news_relate';

    protected $primaryKey = 'relate_news_id';

    protected $fillable = [
        'news_id'
    ];

    public function news(): BelongsTo
    {
        return $this->belongsTo(News::class, 'news_id', 'news_id');
    }
}
