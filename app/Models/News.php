<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * @property string image
 * @property int total_view
 * @property string title
 */
class News extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

//    use RevisionableTrait;

    protected $table = 'tbl_news';

    protected $primaryKey = 'news_id';

    protected $guarded = ["id"];

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope('invalid', function (Builder $builder) {
            $builder->where('invalid', 0);
        });
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'user_id');
    }

    public function Category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'cate_id', "id");
    }

    public function newsRelates(): HasMany
    {
        return $this->HasMany(NewsRelate::class, 'news_id', 'news_id');
    }

    public function getCreatedAtAttribute($date): string
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function setSlugAttribute($value): void
    {
        if ($value != null) {
            $this->attributes['slug'] = $value;
        } else {
            $this->attributes['slug'] = Str::slug($this->title, "-");
        }

    }

    public function setImageAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "/news/images/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["image"] = "assets/uploads/news/images/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["image"] = "assets/uploads/news/images/" . UploadBase64::run($value, "/news/images/", Str::slug($this->title) . Str::random(5) ?? null);
            }
        }
    }

    public function preview(): string
    {
        return '<a target="_blank" href="' . $this->getPreviewUrl() . '"><i class="las la-eye mr-2"></i>Xem trước</a>';
    }

    public function getPreviewUrl(): string
    {
        $categorySlug = $this->Category()->first()?->slug;
        $slug = $this->slug;
        $id = $this["news_id"];
        return url($categorySlug . "/" . $slug . "-n" . $id . ".html");
    }
}
