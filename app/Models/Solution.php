<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * @property string $title
 * @property int $id
 */
class Solution extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = "tbl_solution";
    protected $guarded = ["id"];

    public function findMaxId(): int
    {
        return $this->newQuery()->max("id") + 1;
    }

    public function setLinkDemoAttribute(): void
    {
        $this->attributes["link_demo"] = Str::slug($this->title) . "-s" . ($this->id ?? $this->findMaxId()) . ".html";
    }

    public function getPreviewLink(): string
    {
        return $this->link_demo;
    }
}
