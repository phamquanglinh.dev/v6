<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\UploadedFile;

class TeamInfo extends Model
{
    use CrudTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tbl_team_info';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function setThumbnailAttribute(UploadedFile|string $value): void
    {

        $attribute_name = "thumbnail";
        $disk = "uploads";
        $destination_path = "team";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["thumbnail"] = "/assets/uploads/team/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["thumbnail"] = "/assets/uploads/team/" . UploadBase64::run($value, "/team/", $this->title ?? null);
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function NewsCategoryId(): BelongsTo
    {
        return $this->belongsTo(Category::class, "news_category_id", "id");
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
