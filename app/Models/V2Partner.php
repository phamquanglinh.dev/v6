<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class V2Partner extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'tbl_v2_partners';

    protected $guarded = ['id'];

    public function setPartnerLogoAttribute($value): void
    {
        if (str_contains($value, "base")) {
            $this->attributes["partner_logo"] = "assets/uploads/media/" . UploadBase64::run($value, "/media/", $this->partner_name ?? null);
        }
    }

    public function getPartnerReorderLabelAttribute(): string
    {
        $logo = "<img style='width:5rem' src='" . url($this->getAttribute('partner_logo')) . "'/>";
        $label = $this->getAttribute('place_in_top_home') ? "<b class='text-success mr-2'>[Đầu trang chủ]</b>" : "";
        $label .= $this->getAttribute('place_in_bottom_home') ? "<b class='text-danger mr-2'>[Cuối trang chủ]</b>" : "";

        return $label . $this->getAttribute('partner_name').$logo;
    }
}
