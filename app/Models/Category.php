<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * @property string img_cover
 * @property string title
 */
class Category extends Model
{
    use CrudTrait;
    use HasFactory;
    /**
     * @var string
     */
    protected $table = 'tbl_categories';

    /**
     * @var string[]
     */
    protected $guarded = [
        "id"
    ];

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope('invalid', function (Builder $builder) {
            $builder->where('invalid', 0);
        });
    }

    public function Parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, "parent_id", "id");
    }

    public function Children(): HasMany
    {
        return $this->hasMany(Category::class, "parent_id", "id");
    }

    public function setSlugAttribute(): void
    {
        $this->attributes["slug"] = Str::slug($this->title);
    }
}
