<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

class Photo extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tbl_photos';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    public function orderButton(): string
    {
        return '<a href="' . url("admin/photo/reorder") . '" class="btn btn-success" data-style="zoom-in"><span class="ladda-label"><i class="la la-arrows-alt"></i> Sắp xếp ảnh</span></a>';
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute(UploadedFile|string $value): void
    {

        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "photos";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["image"] = "/assets/uploads/photos/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["image"] = "/assets/uploads/photos/" . UploadBase64::run($value, "/photos/", $this->title ?? null);
            }
        }
    }

    public function setLandScapeImageAttribute(UploadedFile|string $value): void
    {

        $attribute_name = "landscape_image";
        $disk = "uploads";
        $destination_path = "photos";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["landscape_image"] = "/assets/uploads/photos/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["landscape_image"] = "/assets/uploads/photos/" . UploadBase64::run($value, "/photos/", $this->title ?? null);
            }
        }
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
