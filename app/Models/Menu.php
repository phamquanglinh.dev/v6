<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property string $icon
 */
class Menu extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'tbl_menu';

    protected $fillable = [
        'title',
        'slug',
        'link',
        'link_id',
        'type',
        'mgroup',
        'invalid',
        'nleft',
        'nright',
        'parent_id',
        'mega',
        'icon',
        'new_tab'
    ];

    public function Parent(): BelongsTo
    {
        return $this->belongsTo(Menu::class, "parent_id", "id");
    }

    public function children(): HasMany
    {
        return $this->hasMany(Menu::class, "parent_id", "id");
    }


    public function setIconAttribute(UploadedFile|string $value = null): void
    {

        if ($value != null) {
            $attribute_name = "icon";
            $disk = "uploads";
            $destination_path = "menus";
            if ($value instanceof UploadedFile) {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
                $this->attributes["icon"] = "assets/uploads/menus/images/" . $value->getClientOriginalName();
            } else {
                if (str_contains($value, "base")) {
                    $this->attributes["icon"] = "assets/uploads/menus/images/" . UploadBase64::run($value, "/menus/images/", $this->title ?? null);
                }
            }
        }
    }

    public function Tabs(): HasMany
    {
        return $this->hasMany(MegaMenu::class, "parent_id", "id")->orderBy("order", "ASC");
    }

    public function MegaTabs(): BelongsToMany
    {
        return $this->belongsToMany(MegaMenu::class, "tbl_mega_relate", "menu_id", "mega_id");
    }
}
