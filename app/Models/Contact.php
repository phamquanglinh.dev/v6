<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $branch
 * @property string $company_name
 * @property string $contact_name
 * @property string $contact_number
 * @property string $email
 * @property string $address
 * @property string $message
 */
class Contact extends Model
{
    use HasFactory;

    protected $table = "tbl_contacts";
    protected $fillable = [
        'branch',
        'company_name',
        'contact_name',
        'contact_number',
        'email',
        'address',
        'message'
    ];
}
