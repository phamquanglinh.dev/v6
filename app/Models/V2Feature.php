<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class V2Feature extends Model
{
    use HasFactory;

    protected $table = 'tbl_v2_feature';

    protected $guarded = ['id'];
}
