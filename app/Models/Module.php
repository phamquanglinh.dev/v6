<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * @property string $name
 * @property string $url
 * @property string $order
 */
class Module extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_modules";
    protected $fillable = [
        "name",
        "url",
        "order",
        "icon"
    ];

    public function setIconAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "icon";
        $disk = "uploads";
        $destination_path = "/news/images/";

        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["icon"] = "assets/uploads/news/images/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["icon"] = "assets/uploads/news/images/" . UploadBase64::run($value, "/news/images/", Str::slug($this->name) . Str::random(5) ?? null);
            }
        }
    }
}
