<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string avatar
 * @property string password
 * @property string username
 * @property int user_id
 * @property int invalid
 */
class User extends Authenticatable
{
    use CrudTrait;
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'tbl_users';
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_name',
        'full_name',
        'password',
        'avatar',
        'description',
        'group_user',
        'invalid',

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value): void
    {
        if ($value != "" && $value != null)
            $this->attributes["password"] = md5($value);
    }

    public function getUserRole(): string
    {
        return match ($this->group_user ?? 3) {
            0, "0" => 'Quản trị viên',
            1, "1" => 'Biên tập viên',
            2, "2" => 'Hành chính nhân sự',
            3, "3" => 'Chưa xác định'
        };
    }

}
