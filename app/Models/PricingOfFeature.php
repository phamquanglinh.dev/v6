<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricingOfFeature extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_pricing_of_feature";
    protected $guarded = ["id"];
}
