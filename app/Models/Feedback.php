<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * @property string avatar
 * @property string avatar_title
 */
class Feedback extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'tbl_feedback';

    protected $fillable = [
        'title',
        'name',
        'position',
        'note',
        'content',
        'priority',
        'active',
        'link',
        'avatar',
        'avatar_title',
        'type_feedback'
    ];

    public function setAvatarAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "avatar";
        $disk = "uploads";
        $destination_path = "/feedback/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["avatar"] = $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["avatar"] = UploadBase64::run($value, "/feedback/", Str::slug($this->title ?? Str::random()));
            }
        }
    }

    public function setAvatarTitleAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "avatar_title";
        $disk = "uploads";
        $destination_path = "/feedback/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["avatar_title"] = $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["avatar_title"] = UploadBase64::run($value, "/feedback/", Str::slug($this->title ?? Str::random()));
            }
        }
    }
}
