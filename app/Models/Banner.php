<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * @property string image
 */
class Banner extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;
//    use RevisionableTrait;

    protected $table = 'tbl_banner';
    protected $primaryKey = 'banner_id';

    protected $guarded = [
        'banner_id'
    ];

    public function identifiableName()
    {
        return $this->title;
    }

    private function getSystemUserId()
    {
        return backpack_user()->user_id;
    }

    public function setImageAttribute(UploadedFile|string $value): void
    {

        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "banners";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["image"] = "/assets/uploads/banners/images/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["image"] = "/assets/uploads/banners/images/" . UploadBase64::run($value, "/banners/images/", $this->title ?? null);
            }
        }
    }

    public function orderButton(): string
    {
        return '<a href="' . url("admin/banner/reorder") . '" class="btn btn-success" data-style="zoom-in"><span class="ladda-label"><i class="la la-arrows-alt"></i> Sắp xếp banner</span></a>';
    }
}
