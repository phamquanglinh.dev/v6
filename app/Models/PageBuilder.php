<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageBuilder extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_landing_page";
    protected $guarded = ["body"];
    public function getPageUrl(): string
    {
        return "/page/" . $this->slug;
    }

    public function previewOnWeb(): string
    {
        return '<a target="_blank" href="' . url($this->getPageUrl()) . '" class="btn btn-sm btn-link"><i class="la la-eye"></i> Xem trên web </a>';
    }
}
