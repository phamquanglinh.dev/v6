<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class V2Package extends Model
{
    use HasFactory;

    const ON_CLOUD_PACKAGE = 1;
    const ON_PREMISE_PACKAGE = 2;

    protected $table = 'tbl_v2_packages';

    protected $guarded = ['id'];

    /**
     * @return string[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 2:06 pm
     */
    public static function OnCloudKey(): array
    {
        return [
            'Startup',
            'Professional',
            'Enterprise'
        ];
    }

    /**
     * @return string[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 2:06 pm
     */
    public static function OnPremiseKey(): array
    {
        return [
            'Master',
            'Enterprise'
        ];
    }
}
