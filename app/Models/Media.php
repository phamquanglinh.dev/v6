<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string avatar
 */

class Media extends Model
{
    use HasFactory;

    protected $table = 'tbl_media';

    protected $fillable = [
        'link',
        'avatar',
        'type'
    ];
}
