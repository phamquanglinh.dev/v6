<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $slug
 * @property string $folder
 */
class PageFeature extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = "tbl_page_feature";
    protected $guarded = ["id"];
    protected $casts = [
        'slides' => 'array'
    ];

    public function setSlidesAttribute($value)
    {
        $attribute_name = "slides";
        $disk = "uploads";
        $destination_path = "news/sub/";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public function getPageUrl(): string
    {
        return "/tinh-nang/" . $this->folder . "/" . $this->slug . ".html";
    }

    public function previewOnWeb(): string
    {
        return '<a target="_blank" href="' . url($this->getPageUrl()) . '" class="btn btn-sm btn-link"><i class="la la-eye"></i> Xem trên web </a>';
    }
}
