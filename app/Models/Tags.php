<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    use HasFactory;

    protected $table = 'tbl_tags';
    protected $primaryKey = 'tag_id';

    protected $fillable = [
        'tag_name',
        'invalid',
        'tag_type',
        'order'
    ];
}
