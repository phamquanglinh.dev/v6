<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class V2Pricing extends Model
{
    use HasFactory;

    protected $table = 'tbl_v2_pricings';
    protected $guarded = ['id'];
    const SMALL_BUSINESS = 'small_business';
    const MEDIUM_BUSINESS = 'medium_business';
    const BIG_BUSINESS = 'big_business';

    public static function getBusinessDetail(string $business_size): array
    {
        switch ($business_size) {
            case self::SMALL_BUSINESS:
                return [
                    'title' => 'Doanh nghiệp nhỏ',
                    'description' => 'Có đội ngũ nhân sự đa nhiệm linh hoạ'
                ];
            case self::MEDIUM_BUSINESS:
                return [
                    'title' => 'Doanh nghiệp vừa & nhỏ',
                    'description' => 'Đang mở rộng quy mô'
                ];
            case self::BIG_BUSINESS:
                return [
                    'title' => 'Doanh nghiệp tầm trung & lớn',
                    'description' => 'Có quy trình hoạt động chuyên nghiệp bài bản'
                ];
            default:
                return [
                    'title' => '',
                    'description' => ''
                ];
        }
    }
}
