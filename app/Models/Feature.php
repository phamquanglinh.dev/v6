<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * @property string $thumbnail
 * @property string $icon
 */
class Feature extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_features";
    protected $fillable = [
        'header',
        'title',
        'icon',
        'thumbnail',
        'description',
        'body',
        'url',
    ];

    public function setThumbnailAttribute(UploadedFile|string $value = null): void
    {
        if ($value) {
            $attribute_name = "thumbnail";
            $disk = "uploads";
            $destination_path = "/features/images/";
            if ($value instanceof UploadedFile) {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
                $this->attributes["thumbnail"] = "assets/uploads/features/images/" . $value->getClientOriginalName();
            } else {
                if (str_contains($value, "base")) {
                    $this->attributes["thumbnail"] = "assets/uploads/features/images/" . UploadBase64::run($value, "/features/images/", $this->title ?? null);
                }
            }
        }
    }

    public function setIconAttribute(UploadedFile|string $value = null): void
    {
        if ($value != null) {
            $attribute_name = "icon";
            $disk = "uploads";
            $destination_path = "/features/images/";
            if ($value instanceof UploadedFile) {
                $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
                $this->attributes["icon"] = "assets/uploads/features/images/" . $value->getClientOriginalName();
            } else {
                if (str_contains($value, "base")) {
                    $this->attributes["icon"] = "assets/uploads/features/images/" . UploadBase64::run($value, "/features/images/", "ic_" . $this->title ?? null);
                }
            }
        }
    }
}
