<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $page_slug
 */
class SubPage extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = "tbl_sub_pages";
    protected $guarded = ['id'];
    protected $casts = [
        "page_tabs" => 'array'
    ];

    public function getPreviewLinkUrl(): string
    {
        return url("/page/" . $this->page_slug);
    }
}
