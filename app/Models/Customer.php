<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;

/**
 * @property string $logo
 * @property string $avatar
 */
class Customer extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'tbl_media';
    protected $fillable = [
        'link',
        'avatar',
        "type",
        "priority",
        'name',
    ];

    public function setAvatarAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "avatar";
        $disk = "uploads";
        $destination_path = "/media/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["avatar"] = $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["avatar"] = UploadBase64::run($value, "/media/", $this->title ?? null);
            }
        }
    }
}
