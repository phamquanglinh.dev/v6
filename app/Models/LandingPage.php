<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

/**
 * @property string $title
 * @property string $thumbnail
 * @property string $body
 * @property string $slug
 */
class LandingPage extends Model
{
    use CrudTrait;
    use HasFactory;
    use SoftDeletes;

    protected $table = "tbl_landing_page";
    protected $guarded = ["id"];

    public function setSlugAttribute($value): void
    {
        if ($value == "") {
            $this->attributes["slug"] = Str::slug($this->title);
        } else {
            $this->attributes["slug"] = $value;
        }
    }

    public function setThumbnailAttribute($value): void
    {
        $attribute_name = "thumbnail";
        $disk = "uploads";
        $destination_path = "/news/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["thumbnail"] = $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["thumbnail"] = UploadBase64::run($value, "/news/", Str::slug($this->title ?? Str::random()));
            }
        }
    }

    public function getPageUrl(): string
    {
        return "/page/" . $this->slug;
    }

    public function previewOnWeb(): string
    {
        return '<a target="_blank" href="' . url($this->getPageUrl()) . '" class="btn btn-sm btn-link"><i class="la la-eye"></i> Xem trên web </a>';
    }

    public function pageBuilder(): string
    {
        return '<a target="_blank" href="' . route("page-builder.edit", ['id' => $this->id]) . '" class="btn btn-sm btn-link"><i class="la la-cocktail"></i> Page Builder </a>';
    }

    public function resetBody(): string
    {
        return '<a href="' . route("page-builder.reset", ['id' => $this->id]) . '" class="btn btn-sm btn-link"><i class="la la-cocktail"></i> Reset Body </a>';
    }

//    public function setBodyAttribute(): void
//    {
//        if ($this->ladi_url != null) {
//            $this->attributes['body'] = file_get_contents($this->ladi_url);
//            $this->attributes['ladi_url'] = null;
//        } else {
//            $this->attributes['body'] = $this->body;
//        }
//    }
}
