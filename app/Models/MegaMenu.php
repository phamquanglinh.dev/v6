<?php

namespace App\Models;

use App\Untils\UploadBase64;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class MegaMenu extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_mega_tab";
    protected $guarded = ["id"];

    public function parentMenu(): BelongsTo
    {
        return $this->belongsTo(Menu::class, "parent_id", "id");
    }

    public function childrenMenu(): BelongsToMany
    {
        return $this->belongsToMany(Menu::class, "tbl_mega_relate", "mega_id", "menu_id");
    }

    public function setIconAttribute(UploadedFile|string $value): void
    {
        $attribute_name = "icon";
        $disk = "uploads";
        $destination_path = "/menus/";
        if ($value instanceof UploadedFile) {
            $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $fileName = $value->getClientOriginalName());
            $this->attributes["icon"] = "assets/uploads/menus/" . $value->getClientOriginalName();
        } else {
            if (str_contains($value, "base")) {
                $this->attributes["icon"] = "assets/uploads/menus/" . UploadBase64::run($value, "/menus/", Str::slug($this->name ?? Str::random()));
            }
        }
    }
}
