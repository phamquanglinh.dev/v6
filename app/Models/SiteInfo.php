<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteInfo extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'tbl_site_info';
    protected $fillable = [
        'title',
        'description',
        'content',
        'phone',
        'email',
        'website',
        'yahoo',
        'facebook',
        'address',
        'logo',
        'cname',
        'profile',
        'copyright',
        'hotline',
        'skype',
        'fax',
        'keyword_seo',
        'description_seo',
        'text_run',
        'code_youtube',
        'twitter',
        'mobile',
        'cname_en',
        'youtube',
        'video_link',
        'google',
        'video_image_link',
        'news_about',
        'news_time_line',
        'campaign_url',
        'footer_product',
        'footer_feature',
        'faqs',
        'profession',
        'partners',
        'experience_years',
        'contact_form'
    ];
}
