<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property int $users
 * @property int $price
 * @property int $user_price
 * @property int $customers
 * @property string $setup_time
 * @property int $domain_target
 * @property int $order
 */
class Pricing extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = "tbl_pricing";
    protected $fillable = [
        "name",
        "users",
        "price",
        "user_price",
        "customers",
        "setup_time",
        "domain_target",
        "order"
    ];
}
