<?php

namespace App\Listeners;

use App\Events\GetRecentNews;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class SendViewRecentPosts
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(GetRecentNews $event): void
    {
        $view = $event->getView();

        View::composer("admin.$view.header", function ($view) {

            return $view->with('userLogged', Auth::user());
        });
    }
}
