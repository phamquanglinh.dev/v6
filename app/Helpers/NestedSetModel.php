<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class NestedSetModel
{
    /**
     * @var string
     */
    private string $tableName;

    /**
     * @var string|array
     */
    private string|array $identifier;

    /**
     * @var string
     */
    private string $leftColumnName;

    /**
     * @var string
     */
    private string $rightColumnName;

    /**
     * @var string
     */
    private string $parentColumnName;

    /**
     * @var string
     */
    private string $levelColumnName;

    /**
     * @param string $tableName
     * @param string|array $identifier
     * @param string $leftColumnName
     * @param string $rightColumnName
     * @param string $parentColumnName
     * @param string $levelColumnName
     * @return void
     */
    public function init(
        string $tableName,
        string|array $identifier,
        string $leftColumnName,
        string $rightColumnName,
        string $parentColumnName,
        string $levelColumnName
    ) {
        $this->tableName = $tableName;
        $this->identifier = $identifier;
        $this->leftColumnName = $leftColumnName;
        $this->rightColumnName = $rightColumnName;
        $this->parentColumnName = $parentColumnName;
        $this->levelColumnName = $levelColumnName;
    }

    /**
     * @return int
     */
    public function getRootNode(): int
    {
        $rootNote = DB::table($this->tableName)->where($this->parentColumnName, 0)->first();
        return $rootNote->{$this->identifier};
    }

    /**
     * @param $parentId
     * @param $nodeId
     * @return void
     */
    public function addNode($parentId, $nodeId): void
    {
        $parentNode = DB::table($this->tableName)->where($this->identifier, $parentId)->first();
        $parentRight = $parentNode->nright;
        $parentLevel = $parentNode->level;

        DB::table($this->tableName)->where($this->leftColumnName, '>', $parentRight)->increment($this->leftColumnName, 2);
        DB::table($this->tableName)->where($this->rightColumnName, '>=', $parentRight)->increment($this->rightColumnName, 2);
        DB::table($this->tableName)->where($this->identifier, $nodeId)->update([
            $this->leftColumnName => $parentRight,
            $this->rightColumnName => $parentRight + 1,
            $this->levelColumnName => $parentLevel + 1,
            $this->parentColumnName => $parentId,
        ]);
    }

    /**
     * @param $nodeId
     * @param $softDeleteFieldName
     * @return void
     */
    public function removeNode($nodeId, $softDeleteFieldName): void
    {
        $node = DB::table($this->tableName)->where($this->identifier, $nodeId)->first();
        $size = $node->{$this->rightColumnName} - $node->{$this->leftColumnName} + 1;

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $node->{$this->leftColumnName})
            ->where($this->rightColumnName, '<=', $node->{$this->rightColumnName})
            ->update([
                $this->parentColumnName => null,
                $this->levelColumnName => null,
                $this->leftColumnName => null,
                $this->rightColumnName => null,
                $softDeleteFieldName => 1
            ]);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>', $node->{$this->rightColumnName})
            ->decrement($this->leftColumnName, $size);

        DB::table($this->tableName)
            ->where($this->rightColumnName, '>', $node->{$this->rightColumnName})
            ->decrement($this->rightColumnName, $size);
    }

    /**
     * @param Model $node
     * @param int $parentNodeId
     * @return void
     */
    public function moveNodeInto(Model $node, int $parentNodeId): void
    {
        $parentNode = DB::table($this->tableName)->where($this->identifier, $parentNodeId)->first();

        if ($node[$this->rightColumnName] > $parentNode->nright) {
            $this->moveNodeIntoBefore($node, $parentNode);
        } else {
            $this->moveNodeIntoAfter($node, $parentNode);
        }
    }

    /**
     * @param Model $node
     * @param object $parentNode
     * @return void
     */
    private function moveNodeIntoBefore(Model $node, object $parentNode): void
    {
        $size = $node[$this->rightColumnName] - $node[$this->leftColumnName] + 1;
        $distance = $node[$this->leftColumnName] - $parentNode->{$this->leftColumnName} + $size - 1;
        $leftNode = $node[$this->leftColumnName];
        $rightNode = $node[$this->rightColumnName];
        $increaseLevel = $parentNode->{$this->levelColumnName} + 1 - $node[$this->levelColumnName];

        DB::table($this->tableName)->where($this->leftColumnName, '<=', $parentNode->{$this->leftColumnName})->decrement($this->leftColumnName, $size);
        DB::table($this->tableName)->where($this->rightColumnName, '<', $parentNode->{$this->leftColumnName})->decrement($this->rightColumnName, $size);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '=', $leftNode)
            ->where($this->rightColumnName, '=', $rightNode)
            ->update([
                $this->parentColumnName => $parentNode->{$this->identifier},
            ]);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $leftNode)
            ->where($this->rightColumnName, '<=', $rightNode)
            ->increment($this->levelColumnName, $increaseLevel);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $leftNode)
            ->where($this->leftColumnName, '<=', $rightNode)
            ->decrement($this->leftColumnName, $distance);

        DB::table($this->tableName)
            ->where($this->rightColumnName, '>=', $leftNode)
            ->where($this->rightColumnName, '<=', $rightNode)
            ->decrement($this->rightColumnName, $distance);

        DB::table($this->tableName)->where($this->leftColumnName, '<', $leftNode)->increment($this->leftColumnName, $size);
        DB::table($this->tableName)->where($this->rightColumnName, '<', $leftNode)->increment($this->rightColumnName, $size);
    }

    /**
     * @param Model $node
     * @param object $parentNode
     * @return void
     */
    private function moveNodeIntoAfter(Model $node, object $parentNode): void
    {
        $size = $node[$this->rightColumnName] - $node[$this->leftColumnName] + 1;
        $distance = $parentNode->{$this->rightColumnName} - $node[$this->leftColumnName];
        $leftNode = $node[$this->leftColumnName];
        $rightNode = $node[$this->rightColumnName];
        $increaseLevel = $parentNode->{$this->levelColumnName} + 1 - $node[$this->levelColumnName];

        DB::table($this->tableName)->where($this->leftColumnName, '>', $parentNode->{$this->rightColumnName})->increment($this->leftColumnName, $size);
        DB::table($this->tableName)->where($this->rightColumnName, '>=', $parentNode->{$this->rightColumnName})->increment($this->rightColumnName, $size);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '=', $leftNode)
            ->where($this->rightColumnName, '=', $rightNode)
            ->update([
                $this->parentColumnName => $parentNode->{$this->identifier},
            ]);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $leftNode)
            ->where($this->rightColumnName, '<=', $rightNode)
            ->increment($this->levelColumnName, $increaseLevel);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $leftNode)
            ->where($this->rightColumnName, '<=', $rightNode)
            ->increment($this->leftColumnName, $distance);

        DB::table($this->tableName)
            ->where($this->leftColumnName, '>=', $leftNode)
            ->where($this->rightColumnName, '<=', $rightNode)
            ->increment($this->rightColumnName, $distance);

        DB::table($this->tableName)->where($this->leftColumnName, '>', $rightNode)->decrement($this->leftColumnName, $size);
        DB::table($this->tableName)->where($this->rightColumnName, '>', $rightNode)->decrement($this->rightColumnName, $size);
    }

    /**
     * @param int $currentNodeId
     * @param int $newPositionNodeId
     * @return void
     */
    public function moveNodePosition(int $currentNodeId, int $newPositionNodeId): void
    {
        $currentNode = DB::table($this->tableName)->where($this->identifier, $currentNodeId)->first();

        $newPositionNode = $this->getParentNewPositionNode($newPositionNodeId, $currentNode->{$this->levelColumnName});

        if ($this->isRootNodeCheck($newPositionNode->{$this->identifier})) {
            return;
        }
        $curentNodeSize = $currentNode->{$this->rightColumnName} - $currentNode->{$this->leftColumnName} + 1;

        if ($this->moveUpCheck($currentNode->{$this->leftColumnName}, $newPositionNode->{$this->leftColumnName})) {
            $distance = $currentNode->{$this->rightColumnName} - ($newPositionNode->{$this->leftColumnName} - 1);

            DB::table($this->tableName)->where($this->leftColumnName, '<', $newPositionNode->{$this->leftColumnName})
                ->decrement($this->leftColumnName, $curentNodeSize);
            DB::table($this->tableName)->where($this->rightColumnName, '<', $newPositionNode->{$this->leftColumnName})
                ->decrement($this->rightColumnName, $curentNodeSize);

            DB::table($this->tableName)->where($this->leftColumnName, '>=', $currentNode->{$this->leftColumnName})
                ->where($this->leftColumnName, '<=', $currentNode->{$this->rightColumnName})
                ->decrement($this->leftColumnName, $distance);
            DB::table($this->tableName)->where($this->rightColumnName, '>=', $currentNode->{$this->leftColumnName})
                ->where($this->rightColumnName, '<=', $currentNode->{$this->rightColumnName})
                ->decrement($this->rightColumnName, $distance);

            DB::table($this->tableName)->where($this->rightColumnName, '<=', $newPositionNode->{$this->rightColumnName})
                ->increment($this->rightColumnName, $curentNodeSize);
            DB::table($this->tableName)->where($this->leftColumnName, '<=', $newPositionNode->{$this->rightColumnName})
                ->increment($this->leftColumnName, $curentNodeSize);
        } else {
            $distance = $newPositionNode->{$this->rightColumnName} - ($currentNode->{$this->leftColumnName} - 1);
            DB::table($this->tableName)->where($this->leftColumnName, '>', $newPositionNode->{$this->rightColumnName})
                ->increment($this->leftColumnName, $curentNodeSize);
            DB::table($this->tableName)->where($this->rightColumnName, '>', $newPositionNode->{$this->rightColumnName})
                ->increment($this->rightColumnName, $curentNodeSize);

            DB::table($this->tableName)->where($this->leftColumnName, '>=', $currentNode->{$this->leftColumnName})
                ->where($this->leftColumnName, '<=', $currentNode->{$this->rightColumnName})
                ->increment($this->leftColumnName, $distance);
            DB::table($this->tableName)->where($this->rightColumnName, '>=', $currentNode->{$this->leftColumnName})
                ->where($this->rightColumnName, '<=', $currentNode->{$this->rightColumnName})
                ->increment($this->rightColumnName, $distance);

            DB::table($this->tableName)->where($this->rightColumnName, '>=', $newPositionNode->{$this->leftColumnName})
                ->decrement($this->rightColumnName, $curentNodeSize);
            DB::table($this->tableName)->where($this->leftColumnName, '>=', $newPositionNode->{$this->leftColumnName})
                ->decrement($this->leftColumnName, $curentNodeSize);
        }
    }

    /**
     * @param int $newPositionNodeId
     * @param int $currentNodeLevel
     * @return object
     */
    private function getParentNewPositionNode(int $newPositionNodeId, int $currentNodeLevel): object
    {
        $newPositionNode = DB::table($this->tableName)->where($this->identifier, $newPositionNodeId)->first();

        if ($this->isRootNodeCheck($newPositionNode->{$this->identifier})) {
            return $newPositionNode;
        }

        while (!$this->levelNodeCheck($currentNodeLevel, $newPositionNode->{$this->levelColumnName})) {
            $newPositionNode = DB::table($this->tableName)->where($this->identifier, $newPositionNode->{$this->parentColumnName})->first();
        }

        return $newPositionNode;
    }

    /**
     * @param int $nodeId
     * @return bool
     */
    private function isRootNodeCheck(int $nodeId): bool
    {
        return $nodeId === $this->getRootNode();
    }

    /**
     * @param int $currentNodeLevel
     * @param int $newPositionNodeLevel
     * @return bool
     */
    private function levelNodeCheck(int $currentNodeLevel, int $newPositionNodeLevel): bool
    {
        return $currentNodeLevel === $newPositionNodeLevel;
    }

    /**
     * @param int $currentNodeLeft
     * @param int $newPositionNodeLeft
     * @return bool
     */
    private function moveUpCheck(int $currentNodeLeft, int $newPositionNodeLeft): bool
    {
        return $currentNodeLeft > $newPositionNodeLeft;
    }
}