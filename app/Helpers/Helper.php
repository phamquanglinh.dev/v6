<?php

use App\Models\SiteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (! function_exists("embed_form")) {
    function embed_form($url)
    {
        try {
            $form = DB::table("tbl_form")->where("position", $url)->first();

            return $form->link ?? "";
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
if (! function_exists("static_link")) {
    function static_link($name)
    {
        $link = DB::table("tbl_pricing_of_feature")->where("name", $name)->first();

        return $link->url ?? "#";
    }
}
if (! function_exists("site_info")) {
    function site_info($key)
    {
        $link = SiteInfo::query()->first();

        return $link[$key];
    }
}

if (! function_exists('createSlug')) {
    /**
     * @param string $text
     * @return string
     */
    function createSlug(string $text): string
    {
        $search = array(
            '#(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)#',
            '#(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)#',
            '#(ì|í|ị|ỉ|ĩ)#',
            '#(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)#',
            '#(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)#',
            '#(ỳ|ý|ỵ|ỷ|ỹ)#',
            '#(đ)#',
            '#(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)#',
            '#(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)#',
            '#(Ì|Í|Ị|Ỉ|Ĩ)#',
            '#(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)#',
            '#(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)#',
            '#(Ỳ|Ý|Ỵ|Ỷ|Ỹ)#',
            '#(Đ)#',
            "/[^a-zA-Z0-9\-\_]/",
        );

        $replace = array('a', 'e', 'i', 'o', 'u', 'y', 'd', 'A', 'E', 'I', 'O', 'U', 'Y', 'D', '-');
        $text = preg_replace($search, $replace, $text);
        $text = preg_replace('/(-)+/', '-', $text);

        return strtolower($text);
    }
}

if (! function_exists('changeObjectToArray')) {
    /**
     * @param object $objectChange
     * @return array
     */
    function changeObjectToArray(object $objectChange): array
    {
        $returnArray = [];
        $nameSpace = new \ReflectionObject($objectChange);

        foreach ((array)$objectChange as $key => $object) {
            $index = str_replace($nameSpace->getName(), '', $key);
            $index = preg_replace('/[\00-]/', '', $index);
            $returnArray[$index] = $object;
        }

        return $returnArray;
    }
}

if (! function_exists('existsImageCheck')) {
    /**
     * @param string $path
     * @return bool
     */
    function existsImageCheck(string $path): bool
    {
        return file_exists(public_path($path)) && $path !== "";
    }
}

if (! function_exists('stringCheck')) {
    /**
     * @param string|object $stringCheck
     * @param string|object $stringReturn
     * @return string
     */
    function stringCheck(string|object $stringCheck, string|object $stringReturn = "", string $getField = "getId"): string
    {
        return is_string($stringCheck) ? $stringReturn : $stringCheck->{$getField}();
    }
}
if (! function_exists('backpack_pro')) {
    function backpack_pro(): bool
    {
        return true;
    }
}
if (! function_exists('module_icon')) {
    function module_icon($key): string
    {
        $icons = [
            1 => 'facebook-logo.webp',
            2 => 'tong dai.webp',
            3 => 'kho.webp',
            4 => 'hrm.webp',
            5 => 'tai chinh.webp',
            6 => 'IP va gps.webp',
            7 => 'POS.webp',
            8 => 'bao mat.webp',
            9 => 'bao hanh.webp',
            10 => 'lich di tuyen.webp',
            11 => 'diem thuong.webp',
            12 => 'diem thuong.webp',
        ];

        return url("site/images") . "/" . $icons[$key];
    }
}
if (! function_exists("hasAccess")) {
    function hasAccess($permission): bool
    {
//        dd($permission);
        $level = match ($permission) {
            "admin" => [0],
            "manager" => [0, 1],
            "hr" => [0, 2],
            default => [3],
        };
        if (backpack_auth()->check()) {
            return in_array(backpack_user()->group_user ?? 3, $level);
        };

        return false;
    }
}

if (! function_exists('version2_url')) {
    function version2_url($url): string
    {
        return asset('site-version-2/' . $url);
    }
}

if (! function_exists('v2_url')) {
    function v2_url($url): string
    {
//        return ('/v6/public/v2-release/' . $url);
        return asset('v2-reload/' . $url);
    }
}