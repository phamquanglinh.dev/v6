<?php

namespace App\Providers;

use App\Composer\DashboardComposer;
use App\Composer\MetaSeoComposer;
use App\Composer\NavbarComposer;
use App\Composer\PhotoListComposer;
use App\Composer\ShortcutComposer;
use App\Composer\V2NavbarComposer;
use App\Helpers\NestedSetModel;
use App\Repository\BannerRepository;
use App\Repository\FooterRepository;
use App\Repository\MenuRepository;
use App\Repository\NewsRelateRepository;
use App\Repository\NewsRepository;
use App\ViewModels\Sites\Footer\FooterListViewModel;
use App\ViewModels\Sites\Home\MetaSeoViewModel;
use App\ViewModels\Sites\Sidebar\RightSidebarViewModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\ViewModels\Sites\Header\HeaderListViewModel;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        View::composer("vendor.backpack.base.inc.dashboard-content", DashboardComposer::class);
        View::composer("sites.shared.shortcut", ShortcutComposer::class);
        View::composer("sites.team.carousel", PhotoListComposer::class);
        View::composer("sites.shared.meta-seo", MetaSeoComposer::class);
        View::composer('sites.shared.navbar', NavbarComposer::class);
        View::composer('version2.layouts.inc.navbar', V2NavbarComposer::class);
        View::composer('sites.shared.footer', function ($view) {
            $footerRepository = new FooterRepository();
            return $view->with("footerListViewModel", new FooterListViewModel(footer: $footerRepository->getFooterById()));
        });
        View::composer("sites.shared.right_sidebar", function ($view) {
            $topNews = (new NewsRepository(new NewsRelateRepository()))->getTopNewsForSites();
            $topRightBanner = (new BannerRepository())->getBannerForRightSidebar();
            return $view->with("rightSidebarViewModel", new RightSidebarViewModel(topNews: $topNews, banners: $topRightBanner));
        });

    }
}
