<?php

namespace App\Repository;

use App\Models\Category;
use App\Helpers\NestedSetModel;
use App\Models\Menu;
use App\ViewModels\Admin\Menu\MenuAdd\Object\CategoryMenuObjectAdd;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Container\BindingResolutionException;
use App\ViewModels\Admin\Category\CategoryAdd\Object\CategoryObjectAdd;
use App\ViewModels\Admin\Category\CategoryEdit\Object\CategoryObjectEdit;

class CategoryRepository extends BaseRepository
{
    private const PAGINATE = 20;
    private const PATH_IMAGE = 'category';

    /**
     * @param NestedSetModel $nestedSetModel
     * @throws BindingResolutionException
     */
    public function __construct(readonly private NestedSetModel $nestedSetModel)
    {
        parent::__construct();

        $nestedSetModel->init(
            tableName : 'tbl_categories',
            identifier : 'id',
            leftColumnName : 'nleft',
            rightColumnName : 'nright',
            parentColumnName : 'parent_id',
            levelColumnName : 'level'
        );
    }

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return Category::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getCategoryPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy('nleft', 'asc')->paginate(self::PAGINATE);
    }

    /**
     * @param Category|null $category Nếu có param này thì sẽ lấy tất cả categories khác category này
     * @param bool $getRoot Nếu false thì sẽ lấy tất cả category trừ root node
     * @return Collection
     */
    public function getCategoryAll(Category $category = null, bool $getRoot = true, $trash = false): Collection
    {
        if ($category) {
            return $this->getBuilder()->orderBy('nleft', 'asc')
                ->whereNotBetween('nright', [$category['nleft'], $category['nright']])
                ->get();
        }

        if (! $getRoot) {
            return $this->getBuilder()->where('parent_id', '>', 0)
                ->orderBy('nleft', 'asc')->get();
        }
        if (! $trash) {
            return $this->getBuilder()->where("invalid", 0)->orderBy("nleft", "asc")->get();
        }

        return $this->getBuilder()->orderBy('nleft', 'asc')->get();
    }

    public function recursiveCategoryMenu($key = 1): CategoryMenuObjectAdd
    {
        /**
         * @var Category $category
         */
        $category = $this->getCategoryById($key);
        $child = $this->getBuilder()->where("parent_id", $key)->get();

        return new CategoryMenuObjectAdd(
            id : $category["id"],
            title : $category["title"],
            level : $category["level"],
            child : $child->map(function (Category $categoryItem) {
                return $this->recursiveCategoryMenu(key : $categoryItem["id"]);
            })->toArray()
        );
    }

    /**
     * @param int $categoryId
     * @param CategoryObjectEdit $categoryObjectEdit
     * @return void
     */

    public function updateCategory(int $categoryId, CategoryObjectEdit $categoryObjectEdit): void
    {
        $category = $this->getBuilder()->where('id', $categoryId)->first();

        if ($category) {
            $dataCategoryUpdate = changeObjectToArray($categoryObjectEdit);

            $dataCategoryUpdate['img_cover'] = $this->handleImageFile(
                file : $categoryObjectEdit->getImgCover(),
                path : self::PATH_IMAGE,
                oldPath : $category['img_cover'] ?? ""
            );

            $category->update($dataCategoryUpdate);

            if (! $this->getBuilder()->where('id', $categoryId)->where('parent_id', $categoryObjectEdit->getCate())->first()) {
                $this->nestedSetModel->moveNodeInto($category, $categoryObjectEdit->getCate());
            }
        }
    }

    /**
     * @param CategoryObjectAdd $categoryObjectAdd
     * @return void
     */
    public function createCategory(CategoryObjectAdd $categoryObjectAdd): void
    {
        $dataCategoryCreate = changeObjectToArray($categoryObjectAdd);

        /** @var Category $categoryModel */
        $categoryModel = $this->getBuilder()->create($dataCategoryCreate);

        if (empty($parentNodeId = $categoryObjectAdd->getCate())) {
            $parentNodeId = $this->nestedSetModel->getRootNode();
        }

        $categoryModel->img_cover = $this->handleImageFile(
            file : $categoryObjectAdd->getImgCover(),
            path : self::PATH_IMAGE
        );

        if ($categoryModel->img_cover) {
            $categoryModel->save();
        }

        $this->nestedSetModel->addNode($parentNodeId, $categoryModel['id']);
    }

    /**
     * @param int $deleteId
     * @return void
     */
    public function deleteCategory(int $deleteId): void
    {
        $this->nestedSetModel->removeNode(nodeId : $deleteId, softDeleteFieldName : 'invalid');
    }

    /**
     * @param int $categoryId
     * @return Category|null
     */
    public function getCategoryById(int $categoryId): Model|null
    {
        return $this->getBuilder()->where("id", $categoryId)->first();
    }

    /**
     * @param int|null $currentNodeId
     * @param int|null $newPositionNodeId
     * @return void
     */
    public function moveNodePosition(int|null $currentNodeId, int|null $newPositionNodeId): void
    {
        $this->nestedSetModel->moveNodePosition(
            currentNodeId : $currentNodeId ?? $this->nestedSetModel->getRootNode(),
            newPositionNodeId : $newPositionNodeId ?? $this->nestedSetModel->getRootNode()
        );
    }

    public function getAllCategories(array $condition, $order = null, $direction = null, $limit = 0): Collection|array
    {
        $query = $this->getBuilder()->where($condition);

        if (! $order && $direction) {
            $query->orderBy($order, $direction);
        }

        if (! $limit) {
            $query->limit($limit);
        }

        return $query->get();
    }

    public function getOneCategory(array $condition): Model|null
    {
        return $this->getBuilder()->where($condition)->first();
    }
}