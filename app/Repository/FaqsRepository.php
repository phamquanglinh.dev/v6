<?php

namespace App\Repository;

use App\Models\SiteInfo;
use Illuminate\Database\Eloquent\Model;
use App\ViewModels\Admin\Faqs\FaqsEdit\Object\FaqsEditObject;

class FaqsRepository extends BaseRepository
{
    private CONST FAQS_ID = 1;

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return SiteInfo::class;
    }

    /**
     * @param FaqsEditObject $faqsEditObject
     * @return void
     */
    public function updateFaqs(FaqsEditObject $faqsEditObject): void
    {
        $this->getBuilder()->where('id',self::FAQS_ID)->update(changeObjectToArray($faqsEditObject));
    }

    /**
     * @return SiteInfo|null
     */
    public function getFaqsById(): Model|null
    {
        return $this->getBuilder()->find(self::FAQS_ID);
    }
}