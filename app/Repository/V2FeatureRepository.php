<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 4:34 pm
 */

namespace App\Repository;

use App\Models\V2Feature;
use Illuminate\Database\Eloquent\Collection;

class V2FeatureRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return V2Feature::class;
    }

    public function getAllV2Feature(): Collection|array
    {
        return $this->getBuilder()->orderBy('id', 'ASC')->get();
    }

    public function bulkUpdateAllFeature(array $features): bool
    {
        foreach ($features as $featureId => $feature) {
            $this->getBuilder()->updateOrInsert([
                'id' => $featureId
            ], $feature);
        }

        return true;
    }
}