<?php

namespace App\Repository;

use App\Models\LandingPage;
use Illuminate\Database\Eloquent\Collection;

class LandingPageRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return LandingPage::class;
    }

    public function getPageBySlug($slug): object|null
    {

        return $this->getBuilder()->where("slug", $slug)->firstOrFail();
    }

    public function saveBody(mixed $id, string $html, string|null $script, string|null $style): void
    {
        $old = $this->getBuilder()->where("id", $id)->first();
        $this->getBuilder()->where("id", $id)->update([
            "body" => $html,
        ]);
        if ($style) {
            $this->getBuilder()->where("id", $id)->update([
                "style" => $style,
            ]);
        }
        if ($script) {
            $this->getBuilder()->where("id", $id)->update([
                "script" => $old["script"] . " " . $script,
            ]);
        }
    }

    public function getPreviewUrl(mixed $id): string
    {
        $slug = $this->getBuilder()->where("id", $id)->first()->slug;
        return url("/page/" . $slug);
    }

    public function getMostPopularLandingPage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("view", "DESC")->limit($limit)->get();
    }

    public function getLatestLandingPage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("created_at", "DESC")->limit($limit)->get();
    }

}