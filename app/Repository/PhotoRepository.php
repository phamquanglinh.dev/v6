<?php

namespace App\Repository;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class PhotoRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Photo::class;
    }

    public function getPhotoForTeamSite(): array|Collection
    {
        return $this->getBuilder()->orderBy("order", "ASC")->limit(5)->get();
    }

    public function getPhotoById(mixed $id): \Illuminate\Database\Eloquent\Model|Collection|Builder|array|null
    {
        return $this->getBuilder()->find($id);
    }

    public function getAllPhotos(): Collection|array
    {
        return $this->getBuilder()->orderBy("order")->get();
    }
}