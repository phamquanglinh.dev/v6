<?php

namespace App\Repository;

use App\Models\Feedback;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\Feedback\FeedbackAdd\Object\FeedbackObjectAdd;
use App\ViewModels\Admin\Feedback\FeedbackEdit\Object\FeedbackObjectEdit;

class FeedbackRepository extends BaseRepository
{
    private const PAGINATE = 20;
    private const PATH_IMAGE = 'feedback';

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return Feedback::class;
    }

    public function getAllFeedback(): Collection|array
    {
        return $this->getBuilder()->orderBy("created_at", 'desc')->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getFeedbackPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy('created_at', 'desc')->paginate(self::PAGINATE);
    }

    public function getFeedbackForHome(): Collection
    {
        return $this->getBuilder()->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param int $feedbackId
     * @param FeedbackObjectEdit $feedbackObjectEdit
     * @return void
     */
    public function updateFeedback(int $feedbackId, FeedbackObjectEdit $feedbackObjectEdit): void
    {
        $dataFeedbackUpdate = changeObjectToArray($feedbackObjectEdit);

        $feedback = $this->getBuilder()->where('id', $feedbackId)->first();

        $dataFeedbackUpdate['avatar'] = $this->handleImageFile(
            file: $feedbackObjectEdit->getAvatar(),
            path: self::PATH_IMAGE,
            oldPath: $feedback['avatar']
        );

        $dataFeedbackUpdate['avatar_title'] = $this->handleImageFile(
            file: $feedbackObjectEdit->getAvatarTitle(),
            path: self::PATH_IMAGE,
            oldPath: $feedback['avatar_title']
        );

        $feedback->update($dataFeedbackUpdate);
    }

    /**
     * @param FeedbackObjectAdd $feedbackObjectAdd
     * @return void
     */
    public function createFeedback(FeedbackObjectAdd $feedbackObjectAdd): void
    {
        $dataFeedbackCreate = changeObjectToArray($feedbackObjectAdd);

        $feedbackModel = $this->getBuilder()->create($dataFeedbackCreate);

        /** @var Feedback $feedbackModel */
        $feedbackModel->avatar = $this->handleImageFile(
            file: $feedbackObjectAdd->getAvatar(),
            path: self::PATH_IMAGE
        );

        $feedbackModel->avatar_title = $this->handleImageFile(
            file: $feedbackObjectAdd->getAvatarTitle(),
            path: self::PATH_IMAGE
        );

        if ($feedbackModel->avatar || $feedbackModel->avatar_title) {
            $feedbackModel->save();
        }
    }

    /**
     * @param int $feedbackId
     * @return void
     */
    public function deleteFeedback(int $feedbackId): void
    {
        $this->getBuilder()->where('id', $feedbackId)->delete();
    }

    /**
     * @param int $categoryId
     * @return Feedback|null
     */
    public function getFeedbackById(int $categoryId): Model|null
    {
        return $this->getBuilder()->find($categoryId);
    }
}