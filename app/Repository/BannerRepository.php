<?php

namespace App\Repository;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\Banner\BannerAdd\Object\BannerObjectAdd;
use App\ViewModels\Admin\Banner\BannerEdit\Object\BannerObjectEdit;

class BannerRepository extends BaseRepository
{
    private const PAGINATE = 20;
    private const PATH_IMAGE = 'banner';

    /**
     * @param $position
     * @return Collection|array
     */
    public function getBannerByPosition($position): Collection|array
    {
        return $this->getBuilder()->where(["position" => $position, "invalid" => 0])->orderBy("order")->get();
    }

    /**
     * @return string
     */


    public function getModelClassName(): string
    {
        return Banner::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getBannerPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy('created_at', 'desc')->paginate(self::PAGINATE);
    }

    /**
     * @return Collection
     */
    public function getBannerAll(): Collection
    {
        return $this->getBuilder()->orderBy('created_at', 'desc')->get();
    }

    /**
     * @param int $bannerId
     * @param BannerObjectEdit $bannerObjectEdit
     * @return void
     */
    public function updateBanner(int $bannerId, BannerObjectEdit $bannerObjectEdit): void
    {
        $banner = $this->getBuilder()->where('banner_id', $bannerId)->first();

        if ($banner) {

            $dataBannerUpdate = changeObjectToArray($bannerObjectEdit);

            $dataBannerUpdate['image'] = $this->handleImageFile(
                file: $bannerObjectEdit->getImage(),
                path: self::PATH_IMAGE,
                oldPath: $banner['image'] ?? ""
            );

            $banner->update($dataBannerUpdate);
        }
    }

    /**
     * @param BannerObjectAdd $bannerObjectAdd
     * @return void
     */
    public function createBanner(BannerObjectAdd $bannerObjectAdd): void
    {
        $dataBannerCreate = changeObjectToArray($bannerObjectAdd);

        /** @var Banner $bannerModel */
        $bannerModel = $this->getBuilder()->create($dataBannerCreate);

        $bannerModel->image = $this->handleImageFile(
            file: $bannerObjectAdd->getImage(),
            path: self::PATH_IMAGE
        );

        if ($bannerModel->image) {
            $bannerModel->save();
        }
    }

    /**
     * @param int[] $bannerIds
     * @return void
     */
    public function deleteBanner(array $bannerIds): void
    {
        $this->getBuilder()->whereIn('banner_id', $bannerIds)->delete();
    }

    /**
     * @param int $bannerId
     * @return Banner|null
     */
    public function getBannerById(int $bannerId): Model|null
    {
        return $this->getBuilder()->find($bannerId);
    }

    public function getBannerForRightSidebar(): Collection|array
    {
        return $this->getBuilder()
            ->where("position", 2)->orderBy("order", "ASC")
            ->orderBy("updated_at", "DESC")->limit(3)->get();
    }
}