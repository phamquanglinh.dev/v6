<?php

namespace App\Repository;

use App\Models\PartnerType;
use Illuminate\Database\Eloquent\Collection;

class PartnerTypeRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return PartnerType::class;
    }

    public function getAll(): Collection|array
    {
        return $this->getBuilder()->orderBy("created_at")->get();
    }
}