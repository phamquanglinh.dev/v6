<?php

namespace App\Repository;

use App\Models\Partner;
use Illuminate\Database\Eloquent\Collection;

class PartnerRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Partner::class;
    }

    public function getAllPartner(): Collection|array
    {
        return $this->getBuilder()->where("type", 1)->get();
    }
}