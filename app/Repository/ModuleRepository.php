<?php

namespace App\Repository;

use App\Models\Module;
use App\ViewModels\Admin\Module\Object\ModuleObject;
use Illuminate\Support\Collection;

/**
 *
 */
class ModuleRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Module::class;
    }

    /**
     * @return Collection
     */
    public function listModules(): Collection
    {
        return $this->getBuilder()->orderBy("order", "ASC")->get();
    }

    /**
     * @param ModuleObject[] $modules
     * @return void
     */
    public function updateModules(array $modules): void
    {
        foreach ($modules as $module) {
            $this->getBuilder()->where("order", $module->getOrder())->update(changeObjectToArray($module));
        }
    }
}