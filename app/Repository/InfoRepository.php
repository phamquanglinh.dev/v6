<?php

namespace App\Repository;

use App\Models\SiteInfo;
use Illuminate\Database\Eloquent\Model;
use App\ViewModels\Admin\Info\InfoEdit\Object\InfoObjectEdit;

class InfoRepository extends BaseRepository
{
    private CONST INFO_ID = 1;
    private CONST PATH_IMAGE = '';

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return SiteInfo::class;
    }

    /**
     * @param InfoObjectEdit $infoObjectEdit
     * @return void
     */
    public function updateInfo(InfoObjectEdit $infoObjectEdit): void
    {
        $dataInfoUpdate = changeObjectToArray($infoObjectEdit);


        $banner = $this->getBuilder()->where('id', self::INFO_ID)->first();

        $dataInfoUpdate['logo'] = $this->handleImageFile(
            file: $infoObjectEdit->getLogo(),
            path: self::PATH_IMAGE,
            oldPath: $banner['logo'] ?? "",
            shortName: true
        );

        $dataInfoUpdate['video_image_link'] = $this->handleImageFile(
            file: $infoObjectEdit->getVideoImageLink(),
            path: self::PATH_IMAGE,
            oldPath: $banner['video_image_link'] ?? ""
        );

        $banner->update($dataInfoUpdate);
    }

    /**
     * @return SiteInfo|null
     */
    public function getInfoById(): Model|null
    {
        return $this->getBuilder()->find(self::INFO_ID);
    }
}