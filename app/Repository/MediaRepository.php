<?php

namespace App\Repository;

use App\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\Media\MediaAdd\Object\MediaObjectAdd;
use App\ViewModels\Admin\Media\MediaEdit\Object\MediaObjectEdit;

class MediaRepository extends BaseRepository
{
    private CONST PAGINATE = 20;
    private CONST PATH_IMAGE = 'media';

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return Media::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getMediaPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->paginate(self::PAGINATE);
    }

    /**
     * @return Collection
     */
    public function getMediaAll(): Collection
    {
        return $this->getBuilder()->get();
    }

    /**
     * @param int $mediaId
     * @param MediaObjectEdit $mediaObjectEdit
     * @return void
     */
    public function updateMedia(int $mediaId, MediaObjectEdit $mediaObjectEdit): void
    {
        $media = $this->getBuilder()->where('id', $mediaId)->first();

        if ($media) {

            $dataMediaUpdate = changeObjectToArray($mediaObjectEdit);

            $dataMediaUpdate['avatar'] = $this->handleImageFile(
                file: $mediaObjectEdit->getAvatar(),
                path: self::PATH_IMAGE,
                oldPath: $media['avatar'] ?? ""
            );

            $media->update($dataMediaUpdate);
        }
    }

    /**
     * @param MediaObjectAdd $mediaObjectAdd
     * @return void
     */
    public function createMedia(MediaObjectAdd $mediaObjectAdd): void
    {
        $dataMediaCreate = changeObjectToArray($mediaObjectAdd);

        $mediaModel = $this->getBuilder()->create($dataMediaCreate);

        /** @var Media $mediaModel */
        $mediaModel->avatar = $this->handleImageFile(
            file: $mediaObjectAdd->getAvatar(),
            path: self::PATH_IMAGE
        );

        if ($mediaModel->avatar) {
            $mediaModel->save();
        }
    }

    /**
     * @param int $mediaId
     * @return void
     */
    public function deleteMedia(int $mediaId): void
    {
        $this->getBuilder()->where('id', $mediaId)->delete();
    }

    /**
     * @param int $mediaId
     * @return Media|null
     */
    public function getMediaById(int $mediaId): Model|null
    {
        return $this->getBuilder()->find($mediaId);
    }
}