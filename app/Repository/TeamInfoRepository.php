<?php

namespace App\Repository;

use App\Models\TeamInfo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TeamInfoRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return TeamInfo::class;
    }

    public function getTeamInfo(): Builder|Model
    {
        return $this->getBuilder()->where("id", 1)->first();
    }

    public function getNewspaper(NewsRepository $newsRepository): Collection
    {
        $categoryId = $this->getTeamInfo()["news_category_id"];
        return $newsRepository->getNewsByCategoryId($categoryId);
    }
}