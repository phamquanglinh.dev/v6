<?php

namespace App\Repository;

use App\Models\SubPage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class SubPageRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return SubPage::class;
    }

    public function getBySlug($slug): null|Model|Builder
    {
        return $this->getBuilder()->where("page_slug", $slug)->first();
    }
    public function getMostSubPage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("view", "DESC")->limit($limit)->get();
    }

    public function getLatestSubPage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy('created_at', "DESC")->limit($limit)->get();
    }
}