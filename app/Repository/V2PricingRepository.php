<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 04/03/2024 4:37 pm
 */

namespace App\Repository;

use App\Models\V2Pricing;
use Illuminate\Database\Eloquent\Collection;

class V2PricingRepository extends BaseRepository
{
    public function getModelClassName(): string
    {
        return V2Pricing::class;
    }

    public function getAllV2Pricing(): Collection|array
    {
        return $this->getBuilder()->orderBy('id', 'ASC')->get();
    }

    public function bulkUpdateAllPricing(array $pricings): void
    {
        $this->getBuilder()->truncate();
        $this->getBuilder()->insert($pricings);
    }
}