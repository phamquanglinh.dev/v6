<?php

namespace App\Repository;

use App\Models\News;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\News\NewsAdd\Objects\NewsObject;
use Illuminate\Contracts\Container\BindingResolutionException;
use App\ViewModels\Admin\News\NewsEdit\Objects\NewsObject as NewsObjectUpdate;

class NewsRepository extends BaseRepository
{
    private const PAGINATE = 20;
    private const PATH_IMAGE = '';

    /**
     * @param NewsRelateRepository $newsRelateRepository
     * @throws BindingResolutionException
     */
    public function __construct(
        readonly private NewsRelateRepository $newsRelateRepository
    )
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return News::class;
    }

    /**
     * @param bool $invalidList
     * @param string $keyword
     * @return LengthAwarePaginator
     */
    public function getNewsPaginateAll(bool $invalidList, string $keyword): LengthAwarePaginator
    {
        $newsQuery = $this->getBuilder()->with('creator', 'category');

        if ($keyword) {
            $newsQuery = $newsQuery->where('title', 'LIKE', '%' . $keyword . '%');
        }

        if ($invalidList) {
            $newsQuery = $newsQuery->withoutGlobalScope('invalid')->where('invalid', 1);
        }

        return $newsQuery->orderBy('news_id', 'desc')->paginate(self::PAGINATE);
    }

    /**
     * @return Collection
     */
    public function getNewsAll(): Collection
    {
        return $this->getBuilder()->limit(10)->get();
    }

    /**
     * @param int $newId
     * @param NewsObjectUpdate $newsObjectUpdate
     * @return void
     */
    public function updateNews(int $newId, NewsObjectUpdate $newsObjectUpdate): void
    {
        $new = $this->getBuilder()->where('news_id', $newId)->first();

        if ($new) {
            DB::beginTransaction();

            try {
                $newArrayDataUpdate = changeObjectToArray($newsObjectUpdate);

                $newArrayDataUpdate['image'] = $this->handleImageFile(
                    file : $newsObjectUpdate->getImage(),
                    path : self::PATH_IMAGE,
                    oldPath : $new['image']
                );

                $new->update($newArrayDataUpdate);

                $this->newsRelateRepository->updateNewsRelate($newId, $newsObjectUpdate->getNewsRelate());

                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
            }
        }
    }

    /**
     * @param NewsObject $newsObject
     * @return void
     */
    public function createNew(NewsObject $newsObject,): void
    {
        DB::beginTransaction();

        try {
            $newArrayDataCreate = changeObjectToArray($newsObject);

            $newModel = $this->getBuilder()->create($newArrayDataCreate);

            /** @var News $newModel */
            $newModel->image = $this->handleImageFile(
                file : $newsObject->getImage(),
                path : self::PATH_IMAGE

            );

            if ($newModel->image) {
                $newModel->save();
            }

            $this->newsRelateRepository->createNewsRelate($newModel['news_id'], $newsObject->getNewsRelate());

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * @param int[] $newsIds
     * @return void
     */
    public function deleteNews(array $newsIds): void
    {
        $this->getBuilder()->whereIn('news_id', $newsIds)->update(['invalid' => 1]);

        $this->newsRelateRepository->deleteNewsRelate($newsIds);
    }

    /**
     * @param int $newsId
     * @return News|null
     */
    public function getNewsById(int $newsId): Model|null
    {
        return $this->getBuilder()->find($newsId);
    }

    /**
     * @param int $categoryId
     * @param int $page
     * @return Collection
     */
    public function getNewsByCategoryId(int $categoryId, int $page = 1): Collection
    {
        if ($page == 1) {
            $start = ($page - 1) * 11;
            $take = 11;
        } else {
            $start = ($page - 1) * 10 + 1;
            $take = 10;
        }

        return $this->getBuilder()->where('cate_id', $categoryId)
            ->where("invalid", 0)
            ->orderBy("order", "DESC")
            ->orderBy("created_at", "DESC")
            ->with('category')->skip($start)->take($take)->get();
    }

    public function getNewsByCategoryIdWithLimit($categoryId, int $limit = 3)
    {
        return $this->getBuilder()->where('cate_id', $categoryId)->limit($limit)->get();
    }

    /**
     * @param string $slug
     * @return ?News
     */
    public function getNewsBySlugForSites(string $slug): ?Model
    {
        return $this->getBuilder()->where('slug', $slug)
            ->with('category')->firstOrFail();
    }

    public function getTopNewsForSites(): Collection|array
    {
        return $this->getBuilder()->with("category")->orderBy("total_view", "DESC")->limit(12)->get();
    }

    /**
     * @param int $categoryId
     * @param int $newsId
     * @return \App\ViewModels\Sites\News\Object\NewsObject[]
     */
    public function getRecentNewsByCategoryId(int $categoryId, int $newsId): array
    {
        $recentNews = $this->getBuilder()->select(['news_id', 'image', 'title', 'created_at', 'cate_id'])
            ->where('cate_id', $categoryId)
            ->where('news_id', '!=', $newsId)
            ->orderBy('created_at', 'desc')
            ->where("invalid", 0)
            ->with('category')->limit(3)->get();

        return $recentNews->map(
            fn(News $news) => new \App\ViewModels\Sites\News\Object\NewsObject(
                news_id : $news['news_id'],
                title : $news['title'],
                cate_title : $news['category']['title'],
                image : $news['image'],
                created_at : $news['created_at']
            )
        )->toArray();
    }

    public function searchByParams(mixed $params, int $page = 1): Collection|array
    {
        if ($page == 1) {
            $start = ($page - 1) * 11;
            $take = 11;
        } else {
            $start = ($page - 1) * 10 + 1;
            $take = 10;
        }

        return $this->getBuilder()
            ->where("invalid", 0)
            ->where("title", "like", "%$params%")
            ->with("category")
            ->orderBy("order", "DESC")
            ->skip($start)->take($take)->get();
    }

    public function getMostPopularNewspaper($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("total_view", "DESC")->limit($limit)->get();
    }

    public function getLatestNewspaper($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy('created_at', "DESC")->limit($limit)->get();
    }

    /**
     * @param mixed $delegateNews
     *
     * @return bool
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 10:57:17
     */
    public function updateDelegateNews(mixed $delegateNews): bool
    {
        $this->getBuilder()->update([
            'delegate_type' => 0
        ]);

        foreach ($delegateNews as $delegatePosition => $newId) {
            $this->getBuilder()->where([
                'news_id' => $newId
            ])->update([
                'delegate_type' => $delegatePosition
            ]);
        }

        return true;
    }

    public function getDelegateNewIds(): array
    {
        return $this->getAllGeneric(
            fields : [
                'news_id',
                'delegate_type'
            ],
            conditions : [
                'delegate_type !=' => 0
            ],
            order : 'delegate_type',
            direction : 'asc',
            limit : 3
        )->pluck('news_id', 'delegate_type')->toArray();
    }

    public function getDelegateNews(): Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator|array
    {
        return $this->getAllGeneric(
            conditions : [
                'delegate_type !=' => 0
            ],
            order : 'delegate_type',
            direction : 'asc',
            limit : 3
        );
    }
}