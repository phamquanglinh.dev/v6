<?php

namespace App\Repository;

use App\Models\Solution;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class SolutionRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return Solution::class;
    }

    public function getSolutionById($id): Model|Collection|Builder|array|null
    {
        return $this->getBuilder()->find($id);
    }
    public function getMostPopularSolution($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("view", "DESC")->limit($limit)->get();
    }

    public function getLatestSolution($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy('created_at', "DESC")->limit($limit)->get();
    }
}