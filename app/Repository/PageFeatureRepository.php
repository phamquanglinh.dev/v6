<?php

namespace App\Repository;

use App\Models\PageFeature;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PageFeatureRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return PageFeature::class;
    }

    public function getFeaturePage($slug, $folder): Model|Builder
    {
        return $this->getBuilder()->where("folder", $folder)->where("slug", $slug)->firstOrFail();
    }
    public function getMostPopularFeaturePage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy("view", "DESC")->limit($limit)->get();
    }

    public function getLatestFeaturePage($limit = 10): Collection|array
    {
        return $this->getBuilder()->orderBy('created_at', "DESC")->limit($limit)->get();
    }
}
