<?php

namespace App\Repository;

use App\Models\Pricing;
use App\ViewModels\Admin\Pricing\Object\PricingStoreObject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PricingRepository extends BaseRepository
{
    const PAGE = 15;
    const UPLOAD = "pricing";

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Pricing::class;
    }

    public function getPricingPagination(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy("order", "ASC")->paginate(self::PAGE);
    }

    public function getPricingById($id): Model|Builder
    {
        return $this->getBuilder()->where("id", $id)->firstOrFail();
    }

    public function createPricing(PricingStoreObject $storeObject): void
    {
        $pricingCreate = changeObjectToArray($storeObject);
        $this->getBuilder()->create($pricingCreate);
    }

    public function updatePricing(PricingStoreObject $updateObject, $id): void
    {
        /**
         * @var Pricing $prcingModel
         */
        $pricingModel = $this->getPricingById($id);
        $pricingUpdate = changeObjectToArray($updateObject);
        $pricingModel->update($pricingUpdate);
        $pricingModel->save();
    }

    public function deletePricing($id): void
    {
        $this->getBuilder()->where("id", $id)->delete();
    }
}