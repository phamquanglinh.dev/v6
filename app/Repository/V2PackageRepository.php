<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 10:13 am
 */

namespace App\Repository;

use App\Models\V2Package;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class V2PackageRepository extends BaseRepository
{

    /**
     * @return string
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 10:15 am
     */
    public function getModelClassName(): string
    {
        return V2Package::class;
    }

    /**
     * @return Collection|array
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 10:15 am
     */
    public function getAllV2OnCloudPackage(): Collection|array
    {
        return $this->getBuilder()->where('type', V2Package::ON_CLOUD_PACKAGE)->orderBy('id', 'ASC')->get();
    }

    public function bulkUpdateAllOnCloudPackage(array $packages): bool
    {
        foreach ($packages as $package) {
            $this->getBuilder()->updateOrInsert([
                'name' => $package['name'],
                'type' => V2Package::ON_CLOUD_PACKAGE
            ], $package);
        }

        return true;
    }

    public function getAllV2OnPremisePackage(): Collection|array
    {
        return $this->getBuilder()->where('type', V2Package::ON_PREMISE_PACKAGE)->orderBy('id', 'ASC')->get();
    }

    public function bulkUpdateAllOnPremisePackage(array $packages): bool
    {
        foreach ($packages as $package) {
            $this->getBuilder()->updateOrInsert([
                'name' => $package['name'],
                'type' => V2Package::ON_PREMISE_PACKAGE
            ], $package);
        }

        return true;
    }
}