<?php

namespace App\Repository;

use App\Models\Menu;
use App\Helpers\NestedSetModel;
use App\ViewModels\Admin\Menu\MenuOrder\Object\MenuOrderObject;
use App\ViewModels\Admin\Menu\MenuOrder\Object\MenuOrderUpdateObject;
use App\ViewModels\Sites\Header\SecondMenuObject;
use App\ViewModels\Sites\Header\ThirdMenuObject;
use Illuminate\Database\Eloquent\Model;
use App\ViewModels\Sites\Header\MenuObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\Menu\MenuAdd\Object\MenuObjectAdd;
use App\ViewModels\Admin\Menu\MenuEdit\Object\MenuObjectEdit;
use Illuminate\Contracts\Container\BindingResolutionException;

class MenuRepository extends BaseRepository
{
    private const PAGINATE = 20;
    private const PATH = "/menus";

    /**
     * @param NestedSetModel $nestedSetModel
     * @throws BindingResolutionException
     */
    public function __construct(readonly private NestedSetModel $nestedSetModel)
    {
        parent::__construct();

        $nestedSetModel->init(
            tableName: 'tbl_menu',
            identifier: 'id',
            leftColumnName: 'nleft',
            rightColumnName: 'nright',
            parentColumnName: 'parent_id',
            levelColumnName: 'level'
        );
    }

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return Menu::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getMenuPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy('nleft', 'asc')
            ->where('parent_id', '>', 0)
            ->paginate(self::PAGINATE);
    }

    /**
     * @param Menu|null $menu
     * @return Collection
     */
    public function getMenuAll(Menu $menu = null): Collection
    {
        if ($menu) {
            return $this->getBuilder()->orderBy('nleft', 'asc')
                ->whereNotBetween('nright', [$menu['nleft'], $menu['nright']])
                ->get();
        }

//        if (!$getRoot) {
//            return $this->getBuilder()->where('parent_id', '>', 0)
//                ->orderBy('nleft', 'asc')->get();
//        }
        return $this->getBuilder()->where("invalid", 0)->orderBy('nleft', 'asc')->get();
    }

    /**
     * @param $key
     * @return MenuObject
     */
    public function getHeaders($key): MenuObject
    {
        /**
         * @var Menu $menu
         */
        $menu = $this->getBuilder()->where("id", $key)->first();

        $child = $this->getBuilder()->orderBy('lft', 'asc')
            ->where('invalid', 0)
//            ->where('mgroup', 0)
            ->where('parent_id', '=', $key)->get();

        return new MenuObject(
            title: $menu['title'],
            mega: $menu['mega'] ?? false,
            links: $menu['link'],
            child: $child->map(
                fn($item) => $this->getHeaders($item['id'])
            )->toArray(),
            icon: $menu['icon'], tabs: $menu->Tabs()->get(), newTab: $menu["new_tab"] == 1);
    }

    public function getMenuForOrder($key): MenuOrderObject
    {
        $menu = $this->getBuilder()->where("id", $key)->first();
        $child = $this->getBuilder()->orderBy('nleft', 'asc')
            ->where('invalid', 0)
            ->where('mgroup', 1)
            ->where('parent_id', '=', $key)->get();
        return new MenuOrderObject(
            id: $menu["id"],
            parent_id: $menu["parent_id"],
            title: $menu['title'],
            invalid: $menu["invalid"],
            type: $menu['type'],
            icon: $menu["icon"],
            child: $child->map(
                fn($item) => $this->getMenuForOrder($item['id'])
            )->toArray());

    }

    public function getHeaderForSite(): MenuObject
    {
        return $this->getHeaders(1);
    }

    /**
     * @param int $menuId
     * @param MenuObjectEdit $menuObjectEdit
     * @return void
     */
    public function updateMenu(int $menuId, MenuObjectEdit $menuObjectEdit): void
    {
        /**
         * @var Menu $menu
         */
        $menu = $this->getBuilder()->where('id', $menuId)->first();

        $dataMenuUpdate = changeObjectToArray($menuObjectEdit);


        unset($dataMenuUpdate['cate']);

        $menu->update($dataMenuUpdate);
        if ($dataMenuUpdate["icon"] instanceof UploadedFile) {
            $menu->icon = $this->handleImageFile(file: $dataMenuUpdate["icon"], path: self::PATH);
            $menu->save();
        }
        if (!$this->getBuilder()->where('id', $menuId)->where('parent_id', $menuObjectEdit->getCate())->first()) {
            $this->nestedSetModel->moveNodeInto($menu, $menuObjectEdit->getCate());
        }
    }

    /**
     * @param MenuObjectAdd $menuObjectAdd
     * @return void
     */
    public function createMenu(MenuObjectAdd $menuObjectAdd): void
    {
        $dataMenuCreate = changeObjectToArray($menuObjectAdd);

        if (empty($parentNodeId = $menuObjectAdd->getCate())) {
            $parentNodeId = $this->nestedSetModel->getRootNode();
        }
        /**
         * @var Menu $menuCreated
         */
        $menuCreated = $this->getBuilder()->create($dataMenuCreate);
        if ($menuCreated->icon) {
            $menuCreated->icon = $this->handleImageFile(file: $dataMenuCreate["icon"], path: self::PATH);
            $menuCreated->save();
        }
        $this->nestedSetModel->addNode($parentNodeId, $menuCreated['id']);
    }

    /**
     * @param int $menuId
     * @return void
     */
    public function deleteMenu(int $menuId): void
    {
        $this->nestedSetModel->removeNode(nodeId: $menuId, softDeleteFieldName: 'invalid');
    }

    /**
     * @param int $menuId
     * @return Menu|null
     */
    public function getMenuById(int $menuId): Model|null
    {
        return $this->getBuilder()->find($menuId);
    }

    /**
     * @param int|null $currentNodeId
     * @param int|null $newPositionNodeId
     * @return void
     */
    public function moveNodePosition(int|null $currentNodeId, int|null $newPositionNodeId): void
    {
        $this->nestedSetModel->moveNodePosition(
            currentNodeId: $currentNodeId ?? $this->nestedSetModel->getRootNode(),
            newPositionNodeId: $newPositionNodeId ?? $this->nestedSetModel->getRootNode()
        );
    }

    public function updateOrderMenu($id, MenuOrderUpdateObject $updateObject): void
    {
        $menu = $this->getMenuById($id);
        $updateData = changeObjectToArray($updateObject);
        $menu->update($updateData);
        $menu->save();
    }
}