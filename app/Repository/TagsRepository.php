<?php

namespace App\Repository;

use App\Models\Tags;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\Tag\TagAdd\Object\TagObjectAdd;
use App\ViewModels\Admin\Tag\TagEdit\Object\TagObjectEdit;

class TagsRepository extends BaseRepository
{
    private CONST PAGINATE = 20;

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return Tags::class;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getTagPaginationAll(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy('created_at', 'desc')->paginate(self::PAGINATE);
    }

    /**
     * @param int $tagId
     * @param TagObjectEdit $tagObjectEdit
     * @return void
     */
    public function updateTag(int $tagId, TagObjectEdit $tagObjectEdit): void
    {
        $dataTagUpdate = changeObjectToArray($tagObjectEdit);

        $tag = $this->getBuilder()->where('tag_id', $tagId)->first();

        $tag?->update($dataTagUpdate);
    }

    /**
     * @param TagObjectAdd $tagObjectAdd
     * @return void
     */
    public function createTag(TagObjectAdd $tagObjectAdd): void
    {
        $dataTagCreate = changeObjectToArray($tagObjectAdd);

        $this->getBuilder()->create($dataTagCreate);
    }

    /**
     * @param int[] $tagIds
     * @return void
     */
    public function deleteTag(array $tagIds): void
    {
        $this->getBuilder()->whereIn('tag_id', $tagIds)->update(['invalid' => 1]);
    }

    /**
     * @param int $bannerId
     * @return Tags|null
     */
    public function getTagById(int $bannerId): Model|null
    {
        return $this->getBuilder()->find($bannerId);
    }
}