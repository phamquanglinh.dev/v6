<?php

namespace App\Repository;

use App\Models\Recruitment;
use Illuminate\Database\Eloquent\Collection;

class RecruitmentRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Recruitment::class;
    }

    public function getAll(): Collection|array
    {
        return $this->getBuilder()->orderBy("created_at", "DESC")->get();
    }
}