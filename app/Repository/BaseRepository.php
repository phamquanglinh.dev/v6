<?php

namespace App\Repository;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Container\BindingResolutionException;

abstract class BaseRepository
{
    /**
     * @return string
     */
    abstract public function getModelClassName(): string;

    /**
     * @var Model
     */
    private Model $model;

    /**
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    private function setModel(): void
    {
        $this->model = app()->make($this->getModelClassName());
    }

    /**
     * @return Builder
     */
    public function getBuilder(): Builder
    {
        return $this->model->query();
    }

    /**
     * @param UploadedFile|string $file is file image
     * @param string $path is name of feature
     * @param string $oldPath is old path of record
     * @param bool $shortName
     * @return string
     */
    public function handleImageFile(UploadedFile|string $file, string $path, string $oldPath = "", bool $shortName = false): string
    {
        if ($file instanceof UploadedFile) {
            $imageName = rand(1, 99) . '_' . $file->getClientOriginalName() . '_' . time() . '.' . $file->getClientOriginalExtension();

            if ($shortName) {
                $imageName = rand(1, 99) . '_' . time() . '.' . $file->getClientOriginalExtension();
            }

            $pathImage = $path ? "assets/uploads/$path/images/" : "assets/uploads/images/";

            $file->move(public_path($pathImage), $imageName);

            return $pathImage . $imageName;
        }

        return $oldPath;
    }

    /**
     * @param array $fields
     * @param array $conditions
     * @param string|null $order
     * @param string $direction
     * @param int|null $limit
     * @param int $offset
     * @param null $paginate
     *
     * @return Collection|LengthAwarePaginator|array
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 09:47:54
     */
    public function getAllGeneric
    (
        array $fields = ['*'],
        array $conditions = [],
        string $order = null,
        string $direction = "ASC",
        int $limit = null,
        int $offset = 0,
        $paginate = null,
    ): Collection|LengthAwarePaginator|array
    {
        $builder = $this->getBuilder()->select($fields);

        if (! empty($conditions)) {
            foreach ($conditions as $condition => $value) {
                $parsedCondition = explode(' ', $condition);

                $column = $parsedCondition[0];

                $operator = $parsedCondition[1] ?? "=";

                $builder->where($column, $operator, $value);
            }
        }

        if ($order) {
            $builder->orderBy($order, $direction);
        }

        if ($limit) {
            $builder->limit($limit);
        }

        if ($paginate) {
            return $builder->paginate($paginate);
        }

        return $builder->get();
    }

    /**
     * @param array $fields
     * @param array $conditions
     * @param string|null $order
     * @param string $direction
     *
     * @return Model|Builder
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 09:50:23
     */
    public function getOneGeneric(
        array $fields = ['*'],
        array $conditions = [],
        string $order = null,
        string $direction = "ASC",
    ): Model|Builder
    {
        $builder = $this->getBuilder()->select($fields);

        if (! empty($conditions)) {
            $builder->where($conditions);
        }

        if ($order) {
            $builder->orderBy($order, $direction);
        }

        return $builder->first();
    }
}