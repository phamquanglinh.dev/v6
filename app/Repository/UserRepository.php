<?php

namespace App\Repository;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use App\ViewModels\Admin\User\UserAdd\Object\UserObject;
use App\ViewModels\Admin\User\UserEdit\Object\UserObject as UserObjectEdit;

class UserRepository extends BaseRepository
{
    private const PAGINATE = 10;
    private const PATH_IMAGE = 'user';

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return User::class;
    }
    public function deleteUser(array $userIds): bool
    {
        return $this->getBuilder()->whereIn('user_id', $userIds)->update(['invalid' => 1]);
    }

    /**
     * @param int $userId
     * @return User|null
     */
    public function getUserById(int $userId): Model|null
    {
        return $this->getBuilder()->find($userId);
    }

    public function getUserByUserName(string $username): Model|null
    {
        return $this->getBuilder()->where("user_name", $username)->first();
    }
}