<?php

namespace App\Repository;

use App\Models\SiteInfo;
use Illuminate\Database\Eloquent\Model;
use App\ViewModels\Admin\Footer\FooterEdit\Object\FooterEditObject;

class FooterRepository extends BaseRepository
{
    private CONST FOOTER_ID = 1;

    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return SiteInfo::class;
    }

    /**
     * @param FooterEditObject $footerEditObject
     * @return void
     */
    public function updateFooter(FooterEditObject $footerEditObject): void
    {
        $footer = $this->getBuilder()->where('id', self::FOOTER_ID)->first();
        $footer->update(changeObjectToArray($footerEditObject));
    }

    /**
     * @return SiteInfo|null
     */
    public function getFooterById(): Model|null
    {
        return $this->getBuilder()->find(self::FOOTER_ID);
    }
}