<?php

namespace App\Repository;

use App\Models\Feature;
use App\ViewModels\Admin\Feature\Object\FeatureObjectAdd;
use App\ViewModels\Admin\Feature\Object\FeatureObjectEdit;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class FeatureRepository extends BaseRepository
{
    const IMAGE_PATH = "features";
    private const PAGINATE = 20;

    /**
     * @inheritDoc
     */
    public function getModelClassName(): string
    {
        return Feature::class;
    }

    public function getFeaturesForHome(): Collection|array
    {
        return $this->getBuilder()->orderBy("id", "ASC")->get();
    }

    public function getAllFeaturesPagination(): LengthAwarePaginator
    {
        return $this->getBuilder()->orderBy("created_at", "ASC")->paginate(self::PAGINATE);
    }

    public function createFeature(FeatureObjectAdd $objectAdd): void
    {
        $dataFeatureCreate = changeObjectToArray($objectAdd);
        /**
         * @var Feature $featureModel
         */
        $featureModel = $this->getBuilder()->create($dataFeatureCreate);
        $featureModel->thumbnail = $this->handleImageFile(
            file: $dataFeatureCreate['thumbnail'], path: self::IMAGE_PATH
        );
        if ($featureModel->thumbnail) {
            $featureModel->save();
        }
        $featureModel->icon = $this->handleImageFile(
            file: $dataFeatureCreate['icon'], path: self::IMAGE_PATH
        );
        if ($featureModel->icon) {
            $featureModel->save();
        }
    }

    public function getFeatureById($id): Model|Builder
    {
        return $this->getBuilder()->where("id", $id)->firstOrFail();
    }

    public function updateFeature($id, FeatureObjectEdit $objectEdit): void
    {
        $dataFeatureUpdate = changeObjectToArray($objectEdit);
        /**
         * @var Feature $featureModel
         */
        $featureModel = $this->getFeatureById($id);
        if ($dataFeatureUpdate["icon"] == null) {
            $dataFeatureUpdate["icon"] = $featureModel->icon;
        }
        if ($dataFeatureUpdate["thumbnail"] == null) {
            $dataFeatureUpdate["thumbnail"] = $featureModel->thumbnail;
        }
        $featureModel->update($dataFeatureUpdate);
        $featureModel->save();
        if ($dataFeatureUpdate["thumbnail"] instanceof UploadedFile) {
            $featureModel->thumbnail = $this->handleImageFile(
                file: $dataFeatureUpdate['thumbnail'], path: self::IMAGE_PATH
            );
            if ($featureModel->thumbnail) {
                $featureModel->save();
            }
        }
        if ($dataFeatureUpdate["icon"] instanceof UploadedFile) {
            $featureModel->icon = $this->handleImageFile(
                file: $dataFeatureUpdate['icon'], path: self::IMAGE_PATH
            );
            if ($featureModel->icon) {
                $featureModel->save();
            }
        }
    }
}