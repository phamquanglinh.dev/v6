<?php

namespace App\Repository;

use App\Models\NewsRelate;

class NewsRelateRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function getModelClassName(): string
    {
        return NewsRelate::class;
    }

    /**
     * @param int $newsId
     * @param int[] $newsRelateIds
     * @return void
     */
    public function createNewsRelate(int $newsId, array $newsRelateIds): void
    {
        $newsRelates = [];
        foreach ($newsRelateIds as $index => $newsRelate) {
            $newsRelates[$index]['news_id'] = $newsId;
            $newsRelates[$index]['relate_news_id'] = $newsRelate;
        }

        $this->getBuilder()->insert($newsRelates);
    }

    /**
     * @param int $newsId
     * @param int[] $newsRelateIds
     * @return void
     */
    public function updateNewsRelate(int $newsId, array $newsRelateIds): void
    {
        $newsRelates = [];

        foreach ($newsRelateIds as $index => $newsRelate) {
            $newsRelates[$index]['news_id'] = $newsId;
            $newsRelates[$index]['relate_news_id'] = $newsRelate;
        }

        if ($newsRelates) {
            $this->deleteNewsRelate($newsId);
            $this->getBuilder()->insert($newsRelates);
        }
    }

    /**
     * @param int|int[] $newsIds
     * @return void
     */
    public function deleteNewsRelate(int|array $newsIds): void
    {
        if (!is_array($newsIds)) {
            $newsIds = [$newsIds];
        }

        $this->getBuilder()->whereIn('news_id', $newsIds)->delete();
    }
}