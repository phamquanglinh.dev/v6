<?php

namespace App\Repository;

use App\Models\Room;

class RoomRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return Room::class;
    }
}