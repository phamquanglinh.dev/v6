<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 07/03/2024 9:03 am
 */

namespace App\Repository;

use App\Models\V2Partner;
use Illuminate\Database\Eloquent\Collection;

class V2PartnerRepository extends BaseRepository
{

    public function getModelClassName(): string
    {
        return V2Partner::class;
    }

    public function getAllV2Partners(
        array $conditions = [],
        string $orderBy = null,
        string $direction = 'ASC',
        int $limit = null,
    ): Collection|array
    {
        $builder = $this->getBuilder();

        if (! empty($conditions)) {
            $builder->where($conditions);
        }

        if ($orderBy) {
            $builder->orderBy($orderBy, $direction);
        }

        if ($limit) {
            $builder->limit($limit);
        }

        return $builder->get();
    }
}