<?php

namespace App\Repository;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class CustomerRepository extends BaseRepository
{
    const PAGE_LENGTH = 15;
    const IMAGE_PATH = "customers";

    public function getModelClassName(): string
    {
        return Customer::class;
    }

    public function getCustomersPagination(): LengthAwarePaginator
    {
        return $this->getBuilder()
            ->orderBy("created_at", "DESC")
            ->paginate(self::PAGE_LENGTH);
    }

    public function createCustomer($attributes): void
    {
        /**
         * @var Customer $customerModel
         */;
        $logo = $this->handleImageFile(
            file: $attributes['logo'], path: self::IMAGE_PATH
        );

        $customerModel = $this->getBuilder()->create($attributes);
        $customerModel->logo = $logo;
        if ($customerModel->logo) {
            $customerModel->save();
        }
    }

    public function updateCustomer($attributes, $id): void
    {

        /**
         * @var Customer $customerModel
         */
        $customerModel = $this->getCustomerById($id);
        $customerModel->update($attributes);
        $customerModel->save();
        if (isset($attributes["logo"])) {
            $logo = $this->handleImageFile(
                file: $attributes["logo"], path: self::IMAGE_PATH
            );
            $customerModel->logo = $logo;
            $customerModel->save();
        }
    }

    public function getCustomerById($id): Model|Builder
    {
        return $this->getBuilder()->where("id", $id)->firstOrFail();
    }

    public function getCustomersForSite(): Collection|array
    {
        return $this->getBuilder()
            ->where("type", 2)
            ->where("priority", 1)
            ->orderBy('priority_order')
            ->limit(12)->get();

    }

    public function getAllActiveCustomers(): Collection|array
    {
        return $this->getBuilder()
            ->where("type", 2)->where("priority", 0)
            ->orderBy("created_at", "DESC")
            ->get();
    }

    public function delete($id)
    {
        return $this->getBuilder()->where("id", $id)->delete();
    }

}