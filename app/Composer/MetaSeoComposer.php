<?php

namespace App\Composer;

use App\Models\SiteInfo;
use App\Repository\FooterRepository;
use App\ViewModels\Sites\Home\MetaSeoViewModel;
use Illuminate\View\View;

class MetaSeoComposer
{
    public function __construct(
        private readonly FooterRepository $footerRepository
    )
    {
    }

    public function compose(View $view): View
    {
        $siteInfo = $this->footerRepository->getFooterById();
        return $view->with("metaSeoViewModel", new MetaSeoViewModel(
            description: $siteInfo["description_seo"] ?? "", keywords: $siteInfo["keyword_seo"]
        ));
    }
}