<?php

namespace App\Composer;

use App\Repository\MenuRepository;
use App\ViewModels\Sites\Header\HeaderListViewModel;
use Illuminate\View\View;

class V2NavbarComposer
{
    public function __construct(
        private readonly MenuRepository $menuRepository
    )
    {
    }

    public function compose(View $view): View
    {

        return $view->with("headerListViewModel", new HeaderListViewModel(menus: $this->menuRepository->getHeaders(1)));
    }
}