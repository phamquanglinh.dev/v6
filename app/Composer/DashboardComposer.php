<?php

namespace App\Composer;

use App\Repository\FeedbackRepository;
use App\Repository\NewsRepository;
use App\Repository\RecruitmentRepository;
use App\Repository\UserRepository;
use App\ViewModels\Admin\DashboardViewModel;
use Illuminate\View\View;

class DashboardComposer
{
    public function __construct(
        private readonly NewsRepository        $newsRepository,
        private readonly UserRepository        $userRepository,
        private readonly RecruitmentRepository $recruitmentRepository,
        private readonly FeedbackRepository    $feedbackRepository,
    )
    {
    }

    public function compose(View $view): View
    {
        $quantity = [];
        $quantity["news"] = $this->newsRepository->getBuilder()->where("invalid", 0)->count();
        $quantity["users"] = $this->userRepository->getBuilder()->where("invalid", 0)->count();
        $quantity["recruitments"] = $this->recruitmentRepository->getBuilder()->count();
        $quantity["feedbacks"] = $this->feedbackRepository->getBuilder()->count();
        $dashboardViewModel = new DashboardViewModel($quantity);
        return $view->with("dashboardViewModel", $dashboardViewModel);
    }
}