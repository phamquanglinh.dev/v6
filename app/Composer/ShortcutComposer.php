<?php

namespace App\Composer;

use App\Repository\LandingPageRepository;
use App\Repository\NewsRepository;
use App\Repository\PageFeatureRepository;
use App\Repository\SolutionRepository;
use App\Repository\SubPageRepository;
use App\ViewModels\Admin\Shortcut\ShortcutViewModel;
use Illuminate\View\View;

class ShortcutComposer
{
    public function __construct(
        private readonly LandingPageRepository $landingPageRepository,
        private readonly PageFeatureRepository $pageFeatureRepository,
        private readonly SubPageRepository     $subPageRepository,
        private readonly SolutionRepository    $solutionRepository,
        private readonly NewsRepository $newsRepository,
    )
    {
    }

    public function compose(View $view): View
    {
        return $view->with("shortcutViewModel", new ShortcutViewModel(
            popularLandingPage: $this->landingPageRepository->getMostPopularLandingPage(5),
            latestLandingPage: $this->landingPageRepository->getLatestLandingPage(5),
            popularPageFeature: $this->pageFeatureRepository->getMostPopularFeaturePage(5),
            latestPageFeature: $this->pageFeatureRepository->getLatestFeaturePage(5),
            popularSolution: $this->solutionRepository->getMostPopularSolution(5),
            latestSolution: $this->solutionRepository->getLatestSolution(5),
            popularSubPage: $this->subPageRepository->getMostSubPage(5),
            latestSubpage: $this->subPageRepository->getLatestSubPage(5),
            popularNewspaper: $this->newsRepository->getMostPopularNewspaper(5),
            latestNewspaper: $this->newsRepository->getLatestNewspaper(5),
        ));
    }
}