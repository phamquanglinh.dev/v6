<?php

namespace App\Composer;

use App\Repository\PhotoRepository;
use App\ViewModels\Sites\TeamInfo\Object\PhotoListViewModel;
use Illuminate\View\View;


class PhotoListComposer
{
    public function __construct(
        private readonly PhotoRepository $photoRepository
    )
    {
    }

    public function compose(View $view): View
    {
        return $view->with("photoListViewModel", new PhotoListViewModel(photos: $this->photoRepository->getAllPhotos()));
    }
}