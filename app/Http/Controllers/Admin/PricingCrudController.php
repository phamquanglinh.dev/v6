<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PricingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PricingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PricingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Pricing::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/pricing');
        CRUD::setEntityNameStrings('Gói on cloud', 'DS Gói');
        $this->crud->denyAccess(["show"]);
        $subQuery = $this->crud->query;
        if ($subQuery->count() >= 6) {
            $this->crud->denyAccess(["create"]);
        }
        if (!(hasAccess("manager"))) {
            $this->crud->denyAccess(["list", "create", "update", "show","delete"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label("Tên gói");
        CRUD::column('users')->label("Số lượng người dùng");
        CRUD::column('price')->type("closure")->label("Chi phí")->function(function ($entry) {
            return $entry["price"] > 0 ? number_format($entry["price"]) . " đ" : "Liên hệ";
        });
        CRUD::column('user_price')->type("closure")->label("Chi phí/ người")->function(function ($entry) {
            return $entry["user_price"] > 0 ? number_format($entry["user_price"]) . " đ" : "Liên hệ";
        });
        CRUD::column('customers')->label("Số lượng khách hàng")->type("number");
        CRUD::column('setup_time')->label("Thời gian setup");
        CRUD::column('domain_target')->label("Chỉ định domain")->type("check");
        CRUD::column('order')->label('Vị trí')->type("closure")->function(function ($entry) {
            return $entry["order"] == 9999 ? "Mặc định" : $entry["order"];
        });

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PricingRequest::class);

        CRUD::field('name')->label("Tên gói");
        CRUD::field('users')->label("Số lượng người dùng");
        CRUD::field('price')->label("Giá gói");
        CRUD::field('user_price')->label("Giá/ Người dùng");
        CRUD::field('customers')->label("Số lượng khách hàng");
        CRUD::field('setup_time')->label("Thời gian setup");
        CRUD::field('domain_target')->label("Chỉ định domain")->type("switch")->default(0);
        CRUD::field('order')->type("hidden")->value(9999);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
