<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Controllers\Admin\Operations\ReviseOperation;
use App\Http\Requests\BannerRequest;
use App\Models\Banner;
use App\Repository\BannerRepository;
use App\ViewModels\Sites\Banner\BannerListViewModel;
use Backpack\CRUD\app\Exceptions\BackpackProRequiredException;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

/**
 * Class BannerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BannerCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;
    use RecoverTrashOperation;
//    use ReviseOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws BackpackProRequiredException
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Banner::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/banner');
        CRUD::setEntityNameStrings('banner', 'DS Banner');
        $this->crud->denyAccess(["show"]);
        CRUD::enableExportButtons();
        if (!hasAccess("manager")) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
        $this->crud->addButtonFromModelFunction("top", "orderButton", "orderButton", "top");
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {

        $this->crud->addFilter([
            'name' => 'indexFilter',
            'type' => 'select2',
            'label' => 'Banner Trang chủ',
        ], function () {
            return [
                1 => 'Banner trang chủ',
                2 => 'Banner cột phải'
            ];
        }, function ($value) {
            $this->crud->query->where("position", $value);
        });
        $this->crud->orderBy("order", "ASC");
        CRUD::column('title')->label("Tiêu đề banner");
        CRUD::column('image')->label("Ảnh")->type("image");
        CRUD::column('position')->label("Vị trí")->type("select_from_array")->options([
            1 => 'Trang chủ',
            2 => 'Cột phải',
        ]);
        CRUD::column('order')->label("Sắp xếp");
        CRUD::column('url')->label("URL");
        CRUD::column('invalid')->label("Ẩn");
        CRUD::column('click')->label("Lượt click");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {

        CRUD::setValidation(BannerRequest::class);
        CRUD::field('title')->label("Tiêu đề banner");
        CRUD::addField([
            'name' => 'image',
            'type' => 'image',
            'crop' => true,
            'upload' => true,
            'disk' => 'uploads',
            'url' => public_path()
        ]);
        CRUD::field('order')->label("Sắp xếp");
        CRUD::field('position')->label("Vị trí")->type("select_from_array")->options([
            1 => 'Trang chủ',
            2 => 'Cột phải',
        ]);
        CRUD::field('url');
        CRUD::field('invalid')->type("switch")->label("Ẩn");
        CRUD::field('click')->type("hidden")->default(0);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function reorderOperation(BannerRepository $bannerRepository): View
    {
        $bannerCollection = $bannerRepository->getBannerByPosition(1);
        return view("vendor.backpack.crud.bannerReorder", ['bannerListViewModel' => new BannerListViewModel(banners: $bannerCollection)]);
    }

    protected function orderOperation(Request $request): bool
    {
        $photos = $request["banners"];
        foreach ($photos as $key => $item) {
            Banner::query()->where("banner_id", $item)->update([
                'order' => $key
            ]);
        }
        return true;
    }
}
