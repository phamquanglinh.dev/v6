<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use App\Repository\PhotoRepository;
use App\ViewModels\Sites\TeamInfo\Object\PhotoListViewModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class PhotoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PhotoCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup(): void
    {
        CRUD::setModel(\App\Models\Photo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/photo');
        CRUD::setEntityNameStrings('Ảnh', 'DS Ảnh');
        $this->crud->denyAccess(["show"]);
        if (!(hasAccess("hr"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
        $this->crud->addButtonFromModelFunction("top", "orderButton", "orderButton", "top");

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        $this->crud->query->orderBy("order", "ASC");
        CRUD::addColumn(['name' => 'order', 'type' => 'number', "label" => 'Vị trí']);
        CRUD::addColumn(['name' => 'image', 'type' => 'image', 'label' => 'Ảnh xem trước']);
        CRUD::addColumn(['name' => 'landscape_image', 'type' => 'image', 'label' => 'Ảnh chi tiết']);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(PhotoRequest::class);
        CRUD::addField([
            'name' => 'image',
            'type' => 'image',
            'label' => 'Ảnh xem trước (1:1)',
            'crop' => true,
            'aspect_ratio' => 1,
            'upload' => true,
            'disk' => 'uploads'
        ]);
        CRUD::addField([
            'name' => 'landscape_image',
            'type' => 'image',
            'label' => 'Ảnh chi tiết (16:9) ',
            'crop' => true,
            'aspect_ratio' => 16 / 9,
            'upload' => true,
            'disk' => 'uploads'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        $this->setupCreateOperation();
    }

    protected function reorderOperation(PhotoRepository $photoRepository): View
    {
        $photoCollection = $photoRepository->getAllPhotos();
        return view("vendor.backpack.crud.photoReorder", ['photoListViewModel' => new PhotoListViewModel(photos: $photoCollection)]);
    }

    protected function orderOperation(Request $request): bool
    {
        $photos = $request["photos"];
        foreach ($photos as $key => $item) {
            Photo::query()->where("id", $item)->update([
                'order' => $key
            ]);
        }
        return true;
    }
}
