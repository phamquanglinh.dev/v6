<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SiteInfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SiteInfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SiteInfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SiteInfo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/site-info');
        CRUD::setEntityNameStrings('Thông tin trang', 'Thông tin trang');
        $this->crud->denyAccess(["create", "delete", "list"]);
        $this->crud->denyAccess(["show"]);
        if (!(hasAccess("admin"))) {
            $this->crud->denyAccess(["list", "create", "update", "show","delete"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        $this->setupUpdateOperation();
//        CRUD::column('title');
//        CRUD::column('description');
//        CRUD::column('content');
//        CRUD::column('phone');
//        CRUD::column('email');
//        CRUD::column('website');
//        CRUD::column('yahoo');
//        CRUD::column('facebook');
//        CRUD::column('address');
//        CRUD::column('logo');
//        CRUD::column('cname');
//        CRUD::column('profile');
//        CRUD::column('copyright');
//        CRUD::column('hotline');
//        CRUD::column('skype');
//        CRUD::column('fax');
//        CRUD::column('keyword_seo');
//        CRUD::column('description_seo');
//        CRUD::column('text_run');
//        CRUD::column('code_youtube');
//        CRUD::column('twitter');
//        CRUD::column('mobile');
//        CRUD::column('cname_en');
//        CRUD::column('youtube');
//        CRUD::column('video_link');
//        CRUD::column('google');
//        CRUD::column('video_image_link');
//        CRUD::column('news_about');
//        CRUD::column('news_time_line');
//        CRUD::column('campaign_url');
//        CRUD::column('footer_product');
//        CRUD::column('footer_feature');
//        CRUD::column('faqs');
//        CRUD::column('profession');
//        CRUD::column('partners');
//        CRUD::column('experience_years');
//        CRUD::column('contact_form');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SiteInfoRequest::class);

//        CRUD::field('title');
//        CRUD::field('description')->label("Mô tả")->type("textarea");
        CRUD::field('content')->label("Hiển thị ở trang chủ");
//        CRUD::field('phone')->label("SĐT");
//        CRUD::field('email');
//        CRUD::field('website');
//        CRUD::field('yahoo');
//        CRUD::field('facebook');
//        CRUD::field('address')->label("Địa chỉ")->type("ckeditor");
//        CRUD::field('logo')->type("image");
//        CRUD::field('cname');
//        CRUD::field('profile');
        CRUD::field('copyright')->type("adonis-editor");
//        CRUD::field('hotline');
//        CRUD::field('skype');
//        CRUD::field('fax');
        CRUD::field('keyword_seo');
        CRUD::field('description_seo');
//        CRUD::field('text_run');
//        CRUD::field('code_youtube');
//        CRUD::field('twitter');
//        CRUD::field('mobile');
//        CRUD::field('cname_en');
//        CRUD::field('youtube')->label("Link kênh youtube");
        CRUD::field('video_link')->label('Video youtube trang chủ');
//        CRUD::field('google');
//        CRUD::field('video_image_link');
//        CRUD::field('news_about');
//        CRUD::field('news_time_line');
//        CRUD::field('campaign_url');
//        CRUD::field('footer_product');
//        CRUD::field('footer_feature');
//        CRUD::field('faqs');
        CRUD::field('partners')->label("Doanh nghiệp sử dụng");
        CRUD::field('profession')->label("Ngành nghề");
        CRUD::field('experience_years')->label("Năm kinh nghiệm");
//        CRUD::field('contact_form')->label("Link form liên hệ")->type("text");

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
