<?php

namespace App\Http\Controllers\Admin\V2;

use App\Http\Controllers\Controller;
use App\Models\V2Package;
use App\Repository\V2FeatureRepository;
use App\Repository\V2PackageRepository;
use App\Repository\V2PricingRepository;
use App\ViewModels\Admin\V2FeatureShowViewModel;
use App\ViewModels\Admin\V2OnCloudPackageShowViewModel;
use App\ViewModels\Admin\V2OnPremisePackageShowViewModel;
use App\ViewModels\Admin\V2PricingShowViewModel;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Prologue\Alerts\Facades\Alert;

class V2PricingCrudController extends Controller
{
    private V2PricingRepository $v2PricingRepository;
    private V2PackageRepository $v2PackageRepository;
    private V2FeatureRepository $v2FeatureRepository;

    public function __construct(
        V2PricingRepository $v2PricingRepository,
        V2PackageRepository $v2PackageRepository,
        V2FeatureRepository $v2FeatureRepository
    )
    {
        $this->v2PricingRepository = $v2PricingRepository;
        $this->v2PackageRepository = $v2PackageRepository;
        $this->v2FeatureRepository = $v2FeatureRepository;
    }

    public function showV2PricingPageAction(): View
    {
        return view('version2.admin.pricing.show', [
            'v2PricingShowViewModel' => new V2PricingShowViewModel(
                pricingCollection : $this->v2PricingRepository->getAllV2Pricing()
            ),
            'v2FeatureShowViewModel' => new V2FeatureShowViewModel(
                features : $this->v2FeatureRepository->getAllV2Feature()
            ),
            'v2OnCloudPackageShowViewModel' => new V2OnCloudPackageShowViewModel(
                packages : $this->v2PackageRepository->getAllV2OnCloudPackage()
            ),
            'v2OnPremisePackageShowViewModel' => new V2OnPremisePackageShowViewModel(
                packages : $this->v2PackageRepository->getAllV2OnPremisePackage()
            )
        ]);
    }

    public function editV2PricingPageAction(): View
    {
        return view('version2.admin.pricing.edit', [
            'v2PricingShowViewModel' => new V2PricingShowViewModel(
                pricingCollection : $this->v2PricingRepository->getAllV2Pricing()
            ),
            'v2FeatureShowViewModel' => new V2FeatureShowViewModel(
                features : $this->v2FeatureRepository->getAllV2Feature()
            ),
            'v2OnCloudPackageShowViewModel' => new V2OnCloudPackageShowViewModel(
                packages : $this->v2PackageRepository->getAllV2OnCloudPackage()
            ),
            'v2OnPremisePackageShowViewModel' => new V2OnPremisePackageShowViewModel(
                packages : $this->v2PackageRepository->getAllV2OnPremisePackage()
            )
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function updateV2Pricing(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'production_fee' => 'array|required',
            'setup_fee' => 'array|required',
            'setup_number' => 'array|required',
            'number_of_user' => 'array|required',
            'data_limit' => 'array|required',
            'payment_cycle' => 'array|required',
            'business_size' => 'array|required',
            'production_fee.*' => 'string|required',
            'setup_fee.*' => 'string|required',
            'setup_number.*' => 'string|required',
            'number_of_user.*' => 'string|required',
            'data_limit.*' => 'string|required',
            'payment_cycle.*' => 'string|required',
            'business_size.*' => 'string|required'
        ]);

        $input = $request->input();
        $pricings = [];
        for ($index = 0; $index < 3; $index++) {
            $pricings[$index] = [
                'business_size' => $input['business_size'][$index],
                'production_fee' => $input['production_fee'][$index],
                'setup_fee' => $input['setup_fee'][$index],
                'setup_number' => $input['setup_number'][$index],
                'number_of_user' => $input['number_of_user'][$index],
                'data_limit' => $input['data_limit'][$index],
                'payment_cycle' => $input['payment_cycle'][$index],
                'upgrade_fee' => $input['upgrade_fee'][$index]
            ];
        }

        $this->v2PricingRepository->bulkUpdateAllPricing($pricings);

        Alert::success('Thành công');

        return redirect('admin/v2/pricing');
    }

    /**
     * @throws ValidationException
     */
    public function updateV2Feature(Request $request)
    {
        $this->validate($request, [
            'feature' => 'array|required',
            'feature.*.feature_name' => 'string|required',
            'feature.*.feature_tooltip' => 'string|required',
            'feature.*.startup_available' => 'string|nullable',
            'feature.*.professional_available' => 'string|nullable',
            'feature.*.enterprise_available' => 'string|nullable',
        ]);

        $features = $request->get('feature');

        foreach ($features as $key => $feature) {
            $features[$key]['startup_available'] = isset($feature['startup_available']) ? 1 : 0;
            $features[$key]['professional_available'] = isset($feature['professional_available']) ? 1 : 0;
            $features[$key]['enterprise_available'] = isset($feature['enterprise_available']) ? 1 : 0;
        }

        $this->v2FeatureRepository->bulkUpdateAllFeature($features);

        Alert::success('Thành công');

        return redirect()->to('admin/v2/pricing');
    }

    /**
     * @throws ValidationException
     */
    public function updateV2OnCloudPackage(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'array|required',
            'description' => 'array|required',
            'amount_per_month' => 'array|required',
            'user' => 'array|required',
            'data' => 'array|required',
            'benefit_text' => 'array|required',
            'name.*' => 'string|required',
            'description.*' => 'string|required',
            'amount_per_month.*' => 'string|required',
            'user.*' => 'string|required',
            'data.*' => 'string|required',
            'benefit_text.*' => 'string|required',
        ]);

        $input = $request->input();

        $packages = [];

        foreach (V2Package::OnCloudKey() as $index) {
            $packages[$index] = [
                'name' => $input['name'][$index],
                'description' => $input['description'][$index],
                'amount_per_month' => $input['amount_per_month'][$index],
                'user' => $input['user'][$index],
                'data' => $input['data'][$index],
                'benefit_text' => $input['benefit_text'][$index],
            ];
        }

        $this->v2PackageRepository->bulkUpdateAllOnCloudPackage($packages);

        Alert::success('Thành công');

        return redirect()->to('admin/v2/pricing');
    }

    public function updateV2OnPremisePackage(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'name' => 'array|required',
            'description' => 'array|required',
            'amount_per_month' => 'array|required',
            'user' => 'array|required',
            'data' => 'array|required',
            'benefit_text' => 'array|required',
            'name.*' => 'string|required',
            'description.*' => 'string|required',
            'amount_per_month.*' => 'string|required',
            'user.*' => 'string|required',
            'data.*' => 'string|required',
            'benefit_text.*' => 'string|required',
        ]);

        $input = $request->input();

        $packages = [];

        foreach (V2Package::OnPremiseKey() as $index) {
            $packages[$index] = [
                'name' => $input['name'][$index],
                'description' => $input['description'][$index],
                'amount_per_month' => $input['amount_per_month'][$index],
                'user' => $input['user'][$index],
                'data' => $input['data'][$index],
                'benefit_text' => $input['benefit_text'][$index],
            ];
        }

        $this->v2PackageRepository->bulkUpdateAllOnPremisePackage($packages);

        Alert::success('Thành công');

        return redirect()->to('admin/v2/pricing');
    }
}
