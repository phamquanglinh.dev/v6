<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\SubPageRequest;
use App\Models\SubPage;
use App\Untils\UploadBase64;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Prologue\Alerts\Facades\Alert;

/**
 * Class SubPageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SubPageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use CloneOperation;
    use BulkCloneOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(SubPage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sub-page');
        CRUD::setEntityNameStrings('Trang con', 'DS Trang con');
        $this->crud->denyAccess(["show"]);
        if (!(hasAccess("manager"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter([
            'name' => 'title',
            'label' => 'Tiêu đề',
            'type' => 'text',
        ], false, function ($value) {
            $this->crud->query->where("page_title", "like", "%$value%");
        });
        CRUD::column('page_title')->label("Tiêu đề");
        CRUD::column('page_slug')->type("link")->label("Trang")->prefix("/page/");
        CRUD::column('view')->label("Lượt xem");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SubPageRequest::class);

        CRUD::field('page_title')->label("Tiêu đề")->tab("Trang");
        CRUD::field('page_sub_title')->label("Tiêu đề phụ")->type("summernote")->tab("Trang");
        CRUD::field('page_image_title')->label("Ảnh tiêu đề")->tab("Trang")->type("image")->upload(true)->disk("uploads");
        CRUD::field('page_youtube_title')->label("Link video youtube")->tab("Trang")->type("youtube-link");
        CRUD::field('page_description')->tab("SEO");
        CRUD::field('page_keywords')->tab("SEO");
        CRUD::addField([
            'tab' => 'Trang',
            'name' => 'page_title_type',
            'label' => 'Lựa chọn hiển thị',
            'type' => 'select2_from_array',
            'options' => [
                0 => 'Hiển thị hình ảnh',
                1 => 'Hiển thị video Youtube'
            ]
        ]);

        CRUD::addField([
            'tab' => 'Tab',
            'name' => 'page_tabs',
            'label' => 'Các tab',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name' => 'icon',
                    'type' => 'image',
                    'uploads' => true,
                    'disk' => 'uploads',
                ],
                [
                    'name' => 'icon_alt',
                    'type' => 'image',
                    'uploads' => true,
                    'disk' => 'uploads',
                ],
                [
                    'name' => 'title',
                    'label' => 'Tiêu đề',
                ],
                [
                    'name' => 'des',
                    'label' => 'Nội dung',
                    'type' => 'tinymce_2'
                ],
                [
                    'name' => 'thumbnail',
                    'label' => 'Ảnh nội dung',
                    'uploads' => true,
                    'type' => 'image',
                    'disk' => 'uploads',
                ]
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    protected function store()
    {

        $tabs = [];
        $inputCollection = changeObjectToArray($this->crud->getRequest()->request);
        $page = $inputCollection["*parameters"];

        $validate = Validator::make($page, [
            "page_title" => 'required',
            'page_sub_title' => 'required',
            'page_image_title' => 'required',
            'page_description' => 'required',
            'page_keywords' => 'required',
        ], ['*.required' => 'Thông tin không thể để trống']);
        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors());
        }
        $page["page_image_title"] = "/assets/uploads/media/" . UploadBase64::run($page["page_image_title"], "/media/", Str::slug($page["page_title"]));
        $page_tab = json_decode($page["page_tabs"]);
        if (count($page_tab) > 0) {
            foreach ($page_tab as $tab) {
                $tab->icon = "/assets/uploads/media/" . UploadBase64::run($tab->icon, "/media/", Str::slug($tab->title) . "-icon");
                $tab->icon_alt = "/assets/uploads/media/" . UploadBase64::run($tab->icon_alt, "/media/", Str::slug($tab->title) . "-alt-icon");
                $tab->thumbnail = "/assets/uploads/media/" . UploadBase64::run($tab->thumbnail, "/media/", Str::slug($tab->title) . "-thumbnail");
                $tabs[] = $tab;
            }
        }

        $page["page_tabs"] = $tabs;
        $page["page_slug"] = Str::slug($page["page_title"]);
        SubPage::query()->create($page);
        Alert::success("Thêm thành công");
        return redirect("/admin/sub-page");
    }

    protected function update()
    {
        $id = $this->crud->getCurrentEntryId();
        $inputCollection = changeObjectToArray($this->crud->getRequest()->request);
        $page = $inputCollection["*parameters"];
        $tabs = [];
        if ($page["page_image_title"]) {
            if (!str_contains($page["page_image_title"], "media")) {
                $page["page_image_title"] = "/assets/uploads/media/" . UploadBase64::run($page["page_image_title"], "/media/", Str::slug($page["page_title"]));
            }
        }
        $page_tab = json_decode($page["page_tabs"]);
        if (count($page_tab) > 0) {
            foreach ($page_tab as $tab) {
                if (!str_contains($tab->icon, "uploads")) {
                    $tab->icon = "/assets/uploads/media/" . UploadBase64::run($tab->icon, "/media/", Str::slug($tab->title) . "-icon");
                }
                if (!str_contains($tab->icon_alt, "uploads")) {
                    $tab->icon_alt = "/assets/uploads/media/" . UploadBase64::run($tab->icon_alt, "/media/", Str::slug($tab->title) . "-alt-icon");
                }
                if (!str_contains($tab->thumbnail, "uploads")) {
                    $tab->thumbnail = "/assets/uploads/media/" . UploadBase64::run($tab->thumbnail, "/media/", Str::slug($tab->title) . "-thumbnail");
                }
                $tabs[] = $tab;
            }
        }

        $page["page_tabs"] = $tabs;
        $page["page_slug"] = Str::slug($page["page_title"]);
        unset($page["_token"]);
        unset($page["_method"]);
        unset($page['_http_referrer']);
        unset($page['_current_tab']);
        unset($page['save_action']);
        SubPage::query()->where("id", $id)->update($page);
        Alert::success("Update");
        return redirect("/admin/sub-page");
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
