<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MegaMenuRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MegaMenuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MegaMenuCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\MegaMenu::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/mega-menu');
        CRUD::setEntityNameStrings('MenuTab', 'MenuTab');
        $this->crud->denyAccess(["show"]);
        if (!hasAccess("admin")) {
            $this->crud->denyAccess(["list", "create", "update", "show"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->query->orderBy("order");
        CRUD::column('name')->label("Tên");
        CRUD::addColumn([
            'name' => 'parent_id',
            'type' => 'select',
            "model" => "App\Models\Menu",
            "entity" => "parentMenu",
            "label" => "Tab của menu"
        ]);
        CRUD::addColumn([
            'name' => 'icon',
            'type' => 'image',
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MegaMenuRequest::class);

        CRUD::addField([
            'name' => 'parent_id',
            'type' => 'select2',
            "model" => "App\Models\Menu",
            "entity" => "parentMenu",
            "label" => "Tab của menu"
        ]);
        CRUD::field('name')->label("Tên");
        CRUD::addField([
            'name' => 'icon',
            'type' => 'image',
            "crop" => true,
            "aspect_ratio" => 1,
            "upload" => true,
            "disk" => "uploads"
        ]);
        CRUD::addField([
            'name' => 'order',
            'label' => 'Vị trí'
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - );
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
