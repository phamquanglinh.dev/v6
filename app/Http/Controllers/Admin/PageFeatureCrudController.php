<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\PageFeatureRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PageFeatureCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageFeatureCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PageFeature::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/page-feature');
        CRUD::setEntityNameStrings('Trang tính năng', 'Các trang tính năng');
        $this->crud->denyAccess(["show"]);
        $this->crud->addButtonFromModelFunction("line", "previewOnWeb", "previewOnWeb", "line");
        if (!(hasAccess("manager"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    public function folder(): array
    {
        return [
            'ban-hang' => "Bán hàng",
            'marketing' => 'Marketing',
        ];
    }

    protected function setupListOperation()
    {
        $this->crud->addFilter([
            'name' => 'title',
            'label' => 'Tiêu đề',
            'type' => 'text',
        ], false, function ($value) {
            $this->crud->query->where("title", "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'folder',
            'label' => 'Thư mục',
            'type' => 'select2',
        ], function () {
            return [
                'ban-hang' => 'Bán hàng',
                'marketing' => 'Marketing'
            ];
        }, function ($value) {
            $this->crud->query->where("folder", "=", "$value");
        });
        CRUD::column('folder')->label("Thư mục")->type("select_from_array")->options($this->folder());
        CRUD::column('title')->label("Tiêu đề");
        CRUD::column('description')->label("GT Ngắn");
        CRUD::column('view')->label("Lượt xem");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PageFeatureRequest::class);
        CRUD::field('folder')->label("Thư mục")->type("select2_from_array")->options($this->folder());
        CRUD::field('slug')->label("Chuỗi slug");
        CRUD::field('title')->label("Tiêu đề trang");
        CRUD::field('description')->label("Mô tả ngắn")->type("textarea");
        CRUD::addField([
            'name' => 'slides',
            'type' => 'upload_multiple',
            'disk' => 'uploads',
            'upload' => true,
        ]);
        CRUD::field('body')->type("adonis-editor")->label("Nội dung trang");


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - );
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
