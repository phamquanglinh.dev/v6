<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\V2PartnerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class V2PartnerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class V2PartnerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\V2Partner::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/v2-partner');
        CRUD::setEntityNameStrings('Đối tác', 'Đối tác');
        $this->crud->enableReorder();
        $this->crud->set('reorder.label', 'partner_reorder_label');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->query->orderBy('lft', 'DESC');
        CRUD::column('partner_name')->label('Tên đối tác');
        CRUD::column('partner_logo')->label('Logo đối tác')->type('image');
        CRUD::column('place_in_top_home')->label('Đặt ở đầu trang chủ')->type('check');
        CRUD::column('place_in_bottom_home')->label('Đặt ở cuối trang chủ')->type('check');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(V2PartnerRequest::class);

        CRUD::field('partner_name')->label('Tên đối tác');
        CRUD::addField([
            'name' => 'partner_logo',
            'label' => 'Logo đối tác',
            'type' => 'image',
            'crop' => true,
            'upload' => true,
        ]);
        CRUD::field('place_in_top_home')->label('Đặt ở đầu trang chủ')->type('switch');
        CRUD::field('place_in_bottom_home')->label('Đặt ở cuối trang chủ')->type('switch');
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
