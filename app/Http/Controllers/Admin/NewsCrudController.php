<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Controllers\Admin\Operations\ReviseOperation;
use App\Http\Requests\NewsRequest;
use App\Models\Category;
use Backpack\CRUD\app\Exceptions\BackpackProRequiredException;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use function Symfony\Component\Translation\t;

/**
 * Class NewsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class NewsCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;

//    use ReviseOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws BackpackProRequiredException
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\News::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/news');
        CRUD::setEntityNameStrings('Tin tức', 'DS Tin tức');
        $this->crud->denyAccess(["show"]);
        if (!(hasAccess("manager") || hasAccess("hr"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
        CRUD::enableExportButtons();
    }

    public function filters(): void
    {
//        if (backpack_user()->group_user != 0) {
//            $this->crud->query->where("creator_id", backpack_user()->id);
//        }
        $this->crud->addFilter([
            "name" => 'title',
            'type' => 'text',
            "label" => 'Tiêu đề'
        ], false, function ($value) {
            $this->crud->query->where("title", "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'category',
            'type' => 'select2',
            'label' => 'Danh mục'
        ], function () {
            return Category::query()->get()->pluck("title", "id")->toArray();
        }, function ($value) {
            $this->crud->query->where("cate_id", $value);
        });
        $this->crud->addFilter([
            "name" => 'pin',
            'type' => 'simple',
            "label" => 'Bài viết đã ghim'
        ], false, function ($value) {
            $this->crud->query->withoutGlobalScopes()->where("order", $value ? 1 : 0);
        });
        $this->crud->addFilter([
            "name" => 'invalid',
            'type' => 'simple',
            "label" => 'Bài viết đã hiệu hóa'
        ], false, function ($value) {
            $this->crud->query->withoutGlobalScopes()->where("invalid", $value ? 1 : 0);
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->filters();
        CRUD::addColumn([
            'name' => 'cate_id',
            'type' => 'select',
            'model' => 'App\Models\Category',
            'entity' => "category",
            'attribute' => 'title',
            'label' => 'Danh mục'
        ]);
        CRUD::column('title')->label("Tiêu đề");
        CRUD::addColumn([
            'name' => 'creator_id',
            'type' => 'select',
            'model' => 'App\Models\User',
            'entity' => "creator",
            'attribute' => 'full_name',
            'label' => 'Người đăng'
        ]);
        CRUD::column('image')->type("image")->label("Ảnh bìa");
        CRUD::column('total_view')->label("Lượt xem thực tế")->type("number");
        CRUD::column('rate_total')->label("Lượt xem ảo")->type("number");
        CRUD::column('previews')->type("model_function")->function_name("preview")->label("Xem trước");
        CRUD::column('order')->type("check")->label("Ghim");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {

        CRUD::setValidation(NewsRequest::class);
        CRUD::addField([
            'name' => 'creator_id',
            'type' => 'hidden',
            'default' => backpack_user()->id,
        ]);
        CRUD::addField([
            'name' => 'cate_id',
            'type' => 'select2',
            'model' => 'App\Models\Category',
            'entity' => "category",
            'attribute' => 'title',
            'label' => 'Danh mục'
        ]);
        CRUD::field('title')->label("Tiêu đề");
        CRUD::field('slug')->type("text")->label("URL Tùy chỉnh");
        CRUD::field('quote');
        CRUD::field('description')->label("Mô tả ngắn");
        CRUD::field('content')->type("adonis-editor")->label("Nội dung bài vết");

        CRUD::field('rate_total')->label("Lượt xem ảo")->type('number');
//        CRUD::field('creator_id')->type("hidden")->default(backpack_user()->id);
        CRUD::addField([
            'name' => 'image',
            'type' => 'image',
            'crop' => true,
            'upload' => true,
            'disk' => 'uploads',
        ]);
        CRUD::field('invalid')->label("Vô hiệu hóa")->type("switch")->default(0);
        CRUD::field('order')->type("switch")->label("Ghim");
        CRUD::field('description_seo')->label("Tiêu đề SEO");
        CRUD::field('keyword_seo')->label("Keyword SEO");

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
