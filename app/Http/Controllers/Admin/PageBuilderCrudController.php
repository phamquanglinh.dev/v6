<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PageBuilderRequest;
use App\Models\RegisterForm;
use App\Repository\LandingPageRepository;
use App\Untils\UploadBase64;
use App\ViewModels\Admin\PageBuilderViewModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Illuminate\View\View;

/**
 * Class PageBuilderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PageBuilderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PageBuilder::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/page-builder');
        CRUD::setEntityNameStrings('page builder', 'page builders');
        $this->crud->denyAccess(["create", "delete", "show", "list", "delete"]);
        $this->crud->addButtonFromModelFunction("line", "previewOnWeb", "previewOnWeb", "line");
        if (!(hasAccess("manager"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('title')->label("Trang");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PageBuilderRequest::class);
        CRUD::field('body')->type("");


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function edit($id, LandingPageRepository $landingPageRepository): View
    {
        $page = $landingPageRepository->getBuilder()->where("id", $id)->firstOrFail();
        $forms = RegisterForm::query()->where("position", "!=", "form-popup")->get();
        return view("vendor.backpack.crud.page-builder",
            [
                "pageBuilderViewModel" => new PageBuilderViewModel(
                    landingPage: $page,
                    forms: $forms
                ),
            ]
        );
    }

    protected function pageBuilderSaveAction(Request $request, LandingPageRepository $landingPageRepository): JsonResponse
    {
        $html = $request["htmlData"];
        $script = $request["scriptData"];
        $css = $request["cssData"];
        $id = $request["id"];
        try {
            $landingPageRepository->saveBody(id: $id, html: $html, script: $script, style: $css);
            return response()->json(["redirectLink" => $landingPageRepository->getPreviewUrl($id)]);
        } catch (\Exception $exception) {
            return response()->json(["message" => $exception->getMessage()], 500);
        }

    }

    protected function imageUpload(Request $request): JsonResponse
    {
        /**
         * @var UploadedFile $file
         */
        $file = $request->file("file");
        $name = Str::random(5) . Str::slug($file->getClientOriginalName());

        $file->move(public_path("/assets/uploads/landingPage/"), $name);
        return response()->json(["image" => url("/assets/uploads/landingPage/" . $name)]);
    }
}
