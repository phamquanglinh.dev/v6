<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Repository\CustomerRepository;
use App\ViewModels\Admin\Object\CustomerAdminObject;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Doctrine\DBAL\Schema\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class CustomerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;
    use ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Customer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/customer');
        CRUD::setEntityNameStrings('Khách hàng tiêu biểu', 'DS Khách hàng tiêu biểu');
        $this->crud->denyAccess(["show"]);
        if (! hasAccess("manager")) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
        $this->crud->set('reorder.label', 'id');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->query->where("type", 2);
        CRUD::column('id')->label("ID")->prefix("#");
        CRUD::column('name')->label("Tên khách hàng(thương hiệu)");
        CRUD::addColumn([
            'name' => 'avatar',
            'type' => 'image',
            'prefix' => "assets/uploads/media/"
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(CustomerRequest::class);

        CRUD::field('name')->label("Tên khách hàng(thương hiệu)");
        CRUD::field('link')->label("Link");
        CRUD::addField([
            'name' => 'avatar',
            "label" => "Ảnh",
            'type' => 'image',
            'crop' => true,
            'upload' => true,
            'aspect_ratio' => 1.6,
            'disk' => 'uploads',
            'prefix' => 'assets/uploads/media/'
        ]);
        CRUD::field("type")->type("hidden")->default(2);
        CRUD::field("priority")->type("switch")->default(0)->label("Ưu tiên");
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * @param CustomerRepository $customerRepository
     * @return \Illuminate\View\View
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 30/11/2023 1:38 pm
     */
    public function reorderOperation(CustomerRepository $customerRepository): \Illuminate\View\View
    {
        $customers = $customerRepository->getCustomersForSite()->map(fn(Customer $customer) => new CustomerAdminObject(
            logo : $customer['avatar'], name : $customer['name'], id : $customer['id']
        ))->toArray();

        return view('vendor.backpack.crud.customerReorder', [
            'customers' => $customers
        ]);
    }

    /**
     * @param Request $request
     * @return bool
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 30/11/2023 1:44 pm
     */
    protected function orderOperation(Request $request): bool
    {
        $photos = $request["banners"];
        foreach ($photos as $key => $item) {
            Customer::query()->where("id", $item)->update([
                'priority_order' => $key
            ]);
        }
        return true;
    }
}
