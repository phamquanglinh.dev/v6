<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\LandingPageRequest;
use App\Models\LandingPage;
use App\Repository\LandingPageRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Prologue\Alerts\Facades\Alert;

/**
 * Class LandingPageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LandingPageCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\LandingPage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/landing-page');
        CRUD::setEntityNameStrings('landing page', 'landing pages');
        $this->crud->denyAccess(["show"]);
//        $this->crud->addButtonFromModelFunction("line", "previewOnWeb", "previewOnWeb", "line");
        if (!hasAccess("manager")) {
            $this->crud->denyAccess(["list", "create", "update", "show"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter([
            'name' => 'title',
            'label' => 'Tiêu đề',
            'type' => 'text',
        ], false, function ($value) {
            $this->crud->query->where("title", "like", "%$value%");
        });
        CRUD::column('title')->label("Tiêu đề");
        CRUD::column('thumbnail')->label("Ảnh thumbnail")->type('image')->prefix("assets/uploads/news/");
        CRUD::column('slug')->label("Trang")->prefix(url("") . "/page/")->type("link");
        CRUD::column('view')->label("Lượt xem");
        CRUD::column('enable_popup')->label("Bật popup")->type('check');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LandingPageRequest::class);
        CRUD::field('title')->label("Tiêu đề");
        CRUD::field('thumbnail')->label("Ảnh thumbnail")->type('image');
        CRUD::field('slug')->type("text")->default("URL Tùy chỉnh")->prefix("https://getfly.vn/page/");
        CRUD::field('description')->type("textarea")->label("Mô tả ngắn");
        CRUD::addField([
            'name' => 'body',
            'type' => 'textarea',
            'label' => 'nội dung'
        ]);
        CRUD::addField([
            'name' => 'custom_form',
            'label' => 'Opt In Form',
        ]);
        CRUD::addField([
            'name' => 'enable_popup',
            'label' =>'Bật Pop Up Trang Chủ',
            'type' =>'switch',
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function resetContent(LandingPageRepository $landingPageRepository, $id)
    {
        /**
         * @var LandingPage $landingPage
         */
        $landingPage = $landingPageRepository->getBuilder()->find($id);
        $landingPage->update(
            ['body' => '', 'style' => '', 'script' => null]
        );
        Alert::success("Reset thành công");
        return redirect()->back();
    }


}
