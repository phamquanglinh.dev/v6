<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;


class UploadController extends Controller
{

    const Folder = "news";

    public function tinymce(Request $request): array
    {
        $random = Str::random(5);
        /**
         * @var UploadedFile $upload
         */
        $upload = $request->file("file");
        $upload->move(public_path() . "/assets/uploads/" . self::Folder . "/", $random . $upload->getClientOriginalName());
        $url = "assets/uploads/" . self::Folder . "/" . $random . $upload->getClientOriginalName();
        return [
            "location" => url($url)
        ];
    }
}
