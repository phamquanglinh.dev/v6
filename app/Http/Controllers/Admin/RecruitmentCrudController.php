<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Operations\BulkCloneOperation;
use App\Http\Controllers\Admin\Operations\BulkDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkForceDeleteOperation;
use App\Http\Controllers\Admin\Operations\BulkRestoreOperation;
use App\Http\Controllers\Admin\Operations\CloneOperation;
use App\Http\Controllers\Admin\Operations\ForceDeleteOperationOperation;
use App\Http\Controllers\Admin\Operations\RecoverTrashOperation;
use App\Http\Requests\RecruitmentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RecruitmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RecruitmentCrudController extends CrudController
{
    use CloneOperation;
    use BulkCloneOperation;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use RecoverTrashOperation;
    use ForceDeleteOperationOperation;
    use BulkDeleteOperation;
    use BulkRestoreOperation;
    use BulkForceDeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Recruitment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/recruitment');
        CRUD::setEntityNameStrings('Tuyển dụng', 'Tuyển dụng');
        $this->crud->denyAccess(["show"]);
        $this->crud->addButtonFromModelFunction("line", "viewOnWeb", "viewOnWeb", "line");
        if (!(hasAccess("hr"))) {
            $this->crud->denyAccess(["list", "create", "update", "show", "delete"]);
        }
        CRUD::enableExportButtons();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'job_name',
            'label' => 'Tên công việc',
        ]);
        CRUD::addColumn([
            'name' => 'address',
            'label' => 'Địa chỉ',
        ]);
        CRUD::addColumn([
            'name' => 'room',
            'label' => 'Phòng ban',
            'type' => 'select',
        ]);
        CRUD::addColumn([
            'name' => 'quantity',
            'label' => 'Số lượng',
            'type' => 'number',
            'suffix' => 'Người'
        ]);
        CRUD::addColumn([
            'name' => 'salary',
            'label' => 'Mức lương',
        ]);
        CRUD::addColumn([
            'name' => 'time',
            'label' => 'Hạn nộp hồ sơ',
            'type' => 'date'
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RecruitmentRequest::class);

        CRUD::addField([
            'name' => 'job_name',
            'label' => 'Tên công việc',
        ]);
        CRUD::addField([
            'name' => 'address',
            'label' => 'Địa chỉ',
        ]);
        CRUD::addField([
            'name' => 'room_id',
            'label' => 'Phòng ban',
            'type' => 'select2',
        ]);
        CRUD::addField([
            'name' => 'recruitment_detail',
            'label' => 'Nội dung chi tiết',
            'type' => 'adonis-editor',
        ]);
        CRUD::addField([
            'name' => 'body',
            'value' => 'không',
            'type' => 'hidden',
        ]);
        CRUD::addField([
            'name' => 'quantity',
            'label' => 'Số lượng',
            'type' => 'number',
            'suffix' => ' Người'
        ]);
        CRUD::addField([
            'name' => 'salary',
            'label' => 'Mức lương',
        ]);
        CRUD::addField([
            'name' => 'time',
            'label' => 'Hạn nộp hồ sơ',
            'type' => 'date'
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation(): void
    {
        $this->setupCreateOperation();
    }
}
