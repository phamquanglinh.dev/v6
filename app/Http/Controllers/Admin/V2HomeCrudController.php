<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\NewsRepository;
use App\Repository\V2PartnerRepository;
use App\ViewModels\Admin\V2CrudHomeViewModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class V2HomeCrudController extends Controller
{
    public function __construct(
        private readonly NewsRepository $newsRepository,
        private readonly V2PartnerRepository $v2PartnerRepository
    ) {}

    public function index()
    {
        $newspapers = $this->newsRepository->getAllGeneric(
            ["news_id", "title"]
        );

        $topPartners = $this->v2PartnerRepository->getAllV2Partners(
            conditions : [
                'place_in_top_home' => 1
            ]
        );

        $bottomPartner = $this->v2PartnerRepository->getAllV2Partners(
            conditions : [
                'place_in_bottom_home' => 1
            ]
        );

        $delegateNewsIds = $this->newsRepository->getDelegateNewIds();

        return view('version2.admin.home.edit', [
            'v2CrudHomeViewModel' => new V2CrudHomeViewModel(
                newspapers : $newspapers,
                topPartners : $topPartners,
                bottomPartners : $bottomPartner,
                delegateNewsIds : $delegateNewsIds,
            )
        ]);
    }

    public function updateDelegateNew(Request $request): RedirectResponse
    {
        $delegateNews = $request->get('delegate_type');

        $this->newsRepository->updateDelegateNews($delegateNews);

        return redirect()->back();
    }
}
