<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FeatureRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FeatureCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FeatureCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Feature::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/feature');
        CRUD::setEntityNameStrings('Tính năng', 'DS Tính năng');
        $this->crud->denyAccess(["show"]);
        if (!hasAccess("manager")) {
            $this->crud->denyAccess(["list", "create", "update", "show","delete"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('header')->label("Tab");
        CRUD::column('title')->label("Tiêu đề");
        CRUD::column('icon')->label("Icon Tab")->type("image");
        CRUD::column('thumbnail')->label("Ảnh")->type("image");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(FeatureRequest::class);

        CRUD::field('header')->label("Tab");
        CRUD::field('title')->label("Tiêu đề");
        CRUD::addField([
            'name' => 'icon',
            'type' => 'image',
            'crop' => true,
            'aspect_ratio' => 1,
            'upload' => true,
            'disk' => 'uploads',
        ]);
        CRUD::addField([
            'name' => 'thumbnail',
            'type' => 'image',
            'crop' => true,
            'upload' => true,
            'disk' => 'uploads',
        ]);
        CRUD::field('description')->type("textarea")->label("Mô tả ngắn");
        CRUD::field('body')->label("Nội dung")->type("adonis-editor");
        CRUD::field('url')->label("URL Xem chi tiết");

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
