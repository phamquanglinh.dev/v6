<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MenuRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\SubPage;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Class MenuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MenuCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }


    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Menu::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/menu');
        CRUD::setEntityNameStrings('menu', 'DS MENU');
        $this->crud->set("reorder.label", "title");
        $this->crud->denyAccess(["show"]);
        if (!hasAccess("admin")) {
            $this->crud->denyAccess(["list", "create", "update", "show"]);
        }
    }

    public function filter(): void
    {
        $this->crud->addFilter([
            'name' => 'Title',
            'label' => 'Tiêu đề',
            'type' => 'text'
        ], false, function (string $value) {
            $this->crud->query->where("title", "like", "%$value%");
        });
        $this->crud->addFilter([
            'name' => 'parent_id',
            'label' => 'Danh mục cha',
            'type' => 'text'
        ], false, function (string $value) {
            $this->crud->query->whereHas("parent", function (Builder $builder) use ($value) {
                $builder->where("title", "like", "%$value%");
            });
        });
    }

    public function tabs(): array
    {
        return [
            "marketing-fly" => "Marketing Fly",
            "sales-fly" => "Sales Fly",
            "module" => "Module mở rộng",
            "services" => "Dịch vụ triển khai"
        ];
    }

    public function types(): array
    {
        return [
            '0' => 'Menu chính',
            '1' => 'Footer Menu'
        ];
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->filter();
        CRUD::column('title')->label("Tiêu đề")->escaped(false);

        CRUD::column('mgroup')->label("Nhóm menu")->type("select_from_array")->options([
            0 => 'Nhập link',
            1 => 'Danh mục bài viết',
            2 => 'Bài viết chi tiết',
            3 => 'Trang tính năng'
        ]);
        CRUD::column('parent_id')->label("Menu cha")->escaped(false);
        CRUD::column('mega')->label("Mega menu")->type("check");
        CRUD::column('link')->label("URL")->type("link");

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        /**
         * @var $menu
         */
        $menu = $this->crud->getCurrentEntry();
        $type = $menu['mgroup'] ?? 0;
        $categoryId = null;
        $newsId = null;
        $pageId = null;
        if ($menu) {
            if ($menu["mgroup"] == 1) {
                $categoryId = str_replace("/", "", explode("-", $menu["link"])[0]);
                $categoryId = str_replace("i", "", $categoryId);
                if (is_numeric($categoryId)) {
                    $categoryId = (int)$categoryId;
                }
            }
            if ($menu['mgroup'] == 2) {
                $newsId = last(explode("-", $menu["link"]));
                $newsId = str_replace(".html", "", $newsId);
                $newsId = str_replace("n", "", $newsId);
                if (is_numeric($newsId)) {
                    $newsId = (int)$newsId;
                }
            }
            if ($menu['mgroup'] == 3) {
                $pageSlug = last(explode("/", $menu["link"]));
                $pageId = SubPage::query()->where("page_slug", $pageSlug)->first()->id ?? null;
            }
        }
        CRUD::setValidation(MenuRequest::class);
        CRUD::field('title')->label("Tiêu đề")->type("tinymce");
        CRUD::field('slug')->type("hidden")->default("");
        CRUD::field('type')->label("Loại")->type("select_from_array")->options($this->types())->default(0);
        CRUD::field('mgroup')->label("Nhóm menu")->type("select_from_array")->options([
            0 => 'Nhập link',
            1 => 'Danh mục bài viết',
            2 => 'Bài viết chi tiết',
            3 => 'Trang tính năng'
        ])->default(0)->attributes(['id' => 'menu_type']);
        CRUD::field('link')->default("#")->wrapper([
            'class' => $type != 0 ? "d-none" : "col-md-12 mb-3",
            "id" => "raw_link"
        ]);
        CRUD::addField([
            'name' => 'alt_category_id',
            "label" => "Danh mục",
            'type' => 'select2_from_array',
            "options" => Category::query()->get()->pluck("title", "id")->toArray(),
            "value" => $categoryId ?? null,
            "wrapper" => [
                'class' => $type != 1 ? "d-none" : "col-md-12 mb-3",
                "id" => "category_select",
            ]
        ]);
        CRUD::addField([
            'name' => 'alt_news_id',
            "label" => "Bài viết",
            'type' => 'select2_from_array',
            "value" => $newsId ?? null,
            "options" => News::query()->get()->pluck("title", "news_id")->toArray(),
            "wrapper" => [
                'class' => $type != 2 ? "d-none" : "col-md-12 mb-3",
                "id" => "news_select"
            ]
        ]);
        CRUD::addField([
            'name' => 'alt_pages_id',
            "label" => "Trang",
            'type' => 'select2_from_array',
            "value" => $pageId ?? null,
            "options" => SubPage::query()->get()->pluck("page_title", "id")->toArray(),
            "wrapper" => [
                'class' => $type != 3 ? "d-none" : "col-md-12 mb-3",
                "id" => "pages_select"
            ]
        ]);
        CRUD::addField([
            'name' => 'new_tab',
            'type' => 'switch',
            'label' => 'Mở trang trong tab mới'
        ]);
        CRUD::field('parent_id')->type("select2_nested")->default(1)->label("Menu Cha");
        CRUD::field('mega')->type("switch")->label("Có menu con dạng mega");
//        CRUD::addField([
//            'name' => 'icon',
//            'type' => 'image',
//            'crop' => true,
//            'aspect_ratio' => 1,
//            'upload' => true,
//            'disk' => 'uploads',
//        ]);
        CRUD::addField([
            "name" => 'MegaTabs',
            "type" => "select2_multiple",
            "model" => "App\Models\MegaMenu",
            "entity" => "MegaTabs",
            "pivot" => true,
            "attribute" => "name",
        ]);
        Widget::add(
            [
                'type' => 'script',
                'content' => asset("admin-assets/js/custom-menu.js"),
                // optional
                // 'stack'    => 'before_scripts', // default is after_scripts
            ]
        );
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    protected function store(Request $request): RedirectResponse
    {
        if ($request["mgroup"] == 1) {
            /**
             * @var Category $category
             */
            $category = Category::query()->find($request["alt_category_id"]);
            $url = "/i" . $category['id'] . "-" . $category['slug'] . ".html";
            $request["link"] = $url;
        }
        if ($request["mgroup"] == 2) {
            /**
             * @var News $news
             */
            $news = News::query()->find($request["alt_news_id"]);
            $category = $news["category"]["slug"];
            $slug = $news['slug'];
            $url = "/" . $category . "/" . $slug . "-n" . $news["news_id"] . ".html";
            $request["link"] = $url;
        }
        if ($request["mgroup"] == 3) {
            /**
             * @var SubPage $page
             */

            $page = SubPage::query()->find($request["alt_pages_id"]);

            $pageSlug = $page["page_slug"];
            $url = "/page/" . $pageSlug;
            $request["link"] = $url;
        }
        return $this->traitStore();
    }

    protected function update(Request $request): array|RedirectResponse
    {
        if ($request["mgroup"] == 1) {
            /**
             * @var Category $category
             */
            $category = Category::query()->find($request["alt_category_id"]);
            $url = "/i" . $category['id'] . "-" . $category['slug'] . ".html";
            $request["link"] = $url;
        }
        if ($request["mgroup"] == 2) {
            /**
             * @var News $news
             */
            $news = News::query()->find($request["alt_news_id"]);
            $category = $news["category"]["slug"];
            $slug = $news['slug'];
            $url = "/" . $category . "/" . $slug . "-n" . $news["news_id"] . ".html";
            $request["link"] = $url;
        }
        if ($request["mgroup"] == 3) {
            /**
             * @var SubPage $page
             */
            $page = SubPage::query()->find($request["alt_pages_id"]);

            $pageSlug = $page["page_slug"];
            $url = "/page/" . $pageSlug;
            $request["link"] = $url;
        }
        return $this->traitUpdate();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
