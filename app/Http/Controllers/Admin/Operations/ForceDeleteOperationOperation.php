<?php

namespace App\Http\Controllers\Admin\Operations;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;
use Prologue\Alerts\Facades\Alert;

trait ForceDeleteOperationOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupForceDeleteOperationRoutes($segment, $routeName, $controller)
    {
        Route::delete($segment . '/{id}/force-delete', [
            'as' => $routeName . '.forceDeleteOperation',
            'uses' => $controller . '@forceDeleteOperation',
            'operation' => 'forceDeleteOperation',
        ]);
    }

    /**
     * Add the default settings, buttons, etc. that this operation needs.
     */
    protected function setupForceDeleteOperationDefaults()
    {
        CRUD::allowAccess((array)'forceDeleteOperation');

        CRUD::operation('forceDeleteOperation', function () {
            CRUD::loadDefaultOperationSettingsFromConfig();
        });

        CRUD::operation('list', function () {
            if(isset($_REQUEST["trash"])){
                CRUD::addButton('line', 'force_delete_operation', 'view', 'crud::buttons.force_delete');
            }
//            CRUD::addButton('line', 'force_delete_operation', 'view', 'crud::buttons.force_delete');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return int
     */
    public function forceDeleteOperation()
    {
        CRUD::hasAccessOrFail('forceDeleteOperation');
        $id = $this->crud->getCurrentEntryId();
        $this->crud->getModel()->newQuery()->whereKey($id)->forceDelete();
        return 1;
    }
}