<?php

namespace App\Http\Controllers\Admin\Operations;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

trait BulkForceDeleteOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupBulkForceDeleteRoutes($segment, $routeName, $controller)
    {
        Route::post($segment . '/bulk-force-delete', [
            'as' => $routeName . '.bulkForceDelete',
            'uses' => $controller . '@bulkForceDelete',
            'operation' => 'bulkForceDelete',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupBulkForceDeleteDefaults()
    {
        CRUD::allowAccess('bulkForceDelete');

        CRUD::operation('bulkForceDelete', function () {
            CRUD::loadDefaultOperationSettingsFromConfig();
        });

        CRUD::operation('list', function () {
            // CRUD::addButton('top', 'bulk_force_delete', 'view', 'crud::buttons.bulk_force_delete');
            CRUD::addButton('bottom', 'bulk_force_delete', 'view', 'crud::buttons.bulk_force_delete');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     *
     */
    public function bulkForceDelete()
    {
        CRUD::hasAccessOrFail('bulkForceDelete');
        $entries = request()->input('entries');
        foreach ($entries as $id) {
            $this->crud->getModel()->newQuery()->whereKey($id)->forceDelete();
        }
        return $entries;
    }
}