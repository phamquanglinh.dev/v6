<?php

namespace App\Http\Controllers\Admin\Operations;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Route;
use Prologue\Alerts\Facades\Alert;

trait RecoverTrashOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRecoverTrashRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/recover-trash', [
            'as' => $routeName . '.recoverTrash',
            'uses' => $controller . '@recoverTrash',
            'operation' => 'recoverTrash',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRecoverTrashDefaults()
    {
        CRUD::allowAccess((array)'recoverTrash');

        CRUD::operation('recoverTrash', function () {
            CRUD::loadDefaultOperationSettingsFromConfig();
        });

        CRUD::operation('list', function () {
            $this->crud->addFilter([
                'type' => 'simple',
                'label' => 'Thùng rác',
                'name' => 'trash',
            ], false, function () {
                $this->crud->query->onlyTrashed();
            });
            if (!isset($_REQUEST['trash'])){
                $this->crud->denyAccess(["recoverTrash"]);
            }else{
                $this->crud->denyAccess(["delete"]);
            }
            // CRUD::addButton('top', 'recover_trash', 'view', 'crud::buttons.recover_trash');
            Alert::success("Khôi phục bản ghi thành công")->flash();
            CRUD::addButton('line', 'recover_trash', 'view', 'crud::buttons.recover_trash');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     */
    public function recoverTrash(): RedirectResponse
    {
        CRUD::hasAccessOrFail('recoverTrash');
        $this->data['crud'] = $this->crud;
        $this->data['title'] = CRUD::getTitle() ?? 'Recover Trash ' . $this->crud->entity_name;
        $id = $this->crud->getCurrentEntryId();
        $this->crud->getModel()->newQuery()->whereKey($id)->restore();
        Alert::success("Khôi phục thành công");
        return redirect()->back();
        // load the view
//        return view('crud::operations.recover_trash', $this->data);
    }
}