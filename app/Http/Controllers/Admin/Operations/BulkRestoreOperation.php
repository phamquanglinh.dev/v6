<?php

namespace App\Http\Controllers\Admin\Operations;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 *  * @property-read CrudPanel $crud
 */
trait BulkRestoreOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupBulkRestoreRoutes($segment, $routeName, $controller)
    {
        Route::post($segment . '/bulk-restore', [
            'as' => $routeName . '.bulkRestore',
            'uses' => $controller . '@bulkRestore',
            'operation' => 'bulkRestore',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupBulkRestoreDefaults()
    {
        CRUD::allowAccess((array)'bulkRestore');

        CRUD::operation('bulkRestore', function () {
            CRUD::loadDefaultOperationSettingsFromConfig();
        });

        CRUD::operation('list', function () {
            $this->crud->enableBulkActions();
            CRUD::addButton('bottom', 'bulk_restore', 'view', 'crud::buttons.bulk_restore');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     *
     */
    public function bulkRestore(): array
    {
        $this->crud->hasAccessOrFail('bulkRestore');
        $entries = request()->input('entries');
        foreach ($entries as $id) {
            $this->crud->getModel()->newQuery()->whereKey($id)->restore();
        }
        return $entries;
    }
}