<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeamInfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TeamInfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TeamInfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TeamInfo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/team-info');
        CRUD::setEntityNameStrings('Thông tin team Getfly', 'Thông tin team Getfly');
        $this->crud->denyAccess(["show"]);
        if (!(hasAccess("hr"))) {
            $this->crud->denyAccess(["list", "create", "update", "show","delete"]);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation(): void
    {
        CRUD::addColumn(['name' => 'thumbnail', 'type' => 'image', "label" => "Ảnh"]);
        CRUD::addColumn(['name' => 'title', 'type' => 'text', "label" => 'Tiêu đề']);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation(): void
    {
        CRUD::setValidation(TeamInfoRequest::class);
        CRUD::addField([
            'name' => 'thumbnail',
            'type' => 'image',
            "label" => "Ảnh",
            'crop' => true,
            'aspect_ratio' => 12 / 8,
            'upload' => true,
            'disk' => 'uploads'
        ]);
        CRUD::addField(['name' => 'title', 'type' => 'text', "label" => 'Tiêu đề']);
        CRUD::addField([
            'name' => 'news_category_id',
            'type' => 'select2_nested',
            'model' => "App\Models\Category",
            'entity' => "NewsCategoryId",
            'attribute' => "title",
            "label" => 'Danh mục tin tức'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
