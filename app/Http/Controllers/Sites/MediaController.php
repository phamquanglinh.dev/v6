<?php

namespace App\Http\Controllers\Sites;

use App\Models\Media;
use Illuminate\View\View;
use App\Repository\MediaRepository;
use App\Http\Controllers\BaseController;
use App\ViewModels\Sites\Media\MediaObject;
use App\ViewModels\Sites\Media\MediaListViewModel;

class MediaController extends BaseController
{
    /**
     * @param MediaRepository $mediaRepository
     */
    public function __construct(
        readonly private MediaRepository $mediaRepository
    ) {}

    /**
     * @return View
     */
    public function listMediasAction(): View
    {
        $mediasCollection = $this->mediaRepository->getMediaAll();

        $mediaListViewModel = new MediaListViewModel(
            medias: $mediasCollection->map(
                fn(Media $media) => new MediaObject(link: $media['link'], avatar: $media['avatar'], type: $media['type'])
            )->toArray()
        );

        dd("Media", $mediaListViewModel);
    }
}
