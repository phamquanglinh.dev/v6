<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use App\Repository\PhotoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function __construct(
        private readonly PhotoRepository $photoRepository
    )
    {
    }

    public function getAjaxPhotoById(Request $request): JsonResponse
    {
        if ($request["id"]) {
            $photo = $this->photoRepository->getPhotoById($request["id"]);
            if ($photo) {
                return response()->json(["url" => url($photo["image"])]);
            }
        }
        return response()->json([], 404);
    }
}
