<?php

namespace App\Http\Controllers\Sites;

use App\Models\News;
use App\Repository\CategoryRepository;
use App\ViewModels\Sites\News\NewsSearchViewModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Repository\NewsRepository;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\BaseController;
use App\ViewModels\Sites\News\NewsListViewModel;
use App\ViewModels\Sites\News\NewsDetailViewModel;
use App\ViewModels\Sites\News\Object\NewsListObject;

class NewsController extends BaseController
{
    public function __construct(
        readonly private NewsRepository     $newsRepository,
        readonly private CategoryRepository $categoryRepository,
    )
    {
    }

    public function searchNewsAction(Request $request): View|RedirectResponse
    {
        $params = $request->params ?? abort("404");
        $newsCollection = $this->newsRepository->searchByParams($params, $request->page ?? 1);

        return \view("sites.newspaper.newspapers_search", [
            "newsSearchViewModel" => new NewsSearchViewModel(params: $params, news: $newsCollection)
        ]);
    }

    public function searchMoreNewsAction(Request $request): string
    {
        $data = "";
        $params = $request->params ?? abort("404");
        $newsCollection = $this->newsRepository->searchByParams($params, $request->page ?? 1);
        $newspapersObjectList = $newsCollection
            ->filter(function (News $news) {
                return $news["category"] != null;
            })->map(function (News $news) {
                return new NewsListObject(
                    news_id: $news['news_id'],
                    cate_title: $news['category']['title'],
                    cate_id: $news["category"]["id"],
                    cate_slug: $news["category"]["slug"],
                    title: $news['title'],
                    slug: $news["slug"],
                    image: $news['image'],
                    description: $news["description"],
                    created_at: Carbon::parse($news['created_at'])->isoFormat("DD-MM-YYYY")
                );
            })->toArray();
        foreach ($newspapersObjectList as $news) {
            $data .= \view("sites.newspaper.newspaper_item", ["news" => $news]);
        }
        return $data;
    }

    public function loadMoreNewspaper(Request $request): string
    {
        if ($request->ajax()) {
            $data = "";
            $newspapersCollection = $this->newsRepository->getNewsByCategoryId(categoryId: $request->categoryId ?? 1, page: $request->page ?? 1);
            $newspapersObjectList = $newspapersCollection
                ->filter(function (News $news) {
                    return $news["category"] != null;
                })->map(function (News $news) {

                    return new NewsListObject(
                        news_id: $news['news_id'],
                        cate_title: $news['category']['title'],
                        cate_id: $news["category"]["id"],
                        cate_slug: $news["category"]["slug"],
                        title: $news['title'],
                        slug: $news["slug"],
                        image: $news['image'],
                        description: $news["description"],
                        created_at: Carbon::parse($news['created_at'])->isoFormat("DD-MM-YYYY")
                    );
                })->toArray();
            foreach ($newspapersObjectList as $news) {
                $data .= \view("sites.newspaper.newspaper_item", ["news" => $news]);
            }
            return $data;
        }
        return "";
    }

    /**
     * @param int $id
     * @param $slug
     * @return View
     */
    public function listNewspapersAction($id, $slug): View
    {
        if (!is_numeric($id)) {
            abort(404);
        }
        $categoryRecord = $this->categoryRepository->getCategoryById($id);
        $newspapersCollection = $this->newsRepository->getNewsByCategoryId($id);

        $newspapersObjectList = $newspapersCollection
            ->filter(function (News $news) {
                return $news["category"] != null;
            })
            ->map(function (News $news) {

                return new NewsListObject(
                    news_id: $news['news_id'],
                    cate_title: $news['category']['title'],
                    cate_id: $news["category"]['id'],
                    cate_slug: $news["category"]["slug"],
                    title: $news['title'],
                    slug: $news["slug"],
                    image: $news['image'],
                    description: $news["description"],
                    created_at: Carbon::parse($news['created_at'])->isoFormat("DD-MM-YYYY")
                );
            })->toArray();
        return view('sites.newspaper.newspapers_list', [
            'newspapersListViewModel' => new NewsListViewModel(news: $newspapersObjectList, currentCategoryId: $id, title: $categoryRecord["title"])
        ]);
    }

    /**
     * @param string $newspaper_data
     * @return View|RedirectResponse
     */
    public function detailNewspaperAction(string $newspaper_data): View|RedirectResponse
    {
        $newspaper_data = explode("-", $newspaper_data);
        $id = str_replace("n", "", last($newspaper_data));

        if ((int)$id != $id) {
            return $this->pageNotFound();
        }
        $newspaper = $this->newsRepository->getNewsById($id);
        if ($newspaper) {
            $newspaper->total_view++;
            $newspaper->save();
            return view('sites.newspaper.newspaper_detail', [
                'newspaperDetailViewModel' => new NewsDetailViewModel($newspaper)
            ]);
        }

        return $this->pageNotFound();
    }

    public function listBlogAction($id, $slug): View
    {
        return $this->listNewspapersAction($id, $slug);
    }
}
