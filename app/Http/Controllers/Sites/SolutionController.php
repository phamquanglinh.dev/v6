<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use App\Repository\SolutionRepository;
use App\ViewModels\Sites\Solution\SolutionDetailViewModel;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SolutionController extends Controller
{
    public function __construct(
        private readonly SolutionRepository $solutionRepository
    )
    {
    }

    public function showSolutionAction($slug): View
    {
        $id = str_replace("s", "", last(explode("-", $slug)));
        $slugRc = $this->solutionRepository->getSolutionById($id);
        if ($slugRc) {
            $slugRc->view = $slugRc["view"] + 1;
            $slugRc->save();
        }
        return \view("sites.solution.solution_detail", [
            'solutionDetailViewModel' => new SolutionDetailViewModel(solution: $slugRc)
        ]);
    }
}
