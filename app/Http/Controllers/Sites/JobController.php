<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Repository\RecruitmentRepository;
use App\Repository\RoomRepository;
use App\ViewModels\Sites\Jobs\JobDetailViewModel;
use App\ViewModels\Sites\Jobs\ListJobsViewModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class JobController extends BaseController
{
    public function __construct(
        private readonly RecruitmentRepository $recruitmentRepository,
        private readonly RoomRepository        $roomRepository,
    )
    {
    }

    public function listJobsAction(): View
    {
        return \view("sites.jobs.list_jobs", [
            'listJobsViewModel' => new ListJobsViewModel(
                jobs: $this->recruitmentRepository->getBuilder()->get(),
                rooms: $this->roomRepository->getBuilder()->get(),
            )
        ]);
    }

    public function ajaxJobAction(Request $request): JsonResponse
    {
        $rooms = $request->rooms ?? [];
        if (in_array("room-all", $rooms)) {
            $jobCollection = $this->recruitmentRepository->getAll();
        } else {
            $selected = [];
            foreach ($rooms as $room) {
                $selected[] = str_replace("room-", "", $room);
            }
            $jobCollection = $this->recruitmentRepository->getBuilder()->whereIn("room_id", $selected)->get();
        }
        $listJobsViewModel = new ListJobsViewModel(
            jobs: $jobCollection, rooms: $this->roomRepository->getBuilder()->get()
        );
        return response()->json([
            'data' => "" . view("sites.jobs.job_items", ["listJobsViewModel" => $listJobsViewModel])
        ]);
    }

    public function detailJobAction($recruitment_id = null): View
    {
        if ($recruitment_id == null) {
            abort(404);
        }
        if (!is_numeric($recruitment_id)) {
            abort(404);
        }
        $recruitment = $this->recruitmentRepository->getBuilder()->where("id", $recruitment_id)->firstOrFail();
        $relate_recruitment = $this->recruitmentRepository->getBuilder()->orderBy("created_at", "DESC")->limit(3)->get();
        $jobDetailViewModel = new JobDetailViewModel(recruitment: $recruitment, recruitments: $relate_recruitment);
        return \view("sites.jobs.job_detail", [
            'jobDetailViewModel' => $jobDetailViewModel
        ]);
    }
}
