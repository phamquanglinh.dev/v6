<?php

namespace App\Http\Controllers\Sites;

use App\Models\Feedback;
use App\Repository\BannerRepository;
use App\Repository\CategoryRepository;
use App\Repository\CustomerRepository;
use App\Repository\FeatureRepository;
use App\Repository\FeedbackRepository;
use App\Repository\InfoRepository;
use App\Repository\ModuleRepository;
use App\Repository\NewsRepository;
use App\Repository\PartnerRepository;
use App\Repository\PartnerTypeRepository;
use App\Repository\PricingRepository;
use App\Repository\V2FeatureRepository;
use App\Repository\V2PackageRepository;
use App\Repository\V2PartnerRepository;
use App\Repository\V2PricingRepository;
use App\ViewModels\Sites\Feedback\FeedbackDetailViewModel;
use App\ViewModels\Sites\Feedback\FeedbackListViewModel;
use App\ViewModels\Sites\Home\HomeViewModel;
use App\ViewModels\Sites\Home\Object\FeedbackObject;
use App\ViewModels\Sites\Module\ModuleViewModel;
use App\ViewModels\Sites\Pages\AboutViewModel;
use App\ViewModels\Sites\Pages\ContactViewModel;
use App\ViewModels\Sites\Pages\Object\ContactObject;
use App\ViewModels\Sites\Partner\PartnerListViewModel;
use App\ViewModels\Sites\Pricing\PricingViewModel;
use App\ViewModels\V2\V2HomeViewModel;
use App\ViewModels\V2\V2PricingClientViewModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use App\Http\Controllers\BaseController;

class HomeController extends BaseController
{
    public function __construct(
        readonly private FeedbackRepository $feedbackRepository,
        readonly private BannerRepository $bannerRepository,
        readonly private InfoRepository $infoRepository,
        readonly private FeatureRepository $featureRepository,
        readonly private CustomerRepository $customerRepository,
        readonly private PricingRepository $pricingRepository,
        readonly private PartnerRepository $partnerRepository,
        readonly private PartnerTypeRepository $partnerTypeRepository,
        readonly private NewsRepository $newsRepository,
        readonly private CategoryRepository $categoryRepository,
        readonly private V2PartnerRepository $v2PartnerRepository,
        readonly private V2PackageRepository $v2PackageRepository,
        readonly private V2PricingRepository $v2PricingRepository,
        private readonly V2FeatureRepository $v2FeatureRepository
    ) {}

    /**
     * @return View
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 09:16:38
     */
    public function listHomeAction(): View
    {
        $feedbacksCollection = $this->feedbackRepository->getFeedbackForHome();

        $bannerCollection = $this->bannerRepository->getBannerByPosition(1);
        $infoCollection = $this->infoRepository->getInfoById();
        $featuresCollection = $this->featureRepository->getFeaturesForHome();
        $customerCollection = $this->customerRepository->getCustomersForSite();

        $allCustomerCollection = $this->customerRepository->getAllActiveCustomers();

//        return view('sites.home.home', [
//            'homeViewModel' => new HomeViewModel(
//                feedbacks : $feedbacksCollection,
//                banners : $bannerCollection,
//                site : $infoCollection,
//                features : $featuresCollection,
//                customers : $customerCollection,
//                allCustomers : $allCustomerCollection
//            ),
//
//        ]);

        $newspaperCollection = $this->newsRepository->getDelegateNews();

        $topPartnerCollection = $this->v2PartnerRepository->getAllV2Partners(
            conditions : [
                'place_in_top_home' => 1
            ],
            orderBy : 'lft',
        );

        $bottomPartnerCollection = $this->v2PartnerRepository->getAllV2Partners(
            conditions : [
                'place_in_bottom_home' => 1
            ],
            orderBy : 'lft',
        );

        return \view('version2.home', [
            'v2HomeViewModel' => new V2HomeViewModel(
                feedbacks : $feedbacksCollection,
                newspapers : $newspaperCollection,
                topPlacePartners : $topPartnerCollection,
                bottomPlacePartners : $bottomPartnerCollection
            )
        ]);
    }

    public function subHomePageAction(): View
    {
        return view("sites.home.sub", [

        ]);
    }

    public function aboutPageAction(): View
    {
        return view("sites.pages.about", [
            'aboutViewModel' => new AboutViewModel(information : $this->infoRepository->getInfoById())
        ]);
    }

    public function pricingPageAction(): View
    {
        $v2PricingClientViewModel = new V2PricingClientViewModel(
            onCloudPackages : $this->v2PackageRepository->getAllV2OnCloudPackage(),
            pricingCollection : $this->v2PricingRepository->getAllV2Pricing(),
            featureCollection : $this->v2FeatureRepository->getAllV2Feature(),
            onPremisePackages : $this->v2PackageRepository->getAllV2OnPremisePackage()
        );

//        return \view('version2.pricing', [
//            'v2PricingClientViewModel' => $v2PricingClientViewModel
//        ]);

        return \view("version2.pricing-ds", [
            'v2PricingClientViewModel' => $v2PricingClientViewModel
        ]);

        return view("sites.pages.pricing", [
            "pricingViewModel" => new PricingViewModel(pricing : $this->pricingRepository->getPricingPagination()),
            'moduleViewModel' => new ModuleViewModel(modules : $this->moduleRepository->listModules())
        ]);
    }

    public function trialPageAction(): View
    {
        return view("sites.pages.trial", [
            'contactViewModel' => new ContactViewModel(
                contactObject : new ContactObject(
                    contact_form : $this->infoRepository->getInfoById()["contact_form"]
                )
            )
        ]);
    }

    public function contactPageAction(): View
    {
        return \view("sites.pages.contact");
    }

    public function partnerPageAction(): View
    {
        return \view("sites.pages.partner", [
            'partnerListViewModel' => new PartnerListViewModel(
                partners : $this->partnerRepository->getAllPartner(),
                partnerTypes : $this->partnerTypeRepository->getAll()
            )
        ]);
    }

    public function storyPageAction(): View
    {
        $feedbackCollection = $this->feedbackRepository->getAllFeedback();

        return \view("sites.pages.stories", ["feedbackListViewModel" => new FeedbackListViewModel(feedbacks : $feedbackCollection)]);
    }

    public function storyDetailPageAction($id): View
    {
        if (! is_numeric($id)) {
            abort(404);
        }
        $feedback = $this->feedbackRepository->getFeedbackById($id);
        if (! $feedback) {
            abort(404);
        }
        $feedbackDetailViewModel = new FeedbackDetailViewModel(feedback : $feedback);

        return \view("sites.pages.story", [
            "feedbackDetailViewModel" => $feedbackDetailViewModel
        ]);
    }

    public function invitePageAction(): View
    {
        return \view("sites.pages.invite", [
            "partnerListViewModel" => new PartnerListViewModel(partners : $this->partnerRepository->getAllPartner(), partnerTypes : $this->partnerTypeRepository->getAll())
        ]);
    }

    public function solutionPageAction(): View
    {
        $feedbacksCollection = $this->feedbackRepository->getFeedbackForHome();
        $bannerCollection = $this->bannerRepository->getBannerByPosition(1);
        $infoCollection = $this->infoRepository->getInfoById();
        $featuresCollection = $this->featureRepository->getFeaturesForHome();
        $customerCollection = $this->customerRepository->getCustomersForSite();
//        dd($customerCollection);
        $allCustomerCollection = $this->customerRepository->getAllActiveCustomers();

        return view('sites.pages.solution', [
            'homeViewModel' => new HomeViewModel(
                feedbacks : $feedbacksCollection,
                banners : $bannerCollection,
                site : $infoCollection,
                features : $featuresCollection,
                customers : $customerCollection,
                allCustomers : $allCustomerCollection
            ),
            "pricingViewModel" => new PricingViewModel(pricing : $this->pricingRepository->getPricingPagination()),

        ]);
    }

    public function webhook(Request $request): void
    {
        Log::info($request->header('x-api-key'));
        Log::info($request->input());
    }

    public function webhookLalaMove(Request $request): void
    {
        Log::info($request->input());
    }
}
