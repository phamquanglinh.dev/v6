<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Repository\BannerRepository;
use App\ViewModels\Sites\Banner\BannerListViewModel;
use App\ViewModels\Sites\Banner\BannerObject;
use Illuminate\Http\Request;
use Illuminate\View\View;

class BannerController extends BaseController
{
    /**
     * @param BannerRepository $bannerRepository
     */
    public function __construct(
        readonly private BannerRepository $bannerRepository
    ) {}

    /**
     * @return View
     */
    public function listBannersAction(): View
    {
        $bannersCollection = $this->bannerRepository->getBannerAll();

        $bannerListViewModel = new BannerListViewModel(
            banners: $bannersCollection->map(
                fn(Banner $banner) => new BannerObject(
                    title: $banner['title'],
                    image: $banner['image'],
                    url: $banner['url'],
                    position: $banner['position']
                )
        )->toArray());

        dd("Banners: ", $bannerListViewModel);
    }
}
