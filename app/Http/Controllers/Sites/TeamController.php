<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use App\Repository\NewsRepository;
use App\Repository\PhotoRepository;
use App\Repository\TeamInfoRepository;
use App\ViewModels\Sites\TeamInfo\TeamViewModel;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TeamController extends Controller
{
    public function __construct(
        private readonly TeamInfoRepository $teamInfoRepository,
        private readonly PhotoRepository    $photoRepository,
        private readonly NewsRepository    $newsRepository,
    )
    {
    }

    public function index(): View
    {
        return view("sites.team.team-getfly", [
            'teamViewModel' => new TeamViewModel(
                teamInfo: $this->teamInfoRepository->getTeamInfo(),
                photos: $this->photoRepository->getPhotoForTeamSite(),
                newspapers: $this->teamInfoRepository->getNewspaper($this->newsRepository)
            )
        ]);
    }
}
