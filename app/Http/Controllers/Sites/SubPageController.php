<?php

namespace App\Http\Controllers\Sites;

use App\Http\Controllers\Controller;
use App\Repository\FeedbackRepository;
use App\Repository\LandingPageRepository;
use App\Repository\PageFeatureRepository;
use App\Repository\SubPageRepository;
use App\ViewModels\Sites\FeaturePage\FeaturePageViewModel;
use App\ViewModels\Sites\Home\SubPageViewModel;
use App\ViewModels\Sites\LandingPage\LandingPageViewModel;
use Illuminate\View\View;

class SubPageController extends Controller
{
    public function __construct(
        private readonly SubPageRepository     $pageRepository,
        private readonly PageFeatureRepository $pageFeatureRepository,
        private readonly LandingPageRepository $landingPageRepository,
        private readonly FeedbackRepository    $feedbackRepository,
    )
    {
    }

    public function index($slug)
    {
        $page = $this->pageRepository->getBySlug($slug);

        if ($page) {
            $page->view = $page["view"] + 1;
            $page->save();
            return view("sites.home.sub", [
                'subPageViewModel' => new SubPageViewModel(page: $page)
            ]);
        } else {
            $landingPage = $this->landingPageRepository->getPageBySlug($slug);
            $landingPage->view = $landingPage["view"] + 1;
            $landingPage->save();
            $feedbacksCollection = $this->feedbackRepository->getFeedbackForHome();
            return \view("sites.pages.landing-page", [
                'landingPageViewModel' => new LandingPageViewModel(landingPage: $landingPage, feedbacks: $feedbacksCollection)
            ]);
        }

    }

    public function pageWithFolder($folder, $slug): View
    {
        $pageCollection = $this->pageFeatureRepository->getFeaturePage(slug: $slug, folder: $folder);
        $pageCollection->view = $pageCollection["view"] + 1;
        $pageCollection->save();
        return \view("sites.pages.feature-page", ['featurePageViewModel' => new FeaturePageViewModel(pageFeature: $pageCollection)]);
    }

    public function marketing(): View
    {
        $page = $this->pageRepository->getBySlug("marketing");
        $page->view = $page["view"] + 1;
        $page->save();
        return view("sites.home.sub", [
            'subPageViewModel' => new SubPageViewModel(page: $page)
        ]);

    }

    public function tempK(): View
    {
        $page = $this->pageRepository->getBySlug("giai-phap-quan-ly-va-cham-soc-khach-hang-toan-dien-cho-smes-viet");
        $page->view = $page["view"] + 1;
        $page->save();
        return view("sites.home.sub", [
            'subPageViewModel' => new SubPageViewModel(page: $page)
        ]);
    }

}
