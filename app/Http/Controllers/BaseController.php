<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;

class BaseController extends Controller
{
    protected CONST ORDER_DEFAULT = 99;

    /**
     * @return RedirectResponse
     */
    public function pageNotFound(): RedirectResponse
    {
        return abort(404);
    }
}
