<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(
        private readonly UserRepository $userRepository
    )
    {
    }

    public function login(Request $request)
    {
        /**
         * @var User $user
         */
        $collection = $request->except("_token");
        $username = $collection["user_name"];
        $password = $collection["password"];
        $user = $this->userRepository->getUserByUserName($username);
        if ($user) {
            if ($user->invalid == 1) {
                return back()->with("invalid", "Tài khoản đã bị vô hiệu hóa");
            }
            if ($user->password == md5($password)) {
                backpack_auth()->loginUsingId($user->user_id);
                return redirect("/admin/dashboard");
            }
            if (Hash::check($password, $user->password)) {
                backpack_auth()->loginUsingId($user->user_id);
                return redirect("/admin/dashboard");
            }
            return back()->with("password", "Sai mật khẩu");
        }

        return back()->with("username", "Không tồn tại user");

    }
}
