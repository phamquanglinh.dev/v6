<?php

namespace App\ViewModels\Sites\Sidebar\Object;

class SidebarNewObject
{
    public function __construct(
        private readonly string $id,
        private readonly string $updated_at,
        private readonly string $title,
        private readonly ?string $categoryId,
        private readonly ?string $categoryName,

    )
    {
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getCategoryId(): ?string
    {
        return $this->categoryId;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }
}