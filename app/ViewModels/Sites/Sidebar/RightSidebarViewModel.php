<?php

namespace App\ViewModels\Sites\Sidebar;

use App\Models\Banner;
use App\Models\News;
use App\ViewModels\Sites\Banner\BannerObject;
use App\ViewModels\Sites\Sidebar\Object\SidebarNewObject;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class RightSidebarViewModel
{
    public function __construct(
        private readonly Collection $topNews,
        private readonly Collection $banners
    )
    {
    }

    /**
     * @return BannerObject[]
     */
    public function getBanners(): array
    {
        return $this->banners->map(
            fn(Banner $banner) => new BannerObject(
                title: $banner["title"],
                image: $banner["image"],
                url: $banner["url"],
                position: $banner["position"],id: $banner["banner_id"]
            )
        )->toArray();
    }

    /**
     * @return SidebarNewObject[]
     */
    public function getTopNews(): array
    {
        return $this->topNews->map(
            fn(News $news) => new SidebarNewObject(
                id: $news["news_id"],
                updated_at: Carbon::parse($news["updated_at"])->isoFormat("DD/MM/YYYY"),
                title: $news["title"],
                categoryId: $news["category"]["id"] ?? null,
                categoryName: $news["category"]["title"] ?? null
            )
        )->toArray();
    }
}