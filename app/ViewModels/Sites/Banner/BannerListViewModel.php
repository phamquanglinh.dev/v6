<?php

namespace App\ViewModels\Sites\Banner;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Collection;

class BannerListViewModel
{
    /**
     * @param BannerObject[] $banners
     */
    public function __construct(
        readonly private Collection|array $banners
    )
    {
    }

    /**
     * @return BannerObject[]
     */
    public function getBanners(): array
    {
        return $this->banners->map(fn(Banner $banner) => new BannerObject(
            title: $banner["title"],
            image: $banner["image"],
            url: $banner["url"],
            position: $banner["position"],
            id: $banner["banner_id"]))
            ->toArray();
    }
}