<?php

namespace App\ViewModels\Sites\Banner;

class BannerObject
{
    /**
     * @param string $title
     * @param string $image
     * @param string $url
     * @param int $position
     */
    public function __construct(
        readonly private string $title,
        readonly private string $image,
        readonly private string $url,
        readonly private int    $position,
        readonly private ?int   $id,
    )
    {
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }
}