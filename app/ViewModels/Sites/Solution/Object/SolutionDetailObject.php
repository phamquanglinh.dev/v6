<?php

namespace App\ViewModels\Sites\Solution\Object;

class SolutionDetailObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly string $content,
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}