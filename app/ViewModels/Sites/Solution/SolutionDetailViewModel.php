<?php

namespace App\ViewModels\Sites\Solution;

use App\Models\Solution;
use App\ViewModels\Sites\Solution\Object\SolutionDetailObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class SolutionDetailViewModel
{
    /**
     * @param Model|Builder $solution
     */
    public function __construct(
        private readonly Model|Builder $solution
    )
    {
    }

    /**
     * @return SolutionDetailObject
     */
    public function getSolution(): SolutionDetailObject
    {
        /**
         * @var Solution $solution
         */
        $solution = $this->solution;
        return new SolutionDetailObject(title: $solution["title"], description: $solution["description"], content: $solution["content"]);
    }
}