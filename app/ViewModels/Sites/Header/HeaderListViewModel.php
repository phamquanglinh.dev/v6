<?php

namespace App\ViewModels\Sites\Header;

class HeaderListViewModel
{
    /**
     * @param MenuObject $menus
     */
    public function __construct(
        readonly private MenuObject $menus
    )
    {
    }

    /**
     * @return MenuObject
     */
    public function getMenus(): MenuObject
    {
        return $this->menus;
    }
}