<?php

namespace App\ViewModels\Sites\Header;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class TabsObject
{
    public function __construct(
        private readonly string           $name,
        private readonly string           $icon,
        private readonly Collection|array $items
    )
    {

    }

    /**
     * @return MenuObject[]
     */
    public function getItems(): array
    {
        return $this->items->map(fn(Menu $menu) => new MenuObject(
            title: $menu["title"],
            mega: $menu["mega"],
            links: $menu["link"],
            child: [],
            icon: $menu["icon"],
            tabs: [],
            newTab: $menu["new_tab"] == 1
        ))->toArray();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

}