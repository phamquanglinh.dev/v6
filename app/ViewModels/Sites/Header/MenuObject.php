<?php

namespace App\ViewModels\Sites\Header;

use App\Models\MegaMenu;
use Illuminate\Database\Eloquent\Collection;

class MenuObject
{

    /**
     * @param string $title
     * @param bool $mega
     * @param string|null $links
     * @param array $child
     * @param string|null $icon
     */
    public function __construct(
        readonly private string           $title,
        readonly private bool             $mega,
        readonly private ?string          $links,
        readonly private array            $child,
        readonly private ?string          $icon,
        readonly private Collection|array $tabs,
        readonly private ?bool            $newTab,
    )
    {

    }

    /**
     * @return bool
     */
    public function getNewTab(): bool
    {
        return $this->newTab ?? false;
    }

    /**
     * @return TabsObject[]
     */
    public function getTabs(): array
    {
        /**
         * @var  MegaMenu $megaMenu
         */
        return $this->tabs->map(fn(MegaMenu $megaMenu) => new TabsObject(
            name: $megaMenu["name"], icon: $megaMenu["icon"], items: $megaMenu->childrenMenu()->orderBy("lft")->get()
        ))->toArray();
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon ?? "site/images/All in one.png";
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isMega(): bool
    {
        return $this->mega;
    }

    /**
     * @return string
     */
    public function getLinks(): string
    {
        return $this->links ?? "#";
    }

    /**
     * @return MenuObject[]|null
     */
    public function getChild(): ?array
    {
        return $this->child;
    }


}