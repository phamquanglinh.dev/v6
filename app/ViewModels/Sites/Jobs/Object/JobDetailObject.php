<?php

namespace App\ViewModels\Sites\Jobs\Object;

use Illuminate\Support\Carbon;

class JobDetailObject
{
    public function __construct(
        private readonly string $job_name,
        private readonly string $salary,
        private readonly string $quantity,
        private readonly string $time,
        private readonly string $address,
        private readonly string $body,

    )
    {
    }

    /**
     * @return string
     */
    public function getJobName(): string
    {
        return $this->job_name;
    }

    /**
     * @return string
     */
    public function getSalary(): string
    {
        return $this->salary;
    }

    /**
     * @return string
     */
    public function getQuantity(): string
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return Carbon::parse($this->time)->isoFormat("d/M/Y");
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}