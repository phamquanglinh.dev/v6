<?php

namespace App\ViewModels\Sites\Jobs\Object;

class RoomObject
{
    public function __construct(
        private readonly string $name,
        private readonly int    $id,
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}