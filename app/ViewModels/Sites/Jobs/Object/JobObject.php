<?php

namespace App\ViewModels\Sites\Jobs\Object;

use Illuminate\Support\Carbon;

class JobObject
{
    public function __construct(
        readonly private int    $id,
        readonly private ?int   $room,
        readonly private string $job_name,
        readonly private int    $quantity,
        readonly private string $salary,
        readonly private string $time,
    )
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    /**
     * @return int|null
     */
    public function getRoom(): ?int
    {
        return $this->room;
    }


    /**
     * @return string
     */
    public
    function getJobName(): string
    {
        return $this->job_name;
    }

    /**
     * @return int
     */
    public
    function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public
    function getSalary(): string
    {
        return $this->salary;
    }

    /**
     * @return string
     */
    public
    function getTime(): string
    {
        return Carbon::parse($this->time)->isoFormat("DD/MM/YY");
    }
}