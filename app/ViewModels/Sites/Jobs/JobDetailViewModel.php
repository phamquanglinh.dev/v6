<?php

namespace App\ViewModels\Sites\Jobs;

use App\Models\Recruitment;
use App\ViewModels\Sites\Jobs\Object\JobDetailObject;
use App\ViewModels\Sites\Jobs\Object\JobObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class JobDetailViewModel
{
    public function __construct(
        private readonly Model|Builder      $recruitment,
        private readonly Collection|Builder $recruitments,
    )
    {
    }

    /**
     * @return JobObject[]
     */
    public function getRecruitments(): array
    {
        return $this->recruitments->map(fn(Recruitment $recruitment) => new JobObject(
            id: $recruitment["id"],
            room: $recruitment["room_id"],
            job_name: $recruitment["job_name"],
            quantity: $recruitment["quantity"],
            salary: $recruitment["salary"],
            time: $recruitment["time"]
        ))->toArray();
    }

    /**
     * @return JobDetailObject
     */
    public function getRecruitment(): JobDetailObject
    {
        $recruitment = $this->recruitment;
        return new JobDetailObject(
            job_name: $recruitment["job_name"],
            salary: $recruitment["salary"],
            quantity: $recruitment["quantity"],
            time: $recruitment["time"],
            address: $recruitment["address"] ?? "Thái Hà Đống Đa Hà Nội",
            body: $recruitment["recruitment_detail"]
        );
    }
}