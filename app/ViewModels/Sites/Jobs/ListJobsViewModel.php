<?php

namespace App\ViewModels\Sites\Jobs;

use App\Models\Recruitment;
use App\Models\Room;
use App\ViewModels\Sites\Jobs\Object\JobObject;
use App\ViewModels\Sites\Jobs\Object\RoomObject;
use Illuminate\Database\Eloquent\Collection;

class ListJobsViewModel
{
    public function __construct(
        readonly private Collection|array $jobs,
        readonly private Collection|array $rooms,
    )
    {
    }

    /**
     * @return RoomObject[]
     */
    public function getRooms(): array
    {
        return $this->rooms->map(fn(Room $room) => new RoomObject(name: $room["name"], id: $room["id"]))->toArray();
    }

    /**
     * @return JobObject[]
     */
    public
    function getJobs(): array
    {
        return $this->jobs->map(
            fn(Recruitment $recruitment) => new JobObject(
                id: $recruitment["id"],
                room: $recruitment->Room()?->first()->id ?? null,
                job_name: $recruitment["job_name"],
                quantity: $recruitment["quantity"],
                salary: $recruitment["salary"],
                time: $recruitment["time"],
            )
        )->toArray();
    }
}