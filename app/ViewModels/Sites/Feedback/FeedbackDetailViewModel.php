<?php

namespace App\ViewModels\Sites\Feedback;

use App\ViewModels\Sites\Feedback\Object\FeedbackDetailObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class FeedbackDetailViewModel
{
    public function __construct(
        private readonly Model|Builder $feedback
    )
    {
    }

    /**
     * @return FeedbackDetailObject
     */
    public function getFeedback(): FeedbackDetailObject
    {
        $feedback = $this->feedback;
        return new FeedbackDetailObject(
            title: $feedback["title"],
            description: $feedback["note"],
            avatar: $feedback["avatar_title"]??"https://getfly.vn/assets/uploads/feedback/Chu_Quang_Huy-1.jpg",
            content: $feedback["content"]
        );
    }
}