<?php

namespace App\ViewModels\Sites\Feedback\Object;

class FeedbackDetailObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly string $avatar,
        private readonly string $content,
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        $avatar = str_replace("/assets/uploads/feedback/", "/", $this->avatar);
        if (str_contains($avatar, "http")) {
            return $avatar;
        }
        return url("/assets/uploads/feedback/" . $avatar);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}