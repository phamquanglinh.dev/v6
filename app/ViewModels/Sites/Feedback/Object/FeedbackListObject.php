<?php

namespace App\ViewModels\Sites\Feedback\Object;

use Illuminate\Support\Str;

class FeedbackListObject
{
    public function __construct(
        private readonly int    $id,
        private readonly string $avatar,
        private readonly string $title,
        private readonly string $description,
        private readonly string $link,
        private readonly int    $type,
    )
    {
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        $avatar = str_replace("/assets/uploads/feedback/", "/", $this->avatar);
        if (str_contains($avatar, "http")) {
            return $avatar;
        }
        return url("/assets/uploads/feedback/" . $avatar);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return Str::limit($this->title, 50);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return Str::limit($this->description, 80);
    }
}