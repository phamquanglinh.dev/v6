<?php

namespace App\ViewModels\Sites\Feedback\Object;

class FeedbackObject
{
    public function __construct(
        private readonly string  $name,
        private readonly string  $title,
        private readonly ?string $note,
        private readonly ?string $link,
        private readonly ?string $content,
        private readonly string  $position,
        private readonly int     $active,
        private readonly int     $priority,
        private readonly int     $type_feedback,
        private readonly ?string $avatar,
        private readonly ?string $avatar_title

    )
    {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'title' => $this->title,
            'note' => $this->note,
            'link' => $this->link ?? "#",
            'content' => $this->content ?? null,
            'position' => $this->position,
            'active' => $this->active ?? 1,
            'priority' => $this->priority,
            'type_feedback' => $this->type_feedback ?? 1,
            'avatar' => $this->avatar ?? "#",
            'avatar_title' => $this->avatar_title ?? null
        ];
    }
}