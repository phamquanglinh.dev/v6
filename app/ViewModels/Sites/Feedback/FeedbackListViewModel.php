<?php

namespace App\ViewModels\Sites\Feedback;

use App\Models\Feedback;
use App\ViewModels\Sites\Feedback\Object\FeedbackListObject;
use Illuminate\Database\Eloquent\Collection;

class FeedbackListViewModel
{
    public function __construct(
        private readonly Collection|array $feedbacks,
    )
    {
    }

    /**
     * @return FeedbackListObject[]
     */
    public function getFeedbacks(): array
    {
        return $this->feedbacks->map(fn(Feedback $feedback) => new FeedbackListObject(
            id: $feedback["id"],
            avatar: $feedback["avatar_title"] ?? "",
            title: $feedback["title"],
            description: $feedback["note"],
            link: $feedback["link"],
            type: $feedback["type_feedback"]
        ))->toArray();
    }
}