<?php

namespace App\ViewModels\Sites\Pricing;

use App\Models\Pricing;
use App\ViewModels\Sites\Pricing\Object\PricingObject;
use Illuminate\Pagination\LengthAwarePaginator;

class PricingViewModel
{
    public function __construct(
        private readonly LengthAwarePaginator $pricing
    )
    {
    }

    /**
     * @return PricingObject[]
     */
    public function getPricing(): array
    {
        return $this->pricing->map(fn(Pricing $pricing) => new PricingObject(
            name: $pricing->name, price: $pricing->price,
            user_price: $pricing->user_price,
            users: $pricing->users,
            customers: $pricing->customers,
            setup_time: $pricing->setup_time,
            domain_target: $pricing->domain_target
        )
        )->toArray();
    }
}