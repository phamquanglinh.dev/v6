<?php

namespace App\ViewModels\Sites\Pricing\Object;

class PricingObject
{
    public function __construct(
        private readonly string  $name,
        private readonly int     $price,
        private readonly int     $user_price,
        private readonly string  $users,
        private readonly int     $customers,
        private readonly ?string $setup_time,
        private readonly int     $domain_target,
    )
    {
    }

    /**
     * @return string|null
     */
    public function getSetupTime(): ?string
    {
        return $this->setup_time;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getUserPrice(): int
    {
        return $this->user_price;
    }

    /**
     * @return string
     */
    public function getUsers(): string
    {
        return $this->users;
    }

    /**
     * @return int
     */
    public function getCustomers(): int
    {
        return $this->customers;
    }

    /**
     * @return int
     */
    public function getDomainTarget(): int
    {
        return $this->domain_target;
    }
}