<?php

namespace App\ViewModels\Sites\Module;

use App\Models\Module;
use App\ViewModels\Sites\Module\Object\ModuleObject;
use Illuminate\Support\Collection;

class ModuleViewModel
{
    public function __construct(
        private readonly Collection $modules
    ) {}

    /**
     * @return ModuleObject[]
     */
    public function getModules(): array
    {
        return $this->modules->map(fn(Module $module) => new ModuleObject(
            name : $module->name, order : $module->order, url : $module->url, icon : $module["icon"]
        ))->toArray();
    }
}