<?php

namespace App\ViewModels\Sites\Module\Object;

class ModuleObject
{
    public function __construct(
        private readonly string $name,
        private readonly string $order,
        private readonly string $url,
        private readonly string $icon,
    )
    {
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}