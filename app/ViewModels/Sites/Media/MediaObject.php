<?php

namespace App\ViewModels\Sites\Media;

class MediaObject
{
    /**
     * @param string $link
     * @param string $avatar
     * @param int $type
     */
    public function __construct(
        readonly private string $link,
        readonly private string $avatar,
        readonly private int $type,
    ) {}

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }
}