<?php

namespace App\ViewModels\Sites\Media;

class MediaListViewModel
{
    /**
     * @param MediaObject[] $medias
     */
    public function __construct(
        readonly private array $medias
    ) {}

    /**
     * @return MediaObject[]
     */
    public function getMedias(): array
    {
        return $this->medias;
    }
}