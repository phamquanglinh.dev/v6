<?php

namespace App\ViewModels\Sites\FeaturePage;

use App\Models\PageFeature;
use App\ViewModels\Sites\FeaturePage\Object\FeaturePageObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class FeaturePageViewModel
{
    public function __construct(
        private readonly Model|Builder $pageFeature
    )
    {
    }

    /**
     * @return FeaturePageObject
     */
    public function getPageFeature(): FeaturePageObject
    {
        /**
         * @var PageFeature $pageFeature
         */
        $pageFeature = $this->pageFeature;
        return new FeaturePageObject(
            title: $pageFeature["title"],
            description: $pageFeature["description"],
            body: $pageFeature["body"],
            slides: $pageFeature["slides"]
        );
    }
}