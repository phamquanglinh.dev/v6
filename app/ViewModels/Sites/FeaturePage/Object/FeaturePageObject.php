<?php

namespace App\ViewModels\Sites\FeaturePage\Object;

class FeaturePageObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly ?string $body,
        private readonly ?array  $slides,
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @return array|null
     */
    public function getSlides(): ?array
    {
        return $this->slides;
    }
}