<?php

namespace App\ViewModels\Sites\Pages;

use App\Models\SiteInfo;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Collection\Collection;

class AboutViewModel
{
    public function __construct(
        readonly private SiteInfo $information
    )
    {
    }

    /**
     * @return SiteInfo
     */
    public function getInformation(): SiteInfo
    {
        return $this->information;
    }
}