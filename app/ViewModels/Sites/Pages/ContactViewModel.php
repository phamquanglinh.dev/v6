<?php

namespace App\ViewModels\Sites\Pages;

use App\ViewModels\Sites\Pages\Object\ContactObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ContactViewModel
{
    public function __construct(
        private readonly ContactObject $contactObject
    )
    {
    }

    /**
     * @return ContactObject
     */
    public function getContactObject(): ContactObject
    {
        return $this->contactObject;
    }
}