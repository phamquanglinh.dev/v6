<?php

namespace App\ViewModels\Sites\Pages\Object;

class ContactObject
{
    public function __construct(
        private readonly string $contact_form
    )
    {
    }

    /**
     * @return string
     */
    public function getContactForm(): string
    {
        return $this->contact_form;
    }
}