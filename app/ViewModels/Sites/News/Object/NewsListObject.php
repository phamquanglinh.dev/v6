<?php

namespace App\ViewModels\Sites\News\Object;

use Illuminate\Support\Str;

class NewsListObject
{
    /**
     * @param int $news_id
     * @param string $cate_title
     * @param string $cate_id
     * @param string $title
     * @param string $slug
     * @param string $image
     * @param string $description
     * @param string $created_at
     */
    public function __construct(
        readonly private int    $news_id,
        readonly private string $cate_title,
        readonly private string $cate_id,
        readonly private string $cate_slug,
        readonly private string $title,
        readonly private string $slug,
        readonly private string $image,
        readonly private string $description,
        readonly private string $created_at,
    )
    {
    }

    /**
     * @return string
     */
    public function getCateSlug(): string
    {
        return $this->cate_slug;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getCateSlug() . "/" . $this->slug . "-n" . $this->news_id;
    }

    /**
     * @return string
     */
    public function getCateId(): string
    {
        return $this->cate_id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return Str::limit($this->description,140);
    }

    /**
     * @return int
     */
    public function getNewsId(): int
    {
        return $this->news_id;
    }

    /**
     * @return string
     */
    public function getCateTitle(): string
    {
        return $this->cate_title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return Str::limit($this->title,100);
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }
}