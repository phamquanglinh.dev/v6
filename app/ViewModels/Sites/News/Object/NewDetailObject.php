<?php

namespace App\ViewModels\Sites\News\Object;

class NewDetailObject
{
    public function __construct(
        private readonly string  $title,
        private readonly string  $updated_at,
        private readonly ?string $category_id,
        private readonly ?string $category_slug,
        private readonly ?string $category_name,
        private readonly string  $description,
        private readonly string  $image,
        private readonly string  $content,
        private readonly string  $title_seo,
        private readonly string  $keyword_seo,
        private readonly string  $view,

    )
    {
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @return string
     */
    public
    function getTitleSeo(): string
    {
        return $this->title_seo;
    }

    /**
     * @return string
     */
    public
    function getKeywordSeo(): string
    {
        return $this->keyword_seo;
    }

    /**
     * @return string|null
     */
    public
    function getCategorySlug(): ?string
    {
        return $this->category_slug;
    }

    /**
     * @return string
     */
    public
    function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public
    function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    /**
     * @return string|null
     */
    public
    function getCategoryId(): ?string
    {
        return $this->category_id;
    }

    /**
     * @return string|null
     */
    public
    function getCategoryName(): ?string
    {
        return $this->category_name;
    }

    /**
     * @return string
     */
    public
    function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public
    function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public
    function getContent(): string
    {
        return $this->content;
    }
}