<?php

namespace App\ViewModels\Sites\News\Object;

class NewsObject
{
    /**
     * @param int $news_id
     * @param string $title
     * @param string $cate_title
     * @param string $image
     * @param string $created_at
     */
    public function __construct(
        readonly private int $news_id,
        readonly private string $title,
        readonly private string $cate_title,
        readonly private string $image,
        readonly private string $created_at
    ) {}

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @return int
     */
    public function getNewsId(): int
    {
        return $this->news_id;
    }

    /**
     * @return string
     */
    public function getCateTitle(): string
    {
        return $this->cate_title;
    }
}