<?php

namespace App\ViewModels\Sites\News;

use App\ViewModels\Sites\News\Object\NewsListObject;

class NewsListViewModel
{
    /**
     * @param NewsListObject[] $news
     */
    public function __construct(
        readonly private array  $news,
        readonly private int    $currentCategoryId,
        readonly private string $title
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getCurrentCategoryId(): int
    {
        return $this->currentCategoryId;
    }

    /**
     * @return NewsListObject[]
     */
    public function getNews(): array
    {
        return $this->news;
    }
}