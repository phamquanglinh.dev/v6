<?php

namespace App\ViewModels\Sites\News;

use App\Models\News;
use App\ViewModels\Sites\News\Object\NewsListObject;
use Illuminate\Database\Eloquent\Collection;

class NewsSearchViewModel
{
    public function __construct(
        readonly private string           $params,
        readonly private Collection|array $news,

    )
    {
    }

    /**
     * @return string
     */
    public function getParams(): string
    {
        return $this->params;
    }

    /**
     * @return NewsListObject[]
     */
    public function getNews(): array
    {
        return $this->news->filter(function (News $news) {
            return $news["category"] != null;
        })->map(function (News $news) {
            return new NewsListObject(
                news_id: $news['news_id'],
                cate_title: $news['category']['title'],
                cate_id: $news['category']['id'],
                cate_slug: $news['category']['slug'],
                title: $news['title'],
                slug: $news["slug"],
                image: $news['image'],
                description: $news["description"],
                created_at: $news['created_at']
            );
        })->toArray();
    }
}