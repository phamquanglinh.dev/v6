<?php

namespace App\ViewModels\Sites\News;

use App\Models\News;
use App\ViewModels\Sites\News\Object\NewDetailObject;
use App\ViewModels\Sites\News\Object\NewsObject;
use Illuminate\Support\Carbon;
use Ramsey\Collection\Collection;

class NewsDetailViewModel
{

    public function __construct(
        readonly private News $news
    )
    {
    }

    /**
     * @return NewDetailObject
     */
    public function getNews(): NewDetailObject
    {
        $news = $this->news;
        $views = $news["total_view"];
        $fake_view = $news["rate_total"] >= 5000 ? $news["rate_total"] : rand(5000, 10000);

        return new NewDetailObject(
            title: $news["title"],
            updated_at: Carbon::parse($news['updated_at'])->isoFormat("DD/MM/YYYY"),
            category_id: $news["category"] ? $news["category"]["id"] : null,
            category_slug: $news["category"] ? $news["category"]["slug"] : null,
            category_name: $news["category"] ? $news["category"]["title"] : null,
            description: $news["description"],
            image: $news["image"],
            content: $news["content"], title_seo: $news["description_seo"] ?? "", keyword_seo: $news["keyword_seo"] ?? "", view: max($views, $fake_view)
        );
    }


}