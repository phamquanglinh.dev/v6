<?php

namespace App\ViewModels\Sites\Partner;

use App\Models\Partner;
use App\ViewModels\Sites\Partner\Object\PartnerListObject;
use App\ViewModels\Sites\Partner\Object\PartnerTypeObject;
use Illuminate\Database\Eloquent\Collection;

class PartnerListViewModel
{
    public function __construct(
        readonly private Collection|array $partners,
        readonly private Collection|array $partnerTypes,
    )
    {
    }

    /**
     * @return PartnerTypeObject[]
     */
    public function getPartnerTypes(): array
    {
        return $this->partnerTypes->map(fn($partner_type) => new PartnerTypeObject(
            partner_type: $partner_type
        ))->toArray();
    }

    /**
     * @return PartnerListObject[]
     */
    public
    function getPartners(): array
    {
        return $this->partners->map(fn(Partner $partner) => new PartnerListObject(avatar: $partner["avatar"], link: $partner["link"], name: $partner["name"]))->toArray();
    }
}