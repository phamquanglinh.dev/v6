<?php

namespace App\ViewModels\Sites\Partner\Object;

use App\Models\Partner;
use App\Models\PartnerType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class PartnerTypeObject
{
    public function __construct(
        private readonly Model|Builder $partner_type,
    )
    {
    }

    /**
     * @return string
     */
    public function getPartnerTabId(): string
    {
        return Str::slug($this->partner_type["name"]);
    }

    public function getPartnerTypeName()
    {
        return $this->partner_type["name"];
    }

    /**
     * @return PartnerListObject[]
     */
    public function getPartnersByType(): array
    {
        /**
         * @var PartnerType $partner_type
         */
        $partner_type = $this->partner_type;
        $partners = $partner_type->Partners()->get();
        return $partners->map(fn(Partner $partner) => new PartnerListObject(
            avatar: $partner["avatar"], link: $partner["link"],name: $partner["name"]
        ))->toArray();
    }
}