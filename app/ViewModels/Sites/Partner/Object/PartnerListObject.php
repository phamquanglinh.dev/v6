<?php

namespace App\ViewModels\Sites\Partner\Object;

class PartnerListObject
{
    public function __construct(
        private readonly string $avatar,
        private readonly string $link,
        private readonly string $name,
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return url("/assets/uploads/media/" . $this->avatar);
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }
}