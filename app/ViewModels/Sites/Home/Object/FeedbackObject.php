<?php

namespace App\ViewModels\Sites\Home\Object;

use Illuminate\Support\Str;

class FeedbackObject
{
    /**
     * @param int $id
     * @param string $title
     * @param string $avatar
     * @param int $type_feedback
     * @param string $content
     * @param string $position
     * @param string $link
     * @param string $name
     */
    public function __construct(
        readonly private int    $id,
        readonly private string $title,
        readonly private string $avatar,
        readonly private int    $type_feedback,
        readonly private string $content,
        readonly private string $position,
        readonly private string $link,
        readonly private string $name,
        readonly private string $note,
    )
    {
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return Str::limit($this->note);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        if (str_contains($this->avatar, "http")) {
            return $this->avatar;
        }
        return url("/assets/uploads/feedback/" . $this->avatar);
    }

    /**
     * @return int
     */
    public function getTypeFeedback(): int
    {
        return $this->type_feedback;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}