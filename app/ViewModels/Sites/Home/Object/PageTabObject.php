<?php

namespace App\ViewModels\Sites\Home\Object;

class PageTabObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $des,
        private readonly string $icon,
        private readonly string $icon_alt,
        private readonly string $thumbnail,
        private readonly string $id,
    )
    {
    }

    /**
     * @return string
     */
    public function getIconAlt(): string
    {
        return $this->icon_alt;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDes(): string
    {
        return $this->des;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }
}