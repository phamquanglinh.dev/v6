<?php

namespace App\ViewModels\Sites\Home\Object;

class CustomerObject
{
    /**
     * @param string $logo
     * @param string $name
     */
    public function __construct(
        private readonly string $logo,
        private readonly string $name,
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return url("assets/uploads/media") . "/" . $this->logo;
    }
}