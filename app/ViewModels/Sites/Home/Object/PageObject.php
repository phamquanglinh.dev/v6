<?php

namespace App\ViewModels\Sites\Home\Object;

class PageObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $sub,
        private readonly string $description,
        private readonly string $keywords,
        private readonly string $image,
        private readonly string $video,
        private readonly string $type,
    )
    {
    }

    /**
     * @return string
     */
    public function getVideo(): string
    {
        return $this->video;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSub(): string
    {
        return $this->sub;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }
}