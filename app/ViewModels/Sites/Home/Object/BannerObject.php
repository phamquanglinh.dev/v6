<?php

namespace App\ViewModels\Sites\Home\Object;
class BannerObject
{
    /**
     * @param int $banner_id
     * @param string $title
     * @param string $image
     * @param int $order
     * @param int $position
     * @param string $url
     */
    public function __construct(
        readonly private int    $banner_id,
        readonly private string $title,
        readonly private string $image,
        readonly private int    $order,
        readonly private int    $position,
        readonly private string $url,
    )
    {
    }

    /**
     * @return int
     */
    public function getBannerId(): int
    {
        return $this->banner_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}