<?php

namespace App\ViewModels\Sites\Home\Object;

class AllCustomersObject
{
    public function __construct(
        private readonly string $name,
        private readonly string $logo,
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return url("assets/uploads/media/") . "/" . $this->logo;
    }
}