<?php

namespace App\ViewModels\Sites\Home;

use App\Models\SubPage;
use App\ViewModels\Sites\Header\TabsObject;
use App\ViewModels\Sites\Home\Object\PageObject;
use App\ViewModels\Sites\Home\Object\PageTabObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;

class SubPageViewModel
{

    /**
     * @param Model|Builder $page
     */
    public function __construct(
        private readonly Model|Builder $page
    )
    {
    }

    public function getPage(): PageObject
    {
        /**
         * @var SubPage $page
         */
        $page = $this->page;
        return new PageObject(
            title: $page["page_title"],
            sub: $page["page_sub_title"],
            description: $page["page_description"],
            keywords: $page["page_keywords"],
            image: $page["page_image_title"]??"",
            video: $page["page_youtube_title"]??"",
            type: $page["page_title_type"]
        );
    }

    /**
     * @return PageTabObject[]
     */
    public function getTabs(): array
    {
        $tabs_arr = [];
        $tabs = $this->page["page_tabs"];
        foreach ($tabs as $tab) {
            $tabs_arr[] = new PageTabObject(title: $tab["title"], des: $tab["des"], icon: $tab["icon"], icon_alt: $tab["icon_alt"] ?? $tab["icon"], thumbnail: $tab["thumbnail"], id: "a" . Str::random(5));
        }
        return $tabs_arr;
    }
}