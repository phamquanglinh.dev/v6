<?php

namespace App\ViewModels\Sites\Home;

use App\Models\Banner;
use App\Models\Customer;
use App\Models\Feedback;
use App\Models\SiteInfo;
use App\ViewModels\Sites\Home\Object\AllCustomersObject;
use App\ViewModels\Sites\Home\Object\BannerObject;
use App\ViewModels\Sites\Home\Object\CustomerObject;
use App\ViewModels\Sites\Home\Object\FeedbackObject;
use Illuminate\Database\Eloquent\Collection;

class HomeViewModel
{
    /**
     * @param Collection $feedbacks
     * @param Collection $banners
     * @param SiteInfo $site
     * @param Collection $features
     * @param Collection $customers
     * @param Collection $allCustomers
     */
    public function __construct(
        readonly private Collection $feedbacks,
        readonly private Collection $banners,
        readonly private SiteInfo   $site,
        readonly private Collection $features,
        readonly private Collection $customers,
        readonly private Collection $allCustomers,
    )
    {
    }

    /**
     * @return AllCustomersObject[]
     */
    public function getAllCustomers(): array
    {
        return $this->allCustomers->map(
            fn(Customer $customer) => new AllCustomersObject(name: $customer["name"], logo: $customer["avatar"])
        )->toArray();
    }

    /**
     * @return CustomerObject[]
     */
    public
    function getCustomers(): array
    {
        return $this->customers->map(
            fn(Customer $customer) => new CustomerObject(logo: $customer["avatar"], name: $customer["name"])
        )->toArray();
    }

    /**
     * @return Collection
     */
    public
    function getFeatures(): Collection
    {
        return $this->features;
    }

    /**
     * @return SiteInfo
     */
    public
    function getSite(): SiteInfo
    {
        return $this->site;
    }

    /**
     * @return FeedbackObject[]
     */
    public
    function getFeedbacks(): array
    {
        return $this->feedbacks->map(
            fn(Feedback $feedback) => new FeedbackObject(
                id: $feedback['id'],
                title: $feedback['title'],
                avatar: str_replace("/assets/uploads/feedback/", "/", $feedback["avatar"]) ?? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png",
                type_feedback: $feedback['type_feedback'],
                content: $feedback['content'],
                position: $feedback['position'],
                link: $feedback['link'],
                name: $feedback['name'],
                note: $feedback['note']
            )
        )->toArray();
    }

    public
    function getMaxGroupFeedback(): int
    {
        $length = count($this->feedbacks);
        if ($length % 3 == 0) {
            return $length / 3;
        } else {
            return $length / 3 + 1;
        }
    }

    /**
     * @return BannerObject[]
     */
    public
    function getBanners(): array
    {
        /**
         * @param int $banner_id
         * @param string $title
         * @param string $image
         * @param int $order
         * @param int $position
         * @param string $url
         */
        return $this->banners->map(
            fn(Banner $banner) => new BannerObject(
                banner_id: $banner['banner_id'],
                title: $banner['title'],
                image: $banner['image'],
                order: $banner['order'],
                position: $banner['position'],
                url: $banner['url']
            )
        )->toArray();
    }

    public
    function getVideoEmbed(): string
    {
        $array = explode("/", $this->getSite()['video_link']);
        return last($array);
    }
}