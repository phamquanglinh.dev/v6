<?php

namespace App\ViewModels\Sites\Home;

class MetaSeoViewModel
{
    public function __construct(
        private readonly string $description,
        private readonly string $keywords,
    )
    {
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }
}