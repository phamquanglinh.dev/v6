<?php

namespace App\ViewModels\Sites\TeamInfo\Object;

class TeamInfoObject
{
    public function __construct(
        readonly private string $thumbnail,
        readonly private string $title,
    )
    {
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}