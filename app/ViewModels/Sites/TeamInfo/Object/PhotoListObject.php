<?php

namespace App\ViewModels\Sites\TeamInfo\Object;

class PhotoListObject
{
    public function __construct(
        private readonly int    $id,
        private readonly string $image,
    )
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }
}