<?php

namespace App\ViewModels\Sites\TeamInfo\Object;

use Illuminate\Support\Str;

class TeamNewspaperObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $description,
        private readonly string $thumbnail,
        private readonly string $url,
    )
    {
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return Str::limit($this->title, 50);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return Str::limit($this->description);
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }
}