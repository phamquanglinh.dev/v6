<?php

namespace App\ViewModels\Sites\TeamInfo\Object;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Collection;

class PhotoListViewModel
{
    public function __construct(
        private readonly Collection|array $photos
    )
    {
    }

    /**
     * @return PhotoListObject[]
     */
    public function getPhotos(): array
    {
        return $this->photos->map(fn(Photo $photo) => new PhotoListObject(
            id: $photo["id"], image: url($photo["landscape_image"])
        ))->toArray();
    }
}