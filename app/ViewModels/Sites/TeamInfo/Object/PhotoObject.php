<?php

namespace App\ViewModels\Sites\TeamInfo\Object;

class PhotoObject
{
    public function __construct(
        private readonly string $image,
    )
    {
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }
}