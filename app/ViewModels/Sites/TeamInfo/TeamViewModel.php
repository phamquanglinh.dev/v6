<?php

namespace App\ViewModels\Sites\TeamInfo;

use App\Models\News;
use App\Models\Photo;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use App\ViewModels\Sites\TeamInfo\Object\PhotoObject;
use App\ViewModels\Sites\TeamInfo\Object\TeamInfoObject;
use App\ViewModels\Sites\TeamInfo\Object\TeamNewspaperObject;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class TeamViewModel
{
    public function __construct(
        private readonly Model|Builder    $teamInfo,
        private readonly Collection|array $photos,
        private readonly Collection|array $newspapers,
    )
    {
    }

    /**
     * @return PhotoObject[]
     */
    public function getPhotos(): array
    {
        return $this->photos->map(fn(Photo $photo) => new PhotoObject(
            image: $photo["image"]
        ))->toArray();
    }

    /**
     * @return TeamInfoObject
     */
    public function getTeamInfo(): TeamInfoObject
    {
        return new TeamInfoObject(
            thumbnail: $this->teamInfo["thumbnail"], title: $this->teamInfo["title"]
        );
    }

    /**
     * @return TeamNewspaperObject[]
     */
    public function getNewspaper(): array
    {
        return $this->newspapers->map(fn(News $news) => new TeamNewspaperObject(
            title: $news["title"],
            description: $news["description"],
            thumbnail: $news["image"], url: url($news["slug"] . "-n" . $news["news_id"].".html")
        ))->toArray();
    }
}