<?php

namespace App\ViewModels\Sites\Footer;

class FooterObject
{
    /**
     * @param string $copyright
     */
    public function __construct(
        private readonly string $copyright,
    )
    {
    }

    /**
     * @return string
     */
    public function getCopyright(): string
    {
        return $this->copyright;
    }
}