<?php

namespace App\ViewModels\Sites\Footer;

class FooterListViewModel
{
    public function __construct(
        readonly private object $footer
    )
    {
    }

    /**
     * @return FooterObject
     */
    public function getFooter(): object
    {
        return new FooterObject($this->footer->copyright);
    }
}