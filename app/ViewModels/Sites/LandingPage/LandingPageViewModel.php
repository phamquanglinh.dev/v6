<?php

namespace App\ViewModels\Sites\LandingPage;

use App\Models\Feedback;
use App\Models\LandingPage;
use App\ViewModels\Sites\Home\Object\FeedbackObject;
use App\ViewModels\Sites\LandingPage\Object\LandingPageObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class LandingPageViewModel
{
    public function __construct(
        private readonly Model|Builder    $landingPage,
        private readonly Collection|array $feedbacks,
    )
    {
    }

    /**
     * @return FeedbackObject[]
     */
    public function getFeedbacks(): array
    {
        return $this->feedbacks->map(
            fn(Feedback $feedback) => new FeedbackObject(
                id: $feedback['id'],
                title: $feedback['title'],
                avatar: str_replace("/assets/uploads/feedback/", "/", $feedback["avatar"]) ?? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png",
                type_feedback: $feedback['type_feedback'],
                content: $feedback['content'],
                position: $feedback['position'],
                link: $feedback['link'],
                name: $feedback['name'],
                note: $feedback['note']
            )
        )->toArray();
    }

    /**
     * @return LandingPageObject
     */
    public function getLandingPage(): LandingPageObject
    {
        /**
         * @var LandingPage $page
         */
        $page = $this->landingPage;
        return new LandingPageObject(
            title : $page->title,
            slug : $page->slug,
            thumbnail : "/assets/uploads/news/" . $page->thumbnail,
            description : $page["description"],
            body : $page->body, show : $page["show"],
            style : $page["style"] ?? "",
            script : $page["script"] ?? "",
            enable_popup : $page["enable_popup"],
            custom_form : $page["custom_form"]??""
        );
    }
}