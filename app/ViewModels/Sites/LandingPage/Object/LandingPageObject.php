<?php

namespace App\ViewModels\Sites\LandingPage\Object;

class LandingPageObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $slug,
        private readonly string $thumbnail,
        private readonly string $description,
        private readonly string $body,
        private readonly int    $show,
        private readonly string $style,
        private readonly string $script,
        private readonly string $enable_popup,
        private readonly string $custom_form
    )
    {
    }

    public function getEnablePopup(): string
    {
        return $this->enable_popup;
    }

    public function getCustomForm(): string
    {
        return $this->custom_form;
    }

    /**
     * @return int
     */
    public function getShow(): int
    {
        return $this->show;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @return string
     */
    public function getScript(): string
    {
        return $this->script;
    }

    /**
     * @return bool
     */
    public function isShow(): bool
    {
        return $this->show == 1;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}