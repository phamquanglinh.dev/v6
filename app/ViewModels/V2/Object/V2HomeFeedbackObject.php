<?php

namespace App\ViewModels\V2\Object;

class V2HomeFeedbackObject
{
    public function __construct(
        private readonly int $id,
        private readonly string $avatar,
        private readonly string $name,
        private readonly string $job_title,
        private readonly string $content,
        private readonly string $title,
    ) {}

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getJobTitle(): string
    {
        return $this->job_title;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}