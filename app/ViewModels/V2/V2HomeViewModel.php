<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 06/03/2024 1:59 pm
 */

namespace App\ViewModels\V2;

use App\Models\Customer;
use App\Models\Feedback;
use App\Models\News;
use App\Models\V2Partner;
use App\ViewModels\Admin\Object\V2PartnerObject;
use App\ViewModels\Sites\Home\Object\FeedbackObject;
use App\ViewModels\Sites\News\Object\NewsListObject;
use App\ViewModels\V2\Object\V2HomeFeedbackObject;
use Illuminate\Database\Eloquent\Collection;

class V2HomeViewModel
{
    public function __construct(
        private readonly Collection|array $feedbacks,
        private readonly Collection|array $newspapers,
        private readonly Collection|array $topPlacePartners,
        private readonly Collection|array $bottomPlacePartners,
    ) {}

    /**
     * @return V2PartnerObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 07/03/2024 9:00 am
     */
    public function getTopPlacePartners(): array
    {
        return $this->topPlacePartners->map(fn(V2Partner $partner) => new V2PartnerObject(
            partner_name : $partner['partner_name'],
            partner_logo : $partner['partner_logo']
        ))->toArray();
    }

    /**
     * @return V2PartnerObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 07/03/2024 9:00 am
     */
    public function getBottomPlacePartners(): array
    {
        return $this->bottomPlacePartners->map(fn(V2Partner $partner) => new V2PartnerObject(
            partner_name : $partner['partner_name'],
            partner_logo : $partner['partner_logo']
        ))->toArray();
    }

    /**
     * @return V2HomeFeedbackObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 06/03/2024 2:08 pm
     */
    public function getFeedbacks(): array
    {
        return $this->feedbacks->map(fn(Feedback $feedback) => new V2HomeFeedbackObject(
            id : $feedback['id'],
            avatar : $feedback['avatar'],
            name : $feedback['name'],
            job_title : $feedback['position'],
            content : $feedback['content'],
            title : $feedback['title']
        ))->toArray();
    }

    /**
     * @return NewsListObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  27/03/2024 15:07:21
     */
    public function getNewspaper(): array
    {
        return $this->newspapers->map(fn(News $news) => new NewsListObject(
            news_id : $news['news_id'],
            cate_title : $news->category?->title,
            cate_id : $news['cate_id'],
            cate_slug : $news->category?->slug,
            title : $news['title'],
            slug : $news['slug'],
            image : $news['image'],
            description : $news['description'],
            created_at : $news['created_at']
        ))->toArray();
    }
}