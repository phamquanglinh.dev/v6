<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 08/03/2024 11:39 am
 */

namespace App\ViewModels\V2;

use App\Models\V2Feature;
use App\Models\V2Package;
use App\Models\V2Pricing;
use App\ViewModels\Admin\Object\V2FeatureShowObject;
use App\ViewModels\Admin\Object\V2PackageShowObject;
use App\ViewModels\Admin\Object\V2PricingShowObject;
use Illuminate\Database\Eloquent\Collection;

use function Symfony\Component\String\b;

class V2PricingClientViewModel
{
    public function __construct(
        private readonly Collection|array $onCloudPackages,
        private readonly Collection|array $pricingCollection,
        private readonly Collection|array $featureCollection,
        private readonly Collection|array $onPremisePackages
    ) {}

    /**
     * @return V2FeatureShowObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  11/03/2024 09:48:06
     */
    public function getFeatureCollection(int $tab): array
    {
        return $this->featureCollection->map(function (V2Feature $v2Feature) {
            return new V2FeatureShowObject(
                id : $v2Feature['id'],
                feature_name : $v2Feature['feature_name'],
                feature_tooltip : $v2Feature['feature_tooltip'],
                startup_available : $v2Feature['startup_available'],
                professional_available : $v2Feature['professional_available'],
                enterprise_available : $v2Feature['enterprise_available'],
                tab : $v2Feature['tab']
            );
        })->filter(function (V2FeatureShowObject $feature) use ($tab) {
            return $feature->getTab() == $tab;
        })->toArray();
    }

    /**
     * @return V2PackageShowObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  11/03/2024 09:54:03
     */
    public function getOnPremisePackages(): array
    {
        return $this->onPremisePackages->map(function (V2Package $v2Package) {
            return new V2PackageShowObject(
                name : $v2Package['name'],
                description : $v2Package['description'],
                amount_per_month : $v2Package['amount_per_month'],
                user : $v2Package['user'],
                data : $v2Package['data'],
                benefit_text : $v2Package['benefit_text']
            );
        })->toArray();
    }

    public function getOnPremisePackage(int $index): V2PackageShowObject
    {
        return $this->getOnPremisePackages()[$index];
    }

    /**
     * @return V2PricingShowObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  11/03/2024 09:11:08
     */
    public function getPricingCollection(): array
    {
        return $this->pricingCollection->map(function (V2Pricing $pricing) {
            return new V2PricingShowObject(
                business_size : $pricing['business_size'],
                production_fee : $pricing['production_fee'],
                setup_fee : $pricing['setup_fee'],
                setup_number : $pricing['setup_number'],
                number_of_user : $pricing['number_of_user'],
                data_limit : $pricing['data_limit'],
                payment_cycle : $pricing['payment_cycle'],
                upgrade_fee : $pricing['upgrade_fee']
            );
        })->toArray();
    }

    public function definePricingTableHead(): array
    {
        return [
            'number_of_user' => 'Số người dùng theo gói (người)',
            'production_fee' => 'Phí phần mềm (đồng/năm)',
            'setup_fee' => 'Phí setup (đồng)',
            'setup_number' => 'Số buổi setup (online)',
            'data_limit' => 'Dung lượng data cho phép',
            'upgrade_fee' => 'Nâng cấp gói user (đồng/user/tháng)',
            'payment_cycle' => 'Thanh toán',
        ];
    }

    public function handleGetPricingTable(): array
    {
        $table = [
            'number_of_user' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'production_fee' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'setup_fee' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'setup_number' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'data_limit' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'upgrade_fee' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

            'payment_cycle' => [
                V2Pricing::SMALL_BUSINESS => '',
                V2Pricing::MEDIUM_BUSINESS => [],
                V2Pricing::BIG_BUSINESS => ''
            ],

        ];

        foreach ($this->getPricingCollection() as $pricingShowObject) {
            switch ($pricingShowObject->getBusinessSize()) {
                case V2Pricing::SMALL_BUSINESS:
                    $table['number_of_user'][$pricingShowObject->getBusinessSize()] = number_format($pricingShowObject->getNumberOfUser());
                    $table['production_fee'][$pricingShowObject->getBusinessSize()] = number_format($pricingShowObject->getProductionFee());
                    $table['setup_fee'][$pricingShowObject->getBusinessSize()] = number_format($pricingShowObject->getSetupFee());
                    $table['setup_number'][$pricingShowObject->getBusinessSize()] = number_format($pricingShowObject->getSetupNumber());
                    $table['data_limit'][$pricingShowObject->getBusinessSize()] = number_format($pricingShowObject->getDataLimit());
                    $table['upgrade_fee'][$pricingShowObject->getBusinessSize()] = $this->handlePricingNumberData($pricingShowObject->getUpgradeFee(), 'đ/user/tháng');
                    $table['payment_cycle'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getPaymentCycle();
                    break;
                case V2Pricing::MEDIUM_BUSINESS:
                    $table['number_of_user'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getNumberOfUser(), 'number_of_user');
                    $table['production_fee'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getProductionFee(), 'production_fee', '', 'tr1');
                    $table['setup_fee'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getSetupFee(), 'setup_fee', '', 'tr2');
                    $table['setup_number'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getSetupNumber(), 'setup_number', '', 'tr3');
                    $table['data_limit'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getDataLimit(), 'data_limit', '', 'tr4');
                    $table['upgrade_fee'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getUpgradeFee(), 'upgrade_fee', 'đ/user/tháng', 'tr5');
                    $table['payment_cycle'][V2Pricing::MEDIUM_BUSINESS] = $this->handlePricingArrayData($pricingShowObject->getPaymentCycle(), 'payment_cycle', ' tháng', 'tr6');
                    break;
                case V2Pricing::BIG_BUSINESS:
                    $table['number_of_user'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getNumberOfUser();
                    $table['production_fee'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getProductionFee();
                    $table['setup_fee'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getSetupFee();
                    $table['setup_number'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getSetupNumber();
                    $table['data_limit'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getDataLimit();
                    $table['upgrade_fee'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getUpgradeFee();
                    $table['payment_cycle'][$pricingShowObject->getBusinessSize()] = $pricingShowObject->getPaymentCycle();
                    break;
                default:
                    abort(404);
            }
        }

        return $table;
    }

    public function getTablePricingData(): array
    {
        $dataTable = [];

        $tableHeadings = [
            'number_of_user' => 'Số người dùng theo gói (người)',
            'data_limit' => 'Dung lượng data cho phép',
            'production_fee' => 'Phí phần mềm (đồng/năm)',
            'setup_fee' => 'Phí setup (đồng)',
            'setup_number' => 'Số buổi setup (online)',
            'upgrade_fee' => 'Nâng cấp gói user (đồng/user/tháng)',
            'payment_cycle' => 'Thanh toán',
        ];

        foreach ($tableHeadings as $key => $heading) {
            $dataTable[$key]['heading'] = $heading;
            $dataTable[$key]['row'] = $this->handleGetPricingTable()[$key];
        }

        return $dataTable;
    }

    public function getMobilePricingData(): array
    {
        $table = [];
        foreach ($this->getPricingCollection() as $pricingShowObject) {
            switch ($pricingShowObject->getBusinessSize()) {
                case V2Pricing::SMALL_BUSINESS:
                    $table[$pricingShowObject->getBusinessSize()]['number_of_user'] = number_format($pricingShowObject->getNumberOfUser());
                    $table[$pricingShowObject->getBusinessSize()]['production_fee'] = number_format($pricingShowObject->getProductionFee());
                    $table[$pricingShowObject->getBusinessSize()]['setup_fee'] = number_format($pricingShowObject->getSetupFee());
                    $table[$pricingShowObject->getBusinessSize()]['setup_number'] = number_format($pricingShowObject->getSetupNumber());
                    $table[$pricingShowObject->getBusinessSize()]['data_limit'] = number_format($pricingShowObject->getDataLimit());
                    $table[$pricingShowObject->getBusinessSize()]['upgrade_fee'] = $this->handlePricingNumberData($pricingShowObject->getUpgradeFee(), 'đ/user/tháng');
                    $table[$pricingShowObject->getBusinessSize()]['payment_cycle'] = $pricingShowObject->getPaymentCycle();
                    break;
                case V2Pricing::MEDIUM_BUSINESS:
                    $table[V2Pricing::MEDIUM_BUSINESS]['number_of_user'] = $this->handlePricingArrayData($pricingShowObject->getNumberOfUser(), 'number_of_user');
                    $table[V2Pricing::MEDIUM_BUSINESS]['production_fee'] = $this->handlePricingArrayData($pricingShowObject->getProductionFee(), 'production_fee', '', 'tr1');
                    $table[V2Pricing::MEDIUM_BUSINESS]['setup_fee'] = $this->handlePricingArrayData($pricingShowObject->getSetupFee(), 'setup_fee', '', 'tr2');
                    $table[V2Pricing::MEDIUM_BUSINESS]['setup_number'] = $this->handlePricingArrayData($pricingShowObject->getSetupNumber(), 'setup_number', '', 'tr3');
                    $table[V2Pricing::MEDIUM_BUSINESS]['data_limit'] = $this->handlePricingArrayData($pricingShowObject->getDataLimit(), 'data_limit', '', 'tr4');
                    $table[V2Pricing::MEDIUM_BUSINESS]['upgrade_fee'] = $this->handlePricingArrayData($pricingShowObject->getUpgradeFee(), 'upgrade_fee', 'đ/user/tháng', 'tr5');
                    $table[V2Pricing::MEDIUM_BUSINESS]['payment_cycle'] = $this->handlePricingArrayData($pricingShowObject->getPaymentCycle(), 'payment_cycle', ' tháng', 'tr6');
                    break;
                case V2Pricing::BIG_BUSINESS:
                    $table[$pricingShowObject->getBusinessSize()]['number_of_user'] = $pricingShowObject->getNumberOfUser();
                    $table[$pricingShowObject->getBusinessSize()]['production_fee'] = $pricingShowObject->getProductionFee();
                    $table[$pricingShowObject->getBusinessSize()]['setup_fee'] = $pricingShowObject->getSetupFee();
                    $table[$pricingShowObject->getBusinessSize()]['setup_number'] = $pricingShowObject->getSetupNumber();
                    $table[$pricingShowObject->getBusinessSize()]['data_limit'] = $pricingShowObject->getDataLimit();
                    $table[$pricingShowObject->getBusinessSize()]['upgrade_fee'] = $pricingShowObject->getUpgradeFee();
                    $table[$pricingShowObject->getBusinessSize()]['payment_cycle'] = $pricingShowObject->getPaymentCycle();
                    break;
                default:
                    abort(404);
            }
        }

        return $table;
    }

    /**
     * @return V2PackageShowObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  11/03/2024 08:36:26
     */
    public function getOnCloudPackages(): array
    {
        return $this->onCloudPackages->map(function (V2Package $v2Package) {
            return
                new V2PackageShowObject(
                    name : $v2Package['name'],
                    description : $v2Package['description'],
                    amount_per_month : $v2Package['amount_per_month'],
                    user : $v2Package['user'],
                    data : $v2Package['data'],
                    benefit_text : $v2Package['benefit_text']
                );
        })->toArray();
    }

    /**
     * @param string $name
     *
     * @return V2PackageShowObject|null
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  11/03/2024 08:36:23
     */
    public function getOnCloudPackage(string $name): ?V2PackageShowObject
    {
        return $this->getOnCloudPackages()[$name] ?? null;
    }

    private function handlePricingArrayData(string $arrayData, string $dataKey, string $suffix = '', $id = ''): array
    {
        try {
            $data = json_decode($arrayData, 1);
        } catch (\Exception $exception) {
            $data = [];
        }

        return [
            'key' => $dataKey,
            'data' => $data,
            'suffix' => $suffix,
            'id' => $id,
        ];
    }

    private function handlePricingNumberData(string $data, string $suffix = ""): string
    {
        $data = str_replace(',', '', $data);
        $data = str_replace($suffix, '', $data);

        return number_format($data) . $suffix;
    }
}