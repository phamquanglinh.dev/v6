<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 04/03/2024 4:23 pm
 */

namespace App\ViewModels\Admin;

use App\Models\V2Pricing;
use App\ViewModels\Admin\Object\V2PricingShowObject;
use Illuminate\Database\Eloquent\Collection;

class V2PricingShowViewModel
{
    public function __construct(
        private readonly Collection|array $pricingCollection
    ) {}

    /**
     * @return V2PricingShowObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 04/03/2024 4:50 pm
     */
    public function getPricingCollection(): array
    {
        return $this->pricingCollection->map(function (V2Pricing $pricing) {
            return new V2PricingShowObject(
                business_size : $pricing['business_size'],
                production_fee : $pricing['production_fee'],
                setup_fee : $pricing['setup_fee'],
                setup_number : $pricing['setup_number'],
                number_of_user : $pricing['number_of_user'],
                data_limit : $pricing['data_limit'],
                payment_cycle : $pricing['payment_cycle'],
                upgrade_fee : $pricing['upgrade_fee']
            );
        })->toArray();
    }
}