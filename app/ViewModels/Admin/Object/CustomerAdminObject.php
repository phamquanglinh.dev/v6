<?php

namespace App\ViewModels\Admin\Object;

class CustomerAdminObject
{
    /**
     * @param string $logo
     * @param string $name
     * @param int $id
     */
    public function __construct(
        private readonly string $logo,
        private readonly string $name,
        private readonly int $id,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return url("assets/uploads/media") . "/" . $this->logo;
    }
}