<?php

namespace App\ViewModels\Admin\Object;

class QuantityObject
{
    public function __construct(
        private readonly int $news,
        private readonly int $feedbacks,
        private readonly int $users,
        private readonly int $recruitments,
    )
    {

    }

    /**
     * @return int
     */
    public function getNews(): int
    {
        return $this->news;
    }

    /**
     * @return int
     */
    public function getFeedbacks(): int
    {
        return $this->feedbacks;
    }

    /**
     * @return int
     */
    public function getUsers(): int
    {
        return $this->users;
    }

    /**
     * @return int
     */
    public function getRecruitments(): int
    {
        return $this->recruitments;
    }
}