<?php

namespace App\ViewModels\Admin\Object;

class LandingPageObject
{
    public function __construct(
        private readonly string $body,
        private readonly string $title,
        private readonly int    $id,
        private readonly string $pageUrl,
        private readonly ?string $script,
        private readonly ?string $style
    )
    {
    }

    /**
     * @return string|null
     */
    public function getScript(): string|null
    {
        return $this->script;
    }

    /**
     * @return string|null
     */
    public function getStyle(): string|null
    {
        return $this->style;
    }

    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}