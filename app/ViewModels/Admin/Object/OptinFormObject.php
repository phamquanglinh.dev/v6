<?php

namespace App\ViewModels\Admin\Object;

class OptinFormObject
{
    public function __construct(
        private readonly string $name,
        private readonly string $body,
        private readonly string $position,
    )
    {
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }
}