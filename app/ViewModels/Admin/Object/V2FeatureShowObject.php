<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 4:29 pm
 */

namespace App\ViewModels\Admin\Object;

class V2FeatureShowObject
{
    public function __construct(
        private readonly int $id,
        private readonly string $feature_name,
        private readonly string $feature_tooltip,
        private readonly int $startup_available,
        private readonly int $professional_available,
        private readonly int $enterprise_available,
        private readonly int $tab
    ) {}

    public function getTab(): int
    {
        return $this->tab;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFeatureName(): string
    {
        return $this->feature_name;
    }

    public function getFeatureTooltip(): string
    {
        return $this->feature_tooltip;
    }

    public function getStartupAvailable(): int
    {
        return $this->startup_available;
    }

    public function getProfessionalAvailable(): int
    {
        return $this->professional_available;
    }

    public function getEnterpriseAvailable(): int
    {
        return $this->enterprise_available;
    }
}