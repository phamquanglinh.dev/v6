<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 04/03/2024 4:40 pm
 */

namespace App\ViewModels\Admin\Object;

use App\Models\V2Pricing;

class V2PricingShowObject
{
    public function __construct(
        private readonly string $business_size,
        private readonly string $production_fee,
        private readonly string $setup_fee,
        private readonly string $setup_number,
        private readonly string $number_of_user,
        private readonly string $data_limit,
        private readonly string $payment_cycle,
        private readonly string $upgrade_fee,
    ) {}

    public function getUpgradeFee(): string
    {
        return $this->upgrade_fee;
    }

    public function getBusinessSize(): string
    {
        return $this->business_size;
    }

    public function getBusinessTitle(): string
    {
        return V2Pricing::getBusinessDetail($this->business_size)['title'];
    }

    public function getBusinessDescription(): string
    {
        return V2Pricing::getBusinessDetail($this->business_size)['description'];
    }

    public function getProductionFee(): string
    {
        return $this->production_fee;
    }

    public function getSetupFee(): string
    {
        return $this->setup_fee;
    }

    public function getSetupNumber(): string
    {
        return $this->setup_number;
    }

    public function getNumberOfUser(): string
    {
        return $this->number_of_user;
    }

    public function getDataLimit(): string
    {
        return $this->data_limit;
    }

    public function getPaymentCycle(): string
    {
        return $this->payment_cycle;
    }
}