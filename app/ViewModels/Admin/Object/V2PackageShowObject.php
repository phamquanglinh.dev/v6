<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 10:19 am
 */

namespace App\ViewModels\Admin\Object;

class V2PackageShowObject
{
    public function __construct(
        private readonly string $name,
        private readonly string $description,
        private readonly string $amount_per_month,
        private readonly string $user,
        private readonly string $data,
        private readonly string $benefit_text,
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getAmountPerMonth(): string
    {
        return $this->amount_per_month;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function getBenefitText(): string
    {
        return $this->benefit_text;
    }
}