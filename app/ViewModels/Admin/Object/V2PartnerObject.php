<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 07/03/2024 8:54 am
 */

namespace App\ViewModels\Admin\Object;

class V2PartnerObject
{
    public function __construct(
        private readonly string $partner_name,
        private readonly string $partner_logo
    ) {}

    public function getPartnerName(): string
    {
        return $this->partner_name;
    }

    public function getPartnerLogo(): string
    {
        return $this->partner_logo;
    }
}