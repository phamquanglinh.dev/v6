<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 08/03/2024 9:56 am
 */

namespace App\ViewModels\Admin;

use App\Models\News;
use App\Models\V2Partner;
use App\ViewModels\Admin\Object\V2PartnerObject;
use Illuminate\Database\Eloquent\Collection;

class V2CrudHomeViewModel
{
    /**
     * @param Collection|array $newspapers
     */
    public function __construct(
        private readonly Collection|array $newspapers,
        private readonly Collection|array $topPartners,
        private readonly Collection|array $bottomPartners,
        private readonly array $delegateNewsIds,
    ) {}

    public function getDelegateNewsIds(): array
    {
        $default = [
            1 => 0,
            2 => 0,
            3 => 0
        ];
        $ids = $this->delegateNewsIds;

        return array_replace($default, $ids);
    }

    public function getNewspapersSelectList(): array
    {
        return $this->newspapers->mapWithKeys(function (News $news) {
            return [$news['news_id'] => $news['title']];
        })->toArray();
    }

    /**
     * @return V2PartnerObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 10:18:51
     */
    public function getTopPartners(): array
    {
        return $this->topPartners->map(fn(V2Partner $partner) => new V2PartnerObject(
            partner_name : $partner['partner_name'],
            partner_logo : $partner['partner_logo'],
        ))->toArray();
    }

    /**
     * @return V2PartnerObject[]
     *
     * @author: Phạm Quang Linh <linhpq@getflycrm.com>
     * @since:  08/03/2024 10:19:59
     */
    public function getBottomPartners(): array
    {
        return $this->bottomPartners->map(fn(V2Partner $partner) => new V2PartnerObject(
            partner_name : $partner['partner_name'],
            partner_logo : $partner['partner_logo']
        ))->toArray();
    }
}