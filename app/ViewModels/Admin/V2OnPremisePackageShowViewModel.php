<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 10:17 am
 */

namespace App\ViewModels\Admin;

use App\Models\V2Package;
use App\ViewModels\Admin\Object\V2PackageShowObject;
use Illuminate\Database\Eloquent\Collection;

class V2OnPremisePackageShowViewModel
{
    public function __construct(
        private readonly Collection|array $packages
    ) {}

    /**
     * @return V2PackageShowObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 10:30 am
     */
    public function getMappedPackageByName(): array
    {
        return $this->packages->mapWithKeys(function (V2Package $package) {
            return [
                $package['name'] => new V2PackageShowObject(
                    name : $package['name'],
                    description : $package['description'],
                    amount_per_month : $package['amount_per_month'],
                    user : $package['user'],
                    data : $package['data'],
                    benefit_text : $package['benefit_text']
                )
            ];
        })->toArray();
    }

    /**
     * @return V2PackageShowObject
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 10:30 am
     */
    public function getMasterPackage(): V2PackageShowObject
    {
        return $this->getMappedPackageByName()['Master'];
    }

    /**
     * @return V2PackageShowObject
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 10:32 am
     */
    public function getEnterprisePackage(): V2PackageShowObject
    {
        return $this->getMappedPackageByName()['Enterprise'];
    }
}