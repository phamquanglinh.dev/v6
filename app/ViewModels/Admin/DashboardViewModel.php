<?php

namespace App\ViewModels\Admin;

use App\ViewModels\Admin\Object\QuantityObject;

class DashboardViewModel
{
    public function __construct(
        private readonly array $quantity
    )
    {
    }

    /**
     * @return QuantityObject
     */
    public function getQuantity(): QuantityObject
    {
        $quantity = $this->quantity;
        return new QuantityObject(
            news: $quantity["news"] ?? 0,
            feedbacks: $quantity["feedback"] ?? 0,
            users: $quantity["users"],
            recruitments: $quantity["recruitments"] ?? 0
        );
    }
}