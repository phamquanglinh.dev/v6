<?php

namespace App\ViewModels\Admin\Shortcut;

use App\Models\LandingPage;
use App\Models\News;
use App\Models\PageFeature;
use App\Models\Solution;
use App\Models\SubPage;
use App\ViewModels\Admin\Shortcut\Object\ShortcutPageObject;
use Illuminate\Database\Eloquent\Collection;

class ShortcutViewModel
{
    public function __construct(
        private readonly Collection|array $popularLandingPage,
        private readonly Collection|array $latestLandingPage,
        private readonly Collection|array $popularPageFeature,
        private readonly Collection|array $latestPageFeature,
        private readonly Collection|array $popularSolution,
        private readonly Collection|array $latestSolution,
        private readonly Collection|array $popularSubPage,
        private readonly Collection|array $latestSubpage,
        private readonly Collection|array $popularNewspaper,
        private readonly Collection|array $latestNewspaper
    )
    {
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getPopularNewspaper(): array
    {
        return $this->popularNewspaper->map(fn(News $news) => new ShortcutPageObject(
            title: $news['title'],
            url: $news->getPreviewUrl(),
            views: $news["total_view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getLatestNewspaper(): array
    {
        return $this->latestNewspaper->map(fn(News $news) => new ShortcutPageObject(
            title: $news['title'],
            url: $news->getPreviewUrl(),
            views: $news["total_view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getPopularLandingPage(): array
    {
        return $this->popularLandingPage->map(fn(LandingPage $landingPage) => new ShortcutPageObject(
            title: $landingPage['title'],
            url: url($landingPage->getPageUrl() ?? "#"),
            views: $landingPage["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getLatestLandingPage(): array
    {
        return $this->latestLandingPage->map(fn(LandingPage $landingPage) => new ShortcutPageObject(
            title: $landingPage['title'],
            url: url($landingPage->getPageUrl() ?? "#"),
            views: $landingPage["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getPopularPageFeature(): array
    {
        return $this->popularPageFeature->map(fn(PageFeature $pageFeature) => new ShortcutPageObject(
            title: $pageFeature['title'],
            url: url($pageFeature->getPageUrl() ?? "#"),
            views: $pageFeature["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getLatestPageFeature(): array
    {
        return $this->latestPageFeature->map(fn(PageFeature $pageFeature) => new ShortcutPageObject(
            title: $pageFeature['title'],
            url: url($pageFeature->getPageUrl() ?? "#"),
            views: $pageFeature["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getPopularSolution(): array
    {
        return $this->popularSolution->map(fn(Solution $solution) => new ShortcutPageObject(
            title: $solution['title'],
            url: url($solution->getPreviewLink() ?? "#"),
            views: $solution["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getLatestSolution(): array
    {
        return $this->latestSolution->map(fn(Solution $solution) => new ShortcutPageObject(
            title: $solution['title'],
            url: url($solution->getPreviewLink() ?? "#"),
            views: $solution["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getPopularSubPage(): array
    {
        return $this->popularSubPage->map(fn(SubPage $subPage) => new ShortcutPageObject(
            title: $subPage['page_title'],
            url: url($subPage->getPreviewLinkUrl() ?? "#"),
            views: $subPage["view"])
        )->toArray();
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getLatestSubpage(): array
    {
        return $this->latestSubpage->map(fn(SubPage $subPage) => new ShortcutPageObject(
            title: $subPage['page_title'],
            url: url($subPage->getPreviewLinkUrl() ?? "#"),
            views: $subPage["view"])
        )->toArray();
    }

    public function getLabel($key): string
    {
        $labels = [
            0 => 'Tin tức nổi bật',
            1 => 'Tin tức mới nhất',
            2 => 'Landing Page nổi bật',
            3 => 'Landing Page mới nhất',
            4 => 'Trang tính năng nổi bật',
            5 => 'Trang tính năng mới nhất',
            6 => 'Trang giải pháp nổi bật',
            7 => 'Trang giải pháp mới nhất',
            8 => 'Trang con nổi bật',
            9 => 'Trang con mới nhất',

        ];
        return $labels[$key];
    }

    /**
     * @return ShortcutPageObject[]
     */
    public function getData($key): array
    {
        $labels = [
            0 => $this->getPopularNewspaper(),
            1 => $this->getLatestNewspaper(),
            2 => $this->getPopularLandingPage(),
            3 => $this->getLatestLandingPage(),
            4 => $this->getPopularPageFeature(),
            5 => $this->getLatestPageFeature(),
            6 => $this->getPopularSolution(),
            7 => $this->getLatestSolution(),
            8 => $this->getPopularSubPage(),
            9 => $this->getLatestSubpage(),
        ];
        return $labels[$key];
    }
}