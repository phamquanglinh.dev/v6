<?php

namespace App\ViewModels\Admin\Shortcut\Object;

use Illuminate\Support\Str;

class ShortcutPageObject
{
    public function __construct(
        private readonly string $title,
        private readonly string $url,
        private readonly ?int   $views,
    )
    {
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return Str::limit($this->title, 80);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getViews(): string
    {
        return number_format($this->views ?? 0);
    }
}