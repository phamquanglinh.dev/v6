<?php

namespace App\ViewModels\Admin;

use App\Models\LandingPage;
use App\Models\RegisterForm;
use App\ViewModels\Admin\Object\LandingPageObject;
use App\ViewModels\Admin\Object\OptinFormObject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class PageBuilderViewModel
{
    public function __construct(
        private readonly Model|Builder    $landingPage,
        private readonly Collection|array $forms
    )
    {
    }

    /**
     * @return LandingPageObject
     */
    public function getLandingPage(): LandingPageObject
    {
        /**
         * @var LandingPage $page
         */
        $page = $this->landingPage;
        return new LandingPageObject(body: $page["body"], title: $page["title"], id: $page["id"], pageUrl: $page->getPageUrl(), script: $page["script"], style: $page["style"]);
    }

    /**
     * @return OptinFormObject[]
     */
    public function getForms(): array
    {
        return $this->forms->map(fn(RegisterForm $form) => new OptinFormObject(name: $form["name"], body: $form["link"], position: $form["position"]))->toArray();
    }
}