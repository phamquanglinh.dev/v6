<?php
/**
 * @author Phạm Quang Linh <linhpq@getflycrm.com>
 * @since 05/03/2024 4:29 pm
 */

namespace App\ViewModels\Admin;

use App\Models\Feature;
use App\Models\V2Feature;
use App\ViewModels\Admin\Object\V2FeatureShowObject;
use Illuminate\Database\Eloquent\Collection;

class V2FeatureShowViewModel
{
    public function __construct(
        private readonly Collection|array $features
    ) {}

    /**
     * @return V2FeatureShowObject[]
     * @author Phạm Quang Linh <linhpq@getflycrm.com>
     * @since 05/03/2024 4:33 pm
     */
    public function getFeatures(): array
    {
        return $this->features->map(function (V2Feature $feature) {
            return new V2FeatureShowObject(
                id : $feature['id'],
                feature_name : $feature['feature_name'],
                feature_tooltip : $feature['feature_tooltip'],
                startup_available : $feature['startup_available'],
                professional_available : $feature['professional_available'],
                enterprise_available : $feature['enterprise_available'],tab: $feature['tab']
            );
        })->toArray();
    }
}