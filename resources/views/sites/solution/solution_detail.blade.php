@php
    use App\ViewModels\Sites\Solution\SolutionDetailViewModel;
    /**
    * @var SolutionDetailViewModel $solutionDetailViewModel
    */
    $solution = $solutionDetailViewModel->getSolution();
@endphp
@extends("sites.shared.layout")

@section("title")
    {{$solution->getTitle()}}
@endsection
@section("content")
    <style>
        @media (max-width: 768px) {
            tr{
                display: grid;
            }
            #solution-content img{
                width: 100%!important;
                height: auto!important;
                padding: 1rem;
            }
        }
        h1 {
            font-size: 2rem!important;
            color: #dc6803!important;
        }

        #solution-content ul {
            list-style: disc !important;
        }

        #solution-content ol {
            list-style: auto !important;
        }

        #solution-content h2 {
            font-size: 1.5rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #solution-content h3 {
            font-size: 1rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #solution-content a {
            color: #0b4d75;
        }

        #solution-content p{
            margin-bottom: 0.5rem;
        }
    </style>
    <div class="container py-10 w-full px-2" style="margin: auto">
        <div class="max-w-3/4 m-auto">
            <h1 class="text-primary-orange500 text-3xl font-bold">
                {{$solution->getTitle()}}
            </h1>
            <div class="desctiption mt-5 mb-5 text-md font-semibold italic">
                {!! $solution->getDescription() !!}
            </div>
            <div id="solution-content" class="content-wrapper content ">
                {!! $solution->getContent() !!}
            </div>
        </div>
    </div>
@endsection