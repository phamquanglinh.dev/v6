<div class="2xl:px-[15rem] xl:px-[10rem] lg:px-[5rem] box-banner bg-banner py-10">
    <div class="banner-action">
        <div class="banners mobile">
            <p class="banner-title">{{$homeViewModel->getSite()['partners']}}++</p>
            <p class="banner-des">Doanh nghiệp sử dụng</p>
        </div>
        <div class="banners mobile banner-center">
            <p class="banner-title">{{$homeViewModel->getSite()['profession']}}++</p>
            <p class="banner-des">Ngành nghề</p>
        </div>
        <div class="banners mobile">
            <p class="banner-title">{{$homeViewModel->getSite()['experience_years']}}</p>
            <p class="banner-des">Năm kinh nghiệm</p>
        </div>
    </div>
    <div>
        <a href="{{route("sites.trial")}}"
           class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase">
            Trải nghiệm thử
        </a>
    </div>
</div>