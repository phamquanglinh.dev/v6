@php
    use App\ViewModels\Admin\Shortcut\ShortcutViewModel;
    /**
* @var ShortcutViewModel $shortcutViewModel
 */

@endphp
<div class="container-fluid">
    <div class="row">
        @for($i=0;$i<=9;$i++)
            <div class="col-md-6 mb-3">
                <div class="border py-2 rounded h-100 bg-white">
                    <div class="text-center mb-2 font-weight-bold">{{$shortcutViewModel->getLabel($i)}}</div>
                    <div class="mb-1 px-4">
                        @foreach($shortcutViewModel->getData(key: $i) as $stt => $shortcut)
                            <div class="d-flex w-100 justify-content-between mb-1 ">
                                <div>
                                    <a href="{{$shortcut->getUrl()}}" target="_blank" class="text-dark nav-link m-0">{{$shortcut->getTitle()}}</a>
                                </div>
                                <div class="text-muted d-flex justify-content-between align-items-center">
                                    <i class="mr-1 la la-eye"></i>
                                    <span class="">{{$shortcut->getViews()}}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endfor
    </div>
</div>