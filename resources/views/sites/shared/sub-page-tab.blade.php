@php
    use App\ViewModels\Sites\Home\SubPageViewModel;
     /**
 * @var SubPageViewModel $subPageViewModel
  */
     $tabs = $subPageViewModel->getTabs();
@endphp
<style>

    #myTabContent ul {
        list-style: disc;
    }

    #myTabContent ol {
        list-style: auto;
    }

    .box-detail .text-blue-600 {
        border-color: orange !important;
    }

    .sub-tab-list {
        list-style: none;
    }
</style>
<div class="box-detail">
    <div class="flex flex-col items-center">
        <p class="detail-title">{{$page->getTitle()}}</p>
        <div class="mb-4 border-b border-gray-200 dark:border-gray-700">
            <ul class="flex flex-wrap gap-4 -mb-px text-sm font-medium text-center" id="myTab"
                data-tabs-toggle="#myTabContent" role="tablist">
                @foreach($tabs as $tab )
                    <li class="sub-tab-list" role="presentation">
                        <button class="group button-detail {{$loop->first?"active":""}}" id="{{$tab->getId()}}-tab"
                                data-tabs-target="#{{$tab->getId()}}"
                                type="button"
                                style="width: 6em"
                                role="tab" aria-controls="profile">
                            <img class="icon img-detail {{$loop->first?"hidden":""}}"
                                 src="{{url($tab->getIcon())}}">
                            <img class="{{!$loop->first?"hidden":""}} icon_alt img-detail {{$loop->first?"active":""}}"
                                 src="{{url($tab->getIconAlt())}}">
                        </button>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div id="myTabContent" class="px-2">
        @foreach($tabs as $tab)
            <div class="des-detail" id="{{$tab->getId()}}" role="tabpanel" aria-labelledby="{{$tab->getId()}}-tab">
                <div class="md:w-1/2 w-full">
                    <p class="pb-4 text-btn-color font-bold text-2xl mt-3">{{$tab->getTitle()}}</p>
                    <div class="p-2 py-10">
                        {!! $tab->getDes() !!}
                    </div>
                </div>
                <div class="md:w-1/2 w-full">
                    <div class="flex justify-end">
                        <img src="{{url($tab->getThumbnail())}}" class="w-full">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<style>
    .img-detail {
        transition: 0.4s;
    }

    .icon_alt {
        background: #dc6803 !important;
    }

    .button-detail:hover >
    .icon_alt {
        display: block;
    }

    .button-detail:hover >
    .icon {
        display: none;

    }


    .button-detail {
        transition: 0.4s;
    }
</style>
@push("after_scripts")
    <script>
        $(document).ready(function () {
            $(".button-detail").click((e) => {
                $(".active").find(".icon").removeClass("hidden")
                $(".active").find(".icon_alt").addClass("hidden")
                $(".active").removeClass("active")
                const selectBtn = $(e.currentTarget)
                selectBtn.addClass("active")
                selectBtn.find(".icon").addClass("hidden")
                selectBtn.find(".icon_alt").removeClass("hidden")
            })
        })
    </script>
@endpush