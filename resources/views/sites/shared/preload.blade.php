<div class="preload transition-all">
    <img src="{{asset("site/images/load.gif")}}" alt="preload">
</div>
<style>
    .preload {
        position: absolute;
        width: 100%;
        height: 100vh;
        z-index: 1000;
        background: white;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    #content {
        display: none;
    }

    #navbar {
        display: none;
    }
</style>
@push("after_scripts")
    <script>
        $(window).on('load', function(){
            setTimeout(()=>{
                $(".preload").css({"zIndex": -9999, "opacity": 0})
                $("#content").css("display", "block")
                $("#navbar").css("display", "block")
            },100)
        });
    </script>
@endpush