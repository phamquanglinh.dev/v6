<head>
    @yield("newspaper_seo")
    <title>@yield("title")</title>
</head>
@yield("content")
