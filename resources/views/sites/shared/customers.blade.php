@php
    use App\ViewModels\Sites\Home\HomeViewModel;
    /**
* @var HomeViewModel $homeViewModel
 */
@endphp
<div class="2xl:px-[25rem] xl:px-[15rem] lg:px-[5rem]">
    <p class="title-box">Khách hàng tiêu biểu</p>
    <div class="box-customer-group flex items-center justify-between 2xl:mt-10 mt-5 ">
        @foreach($homeViewModel->getCustomers() as $customer)
            <div class="text-center uppercase p-3">
                <img alt="customer-logo" src="{{url($customer->getLogo())}}" style="max-width: 180px"
                     class="rounded border mb-1 rounded-lg shadow-lg">
                <div class="text-blue-900 font-bold mt-3">
                    {{$customer->getName()}}
                </div>
            </div>
            @if(($loop->index+1)%4==0 && !$loop->last)
    </div>
    <div class="box-customer-group box-customer-group flex items-center justify-between 2xl:mt-10 mt-5">
        @endif
        @endforeach
    </div>
</div>
<div class="box-slide p-[10rem]">
    <div id="controls-carousel" class="relative w-full" data-carousel="slide">
        <div class="relative h-44 overflow-hidden rounded-lg">
            @foreach($homeViewModel->getAllCustomers() as $index => $customer)
                @if($index%6==0)
                    <div class="hidden duration-700" data-carousel-item="@if($index==0) active @endif">
                        <div class="box-slide-detail">
                            @endif
                            <div class="lg:w-1/3 w-full p-3 bg-white rounded-md mb-10  xl:mb-4">
                                <div class="flex flex-col items-center justify-center gap-2">
                                    <img alt="logo" src="{{$customer->getLogo()}}" class="rounded shadow-lg"
                                         style="max-width: 150px">
                                    <p class="text-blue-color font-semibold uppercase text-xs">{{$customer->getName()}}</p>
                                </div>
                            </div>
                            @if(($index+1)%6==0 || $index+1 == count($homeViewModel->getAllCustomers()))
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <button type="button" class="btn-next left-0 bg-white" data-carousel-prev>
            <img src="{{url("site/images/left.png")}}" alt="left">
        </button>
        <button type="button" class="btn-next right-0 bg-white" data-carousel-next>
            <img src="{{url("site/images/right.png")}}" alt="right">
        </button>
    </div>
</div>