@php
    use App\ViewModels\Sites\Sidebar\RightSidebarViewModel;
    /**
* @var RightSidebarViewModel $rightSidebarViewModel
 */
@endphp
<div class="w-1/4 mobile">
    <div class="flex gap-6 flex-col">
        @foreach($rightSidebarViewModel->getBanners() as $banner)
            <a href="{{url($banner->getUrl())}}">
                <img src="{{url($banner->getImage())}}" style="width: 100%">
            </a>
        @endforeach
    </div>
    <div class="flex flex-col overflow-y-scroll" style="height: 500px;">
        <p class="text-primary-blue500 font-bold pt-3">BÀI VIẾT NỔI BẬT</p>
        @foreach($rightSidebarViewModel->getTopNews() as $news)
            <div class="box-news">
                <div class="flex gap-1">
                    <p class="text-neutral-600">{{$news->getUpdatedAt()}}</p> -
                    <a href="{{url("i".$news->getCategoryId()."-".\Illuminate\Support\Str::slug($news->getCategoryName()).".html")}}"
                       class="text-primary-blue500 font-semibold">
                        {{$news->getCategoryName()}}</a>
                </div>
                <p>
                    <a href="{{url(\Illuminate\Support\Str::slug($news->getCategoryName())."/".\Illuminate\Support\Str::slug($news->getTitle())."-n-".$news->getId().".html")}}">{{$news->getTitle()}}</a>
                </p>
            </div>
        @endforeach
    </div>
</div>