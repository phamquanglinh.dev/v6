<!DOCTYPE html>
<html lang="en">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W9XPPRX9');</script>
<!-- End Google Tag Manager -->
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="google-site-verification" content="hbu4NVXkkwmENuShmg5td1dmxCwNWR7hyhB_K9Cya94"/>
    <meta name="og:url" content="{{url()->full()}}">
    @if(isset($newspaper_seo))
        @yield("newspaper_seo")
    @else
        @include("sites.shared.meta-seo")
    @endif
    <link rel="index" title="Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện">
    <title>@yield("title") - Getfly.vn</title>

    <link type="image/x-icon" rel="shortcut icon" href="{{url("site/images/favicon.ico")}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,500;0,600;0,700;1,300;1,400&family=Poppins:wght@300;400;600;700&display=swap">
    <link rel="stylesheet" href="{{url("site/build/tailwind.css")}}">
    <link rel="stylesheet" href="{{url("site/build/app.css")}}"/>
    {{--    <script src="https://cdn.tailwindcss.com"></script>--}}
    <style>

        .bg-banner {
            background-image: url("{{url("site/images/banner1.webp")}}");
            padding: 1.5rem;
        }

        .bg-banner2 {
            background-image: url("{{url("site/images/banner2.webp")}}");
            padding: 1.5rem
        }

        .bg-banner3 {
            background-image: url("{{url("site/images/banner3.webp")}}");
        }

        .bg-banner4 {
            background-image: url("{{url("site/images/banner4.webp")}}");
        }

        .bg-footer {
            background-color: #2A2B2D;
        }
    </style>

    @stack("after_style")
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5SWLHNK');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3RDFHX6J42"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'G-3RDFHX6J42');
    </script>
    <script>
        !function (w, d, t) {
            w.TiktokAnalyticsObject = t;
            var ttq = w[t] = w[t] || [];
            ttq.methods = ["page", "track", "identify", "instances", "debug", "on", "off", "once", "ready", "alias", "group", "enableCookie", "disableCookie"], ttq.setAndDefer = function (t, e) {
                t[e] = function () {
                    t.push([e].concat(Array.prototype.slice.call(arguments, 0)))
                }
            };
            for (var i = 0; i < ttq.methods.length; i++) ttq.setAndDefer(ttq, ttq.methods[i]);
            ttq.instance = function (t) {
                for (var e = ttq._i[t] || [], n = 0; n < ttq.methods.length; n++) ttq.setAndDefer(e, ttq.methods[n]);
                return e
            }, ttq.load = function (e, n) {
                var i = "https://analytics.tiktok.com/i18n/pixel/events.js";
                ttq._i = ttq._i || {}, ttq._i[e] = [], ttq._i[e]._u = i, ttq._t = ttq._t || {}, ttq._t[e] = +new Date, ttq._o = ttq._o || {}, ttq._o[e] = n || {};
                var o = document.createElement("script");
                o.type = "text/javascript", o.async = !0, o.src = i + "?sdkid=" + e + "&lib=" + t;
                var a = document.getElementsByTagName("script")[0];
                a.parentNode.insertBefore(o, a)
            };

            ttq.load('CM0GD6JC77U7MRPGKMJG');
            ttq.page();
        }(window, document, 'ttq');
    </script>
</head>

<body>
@include("sites.shared.preload")
@include("sites.shared.navbar")
<div id="content">
    @yield("content")
</div>
@include("sites.shared.popup")
@include("sites.shared.footer")

</body>

<script type="text/javascript">
    _linkedin_partner_id = "5637780";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script>
<script type="text/javascript">
    (function (l) {
        if (!l) {
            window.lintrk = function (a, b) {
                window.lintrk.q.push([a, b])
            };
            window.lintrk.q = []
        }
        var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";
        b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);
    })(window.lintrk);
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt=""
         src="https://px.ads.linkedin.com/collect/?pid=5637780&fmt=gif"/>
</noscript>

<script src="{{url("site/jquery.min.js")}}"></script>
<script src="{{url("site/flowbite.min.js")}}"></script>
<link href="{{url("site/build/flowbite.min.css")}}" rel="stylesheet">
<script>
    !function (s, u, b, i, z) {
        var o, t, r, y;
        s[i] || (s._sbzaccid = z, s[i] = function () {
            s[i].q.push(arguments)
        }, s[i].q = [], s[i]("setAccount", z), r = ["widget.subiz.net", "storage.googleapis" + (t = ".com"), "app.sbz.workers.dev", i + "a" + (o = function (k, t) {
            var n = t <= 6 ? 5 : o(k, t - 1) + o(k, t - 3);
            return k !== t ? n : n.toString(32)
        })(20, 20) + t, i + "b" + o(30, 30) + t, i + "c" + o(40, 40) + t], (y = function (k) {
            var t, n;
            s._subiz_init_2094850928430 || r[k] && (t = u.createElement(b), n = u.getElementsByTagName(b)[0], t.async = 1, t.src = "https://" + r[k] + "/sbz/app.js?accid=" + z, n.parentNode.insertBefore(t, n), setTimeout(y, 2e3, k + 1))
        })(0))
    }(window, document, "script", "subiz", "acrrnsgtisduismngghn")

</script>
@stack("after_scripts")
<style>
    /*body div#cprrnshgzpuiizbycyzvk .chat-button--right {*/
    /*    display: none !important;*/
    /*}*/

    /*#cprrnshgzpuiizbycyzvk .widget-preview {*/
    /*    margin-bottom: 42px !important;*/
    /*    margin-right: 10px !important;*/
    /*}*/
</style>
<script>
    const openChat = () => {
        $(".chat-button--img").click()
    }
</script>
</html>