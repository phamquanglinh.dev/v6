@php
    use App\ViewModels\Sites\Home\HomeViewModel;

    /**
     * @var HomeViewModel $homeViewModel
     */
@endphp
<style>
    .hover\:opacity-30:hover {
        opacity: 30% !important;
    }
</style>
<div id="carousel-wrapper" class="relative">
    <div id="carousel-inner" class="relative w-full overflow-hidden ">
        @foreach($homeViewModel->getBanners() as $banner)
            <div id="banner_{{$loop->index+1}}"
                 class="carousel-item relative transition-all  {{!$loop->first?" hidden":"active"}}"
                 style="transition-delay: 1s">
                <img class="w-full"
                     src="{{!$loop->first?"":url($banner->getImage())}}"
                     data-src="{{url($banner->getImage())}}" alt="">
            </div>
        @endforeach

    </div>
    <a class="hover:bg-gray-900 hover:opacity-30 cursor-pointer transition-all absolute top-0 left-0 justify-center items-center h-full flex px-2"
       id="banner-prev">
        <img class="cursor-pointer" alt="#" style="width: 3em ;height: 3em"
             src="https://cdn-icons-png.flaticon.com/512/318/318477.png">
    </a>
    <a class="hover:bg-gray-900 hover:opacity-30 cursor-pointer transition-all absolute top-0 right-0 justify-center items-center h-full flex px-2"
       id="banner-next">
        <img class="cursor-pointer" alt="#" style="width: 3em ;height: 3em"
             src="https://cdn-icons-png.flaticon.com/512/181/181669.png">
    </a>
</div>
@push("after_scripts")
    <style>
        #banner-next img, #banner-prev img {
            opacity: 1 !important;
            filter: invert();

        }

        .carousel-right {
            position: relative;
            animation: slider-right 1s;
        }

        .carousel-left {
            position: relative;
            animation: slider-left 1s;
        }


        @keyframes slider-right {
            0% {
                right: -20px;
                opacity: 0;
            }
            100% {
                right: 0;
                opacity: 1;
            }

        }

        @keyframes slider-left {
            0% {
                left: -20px;
                opacity: 0;

            }
            100% {
                left: 0;
                opacity: 1;
            }

        }
    </style>
    <script>
        let banners = parseInt("{{count($homeViewModel->getBanners())}}")
        let currentBanner = 1
        const showBanner = () => {

            $(".carousel-item").addClass("hidden")
            const currentItem = $("#banner_" + currentBanner)
            const imgSrc = currentItem.find("img").attr("data-src")
            currentItem.find("img").attr("src", imgSrc)
            currentItem.removeClass("hidden")
        }
        $("#banner-prev").click(() => {

            currentBanner--

            if (currentBanner < 1) {
                currentBanner = banners
            }
            $(".carousel-item").removeClass("carousel-left")
            $(".carousel-item").addClass("carousel-right")
            showBanner()

        })
        $("#banner-next").click(() => {
            currentBanner++
            if (currentBanner > banners) {
                currentBanner = 1
            }
            $(".carousel-item").removeClass("carousel-right")
            $(".carousel-item").addClass("carousel-left")
            showBanner()

        })
        setInterval(() => {
            currentBanner++
            if (currentBanner > banners) {
                currentBanner = 1
            }
            $(".carousel-item").removeClass("carousel-right")
            $(".carousel-item").addClass("carousel-left")
            showBanner()
        }, 10000)
    </script>
@endpush