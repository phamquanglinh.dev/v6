@php
    use App\ViewModels\Sites\Home\HomeViewModel;
    /**
* @var HomeViewModel $homeViewModel
 */
@endphp
<div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] my-5">
    <p class="title-box">Tính năng nổi bật của phần mềm getfly crm</p>
    <div class="feature-header p-2" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
        <div class="feature-group">
            @foreach($homeViewModel->getFeatures() as $feature)
                <div class="feature-box box-animation" id="{{$feature["id"]}}-tab"
                     data-tabs-target="#t-{{$feature["id"]}}" type="button"
                     role="tab" aria-controls="t-{{$feature["id"]}}" aria-selected="false">
                    <p>
                        <img src="{{$feature["icon"]}}">
                        {{$feature["header"]}}
                    </p>
                </div>
                @if(($loop->index+1)%3==0 && !$loop->last)
        </div>
        <div class="feature-group">
            @endif
            @endforeach
        </div>
    </div>
    <div id="myTabContent" class="p-1">
        @foreach($homeViewModel->getFeatures() as $feature)
            <div class="feature-content my-5" id="t-{{$feature["id"]}}" role="tabpanel"
                 aria-labelledby="{{$feature["id"]}}-tab">
                <div class="md:w-1/2 w-full">
                    <img src="{{url($feature["thumbnail"])}}">
                </div>
                <div class="md:w-1/2 w-full">
                    <p class="font-bold uppercase text-btn-color text-2xl mb-3">{{$feature["title"]}}
                    </p>
                    <p class="show-laptop mb-3">
                        <span class="hidden full-content">
                            {{$feature["description"]}}
                        </span>
                        {{\Illuminate\Support\Str::limit($feature["description"],290)}}
                        @if(strlen($feature["description"]) >= 350)
                            <span class="show-more cursor-pointer text-blue-900">
                                Xem thêm
                            </span>
                        @endif
                    </p>
                    <p class="show-pc mb-3">
                        {{$feature["description"]}}
                    </p>
                    <div class="flex justify-start items-end">
                        <a href="{{$feature["url"]}}" class="btn text-white p-2 px-4 rounded-2xl bg-blue-color">Xem chi
                            tiết</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@push("after_scripts")
    <style>
        .box-animation {
            transition: 0.2s;
        }

        .img-active {
            filter: invert(1);
        }

        .show-laptop {
            display: block;
        }

        .show-pc {
            display: none;
        }

        @media (min-width: 1336px) {
            .show-pc {
                display: block;
            }

            .show-laptop {
                display: none;
            }
        }
    </style>
    <script>
        $(".show-more").click((e) => {
            const element = $(e.currentTarget)
            element.parent().removeClass("show-laptop")
            const full_text = element.parent().find(".full-content").text()
            element.parent().text(full_text)
        })
        $("#1-tab").click(function () {
            const currentActiveBox = $(".feature-active")
            currentActiveBox.addClass("feature-box")
            currentActiveBox.removeClass("feature-active")
            $("#1-tab").addClass("feature-active")
            $("#1-tab").removeClass("feature-box")
            $("#1-tab").find("img").addClass("img-active")
        })
        $($(".feature-box")[0]).addClass("feature-active")
        $($(".feature-box")[0]).removeClass("feature-box")
        $(document).ready(function () {
            $(".feature-box").click((function (e) {
                const currentActiveBox = $(".feature-active")
                currentActiveBox.addClass("feature-box")
                currentActiveBox.removeClass("feature-active")
                currentActiveBox.find("img").removeClass("img-active")
                const box = $(e.currentTarget)
                console.log(box)
                box.addClass("feature-active")
                box.removeClass("feature-box")
                box.find("img").addClass("img-active")
            }))
        })
    </script>
@endpush