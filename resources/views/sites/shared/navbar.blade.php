@php
    use App\ViewModels\Sites\Header\HeaderListViewModel;
    /**
     * @var HeaderListViewModel $headerListViewModel
     */
@endphp
<style>
    @media (min-width: 1400px){
        .\33xl\:text-\[1rem\] {
            font-size: 1rem!important;
        }
    }

</style>
<div id="navbar" style="margin-bottom: 84px;">
    <nav class="bg-white border-gray-200 dark:bg-gray-900" style="position: fixed;top: 0;width: 100%;z-index: 40;">
        <div style="font-size: 0.75rem" class="flex-wrap 3xl:text-[1rem] menu-top py-[1rem] 2xl:px-[15rem] lg:px-[10rem] lg:px-[5rem]">
            <a href="{{url("/")}}">
                <img class="logo"  src="{{url("site/images/logo.png")}}">
            </a>
            <div class="flex items-center md:order-2 text-white">
                <a href="{{route("sites.trial")}}" class="button-menu">
                    Trải nghiệm 30 ngày</a>
                <button data-collapse-toggle="mega-menu" type="button"
                        class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden  focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                        aria-controls="mega-menu" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg aria-hidden="true" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                              clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div id="mega-menu" class="hidden items-center justify-between w-full md:flex md:w-auto md:order-1 z-30">
                <ul class="flex flex-col font-medium md:flex-row md:mt-0">
                    @foreach($headerListViewModel->getMenus()->getChild() as $menu)
                        @if($menu->isMega())
                            <li class="li-menu">
                                <button id="mega-menu-dropdown-button"
                                        data-dropdown-toggle="mega-menu-dropdown"
                                        class="w-full flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800"
                                >
                                    {!! $menu->getTitle() !!}
                                    <img class="w-3 h-3 lg:w-2 lg:h-2" src="{{url("site/images/arrow-down.png")}}">
                                </button>
                                <div id="mega-menu-dropdown" class="mega-menu absolute !top-5 z-50 flex w-full hidden">
                                    <ul class="menu-small" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                                        @foreach($menu->getTabs() as $tab)
                                            <li role="presentation">
                                                <button class="mega-button"
                                                        id="{{\Illuminate\Support\Str::slug($tab->getName())}}-tab"
                                                        data-tabs-target="#tab-{{\Illuminate\Support\Str::slug($tab->getName())}}"
                                                        type="button" role="tab" aria-controls="dashboard"
                                                        aria-selected="false">
                                                    <img src="{{url($tab->getIcon())}}">
                                                    <span class="hide">{{$tab->getName()}}</span>
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="relative w-full" id="myTabContent">
                                        @foreach($menu->getTabs() as $tab)
                                            <div class="hidden box-menu-small"
                                                 id="tab-{{\Illuminate\Support\Str::slug($tab->getName())}}"
                                                 role="tabpanel"
                                                 aria-labelledby="dashboard-tab">
                                                <div class="mega-box-detail">
                                                    <div class="w-1/2 flex flex-col">
                                                        @foreach($tab->getItems() as $key => $item)
                                                            @if($key%2==0)
                                                                <a target="{{$item->getNewTab()?"_blank":""}}"
                                                                   href="{{url($item->getLinks())}}"
                                                                   class="mb-3 text-blue-600-important hover:text-blue-600">
                                                                    {!! $item->getTitle() !!}
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="w-1/2 flex flex-col">
                                                        @foreach($tab->getItems()  as $key => $item)
                                                            @if($key%2==1)
                                                                <a
                                                                        target="{{$item->getNewTab()?"_blank":""}}"
                                                                        href="{{$item->getLinks()}}"
                                                                        class="mb-3 text-blue-600-important hover:text-blue-600">  {!! $item->getTitle() !!}</a>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="mega-journey">
                                            <p class="uppercase text-btn-color  font-bold mb-2">Hành trình với
                                                Getfly
                                            </p>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/files.png")}}">
                                                    Dùng thử miễn phí
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Đăng ký nhận tài khoản demo để trải nghiệm
                                                    tính
                                                    năng CRM tiêu chuẩn.
                                                    Nếu bạn
                                                    muốn trải nghiệm
                                                    các module khác, vui lòng liên hệ để được trợ giúp</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/support.png")}}">
                                                    Tư vấn trực tiếp hoặc online
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly sẽ dựa vào mô hình kinh doanh và
                                                    nhu
                                                    cầu
                                                    quản lý của doanh
                                                    nghiệp để
                                                    tư vấn phương
                                                    thức ứng dụng hiệu quả nhất</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/choices.png")}}">
                                                    Lựa chọn gói dịch vụ
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly cung cấp các gói phù hợp với số
                                                    lượng
                                                    người
                                                    dùng tại doanh
                                                    nghiệp.
                                                    Ngoài ra doanh
                                                    nghiệp có thể lựa chọn dịch vụ triển khai để tối ưu hóa thời gian,
                                                    nguồn
                                                    lực
                                                    và chi phí</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/teaching.png")}}">
                                                    Đào tạo sử dụng
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Doanh nghiệp sẽ cần chuẩn bị dữ liệu cơ
                                                    bản
                                                    trên
                                                    CRM để đảm bảo việc
                                                    đào tạo
                                                    sử dụng được
                                                    hiệu quả dựa trên dữ liệu thực của doanh nghiệp</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("/site/images/training (1).png")}}">Đào tạo liên
                                                    tục
                                                    và đồng
                                                    hành
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly cam kết hỗ trợ doanh nghiệp trong
                                                    suốt
                                                    thời
                                                    gian sử dụng dịch
                                                    vụ
                                                    và
                                                    cung cấp các buổi
                                                    đào tạo sử dụng phần mềm liên tục hàng tháng để giúp doanh nghiệp
                                                    ứng
                                                    dụng
                                                    CRM tốt nhất</p>
                                            </div>
                                            <a href="{{route("sites.pricing")}}"
                                               class="text-md font-bold text-orange text-primary-orange500 mt-2"><u>Bảng giá dịch vụ</u></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @elseif($menu->getChild()!=[])
                            <li class="li-menu">
                                <div class="flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800"
                                     id="dropdownNavbarLink"
                                     data-dropdown-toggle="{{\Illuminate\Support\Str::slug($menu->getTitle())}}">
                                    <a target="{{$menu->getNewTab()?"_blank":""}}"
                                       class="block border-gray-100"
                                       href="{{$menu->getLinks()}}">{!!$menu->getTitle()  !!}</a>
                                    <img class="w-3 h-3 lg:w-2 lg:h-2" src="{{url("site/images/arrow-down.png")}}">
                                </div>
                                <div id="{{\Illuminate\Support\Str::slug($menu->getTitle())}}"
                                     class="z-30 hidden font-normal bg-white divide-y divide-gray-100 md:w-dropdown w-96 shadow border">
                                    <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">
                                        @foreach($menu->getChild() as $sub)
                                            @if($sub->getChild()!=[])
                                                <li class="peer li-menu relative">
                                                    <div class=" flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800">
                                                        <a target="{{$sub->getNewTab()?"_blank":""}}"
                                                           class="block border-gray-100"
                                                           href="{{$sub->getLinks()}}">{!!$sub->getTitle()  !!}</a>
                                                        <img class="w-3 h-3 lg:w-2 lg:h-2 md:flex hidden"
                                                             src="{{url("site/images/arrow-right.png")}}">
                                                        <img class="w-3 h-3 lg:w-2 lg:h-2 md:hidden"
                                                             src="{{url("site/images/arrow-down.png")}}">
                                                    </div>
                                                    <ul class="hover-menu absolute w-96 md:w-dropdown top-0 bg-white shadow-lg rounded border"
                                                        style="right: -15rem">
                                                        @foreach($sub->getChild() as $thirdSub)
                                                            <li class="hover:text-blue-600">
                                                                <a target="{{$thirdSub->getNewTab()?"_blank":""}}"
                                                                   href="{{url($thirdSub->getLinks())}}"
                                                                   class=" block p-2 ">{!!$thirdSub->getTitle() !!}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li class="li-menu hover:text-blue-600 px-2">
                                                    <a target="{{$sub->getNewTab()?"_blank":""}}"
                                                       href="{{url($sub->getLinks())}}"
                                                       class=" block ">{!!$sub->getTitle() !!}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @else
                            <li class="li-menu hover:text-blue-600">
                                <a target="{{$menu->getNewTab()?"_blank":""}}"
                                   href="{{url($menu->getLinks())}}">{!! $menu->getTitle() !!}</a>
                            </li>
                        @endif
                    @endforeach

                </ul>
            </div>
        </div>
    </nav>
</div>
@push("after_scripts")
    <script>
        // $(document).ready(() => {
        //     $(".peer").hover((e) => {
        //         $(e.target).find(".hover-menu").css("display", "block")
        //     })
        // })
    </script>
@endpush