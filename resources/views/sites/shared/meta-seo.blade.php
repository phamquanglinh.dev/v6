@php
    use App\ViewModels\Sites\Home\MetaSeoViewModel;
    /**
    * @var MetaSeoViewModel $metaSeoViewModel
     */
@endphp
<meta name="author" content="https://getfly.vn/">
<meta name="description" content="{{$metaSeoViewModel->getDescription()}}">
<meta name="keywords" content="{{$metaSeoViewModel->getKeywords()}}">
<!-- ogp -->
<meta property="og:title" content="@yield("title") - Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện ">
<meta property="og:type" content="article">
<meta property="og:url" content="https://getfly.vn/">

<meta property="og:image" content="{{url("/site/images/fbhome-getfly.png")}}">

<meta property="og:description" content="{{$metaSeoViewModel->getDescription()}}" >
<meta property="fb:app_id" content="637630076348771" />
<meta property="fb:admins" content="vuhoangduy"/>
<meta property="fb:pages" content="328467620572169" />
<meta name="google-site-verification" content="cjPAARainmNgQA2irOLVfhaOfVCZfCpGQ7EJQm9KKDU">
<meta name="ahrefs-site-verification" content="11d0f7f6ad53e5445fecfe9b436c65193c5d1bd8c85ccdee11e16d9b73cc99e5">
<!--  end ogp -->