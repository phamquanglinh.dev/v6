@if(!isset($disablePopup))
    <div>
        <!-- Modal toggle -->
        <button data-modal-target="defaultModal" id="trial-modal" data-modal-toggle="defaultModal"
                class="hidden block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                type="button">
            Toggle modal
        </button>

        <!-- Main modal -->
        <div id="defaultModal" tabindex="-1" aria-hidden="true"
             class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full z-50">
            <div class="relative w-full max-w-xl max-h-full" style="max-width: 40rem">
                <!-- Modal content -->
                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <!-- Modal header -->
                    <div class="flex items-center justify-center p-4 border-b rounded-t dark:border-gray-600">
                        <h3 class="ml-3 text-md font-semibold text-gray-900 dark:text-white">
                            Đăng ký ngay để dùng thử 30 ngày miễn phí phần mềm Getfly CRM
                            <button type="button"
                                    style="position: absolute;right: 0;top: 0;background: #213f80;border-radius: 0 0 0 1.5rem;color: white!important;"
                                    class="relative  text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                    data-modal-hide="defaultModal">
                                <svg class="w-3 h-3 absolute top-[0.5rem] right-[0.5rem]" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 14 14">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="2"
                                          d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                                </svg>
                                <span class="sr-only">Close modal</span>
                            </button>
                        </h3>

                    </div>
                    <!-- Modal body -->
                    <div class="p-1">
                        {!! embed_form("form-popup") !!}
                    </div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
    </div>
@endif
@push("after_scripts")
    <script>
        let showPopup = true
        const openPopUp = () => {
            $("#trial-modal").click()
        }
        $(window).scroll(() => {
            if ($(window).scrollTop() > 1000 && showPopup) {
                openPopUp()
                showPopup = false
            }
        })
    </script>
@endpush