@php
    use App\ViewModels\Sites\Footer\FooterListViewModel;
     /**
     * @var FooterListViewModel $footerListViewModel
     */
     $copyright = $footerListViewModel->getFooter()->getCopyright()
@endphp
<div class="box-banner2 bg-banner2 mt-5">
    <p class="text-2xl text-white">Trải nghiệm giải pháp</p>
    <p class="text-2xl text-white">quản lý và chăm sóc khách hàng toàn diện cho SMEs Việt</p>
    <a href="{{route("sites.trial")}}"
       class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3 text-white">
        Trải nghiệm thử
    </a>
</div>
<div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] footer bg-banner3">
    <div class="flex justify-between w-full my-5">
        <div class="w-3/4 text-white">
            {!! $copyright !!}
        </div>
        <div class="w-1/4 relative">
            <div class="flex flex-row justify-between">
                <div>
                    <p class="text-white mb-3 font-bold">Download</p>
                    <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                        <img class="w-36 mb-3" src="{{url("site/images/appstore.png")}}">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                        <img class="w-36 mb-3" src="{{url("site/images/googleplay.png")}}">
                    </a>
                    <img class="w-36 mb-3" src="{{url("site/images/logo-da-thong-bao-voi-bo-cong-thuong.png")}}">
                    <img class="w-36" src="{{url("site/images/iso.png")}}">
                </div>

            </div>
        </div>
    </div>
</div>
<div class="footer-p bg-footer">
    <div class="w-3/4 mobile">
        <p class="text-white font-light">© Copyright Getfly CRM 2023 - Giải pháp quản lý & chăm sóc khách hàng dành
            cho SMEs</p>
        <p class="text-white font-light">Giấy phép kinh doanh số: 0108107661 do Sở kế hoạch và Đầu tư Hà nội cấp
            ngày
            25/12/2017</p>
    </div>
    <div class="w-1/4 mobile">
        <div class="social-mobile">
            <a href=" https://www.facebook.com/getfly" target="_blank">
                <img class="w-10 h-10" src="{{url("site/images/facebook.png")}}">
            </a>
            <a href="https://www.instagram.com/getfly_crm/" target="_blank">
                <img class="w-10 h-10" src="{{url("site/images/insta.png")}}">
            </a>
            <a href="https://www.youtube.com/@GetFlyCRM" target="_blank">
                <img class="w-10 h-10" src="{{url("site/images/Youtube.png")}}">
            </a>
        </div>
    </div>
</div>
<div class="phone">
    <a href="tel:02462627662"> <img src="{{url("site/images/phone.png")}}"></a>
    <div class="hotline">
        <p>Hotline 24/7: (024) 6262 7662</p>
    </div>
</div>
<div class="fixed bottom-5 right-3">
{{--    <a onclick="openChat()" class="cursor-pointer">--}}
{{--        <img class="mb-3" style="width: 3rem;height: 3rem" src="{{url("site/images/subiz.webp")}}">--}}
{{--    </a>--}}
    <a href="https://zalo.me/2642095819923952061" target="_blank">
        <img class="mb-3" src="{{url("site/images/zalo.png")}}">
    </a>
    <a href="https://www.facebook.com/messages/t/328467620572169" target="_blank">
        <img src="{{url("site/images/messenger.png")}}">
    </a>
</div>
