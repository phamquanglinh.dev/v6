@php
    $identity= \Illuminate\Support\Str::random(6)
@endphp
<div class="{{$identity}}-background">
  <span class="{{$identity}}-text">
        Ghi tiêu để ở đây
  </span>
</div>
<style>
    .{{$identity}}-background {
        background: #f36a26;
        text-align: center;
        padding: 0.75rem;
        margin: 0.25rem;
    }

    .{{$identity}}-text {
        color: white;
        font-size: 1.5rem;
        text-transform: uppercase;
        font-family: "Montserrat", sans-serif;
    }
</style>