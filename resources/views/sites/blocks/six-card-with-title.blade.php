@php
    $prefix ="a". \Illuminate\Support\Str::random(5)
@endphp
<div class="{{$prefix}}-container">
    <div class="{{$prefix}}-title {{$prefix}}-text">Tiêu đề to đùng ở đây</div>
    <div class="{{$prefix}}-row">
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
    </div>
    <div class="{{$prefix}}-row">
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
        <div class="{{$prefix}}-cell">
            <div class="{{$prefix}}-image">
                <img class="" src="https://onestopanime.com.au/wp-content/uploads/2020/07/Nezuko-Kamado-300x300.jpg"
                     style="border-radius: 20rem;aspect-ratio: 1;width: 100px" alt="">
            </div>
            <div class="{{$prefix}}-text">Nội dung ghi ở đây, nội dung sẽ rất là dài nên cần tối ưu
                nội dung
            </div>
        </div>
    </div>
</div>
<style>
    .{{$prefix}}-justify {
        text-align: justify!important;
    }

    .{{$prefix}}-container {
        padding: 3rem
    }

    .{{$prefix}}-title {
        color: #0b4d75;
        font-size: 1.75rem;
        font-weight: bold;
        width: 100%;
        text-transform: uppercase;
    }

    .{{$prefix}}-text {
        font-family: Montserrat, sans-serif !important;
        text-align: center;
        padding-left: 2rem;
        padding-right: 3rem;
        margin-bottom: 0.5rem;
    }

    .{{$prefix}}-font-bold {
        font-weight: bold;
    }

    .{{$prefix}}-image {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 2rem;
    }

    .{{$prefix}}-row {
        display: flex;
        justify-content: flex-start;
        align-items: stretch;
        flex-wrap: nowrap;
    }

    .{{$prefix}}-cell {
        min-height: 75px;
        flex-grow: 1;
        flex-basis: 100%;

        margin-bottom: 1.5rem;
    }

    @media (max-width: 768px) {
        .{{$prefix}}-container {
            padding: 1rem;
        }

        .{{$prefix}}-row {
            flex-wrap: wrap;
        }
    }
</style>