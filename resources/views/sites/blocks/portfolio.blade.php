@php
    $identity= "a".\Illuminate\Support\Str::random(6)
@endphp
<div class="{{$identity}}-container">
    <div class="{{$identity}}-avatar-field">
        <img class="{{$identity}}-avatar"
             src="https://i-ogp.pximg.net/c/540x540_70/img-master/img/2020/12/17/01/26/20/86345291_p0_square1200.jpg">
    </div>
    <div class="{{$identity}}-name">inosuke</div>
    <div class="{{$identity}}-job-title">Demon Slayer: Kimetsu no Yaiba</div>
    <div class="{{$identity}}-description">
        <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
            Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
            Aldus PageMaker including versions of Lorem Ipsum.
        </p>
    </div>
</div>
<style>
    .{{$identity}}-container {
        font-family: "Montserrat", sans-serif !important;
        padding: 2rem;
    }

    .{{$identity}}-job-title {
        font-style: italic;
        text-align: center;
    }

    .{{$identity}}-description {

    }

    .{{$identity}}-avatar {
        width: 100%;

        border-radius: 50rem;
        border: 0.5rem solid #f36a26;
    }

    .{{$identity}}-avatar-field {
        margin-bottom: 1rem;

        margin-top: 1rem;
    }

    .{{$identity}}-name {
        text-align: center;
        color: #f36a26;
        font-size: 1.5rem;
        font-weight: bold;
        text-transform: uppercase;
    }
</style>