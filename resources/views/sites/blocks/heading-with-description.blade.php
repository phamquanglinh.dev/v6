@php
    $identity= \Illuminate\Support\Str::random(6)
@endphp
<div class="{{$identity}}-background">
    <div class="{{$identity}}-title">
        GIẢI PHÁP CHUYỂN ĐỔI SỐ GETFLY MANG LẠI
    </div>
    <div style="margin-top: 1rem;margin-bottom: 1rem;color: white">---</div>
    <div class="{{$identity}}-description">
        (Với kinh nghiệm tích lũy trong quá trình tư vấn triển khai Getfly cho 4000+ doanh nghiệp thuộc trên 200 ngành
        nghề. Giải pháp chuyển đổi số Getfly là chìa khóa giúp doanh nghiệp giải quyết được các vấn đề trên)
    </div>
</div>
<style>
    .{{$identity}}-background {
        background: #233f80;
        text-align: center;
        padding: 0.75rem;
        margin: 0.25rem;
    }

    .{{$identity}}-title {
        color: #f36a26;
        font-size: 1.5rem;
        text-transform: uppercase;
        font-family: "Montserrat", sans-serif;
        font-weight: bold;
    }

    .{{$identity}}-description {
        color: white;
        font-size: 1rem;
        padding-left: 5rem;
        padding-right: 5rem;
        font-family: "Montserrat", sans-serif;
    }
    @media (max-width: 768px) {
        .{{$identity}}-description {
            padding-left: 1rem;
            padding-right: 1rem;

        }
    }
</style>
