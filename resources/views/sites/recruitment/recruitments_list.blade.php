@php
    use App\ViewModels\Sites\News\NewsListViewModel;

    /**
     * @var NewsListViewModel $recruitmentsListViewModel
     */
@endphp

@extends('sites.shared.header')

@include('sites.shared.navbar')

@section('title')
    Recruitments
@endsection

@section('content')
    RECRUITMENTS LIST
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection