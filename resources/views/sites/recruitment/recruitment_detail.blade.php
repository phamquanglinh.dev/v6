@php
    use App\ViewModels\Sites\News\NewsDetailViewModel;

    /**
     * @var NewsDetailViewModel $recruitmentDetailViewModel
     */
@endphp

@extends('sites.shared.header')

@include('sites.shared.navbar')

@section('title')
    Recruitment Detail
@endsection

@section('content')
    RECRUITMENT DETAIL
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection