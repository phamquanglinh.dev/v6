@php
    use App\ViewModels\Sites\Home\SubPageViewModel;
    use Illuminate\Support\Str;
    /**
* @var SubPageViewModel $subPageViewModel
 */
    $page = $subPageViewModel->getPage();
    $tabs = $subPageViewModel->getTabs();
@endphp
@extends("sites.shared.layout")
@section("title")
    {{$page->getTitle()}}
@endsection
@section("content")
    <style>
        .box-subpages ul {
            list-style: disc !important;
            padding-left: 1.5rem;
        }

        .box-subpages ol {
            list-style: auto !important;
        }
    </style>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] my-10">
        <div class="customer-header px-2">
            <div class="md:w-1/2 w-full">
                <h1 class="title-crm md:text-xl text-2xl">{{$page->getTitle()}}</h1>
                <div class="hover:cursor-pointer max-w-2xl py-10 sub-crm mb-3 " id="text-crm">
                    {!! $page->getSub() !!}
                </div>
                <div class="text-center w-full">
                    <a href="{{route("sites.trial")}}"
                       class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase font-bold">
                        Đăng ký trải nghiệm
                    </a>
                </div>
            </div>
            <div class="md:w-1/2 w-full">
                @if($subPageViewModel->getPage()->getImage()!="")
                    <img src="{{url($page->getImage())}}" class="float-right" alt="">
                @else
                    @if($subPageViewModel->getPage()->getVideo()!="")
                        <div class="p-2">
                            <iframe src="{{$subPageViewModel->getPage()->getVideo()}}" class=" float-right w-full rounded-xl" allowfullscreen
                                    style="aspect-ratio: 16/9"
                            ></iframe>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="box-banner bg-banner">
        <div class="banner-action">
            <div class="banners mobile">
                <p class="banner-title">4500++</p>
                <p class="banner-des">Doanh nghiệp sử dụng</p>
            </div>
            <div class="banners mobile banner-center">
                <p class="banner-title">200++</p>
                <p class="banner-des">Ngành nghề</p>
            </div>
            <div class="banners mobile">
                <p class="banner-title">11</p>
                <p class="banner-des">Năm kinh nghiệm</p>
            </div>
        </div>
        <div>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase">
                Trải nghiệm thử
            </a>
        </div>
    </div>
    @include("sites.shared.sub-page-tab",['subPageViewModel'=>$subPageViewModel])

@endsection
@push("after_scripts")
    <script>
        // $("#sub-crm").click((e) => {
        //     const textMore = $("#text-crm").attr("data-more")
        //     console.log(textMore)
        //     $("#text-crm").text(textMore)
        // })
    </script>
@endpush