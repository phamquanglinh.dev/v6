@php
    use App\ViewModels\Sites\Home\HomeViewModel;

    /**
     * @var HomeViewModel $homeViewModel
     */
//    dd($homeViewModel->getBanners())
@endphp
@extends('sites.shared.layout')
@section('title')
    Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện - Getfly CRM
@endsection
@section('content')
    @include("sites.shared.banner",["homeViewModel"=>$homeViewModel])
    <div class="flex flex-wrap 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] mt-5">
        <div class="md:w-1/2 w-full h-[400px] flex justify-center items-center text-justify"
             style="background-size: cover;background-image: url('{{url("site/images/banner_video.jpg")}}')">
            <p class="text-white px-[1rem] py-[8rem]"> {{ $homeViewModel->getSite()['content'] }} </p>
        </div>
        <div class="md:w-1/2 w-full flex justify-center items-center" style="background: black;height: 400px">
            <img alt="lazy-video-why" src="https://img.youtube.com/vi/{{$homeViewModel->getVideoEmbed()}}/sddefault.jpg"
                 class="w-full opacity-70" style="aspect-ratio: 16/9;object-fit: cover">
            <span class="text-white cursor-pointer img_play_video_home absolute text-5xl">▶</span>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] ">
        <p class="title-box">Giải pháp getfly</p>
        <div class="solution p-2">
            <div class="solutions">
                <img alt="data-collection" class="mb-2" src="{{url("site/images/data-collection.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2">Thu thập, lưu trữ và
                    phân loại khách hàng
                    khoa học trên 1
                    nền
                    tảng</p>
                <p class="font-medium">Tạo các phễu thu hút khách hàng để thu thập dữ liệu về hệ thống CRM duy nhất,
                    phân loại khách hàng
                    theo nguồn, nhóm, tự
                    động phân chia khách hàng cho nhân viên có liên quan chăm sóc. Dữ liệu đồng nhất, phân quyền truy
                    cập, đảm bảo tính
                    thống nhất trong thông tin chăm sóc.</p>
            </div>
            <div class="solutions">
                <img alt="customer-care" class="mb-2" src="{{url("site/images/customer-care.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2  ">Nuôi dưỡng khách hàng
                    tự động</p>
                <p class="font-medium">Thiết lập được kịch bản nuôi dưỡng tự động, cá nhân hoá theo từng khách hàng khi
                    họ để lại thông tin
                    để luôn giữ liên
                    lạc với khách hàng.</p>
            </div>
            <div class="solutions">
                <img alt="evaluation" class="mb-2" src="{{url("site/images/evaluation.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2  ">Đo lường và đánh giá
                    hiệu quả hoạt
                    động của doanh
                    nghiệp
                </p>
                <p class="font-medium">Hệ thống KPI tự động tổng hợp số liệu đo lường trên hệ thống bao gồm khách hàng,
                    đơn hàng, công việc,
                    hoạt động... Thiết
                    lập chỉ tiêu KPI giao cho nhân sự, phòng ban để đánh giá hiệu quả hoạt động của công ty, phòng ban,
                    cá nhân.</p>
            </div>
        </div>
        <div class="solution p-2">
            <div class="solutions">
                <img alt="employee" class="mb-2" src="{{url("site/images/employee.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2  ">Chủ động trong hoạt
                    động bán hàng
                    thúc đẩy doanh thu
                </p>
                <p class="font-medium">Phân loại theo dõi khách hàng theo hành trình mua hàng, ghi nhận các thông tin tư
                    vấn của từng khách
                    hàng theo thời
                    gian, chủ động sắp xếp hoạt động thúc đẩy mua hàng, mua thêm hoặc mua lại.</p>
            </div>
            <div class="solutions">
                <img alt="improvement" class="mb-2" src="{{url("site/images/improvement.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2  ">Nâng cấp chất lượng
                    chăm sóc khách
                    hàng</p>
                <p class="font-medium">Lưu trữ lịch sử giao dịch đồng nhất từ khi khách hàng quan tâm tìm hiểu sản phẩm
                    đến quá trình tư vấn
                    và mua hàng. Doanh
                    nghiệp có thể hiểu rõ hơn về khách hàng của mình, chủ động phân loại theo hạng để áp dụng những
                    chính sách chăm sóc,
                    khuyến mãi khác nhau để giữ chân khách hàng.</p>
            </div>
            <div class="solutions">
                <img alt="system" class="mb-2" src="{{url("site/images/system.png")}}">
                <p class="font-bold text-blue-color text-lg lg:text-base mb-2  ">Hệ thống miniERP cho
                    SME
                </p>
                <p class="font-medium">11 module mở rộng đáp ứng đủ tính năng của 1 miniERP cho doanh nghiệp nhỏ và vừa.
                    Khả năng phát triển
                    theo nhu cầu cao.</p>
            </div>
        </div>
    </div>
    @include("sites.shared.features",['homeViewModel'=>$homeViewModel])
    <div class="2xl:px-[15rem] xl:px-[10rem] lg:px-[5rem] box-banner bg-banner py-10  ">
        <div class="banner-action">
            <div class="banners mobile">
                <p class="banner-title">{{$homeViewModel->getSite()['partners']}}++</p>
                <p class="banner-des">Doanh nghiệp sử dụng</p>
            </div>
            <div class="banners mobile banner-center">
                <p class="banner-title">{{$homeViewModel->getSite()['profession']}}++</p>
                <p class="banner-des">Ngành nghề</p>
            </div>
            <div class="banners mobile">
                <p class="banner-title">{{$homeViewModel->getSite()['experience_years']}}</p>
                <p class="banner-des">Năm kinh nghiệm</p>
            </div>
        </div>
        <div>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase">
                Trải nghiệm thử
            </a>
        </div>
    </div>
    <div class="2xl:px-[20rem] xl:px-[10rem] lg:px-[5rem] mt-5">
        <p class="why-title w-full">Vì sao nên chọn getfly crm</p>
        <div class="whys">
            <div class="md:w-1/2 w-full flex justify-center items-center p-5">
                <img alt="lazy-video-why" src="https://img.youtube.com/vi/Nt00L2cj6cQ/sddefault.jpg"
                     class="w-full opacity-70" style="aspect-ratio: 16/9;object-fit: cover">
                <span class="text-white cursor-pointer img_play_video_why text-5xl">▶</span>
            </div>
            <div class="md:w-1/2 w-full p-5">
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="bao-mat" src="{{url("site/images/bao-mat.png")}}">
                    Bảo mật thông tin khách hàng tuyệt đối
                </div>
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="ket-noi" src="{{url("site/images/ket-noi.png")}}">
                    Nền tảng kết nối mở, hỗ trợ tích hợp đa dịch vụ
                </div>
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="tinh-nang" src="{{url("site/images/tinh-nang.png")}}">
                    Liên tục cập nhật tính năng và công nghệ mới nhất
                </div>
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="doi-ngu-tu-van" src="{{url("site/images/doi-ngu-tu-van.png")}}">
                    Đội ngũ tư vấn giải pháp năng lực, chuyên nghiệp
                </div>
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="kinh-nghiem-trien-khai" src="{{url("site/images/kinh-nghiem-trien-khai.png")}}">
                    Kinh nghiệm triển khai 4000+ doanh nghiệp với 200 + ngành nghề
                </div>
                <div class="flex gap-4 items-center mb-5 font-semibold">
                    <img alt="tiet-kiem" src="{{url("site/images/tiet-kiem.png")}}">
                    Tiết kiệm chi phí, thời gian, đột phá doanh thu
                </div>
            </div>
        </div>
    </div>
    <div class="2xl:px-[10rem] xl:px-[10rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="title-box">Phản hồi khách hàng</p>
        <div id="controls-carousel" class="relative w-full" data-carousel="slide">
            <div class="feedback-detail">
                <div class="hidden duration-700" data-carousel-item="active">
                    <div class="slide-detail">
                        @foreach($homeViewModel->getFeedbacks() as $feedback)
                            <div class="mobile w-1/3 slides">
                                <div class="flex flex-col items-center justify-center gap-2">
                                    <img alt="{{$feedback->getTitle()}}" src="{{$feedback->getAvatar()}}"
                                         class="slides-img">
                                    <p class="font-bold text-base">{{$feedback->getName()}}</p>
                                    <p class="text-gray-400 lg:text-sm">{{$feedback->getPosition()}}</p>
                                    <p class="lg:text-sm text-center">{{$feedback->getNote()}}
                                    </p>
                                </div>
                                <div class="pt-3 text-center">
                                    <a href="{{route("sites.story",$feedback->getId())}}"
                                       class="text-btn-color font-medium text-center">Xem thêm
                                        <span class="ml-1">ᐳ</span>
                                    </a>
                                </div>
                            </div>
                            @if(($loop->index+1)%3==0 && !$loop->last)
                    </div>
                </div>
                <div class="hidden duration-700" data-carousel-item>
                    <div class="slide-detail">
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <button type="button" class="btn-next left-0" data-carousel-prev>
                <img src="{{url("site/images/left.png")}}" alt="left">
            </button>
            <button type="button" class="btn-next right-0" data-carousel-next>
                <img src="{{url("site/images/right.png")}}" alt="right">
            </button>
        </div>
    </div>
    @include("sites.shared.customers",['homeViewModel'=>$homeViewModel])
@endsection
@push("after_scripts")
    <style>
        .img_play_video_why {
            position: absolute;
        }
    </style>
    <script>

        $(document).ready(() => {
            $(".img_play_video_home").click((e) => {
                console.log(e)
                const videoBox = $(e.currentTarget).parent()
                videoBox.css("background", "none")
                const video = document.createElement("iframe")
                video.src = "https://youtube.com/embed/{{$homeViewModel->getVideoEmbed()}}?&autoplay=1"
                video.width = "100%"
                video.height = "400px"
                videoBox.html(video)
            })
            $(".img_play_video_why").click((e) => {
                console.log(e)
                const videoBox = $(e.currentTarget).parent()
                videoBox.css("background", "none")
                const video = document.createElement("iframe")
                video.src = "https://youtube.com/embed/Nt00L2cj6cQ?&autoplay=1"
                video.width = "100%"
                video.height = "400px"
                videoBox.html(video)
            })
        })
    </script>
    <script>

        $(document).ready(function () {
            $('.choose_city').change(function () {
                var id = $(this).val();
                if (parseInt(id) !== 1) {
                    $('#getfly_hn').removeClass('hidden');
                    $('#getfly_hcm').addClass('hidden');
                } else {
                    $('#getfly_hcm').removeClass('hidden');
                    $('#getfly_hn').addClass('hidden');
                }
            });
        });
    </script>
@endpush
@section("modal")

@endsection