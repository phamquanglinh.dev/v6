@php
    use App\ViewModels\Sites\Pricing\PricingViewModel;
    use App\ViewModels\Sites\Module\ModuleViewModel;
    /**
    * @var PricingViewModel $pricingViewModel
    * @var ModuleViewModel $moduleViewModel
    */
    $pricing  = $pricingViewModel->getPricing();
    $modules  = $moduleViewModel->getModules();
@endphp
@extends("sites.shared.layout")
@section("title")
    Báo giá
@endsection
@section("content")
    <style>
        .fc {
            border-right-width: 3px;
            border-left-width: 3px;
            border-color: #dc6803!important;
        }

        .border-bottom-2 {
            border-bottom-width: 2px;
        }

        .border-top-2 {
            border-top-width: 2px;
        }

        .bg-odd {
            background: #233f80;
        }

        .bg-even {
            background: #f36f24;
        }
        .order-badge{
            top: -10px;
            left: -10px;
        }
        .order-badge img{
            filter: invert();
        }
        .basis-1\/2{
            flex-basis: 50%;
        }
    </style>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] bg-banner 2xl:py-20 xl:py-10 lg:py-5">
        <div class="flex items-center gap-2 justify-center w-full">
            <div class="w-full text-center">
                <p class="font-bold text-white text-3xl lg:text-2xl mb-2 uppercase">Đừng ngần ngại đầu tư - hãy để
                    getfly
                    lo hộ
                    bạn về giá</p>
                <p class="text-white text-xl xl:text-lg lg:text-base">Hệ thống kết nối mở, sẵn sàng tích hợp</p>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] box-price hide ">
        <div class="price-banner bg-banner">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM ON CLOUD</p>
            <p class="text-white">(Ưu đãi khi mua nhiều năm)</p>
            <a href="https://crm-vietnam.com/page/bang-gia-crm-cai-dat-server-rieng-on-premise-cua-getfly-crm"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
        <div class="border-blue-600 flex gap-2 w-full flex-col items-center border justify-center overflow-x-auto">
            <table class="w-full text-left text-gray-500 dark:text-gray-400 ">
                <thead class="text-xs text-gray-700 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="p-3 md:p-1 lg:p-0 w-40 border-r">
                    </th>
                    @foreach($pricing as $index => $item)
                        <th scope="col" class="p-3 md:p-1 lg:p-0 w-40  @if(!$loop->last) @endif text-center
                         @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif
                         ">
                            <p class="text-lg text-blue-color text-sm mt-2">{{$item->getName()}}</p>
                            @if($item->getPrice()!=0)
                                <p class="text-base text-btn-color my-5">{{number_format($item->getPrice())}}đ/tháng</p>
                                <p>{{number_format($item->getUserPrice())}}đ/1 user/ tháng</p>
                            @else
                                <p class="text-base text-btn-color my-5">Liên hệ</p>
                                <p>Liên hệ</p>
                            @endif
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý Marketing"))}}">Quản lý Marketing</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach

                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Lưu trữ và chăm sóc khách hàng"))}}">
                                Lưu trữ và chăm sóc khách hàng
                            </a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif ">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý đội kinh doanh"))}}">Quản lý đội kinh doanh</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý bán hàng"))}}">Quản lý bán hàng</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý công việc"))}}">Quản lý công việc</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý SMS Brandname và Email Marketing"))}}">Quản lý SMS
                                Brandname <br>và Email Marketing</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Marketing tự động"))}}">Marketing tự động</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Quản lý kênh giới thiệu"))}}">
                                Quản lý kênh giới thiệu</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Đo lường KPI"))}}">Đo lường
                                KPI</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Hỗ trợ App"))}}">Hỗ trợ App</a>
                        </p>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 2em;height: 2em" src="{{url("/site/images/app store.png")}}">
                                <img style="width: 2em;height: 2em" src="{{url("/site/images/google play.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Số lượng người dùng"))}}">Số
                                lượng người dùng</a></p>
                        </p>
                    </th>
                    @foreach($pricing as $item)

                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{$item->getUsers()}} Người
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Số lượng khách hàng"))}}">Số
                                lượng khách hàng</a></p>
                    </th>

                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{number_format($item->getCustomers())}} Khách hàng
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định
                                domain</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            @if($item->getDomainTarget()==0)
                                <div class="flex justify-center">
                                    <img style="width: 1em" src="{{url("/site/images/remove.png")}}">
                                </div>
                            @else
                                <div class="flex justify-center">
                                    <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                                </div>
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Thời gian Setup"))}}">Thời gian
                                Setup</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{$item->getSetupTime()}}
                            </div>
                        </td>
                    @endforeach

                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Bộ
                                tài liệu đào tạo</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{url("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last)@endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc border-bottom-2 @endif">
                            <div class="flex justify-center">
                                <a href="{{route("sites.trial")}}"
                                   class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-5 lg:px-2 lg:p-1 md:text-xs">
                                    Dùng thử
                                </a>
                            </div>
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        </div>
        <div class="price-banner bg-banner mt-20 rounded-lg hide">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM cài đặt sever riêng (on premise)</p>
            <a href="https://crm-vietnam.com/page/bang-gia-crm-cai-dat-server-rieng-on-premise-cua-getfly-crm"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
    </div>
    <div class="hidemobile p-4">
        <div class="price-banner bg-banner">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM ON CLOUD</p>
            <p class="text-white">(Ưu đãi khi mua nhiều năm)</p>
            <a href="https://crm-vietnam.com/page/bang-gia-crm-cai-dat-server-rieng-on-premise-cua-getfly-crm"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
        @foreach($pricing as $item)
            <div class="border p-4 {{!$loop->last?"mb-4":""}}">
                <p class="text-2xl text-blue-color font-bold text-center">{{$item->getName()}}</p>
                <div class="text-center font-bold">
                    <p class="text-base text-btn-color my-2">{{number_format($item->getPrice())}}đ/ tháng</p>
                    <p>{{number_format($item->getUserPrice())}}đ/ 1 người dùng/ tháng</p>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý Marketing"))}}">Quản lý Marketing</a>

                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Lưu trữ và chăm sóc khách hàng"))}}">Lưu trữ và chăm sóc khách hàng</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý đội kinh doanh"))}}">Quản lý đội kinh doanh</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý bán hàng"))}}">Quản lý bán hàng</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý công việc"))}}">Quản lý công việc</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý SMS Brandname và Email Marketing"))}}">Quản lý SMS Brandname
                        và Email Marketing</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Marketing tự động</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Quản lý kênh giới thiệu</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Đo lường KPI</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Hỗ trợ App</a>
                    </div>
                    <div class="flex gap-2 w-1/2 justify-end">
                        <img src="{{url("site/images/app store.png")}}">
                        <img src="{{url("site/images/google play.png")}}">
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Số lượng người dùng"))}}">Số lượng người dùng</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{$item->getUsers()}} Người
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Số lượng khách hàng"))}}">Số lượng khách hàng</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{number_format($item->getCustomers())}} Khách hàng
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    @if($item->getDomainTarget())
                        <img src="{{url("site/images/check.png")}}">
                        <a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định domain</a>
                    @else
                        <img src="{{url("site/images/remove.png")}}">
                        <a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định domain</a>
                    @endif

                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Thời gian Setup"))}}">Thời gian Setup</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{$item->getSetupTime()}}
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{url("site/images/check.png")}}">Bộ tài liệu đào tạo
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Bộ tài liệu đào tạo</a>
                </div>
                <div class="flex justify-center pt-3">
                    <a href="{{route("sites.trial")}}" class="btn-trial btn">
                        Dùng thử
                    </a>
                </div>
            </div>
        @endforeach

    </div>

    <div class="2xl:px-[15rem] lg:px-[10rem] lg:px-[5rem] flex flex-col items-center justify-center pb-[2rem] gap-[0.5rem] hide">
        <p class="text-blue-color font-bold uppercase text-2xl py-10 text-center px-2">Chi tiết gói module mở rộng</p>
        <div class="hidden md:block">
            <ol class="flex flex-wrap flex-wrap">
                @foreach($moduleViewModel->getModules() as $key => $modules)
                        <li class="md:w-1/2 w-full relative pr-3 basis-1/2 h-full mb-5">
                            <div class="absolute order-badge text-white bg-{{in_array($key,[1,5,9,2,6,10])?"even":"odd"}} w-10 h-10 z-10 flex items-center justify-center rounded-full shrink-0 p-2 rounded-full">
                                <img src="{{$modules->getIcon()}}" class="w-6 h-6">
                            </div>
                            <div class="h-full shadow-lg mt-1 p-2 rounded-lg mx-2 cursor-pointer border">
                                <a href="{{$modules->getUrl()}}">
                                    <h3 class=" px-3 py-5 text-xl font-bold h-full text-primary-{{in_array($key,[1,5,9,2,6,10])?"orange500":"blue500"}} text-gray-900 dark:text-white">{{str_replace("<br>","",$modules->getName())}}</h3>
                                </a>
                            </div>
                        </li>
                @endforeach
            </ol>
        </div>
        <div class="border-l md:hidden block">
            @foreach($moduleViewModel->getModules() as $key => $modules)
                <div class="border-b p-2 text-primary-{{$key%2==0?"blue500":"orange500"}} font-bold">
                    <a href="{{$modules->getUrl()}}">
                        <p class="font-bold text-2xl">{{($key+1)<10?"0".$key+1:$key+1}}</p>
                        {{str_replace("<br>","",$modules->getName())}}
                    </a>
                </div>
            @endforeach
        </div>
        <div class="flex justify-center">
        </div>
        <a href="https://crm-vietnam.com/page/bang-gia-crm-cai-dat-server-rieng-on-premise-cua-getfly-crm"
           class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
            Nhận báo giá
        </a>
    </div>
    <div class="hidemobile p-4">
        <div class="price-banner bg-banner rounded-t-lg">
            <p class="uppercase font-bold text-white text-xl text-center">Bảng giá CRM cài đặt sever riêng (on premise)
            </p>
            <a href="https://crm-vietnam.com/page/bang-gia-crm-cai-dat-server-rieng-on-premise-cua-getfly-crm"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
    </div>
@endsection