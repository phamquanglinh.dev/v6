@php
    use App\ViewModels\Sites\Home\HomeViewModel;

       /**
        * @var HomeViewModel $homeViewModel
        **/
        use App\ViewModels\Sites\Pricing\PricingViewModel;
    /**
    * @var PricingViewModel $pricingViewModel
 */
    $pricing  = $pricingViewModel->getPricing();
    $newspaper_seo = true
//    $disablePopup =true;
@endphp
@extends("sites.shared.layout")
@section("newspaper_seo")
    <meta name="author" content="https://getfly.vn/">
    <meta name="description" content="Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện giúp các chủ DN SMEs tiết kiệm chi phí, tăng doanh thu 200 %. Dùng thử Free. Hỗ trợ 24/7">
    <meta name="keywords" content="Phần mềm CRM, Phần mềm quản lý khách hàng, Phần mềm chăm sóc khách hàng, Phần mềm quản lý và chăm sóc khách hàng toàn diện">
    <!-- ogp -->
    <meta property="og:title" content="Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện cho SMEs Việt">
    <meta property="og:type" content="article">
    <meta property="og:url" content="https://getfly.vn/GetflyCRM-Giai-phap-quan-ly-va-cham-soc-khach-hang-toan-dien-cho-SMEs-Viet">

    <meta property="og:image" content="{{url("/site/images/fbhome-getfly.png")}}">

    <meta property="og:description" content="Getfly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện cho SMEs Việt" >
    <meta property="fb:app_id" content="637630076348771" />
    <meta property="fb:admins" content="vuhoangduy"/>
    <meta property="fb:pages" content="328467620572169" />
    <meta name="google-site-verification" content="cjPAARainmNgQA2irOLVfhaOfVCZfCpGQ7EJQm9KKDU">
    <meta name="ahrefs-site-verification" content="11d0f7f6ad53e5445fecfe9b436c65193c5d1bd8c85ccdee11e16d9b73cc99e5">
@endsection
@section("title")
    Giải pháp quản lý và chăm sóc khách hàng toàn diện cho SMEs Việt
@endsection
@section("content")
    <style>
        .fc {
            border-right-width: 3px;
            border-left-width: 3px;
            border-color: #dc6803!important;
        }

        .border-bottom-2 {
            border-bottom-width: 2px;
        }

        .border-top-2 {
            border-top-width: 2px;
        }
    </style>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] features-new">
        <p class="text-2xl font-bold">Giải pháp quản lý và chăm sóc khách hàng toàn diện cho SMEs Việt</p>
        <div class="py-10">
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6" alt="">100%
                khách
                hàng được quản lý
                và
                chăm sóc</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">4000+ SMEs
                Việt của hơn 200
                ngành
                nghề tin dùng</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Là phần mềm
                Việt có giao diện
                thân
                thiện và dễ dùng</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Linh hoạt
                sử dụng trên PC
                &amp;
                App Mobile</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Tối ưu quy
                trình, tiết kiệm
                2-3
                giờ làm việc mỗi ngày, gia tăng đều đặn 200-300% doanh thu</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Hỗ trợ sau
                bán 24/7, có quy
                trình
                đào tạo bài bản theo sát người dùng (online, offline)</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Dễ dàng tùy
                chỉnh đối với yêu
                cầu
                của từng doanh nghiệp</p>
            <p class="flex gap-2 mb-3"><img src="{{asset("site/images/tick-fea.png")}}" class="w-6 h-6">Đáp ứng đa
                ngôn ngữ: Việt -
                Anh -
                Thái - Nhật - Trung quốc - Hàn quốc</p>
        </div>
        <a href="#dangkyngay" class="dangky">
            Đăng ký dùng thử FREE 30 ngày
        </a>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div class="flex py-10 items-center justify-center">
            <p class="font-bold text-2xl text-btn-color">7 VẤN ĐỀ TRONG QUẢN LÝ MÀ HẦU HẾT CÁC DOANH NGHIỆP ĐỀU
                GẶP PHẢI</p>
        </div>
        <div class="box-problem">
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan1.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Dữ liệu khách hàng bị thất lạc, phân tán, chồng chéo, thiếu tính chính xác, bảo
                    mật
                    kém
                    do quản lý thủ công bằng sổ sách, excel…</p>
            </div>
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan2.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Không có cái nhìn toàn diện về khách hàng của doanh nghiệp, gây khó khăn khi ra
                    quyết
                    định và định hướng chiến lược phát triển phù hợp</p>
            </div>
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan3.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Tỷ lệ khách hàng rời bỏ doanh nghiệp cao, tỷ lệ upsell thấp do khách hàng không
                    được
                    chăm sóc và nuôi dưỡng thường xuyên</p>
            </div>
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan4.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Tốn kém chi phí Marketing do không đo lường được hiệu quả chiến dịch truyền thông
                </p>
            </div>
        </div>
        <div class="box-problem flex gap-8 justify-center">
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan5.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Gặp khó khăn trong trao đổi công việc và tương tác nội bộ do không có nền tảng quản
                    lý
                    công việc/ dự án chuẩn chỉnh
                </p>
            </div>
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan6.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Không có hình thức quản lý công việc và đánh giá chất lượng công việc của nhân
                    viên/
                    phòng ban</p>
            </div>
            <div class="problem relative w-1/4">
                <div class="flex justify-center items-center">
                    <img src="{{asset("site/images/iconkhokhan7.png")}}" class="w-24 h-24 absolute top-0">
                </div>
                <p class="box-vande">Tổng hợp báo cáo thủ công gây tốn thời gian, lãng phí nhân lực. Không có công cụ đo
                    lường trực quan về tình trạng sức khỏe của doanh nghiệp</p>
            </div>
        </div>
        <div class="flex py-10 items-center justify-center">
            <p class="font-bold text-2xl text-btn-color">GIẢI PHÁP GỠ RỐI TRONG QUẢN LÝ DOANH NGHIỆP</p>
        </div>
        <div class="flex flex-col product-container features">
            <div class="feature-details">
                <span class="anchor" id="1stFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content">
                    <div class="feature-logo feature-logo-mobile">
                        <img src="{{asset("site/images/icongiaiphap1.png")}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <p class="font-bold text-2xl text-primary-blue500">Quản lý khách hàng tập trung và đồng
                                bộ
                            </p>
                            <p class="text-secondary-neutral600 text-sm">Getfly CRM là công cụ đắc lực giúp
                                bạn quản lý khách hàng tập trung
                                và
                                đồng bộ trên 1 nền tảng.</p>
                            <div class="feature-info">
                                <ul>
                                    <li>Dễ theo dõi, nắm được thông tin khách hàng một cách chi tiết cụ thể nhất</li>
                                    <li>Cập nhật, chỉnh sửa thông tin nhanh chóng, dễ dàng</li>
                                    <li>Tiết kiệm thời gian tìm kiếm thông tin khách hàng</li>
                                    <li>Dễ dàng lọc, phân loại và phân nhóm khách hàng theo từng nhu cầu chi tiết nhất
                                    </li>
                                    <li>Ghi nhận giá trị vòng đời khách hàng giúp xây dựng mối quan hệ lâu bền giữa
                                        thương
                                        hiệu
                                        - khách hàng
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="w-1/2 full">
                            <div class="feature-image">
                                <img src="{{asset("site/images/icongiaiphap1.png")}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-details">
                <span class="anchor" id="2ndFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content">
                    <div class="feature-logo feature-logo-right feature-logo-mobile">
                        <img src="{{asset("site/images/icongiaiphap3.png")}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <div class="feature-image">
                                <img src="{{asset("site/images/giaiphap3.png")}}">
                            </div>
                        </div>
                        <div class="w-1/2 full pl120">
                            <p class="font-bold text-2xl text-primary-blue500">100% khách hàng được chăm sóc nuôi dưỡng
                                chuyên nghiệp</p>
                            <p class="text-secondary-neutral600 text-sm">Tích hợp giải pháp Getfly CRM với Email/ SMS
                                marketing/ Marketing
                                Automation giúp
                                doanh nghiệp chăm sóc khách hàng tự động - cá nhân hóa thông qua các kịch bản được thiết
                                lập
                                sẵn, xây
                                dựng sự tin tưởng của khách hàng với sản phẩm / dịch vụ của doanh nghiệp.</p>
                            <div class="feature-info">
                                <ul>
                                    <li>Đảm bảo 100% khách hàng được chăm sóc và nuôi dưỡng tự động theo kịch bản được
                                        xây dựng
                                        sẵn
                                    </li>
                                    <li>Khai thác tối đa cơ hội bán hàng khi khách hàng rơi vào phễu đăng ký, đến khi
                                        mua hàng
                                        và trở thành
                                        khách hàng thân thiết
                                    </li>
                                    <li>Dễ dàng kiểm soát và đo lường hiệu quả công tác automation marketing trước,
                                        trong và sau
                                        khi bán
                                        hàng
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-details lastFeature">
                <span class="anchor" id="3rdFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content" style="display: flex;align-items: flex-start;">
                    <div class="feature-logo feature-logo-mobile">
                        <img src="{{asset("site/images/icongiaiphap4.png")}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <p class="font-bold text-2xl text-primary-blue500">Quản lý công việc thông minh</p>
                            <p class="text-secondary-neutral600 text-sm">Với nền tảng quản lý công việc chặt chẽ và khoa
                                học, Getfly CRM sẽ
                                giúp
                                cải thiện
                                hiệu suất cá nhân, phòng ban cũng như hiệu suất chung của toàn doanh nghiệp.</p>
                            <ul>
                                <li>Giám sát đầu mục và tiến độ thực hiện công việc của nhân viên</li>
                                <li>Kiểm soát deadline trên từng đầu mục công việc</li>
                                <li>Dễ dàng trao đổi và tag bộ phận liên quan khi cần hỗ trợ</li>
                                <li>Đánh giá mức độ hoàn thành công việc thúc đẩy nhân viên hoàn thành 100% năng lực
                                </li>
                            </ul>
                        </div>
                        <div class="w-1/2 full">
                            <div class="feature-image fadeInRight">
                                <img src="{{asset("site/images/giaiphap4.png")}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-details">
                <span class="anchor" id="2ndFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content" style="display: flex;align-items: flex-start;">
                    <div class="feature-logo feature-logo-right feature-logo-mobile">
                        <img src="{{asset("site/images/icongiaiphap5.png")}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <div class="feature-image">
                                <img src="{{asset("site/images/giaiphap5.png")}}">
                            </div>
                        </div>
                        <div class="w-1/2 full pl120">
                            <div class="feature-info">
                                <p class="font-bold text-2xl text-primary-blue500">Quản lý và đo lường hiệu quả
                                    Marketing</p>
                                <p class="text-secondary-neutral600 text-sm">Getfly CRM cung cấp cho doanh nghiệp nền
                                    tảng quản lý và đo lường
                                    chiến dịch
                                    Marketing hiệu quả. Nhờ đó doanh nghiệp có cơ sở để phân bố ngân sách marketing hợp
                                    lý, tiết kiệm chi
                                    phí. Đồng thời với những con số báo cáo cụ thể, nhà quản lý có thể nắm bắt được hiệu
                                    quả hoạt động của
                                    từng khu vực sales và marketing, tránh những mâu thuẫn nội bộ thường xảy ra giữa hai
                                    bộ phận này. </p>
                                <ul>
                                    <li>Nắm bắt được sự tăng trưởng và biến động cơ hội đăng ký vào từng chiến dịch</li>
                                    <li>Đánh giá tỷ lệ chuyển đổi, nguồn khách hàng, thông điệp truyền thông</li>
                                    <li>Cho phép liên kết với automation marketing để hình thành các kịch bản nuôi dưỡng
                                        khách hàng khi đăng
                                        ký vào chiến dịch
                                    </li>
                                    <li>Đo lường sự tương tác của khách hàng khi triển khai các chiến dịch Marketing: tỷ
                                        lệ mở, đọc email,
                                        click link, mở, đọc SMS…
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-details lastFeature">
                <span class="anchor" id="3rdFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content" style="display: flex;align-items: flex-start;">
                    <div class="feature-logo feature-logo-mobile">
                        <img src="{{asset("site/images/icongiaiphap2.png")}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <div class="feature-info">
                                <p class="font-bold text-2xl text-primary-blue500">Quản lý giám sát đội ngũ kinh doanh
                                </p>
                                <p class="text-secondary-neutral600 text-sm">Getfly CRM hỗ trợ hình thành các quy trình
                                    bán hàng
                                    tự động hóa - chuyên
                                    nghiệp. Từ
                                    đó, nhà quản lý có thể dễ dàng đo lường hiệu quả, đánh giá các hoạt động và kết quả
                                    của đội
                                    ngũ nhân
                                    viên kinh doanh của mình.</p>
                                <ul>
                                    <li>Dễ dàng nắm bắt số lượng, tình trạng khách hàng và doanh số của từng nhân viên
                                        sales
                                    </li>
                                    <li>Giám sát toàn bộ quá trình từ khi tiếp cận khách hàng đến khi lên đơn hàng của
                                        sale
                                    </li>
                                    <li>Giám sát số đơn hàng được lên trong ngày, số đơn hàng cụ thể của nhân viên</li>
                                    <li>Nắm bắt giá trị đơn hàng, chiết khấu công nợ trên từng đơn hàng ấy</li>
                                    <li>Quản lý, thiết lập và nhắc nhở lịch hẹn tới từng nhân viên kinh doanh</li>
                                </ul>
                            </div>
                        </div>
                        <div class="w-1/2 full">
                            <div class="feature-image fadeInRight">
                                <img src="{{asset("site/images/giaiphap2.png")}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="feature-details">
                <span class="anchor" id="2ndFeature"></span>
                <div class="borderline"></div>
                <div class="feature-content" style="display: flex;align-items: flex-start;">
                    <div class="feature-logo feature-logo-right feature-logo-mobile">
                        <img src="{{asset('site/images/icongiaiphap6.png')}}">
                    </div>
                    <div class="box-solu">
                        <div class="w-1/2 full">
                            <div class="feature-image">
                                <img src="{{asset("site/images/giaiphap6.png")}}">
                            </div>
                        </div>
                        <div class="w-1/2 full pl120">
                            <div class="feature-info">
                                <p class="font-bold text-2xl text-primary-blue500">Đo lường KPI tự động - chính xác -
                                    realtime
                                </p>
                                <p class="text-secondary-neutral600 text-sm">Doanh nghiệp thường mất rất nhiều thời gian
                                    để báo
                                    cáo, tổng hợp kết quả kinh doanh.
                                    Tuy nhiên giải pháp Getfly CRM có thể tự động tổng hợp các dữ liệu tương tác với
                                    khách hàng
                                    thành “1
                                    BẢNG CHỈ SỐ DUY NHẤT” thể hiện toàn bộ hoạt động kinh doanh của doanh nghiệp CHÍNH
                                    XÁC đến
                                    từng giây.
                                </p>
                                <ul>
                                    <li>KPI đầy đủ và chi tiết cho khách hàng, từng phòng ban, nhân viên, công việc cụ
                                        thể
                                    </li>
                                    <li>Hiển thị số liệu một cách trực quan, rõ ràng</li>
                                    <li>Dễ dàng theo dõi và truy xuất ngay tại thời gian thực (real-time) giúp tiết kiệm
                                        thời
                                        gian báo cáo,
                                        ra quyết định nhanh chóng
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex gap-2 justify-center items-center">
            <a href="#dangkyngay" class="bg-[#ff6901] text-white text-base rounded-md"
               style="text-decoration: none;padding: 10px 20px;">
                Nhận tư vấn
            </a>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div class="py-10 text-center">
            <h3 class="font-bold text-2xl text-white">Hơn <b>4000 SMEs</b> thuộc 200+ ngành
                nghề đã và
                đang tin tưởng ứng dụng Getfly CRM để quản lý và tăng tốc bán hàng!</h3>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="title-box">Phản hồi khách hàng</p>
        <div id="controls-carousel" class="relative w-full" data-carousel="slide">
            <div class="feedback-detail">
                <div class="hidden duration-700" data-carousel-item="active">
                    <div class="slide-detail">
                        @foreach($homeViewModel->getFeedbacks() as $feedback)
                            <div class="mobile w-1/3 slides">
                                <div class="flex flex-col items-center justify-center gap-2">
                                    <img src="{{$feedback->getAvatar()}}" class="slides-img">
                                    <p class="font-bold text-base">{{$feedback->getName()}}</p>
                                    <p class="text-gray-400 lg:text-sm">{{$feedback->getPosition()}}</p>
                                    <p class="lg:text-sm">{{$feedback->getNote()}}
                                    </p>
                                </div>
                                <div class="text-right pt-3">
                                    <a href="{{route("sites.story",$feedback->getId())}}"
                                       class="text-btn-color font-bold">Xem thêm</a>
                                </div>
                            </div>
                            @if(($loop->index+1)%3==0 && !$loop->last)
                    </div>
                </div>
                <div class="hidden duration-700" data-carousel-item>
                    <div class="slide-detail">
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <button type="button" class="btn-next left-0" data-carousel-prev>
                <img src="{{asset("site/images/left.png")}}">
            </button>
            <button type="button" class="btn-next right-0" data-carousel-next>
                <img src="{{asset("site/images/right.png")}}">
            </button>
        </div>
    </div>
    @include("sites.shared.customers",['homeViewModel'=>$homeViewModel])
    <div class="flex justify-center py-10 bg-[#2c3e50]" id="hero"
         style="background: url({{asset("site/images/bannergetfly.png")}}) repeat top center;">
        <div class="text-center">
            <h2 style="text-align: center;color: #FFF;
    padding: 0;
    margin-bottom: 10px;
    text-transform: uppercase;
    font-size: 24px;
    font-weight: 700;
    margin-top: 0px;">ĐỪNG NGẦN NGẠI ĐỂ ĐẦU TƯ – HÃY ĐỂ GETFLY LO HỘ BẠN VỀ
                GIÁ
            </h2>
            <p style="    color: #fff;
    font-size: 18px;
    padding-top: 20px;
    line-height: 0;">Cam kết đồng hành cùng phát triển.</p>
        </div>
    </div>
    <div class="box-price hide">
        <div class="price-banner bg-banner">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM ON CLOUD</p>
            <p class="text-white">(Ưu đãi khi mua nhiều năm)</p>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
        <div class="border-blue-600 flex gap-2 w-full flex-col items-center border justify-center overflow-x-auto">
            <table class="w-full text-left text-gray-500 dark:text-gray-400 ">
                <thead class="text-xs text-gray-700 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="p-3 md:p-1 lg:p-0 w-40 ">
                    </th>
                    @foreach($pricing as $index => $item)
                        <th scope="col" class="p-3 md:p-1 lg:p-0 w-40  @if(!$loop->last) @endif text-center
                         @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif
                         ">
                            <p class="text-lg text-blue-color text-sm mt-2">{{$item->getName()}}</p>
                            @if($item->getPrice()!=0)
                                <p class="text-base text-btn-color my-5">{{number_format($item->getPrice())}}đ/tháng</p>
                                <p>{{number_format($item->getUserPrice())}}đ/1 user/ tháng</p>
                            @else
                                <p class="text-base text-btn-color my-5">Liên hệ</p>
                                <p>Liên hệ</p>
                            @endif
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý Marketing"))}}">Quản lý Marketing</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach

                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Lưu trữ và chăm sóc khách hàng"))}}">
                                Lưu trữ và chăm sóc khách hàng
                            </a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2 @if(!$loop->last) @endif  @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif ">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý đội kinh doanh"))}}">Quản lý đội kinh doanh</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý bán hàng"))}}">Quản lý bán hàng</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý công việc"))}}">Quản lý công việc</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Quản lý SMS Brandname và Email Marketing"))}}">Quản lý SMS
                                Brandname <br>và Email Marketing</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                            <a href="{{url(static_link("Marketing tự động"))}}">Marketing tự động</a>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Quản lý kênh giới thiệu"))}}">Quản
                                lý kênh giới thiệu</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Đo lường KPI"))}}">Đo lường
                                KPI</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Hỗ trợ App"))}}">Hỗ trợ App</a>
                        </p>
                        </p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 2em;height: 2em" src="{{asset("/site/images/app store.png")}}">
                                <img style="width: 2em;height: 2em" src="{{asset("/site/images/google play.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Số lượng người dùng"))}}">Số
                                lượng người dùng</a></p>
                        </p>
                    </th>
                    @foreach($pricing as $item)

                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{$item->getUsers()}} Người
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Số lượng khách hàng"))}}">Số
                                lượng khách hàng</a></p>
                    </th>

                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{number_format($item->getCustomers())}} Khách hàng
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định
                                domain</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            @if($item->getDomainTarget()==0)
                                <div class="flex justify-center">
                                    <img style="width: 1em" src="{{asset("/site/images/remove.png")}}">
                                </div>
                            @else
                                <div class="flex justify-center">
                                    <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                                </div>
                            @endif
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Thời gian Setup"))}}">Thời gian
                                Setup</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                {{$item->getSetupTime()}}
                            </div>
                        </td>
                    @endforeach

                </tr>
                <tr class="bg-white border-b">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"><a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Bộ
                                tài liệu đào tạo</a></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc @endif">
                            <div class="flex justify-center">
                                <img style="width: 1em" src="{{asset("/site/images/check.png")}}">
                            </div>
                        </td>
                    @endforeach
                </tr>
                <tr class="bg-white">
                    <th scope="row"
                        class="px-6 py-4 md:p-2 lg:p-2 border-r font-medium text-gray-900 whitespace-nowrap">
                        <p class="font-bold text-blue-color"></p>
                    </th>
                    @foreach($pricing as $item)
                        <td class="px-6 py-4 md:p-2 lg:p-2   @if($item->getName()=="Doanh Nghiệp Nhỏ")border-blue-600 fc border-bottom-2 @endif">
                            <div class="flex justify-center">
                                <a href="{{route("sites.trial")}}"
                                   class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-5 lg:px-2 lg:p-1 md:text-xs">
                                    Dùng thử
                                </a>
                            </div>
                        </td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        </div>
        <div class="price-banner bg-banner mt-20 rounded-lg hide">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM cài đặt sever riêng (on premise)</p>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
    </div>
    <div class="hidemobile p-4">
        <div class="price-banner bg-banner">
            <p class="uppercase font-bold text-white text-2xl">Bảng giá CRM ON CLOUD</p>
            <p class="text-white">(Ưu đãi khi mua nhiều năm)</p>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                Nhận báo giá
            </a>
        </div>
        @foreach($pricing as $item)
            <div class="border p-4 {{!$loop->last?"mb-4":""}}">
                <p class="text-2xl text-blue-color font-bold text-center">{{$item->getName()}}</p>
                <div class="text-center font-bold">
                    <p class="text-base text-btn-color my-2">{{number_format($item->getPrice())}}đ/ tháng</p>
                    <p>{{number_format($item->getUserPrice())}}đ/ 1 người dùng/ tháng</p>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý Marketing"))}}">Quản lý Marketing</a>

                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Lưu trữ và chăm sóc khách hàng"))}}">Lưu trữ và chăm sóc khách hàng</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý đội kinh doanh"))}}">Quản lý đội kinh doanh</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý bán hàng"))}}">Quản lý bán hàng</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý công việc"))}}">Quản lý công việc</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Quản lý SMS Brandname và Email Marketing"))}}">Quản lý SMS Brandname
                        và Email Marketing</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Marketing tự động</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Quản lý kênh giới thiệu</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Đo lường KPI</a>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Hỗ trợ App</a>
                    </div>
                    <div class="flex gap-2 w-1/2 justify-end">
                        <img src="{{asset("site/images/app store.png")}}">
                        <img src="{{asset("site/images/google play.png")}}">
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Số lượng người dùng"))}}">Số lượng người dùngo</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{$item->getUsers()}} Người
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Số lượng khách hàng"))}}">Số lượng khách hàng</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{number_format($item->getCustomers())}} Khách hàng
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    @if($item->getDomainTarget())
                        <img src="{{asset("site/images/check.png")}}">
                        <a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định domain</a>
                    @else
                        <img src="{{asset("site/images/remove.png")}}">
                        <a href="{{url(static_link("Chỉ định domain"))}}">Chỉ định domain</a>
                    @endif

                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <div class="w-1/2">

                        <a href="{{url(static_link("Thời gian Setup"))}}">Thời gian Setup</a>
                    </div>
                    <div class="w-1/2 text-right">
                        {{$item->getSetupTime()}}
                    </div>
                </div>
                <div class="border-b flex py-2 gap-2 items-center">
                    <img src="{{asset("site/images/check.png")}}">Bộ tài liệu đào tạo
                    <a href="{{url(static_link("Bộ tài liệu đào tạo"))}}">Bộ tài liệu đào tạo</a>
                </div>
                <div class="flex justify-center pt-3">
                    <a href="{{route("sites.trial")}}" class="btn-trial btn">
                        Dùng thử
                    </a>
                </div>
            </div>
        @endforeach

    </div>
    <div class="fea-gellery gellery" id="gallery">
        <div class="our-story text-center" id="dangkyngay">
            <p style="font-size: 30px; font-weight: bold; color: #f36a26; text-transform: uppercase;">Dùng thử miễn phí
                30
                ngày phần mềm Getfly CRM</p>
            <p style="text-align: center; font-size: 14px; color: #FFF;">Hãy đăng ký trải nghiệm thử 30 ngày miễn phí
                Getfly CRM
                ngay
                hôm nay để xem chúng tôi có thể giúp bạn <br> quản lý công việc kinh doanh, gia tăng doanh thu và tiết
                kiệm
                thời gian tuyệt vời như thế nào. </p>
        </div>
        <div class="flex gap-4 pt-10">
            <div class="w-1/2 text-right">
                <input type="radio" value="1" name="city" class="choose_city_solution w-6 h-6" checked/>
                <label style="color: #FFF; margin-top: 7px;">Hà Nội</label>
            </div>
            <div class="w-1/2">
                <input type="radio" value="2" name="city" class="choose_city_solution w-6 h-6"/>
                <label style="color: #FFF; margin-top: 7px;">Hồ Chí Minh</label>
            </div>
        </div>
        <div class="flex items-center justify-center pt-10">
            <div id="getfly_hn_solution" style="width: 500px;">
                {!! embed_form("dang_ky_hn") !!}
            </div>
            <div id="getfly_hcm_solution" style="width: 500px;" class="hidden">
                {!! embed_form("dang_ky_tphcm") !!}
            </div>
        </div>
    </div>
@endsection
@push("after_scripts")
    <script>
        $(document).ready(function () {
            $('.choose_city_solution').change(function () {
                const ids = $(this).val();
                if (ids === "1") {
                    $('#getfly_hn_solution').removeClass('hidden');
                    $('#getfly_hcm_solution').addClass('hidden');
                } else {
                    $('#getfly_hcm_solution').removeClass('hidden');
                    $('#getfly_hn_solution').addClass('hidden');
                }
            });
        });
    </script>
@endpush