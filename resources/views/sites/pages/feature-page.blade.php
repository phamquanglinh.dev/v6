@php
    use App\ViewModels\Sites\FeaturePage\FeaturePageViewModel;
    /**
* @var FeaturePageViewModel $featurePageViewModel
 */
    $page = $featurePageViewModel->getPageFeature();
    $disablePopup = true;
    $newspaper_seo = true;
@endphp
@extends("sites.shared.layout")
@section("title")
    {{$page->getTitle()}}
@endsection
@section("newspaper_seo")
    <meta name="author" content="https://getfly.vn/">
    <meta name="description"
          content="{{$page->getDescription()}}">
    <meta name="keywords"
          content="Phần mềm CRM, Phần mềm quản lý khách hàng, Phần mềm chăm sóc khách hàng, Phần mềm quản lý và chăm sóc khách hàng toàn diện">
    <!-- ogp -->
    <meta property="og:title" content="{{$page->getTitle()}}">
    <meta property="og:type" content="article">
    <meta property="og:url"
          content="{{url()->full()}}">

    <meta property="og:image" content="https://getflycrm.com/wp-content/uploads/2020/11/web-mobile-slide.jpg">

    <meta property="og:description" content="{{$page->getDescription()}}">
    <meta property="fb:app_id" content="637630076348771"/>
    <meta property="fb:admins" content="vuhoangduy"/>
    <meta property="fb:pages" content="328467620572169"/>
    <meta name="google-site-verification" content="cjPAARainmNgQA2irOLVfhaOfVCZfCpGQ7EJQm9KKDU">
    <meta name="ahrefs-site-verification" content="11d0f7f6ad53e5445fecfe9b436c65193c5d1bd8c85ccdee11e16d9b73cc99e5">
@endsection
@section("content")
    <style>


        .transform.z-10 {
            display: none;
            width: 0 !important;
        }

        .slide-item {
            position: unset !important;
        }

        td {
            padding: 1rem;
        }

        h1 {
            font-size: 2rem !important;
            color: #dc6803 !important;
        }

        h3 {
            font-size: 1.5rem !important;
            font-weight: bold;
        }

        h5 {
            font-size: 1rem !important;
            font-weight: bold;
            margin-bottom: 1rem;
        }

        #feature-page-content ul {
            list-style: disc !important;
        }

        #feature-page-content ol {
            list-style: auto !important;
        }

        #feature-page-content h2 {
            font-size: 1.5rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #feature-page-content h3 {
            font-size: 1rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #feature-page-content a {
            color: #0b4d75;
        }

        #feature-page-content img {
            padding: 2rem;
        }

        #feature-page-content p {
            margin-bottom: 0.5rem;
        }

        .w-50 {
            width: 50%;
        }

        @media (max-width: 768px) {
            tr {
                display: flex;
                flex-direction: column;
                height: 100% !important;
                width: 100% !important;
            }

            td {
                height: 100% !important;
            }

            colgroup {
                display: none;
            }


        }

        @media (min-width: 768px) {
            .md\:box-1\/3 {
                width: 33.33333%;
            }
        }
    </style>
    <div class="box-why bg-gray-300">
        <h1 style="line-height: inherit" class="mt-1 font-semibold text-primary-blue500 text-2xl">
            {{$page->getTitle()}}
        </h1>
        <div class="my-2">
            {{$page->getDescription()}}
        </div>
        <div class="silder my-5">
            <div id="slider-page-feature" class="relative w-full" data-carousel="slide">
                <!-- Carousel wrapper -->
                <div class="rounded-lg h-full">
                    @foreach($page->getSlides() as $slide)
                        <div class="slide-item hidden duration-700 ease-in-out"
                             data-carousel-item="{{$loop->first?"active":""}}">
                            <img src="{{url("/assets/uploads/".$slide)}}"
                                 class="block w-full"
                                 alt="...">
                        </div>
                    @endforeach
                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
                    @foreach($page->getSlides() as $key => $slide)
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                                data-carousel-slide-to="{{$key-1}}"></button>
                    @endforeach
                </div>
                <!-- Slider controls -->
                <button type="button"
                        class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                        data-carousel-prev>
                    <img style="width: 40px" src="{{asset("https://cdn-icons-png.flaticon.com/512/318/318477.png")}}">
                </button>
                <button type="button"
                        class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                        data-carousel-next>
                    <img style="width: 40px;" src="{{asset("https://cdn-icons-png.flaticon.com/512/181/181669.png")}}">
                </button>
            </div>

        </div>

    </div>
    <div class="box-why">
        <div class="mb-10" id="feature-page-content">
            {!! $page->getBody() !!}
        </div>
    </div>
    <hr>
    <div class="border">
        <h1 class="font-semibold text-center my-5">Có thể bạn quan tâm</h1>
        <div class="flex flex-wrap p-5 px-20">
            <div class="md:box-1/3 w-full mb-3">
                <h3 class="text-center font-semibold">Bán hàng</h3>
                <div class="my-5 text-center">Đơn giản rõ ràng</div>
                <div class="flex flex-wrap">
                    <a href="{{url("/tinh-nang/ban-hang/360-thong-tin-khach-hang-f1.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/gp1.png")}}">
                        <div class="pt-2 font-semibold">Quản lý khách hàng</div>
                    </a>
                    <a href="{{url("/tinh-nang/ban-hang/quy-trinh-ban-hang-f6.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/sales5.png")}}">
                        <div class="pt-2 font-semibold">Quy trình bán hàng</div>
                    </a>
                    <a href="{{url("/tinh-nang/ban-hang/giai-phap-quan-ly-phong-kinh-doanh-f25.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/sales33.png")}}">
                        <div class="pt-2 font-semibold">Phát triển phòng KD</div>
                    </a>
                    <a href="{{url("/tinh-nang/ban-hang/ung-dung-mobile-f48.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/sales34.png")}}">
                        <div class="pt-2 font-semibold">Ứng dụng trên mobile</div>
                    </a>
                </div>
            </div>
            <div class="md:box-1/3 w-full mb-3">
                <h3 class="text-center font-semibold">Marketing</h3>
                <div class="my-5 text-center">Tự động hóa - Chuyên nghiệp hóa</div>
                <div class="flex flex-wrap">
                    <a href="{{url("/tinh-nang/maketing/chien-dich-f16.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/marketing10.png")}}">
                        <div class="pt-2 font-semibold">Chiến dịch kinh doanh</div>
                    </a>
                    <a href="{{url("/giai-phap/email-marketing-s8.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/help2.png")}}">
                        <div class="pt-2 font-semibold text-xs text-sm">Email - SMS Brandname</div>
                    </a>
                    <a href="{{url("/tinh-nang/maketing/landing-page-f43.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/marketing8.png")}}">
                        <div class="pt-2 font-semibold text-xs text-sm">OptinForm&Landing Page</div>
                    </a>
                    <a href="{{url("/tinh-nang/maketing/nho-khach-hang-gioi-thieu-khach-hang-f18.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/gp8.png")}}">
                        <div class="pt-2 font-semibold text-xs text-sm">Marketing truyền miệng</div>
                    </a>
                </div>
            </div>
            <div class="md:box-1/3 w-full mb-3">
                <h3 class="text-center font-semibold">Chăm sóc khách hàng</h3>
                <div class="my-5 text-center">Bám sát từng khách hàng</div>
                <div class="flex flex-wrap">
                    <a href="{{asset("/tinh-nang/ban-hang/thoi-gian-lien-lac-lan-cuoi-f4.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/help1.png")}}">
                        <div class="pt-2 font-semibold">Lịch chăm sóc khách hàng</div>
                    </a>
                    <a href="{{asset("/tinh-nang/ban-hang/click-2-call-f26.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/marketing9.png")}}">
                        <div class="pt-2 font-semibold">Tổng đài điện thoại</div>
                    </a>
                    <a href="{{asset("tinh-nang/ban-hang/phan-hoi-khach-hang-f47.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/help3.png")}}">
                        <div class="pt-2 font-semibold">Phản hồi khách hàng</div>
                    </a>
                    <a href="{{url("/tinh-nang/ban-hang/lich-hen-f11.html")}}"
                       class="flex items-center flex-col w-50 cursor-pointer hover:bg-gray-50 transition-all p-5">
                        <img style="width: 3rem" class="rounded-full" src="{{asset("site/images/help7.png")}}">
                        <div class="pt-2 font-semibold ">Công việc - Ticket</div>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection