@php
    use App\ViewModels\Sites\LandingPage\LandingPageViewModel;
    /**
    * @var LandingPageViewModel $landingPageViewModel
    */
    $newspaper_seo = true;
    if(! $landingPageViewModel->getLandingPage()->getEnablePopup()){
        $disablePopup = true;
    }
@endphp
@extends("sites.shared.layout")
@section("title")
    {{$landingPageViewModel->getLandingPage()->getTitle()}}
@endsection
@section("newspaper_seo")
    <meta name="author" content="https://getfly.vn/">
    <meta name="description" content="{{$landingPageViewModel->getLandingPage()->getDescription()}}">
    <meta name="keywords" content="{{$landingPageViewModel->getLandingPage()->getTitle()}}">
    <!-- ogp -->
    <meta property="og:title" content="{{$landingPageViewModel->getLandingPage()->getTitle()}}">
    <meta property="og:type" content="article">
    <meta property="og:url" content="{{url()->current()}}">

    <meta property="og:image" content="{{url($landingPageViewModel->getLandingPage()->getThumbnail())}}">

    <meta property="og:description" content="{{$landingPageViewModel->getLandingPage()->getTitle()}}">
    <meta property="fb:app_id" content="637630076348771"/>
    <meta property="fb:admins" content="vuhoangduy"/>
    <meta property="fb:pages" content="328467620572169"/>
    <meta name="google-site-verification" content="cjPAARainmNgQA2irOLVfhaOfVCZfCpGQ7EJQm9KKDU">
    <meta name="ahrefs-site-verification" content="11d0f7f6ad53e5445fecfe9b436c65193c5d1bd8c85ccdee11e16d9b73cc99e5">
@endsection
@section("content")
    <div class="" id="newspaper_content">
        {!! $landingPageViewModel->getLandingPage()->getBody() !!}
    </div>
    <div class="fea-gellery gellery" id="gallery">
        <div class="our-story text-center mb-3" id="dangkyngay">
            <p style="font-size: 30px; font-weight: bold; color: #f36a26; text-transform: uppercase;">Dùng thử miễn phí
                30
                ngày phần mềm Getfly CRM</p>
            <p style="text-align: center; font-size: 14px; color: #FFF;">Hãy đăng ký trải nghiệm thử 30 ngày miễn phí
                Getfly CRM
                ngay
                hôm nay để xem chúng tôi có thể giúp bạn <br> quản lý công việc kinh doanh, gia tăng doanh thu và tiết
                kiệm
                thời gian tuyệt vời như thế nào. </p>
        </div>
        <div class="p-2 " style="background: white;width: 500px;margin: auto;border-radius: 1rem;">
            {!! $landingPageViewModel->getLandingPage()->getCustomForm() !!}
        </div>
    </div>
@endsection
