@php
    use App\ViewModels\Sites\Pages\AboutViewModel;
    /** @var AboutViewModel $aboutViewModel
     *
     */
    $siteInfo = $aboutViewModel->getInformation()

@endphp
@extends("sites.shared.layout")
@section("title")
    Về Getfly
@endsection
@section("content")
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] bg-banner4 py-5">
        <div class="box-header-banner px-2">
            <div class="w-2/5 mobile">
                <img src="{{asset("site/images/vegetfly.png")}}">
            </div>
            <div class="w-3/5 mobile">
                <button class="box-ve-getfly btn bg-btn-color">
                    Đôi nét về Getfly
                </button>
                <p class="text-white mb-4 text-base lg:text-sm md:text-[12px]">
                    Được thành lập và chính thức ra mắt thị
                    trường vào năm
                    2012,
                    Getfly
                    tự hào là
                    một trong những đơn vị
                    cung cấp giải pháp
                    và công cụ hỗ trợ quản trị doanh nghiệp hàng đầu tại Việt Nam.
                </p>
                <p class="text-white mb-4 text-base lg:text-sm md:text-[12px]">
                    Phát triển từ phần mềm quản lý và chăm sóc
                    khách hàng,
                    Getfly tập
                    trung nâng
                    cấp trở thành nền tảng
                    quản lý doanh nghiệp
                    đồng bộ và linh hoạt. Theo đó, doanh nghiệp có thể ứng dụng giải pháp Getfly để quản lý toàn diện và
                    hiệu quả tại hầu hết
                    các nghiệp vụ trên 1 nền tảng duy nhất. Bao gồm: quản lý khách hàng, marketing, hoạt động bán hàng
                    (báo giá, hợp đồng,
                    doanh thu), chăm sóc khách hàng, công việc, tài chính - kế toán, kho, nhân sự… Cho phép vận hành
                    doanh nghiệp tự động,
                    tiết kiệm thời gian 2-3 giờ mỗi ngày nhưng vẫn đảm bảo mọi hoạt động trơn tru, gia tăng đều đặn
                    doanh số 200-300%/ năm.
                    Bên cạnh đó, với vai trò là công ty công nghệ, Getfly thấu hiểu sứ mệnh của mình trước công cuộc
                    chuyển đổi số quốc gia.
                    Tự tin có thể dẫn dắt cộng đồng doanh nghiệp Việt Nam (cung cấp giải pháp tư vấn và triển khai)
                    chuyển đổi số hiệu quả
                    để hướng tới phát triển bền vững.</p>
                <p class="text-white mb-4 text-base lg:text-sm md:text-[12px]">
                    Cho đến hiện nay, Getfly vinh dự được 4500+ khách hàng thuộc 200+ ngành nghề khác nhau tin tưởng lựa
                    chọn.
                    Tin rằng trong tương lai bằng kinh nghiệm, năng lực nội tại cùng với sự đồng hành của Quý khách
                    hàng, Getfly CRM sẽ mang
                    lại nhiều “giá trị đặc biệt” to lớn, hữu ích giúp doanh nghiệp vượt qua thử thách và bứt tốc thành
                    công.
                </p>
            </div>
        </div>
    </div>
    <div class="py-16 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div class="mission px-2">
            <div class="w-3/5 mobile">
                <div class="mb-14">
                    <p class="vision">Tầm nhìn & sứ mệnh</p>
                    <p>Định vị là công ty công nghệ số, Getfly luôn thấu hiểu và kiên định trước sứ mệnh của mình trong
                        công cuộc chuyển đổi số
                        toàn quốc cũng như mang sản phẩm phần mềm Việt chất lượng cao đến gần tới các doanh nghiệp lớn
                        nhỏ trong nước và rộng
                        khắp Đông Nam Á.</p>
                </div>
                <div class="mb-14">
                    <p class="vision">VĂN HÓA DOANH NGHIỆP</p>
                    <p>Song hành cùng tầm nhìn và sứ mệnh, không thể không nhắc tới giá trị cốt lõi về
                        “#Văn_hóa_biết_ơn” mà Getfly luôn gìn
                        giữ và phát triển. Đối với Getfly, khách hàng luôn được đặt tại vị trí trung tâm và yếu tố ưu
                        tiên hàng đầu. Cũng vì
                        vậy, mỗi thành viên Getfly từ lãnh đạo và toàn thể nhân viên đều luôn giữ trong mình sự nhiệt
                        huyết, tận tâm, tập trung,
                        bền bỉ, sáng tạo để “tối ưu lợi nhuận, tối đa giá trị”, đem đến trải nghiệm khách hàng vượt
                        trội.</p>
                </div>
            </div>
            <div class="w-2/5 mobile">
                <img src="{{asset("site/images/vision-mission.png")}}">
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="title-box">Giá trị cốt lõi 5T</p>
        <div class="values pt-10 px-2">
            <div class="core-values-group mobile">
                <img src="{{asset("site/images/thauhieu.png")}}" class="core-values-img">
                <p class="font-bold text-primary-blue500 text-3xl">01</p>
                <p class="font-bold text-primary-orange500 uppercase text-xl mb-5">Thấu hiểu</p>
                <p class="text-center">Luôn luôn lắng nghe thấu hiểu nhu cầu, đặt mình vào vị trí khách hàng để hiểu cái
                    khó của người sử dụng</p>
            </div>
            <div class="core-values-group mobile">
                <img src="{{asset("site/images/trithuc.png")}}" class="core-values-img">
                <p class="font-bold text-primary-blue500 text-3xl">02</p>
                <p class="font-bold text-primary-orange500 uppercase text-xl mb-5">Tri thức</p>
                <p class="text-center">Sáng tạo là giá trị cốt lõi, luôn luôn đặt bài toán: Khách hàng có lợi gì? Khách
                    hàng có thuận tiện không? Có thể tốt
                    hơn được không?</p>
            </div>
            <div class="core-values-group mobile">
                <img src="{{asset("site/images/tinnhiem.png")}}" class="core-values-img">
                <p class="font-bold text-primary-blue500 text-3xl">03</p>
                <p class="font-bold text-primary-orange500 uppercase text-xl mb-5">Tín nhiệm</p>
                <p class="text-center">Là đối tác chân thành đồng hành cùng phát triển</p>
            </div>
        </div>
        <div class="values px-2">
            <div class="core-values-group mobile">
                <img src="{{asset("site/images/tinhte.png")}}" class="core-values-img">
                <p class="font-bold text-primary-blue500 text-3xl">04</p>
                <p class="font-bold text-primary-orange500 uppercase text-xl mb-5">Tinh tế</p>
                <p class="text-center">Tinh tế trong thiết kế, thuận tiện trong sử dụng, tế nhị trong việc triển khai và
                    hỗ trợ khách hàng</p>
            </div>
            <div class="core-values-group mobile">
                <img src="{{asset("site/images/trogiup.png")}}" class="core-values-img">
                <p class="font-bold text-primary-blue500 text-3xl">05</p>
                <p class="font-bold text-primary-orange500 uppercase text-xl mb-5">Trợ giúp</p>
                <p class="text-center">Trợ giúp theo đúng đặc thù của doanh nghiệp Không áp đặt, máy móc</p>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] px-2">
        <p class="title-box">Vì sao nên chọn getfly crm</p>
        <div class="why2-group px-2">
            <img src="{{asset("site/images/why1.png")}}" class="w-32 h-32">
            <div>
                <p class="text-primary-orange500 font-bold text-xl mb-3">Là phần mềm Việt có giao diện thân thiện và dễ
                    dùng
                </p>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Dễ dàng tùy chỉnh đối với yêu cầu của từng doanh nghiệp
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Đáp ứng đa ngôn ngữ: Việt - Anh - Thái - Nhật - Trung quốc - Hàn quốc
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Linh hoạt sử dụng trên PC & App Mobile
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Nền tảng kết nối mở có thể kết nối được nhiều hệ thống khác nhau
                </div>
            </div>
        </div>
        <div class="why2-group px-2">
            <img src="{{asset("site/images/why2.png")}}" class="w-32 h-32">
            <div>
                <p class="text-primary-orange500 font-bold text-xl mb-3">Support liên tục trong suốt quá trình tìm hiểu
                    và sử dụng hệ thống
                </p>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Đào tạo trực tiếp tại doanh nghiệp
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Đào tạo liên tục offline 4 buổi/ tháng tại văn phòng Getfly CRM
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Các khóa đào tạo online trên Fanpage
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    <span>
                        <span>Kho tàng kiến thức hướng dẫn chi tiết: qua kênh Youtube "Học viện Getfly", khóa học trực tuyến,
                    trang tra cứu thông tin</span>
                         <a class="text-blue-900"
                            href="https://helpdesk.getfly.vn/huong-dan-su-dung/">Getfly Helpdesk</a>.
                    </span>
                </div>
            </div>
        </div>
        <div class="why2-group px-2">
            <img src="{{asset("site/images/why3.png")}}" class="w-32 h-32">
            <div>
                <p class="text-primary-orange500 font-bold text-xl mb-3">Hệ thống giải đáp đa kênh, đa ngôn ngữ</p>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Tổng đài hỗ trợ, chăm sóc khách hàng
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Hỗ trợ giải đáp trực tiếp trên fanpage / group
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Hỗ trợ online từ đội ngũ kỹ thuật
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Hệ thống yêu cầu hỗ trợ ticket
                </div>
            </div>
        </div>
        <div class="why2-group px-2">
            <img src="{{asset("site/images/why4.png")}}" class="w-32 h-32">
            <div>
                <p class="text-primary-orange500 font-bold text-xl mb-3">Không ngừng nâng cấp và phát triển hệ thống CRM
                </p>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Cập nhật và nâng cấp tính năng 2 lần /tháng
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Tập trung nâng cao trải nghiệm giao diện người dùng và nâng cấp các tính năng.
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Luôn bắt kịp xu thế công nghệ trên thế giới
                </div>
                <div class="flex gap-2 items-center mb-3">
                    <img src="{{asset("site/images/check1.png")}}">
                    Cập nhật & nâng cấp tính năng theo yêu cầu
                </div>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="title-box">Ai nên sử dụng getfly crm ?</p>
        <div class="box-who2 px-2">
            <div class="w-1/4 mobile">
                <div class="who-group">
                    <img src="{{asset("site/images/who1.png")}}" class="who-imgage">
                    <p class="pt-12 h-44">Doanh nghiệp cần công cụ để quản lý khách hàng, tránh việc
                        rò rỉ thông
                        tin khi
                        nhân
                        viên nghỉ
                        việc</p>
                </div>
            </div>
            <div class="w-1/4 mobile">
                <div class="who-group">
                    <img src="{{asset("site/images/who2.png")}}" class="who-imgage">
                    <p class="pt-12 h-44">Doanh nghiệp không kiểm soát, đo đếm, đánh giá được chất
                        lượng công
                        việc của
                        nhân
                        viên, mong muốn gia tăng hiệu quả hoạt
                        động trong công việc</p>
                </div>
            </div>
            <div class="w-1/4 mobile">
                <div class="who-group">
                    <img src="{{asset("site/images/who3.png")}}" class="who-imgage">
                    <p class="pt-12 h-44">Doanh nghiệp có khách hàng bị bỏ quên, không được chăm sóc
                        thường
                        xuyên, mong
                        muốn
                        tự động marketing, nuôi dưỡng khách
                        hàng liên tục</p>
                </div>
            </div>
            <div class="w-1/4 mobile">
                <div class="who-group">
                    <img src="{{asset("site/images/who4.png")}}" class="who-imgage">
                    <p class="pt-12 h-44">Doanh nghiệp có chi phí marketing tốn kém vì không có công
                        cụ đo lường
                        để tối
                        ưu
                        hoạt động</p>
                </div>
            </div>
        </div>
    </div>
    <div class="box-banner bg-banner my-10 py-10">
        <div class="banner-action">
            <div class="banners mobile">
                <p class="banner-title">{{$siteInfo['partners']}}++</p>
                <p class="banner-des">Doanh nghiệp sử dụng</p>
            </div>
            <div class="banners mobile banner-center">
                <p class="banner-title">{{$siteInfo['profession']}}++</p>
                <p class="banner-des">Ngành nghề</p>
            </div>
            <div class="banners mobile">
                <p class="banner-title">{{$siteInfo['experience_years']}}++</p>
                <p class="banner-des">Năm kinh nghiệm</p>
            </div>
        </div>
        <div>
            <a href="{{route("sites.trial")}}"
               class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase">
                Trải nghiệm thử
            </a>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] mb-5">
        <p class="title-box">Giải pháp getfly</p>
        <div class="box-solution3 px-2">
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution1.png")}}" class="w-16">
                <p class="group-title">Giải pháp chăm sóc khách hàng</p>
                <p>Getfly CRM là công cụ đắc lực giúp bạn quản lý khách hàng đến từng chi tiết nhỏ nhất, ở bất kỳ đâu và
                    bất
                    kỳ lúc nào.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution2.png")}}" class="w-16">
                <p class="group-title">Giải pháp quản lý kinh doanh</p>
                <p>Getfly CRM giúp số hoá DN, tăng hiệu quả phối hợp giữa các phòng ban, tạo ra các quy trình chuẩn giúp
                    tự
                    động hoá doanh
                    nghiệp.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution3.png")}}" class="w-16">
                <p class="group-title">Giải pháp giao việc tự động</p>
                <p>Getfly CRM giúp cải thiện hoạt động của đội ngũ bán hàng từ đó doanh số tăng trưởng đều đặn thậm chí
                    đột
                    biến.</p>
            </div>
        </div>
        <div class="box-solution3 px-2">
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution4.png")}}" class="w-16">
                <p class="group-title">Giải pháp social CRM</p>
                <p>Hệ thống xây dựng quy trình tương tác với khách hàng trên mạng xã hội bằng cách thu thập thông tin,
                    hành
                    vi, tương tác
                    của người dùng, từ đó đưa ra các giải pháp, chiến lược.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution5.png")}}" class="w-16">
                <p class="group-title">Giải pháp SMS marketing</p>
                <p>Giúp đỡ các doanh nghiệp vừa và nhỏ (SME) tại Việt nam chăm sóc khách hàng tốt hơn qua tin nhắn
                    thương
                    hiệu với chi phí
                    hợp lý.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution6.png")}}" class="w-16">
                <p class="group-title">Giải pháp Email marketing</p>
                <p>Chăm sóc khách hàng tự động bằng kịch bản email marketing đã được thiết lập sẵn, xây dựng sự tin
                    tưởng
                    của khách hàng
                    với sản phẩm, dịch vụ công ty.</p>
            </div>
        </div>
        <div class="box-solution3 px-2">
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution7.png")}}" class="w-16">
                <p class="group-title">Giải pháp kênh Marketing tự động</p>
                <p>Getfly CRM cho phép bạn chủ động quản lý toàn bộ kênh Marketing từ Facebook, Website, các web vệ tinh
                    trỏ
                    về CRM. Mọi
                    thông tin đăng ký của khách hàng đều tự động đổ về 1 nguồn là CRM để nhân viên có thể chăm sóc khách
                    hàng ngay khi nhận
                    được thông tin đăng ký.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution8.png")}}" class="w-16">
                <p class="group-title">Giải pháp Marketing truyền miệng 3.0</p>
                <p>Công cụ Marketing truyền miệng 3.0 tích hợp trong Getfly CRM giúp khách hàng giới thiệu khách hàng
                    hưởng
                    hoa hồng. Các
                    doanh nghiệp chỉ cần làm tốt phần sản phẩm và dịch vụ, phần bán hàng tự nó sẽ vận động và lan toả
                    theo
                    phương pháp
                    truyền miệng.</p>
            </div>
            <div class="solution-group mobile">
                <img src="{{asset("site/images/solution9.png")}}" class="w-16">
                <p class="group-title">Giải pháp đo lường KPI</p>
                <p>Getfly CRM sở hữu giải pháp Call Center giúp doanh nghiệp chăm sóc khách hàng ở bất kỳ đâu và bất kỳ
                    lúc
                    nào. Mọi thông
                    tin trao đổi với khách hàng đều được lưu vào lịch sử khách hàng. Kiểm soát được chất lượng cuộc gọi
                    đến
                    gọi đi, đào tạo
                    kỹ năng telesale của nhân viên hay kiểm tra khiếu nại của khách hàng khi cần.</p>
            </div>
        </div>
    </div>
@endsection