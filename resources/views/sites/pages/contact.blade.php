@php
        @endphp
@extends("sites.shared.layout")
@section("title")
    Liên hệ
@endsection
@section("content")
    <div class=" 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] extend">
        <p class="text-2xl font-bold text-primary-blue500 uppercase text-center">Hãy liên hệ với chúng tôi</p>
        <p class="text-2xl font-bold text-primary-blue500 uppercase text-center">để nhận được sự hỗ trợ tốt nhất</p>
        <div class="contact-box mobile">
            <div class="w-96 shadow-xl rounded-xl mobile">
                <div class="border rounded-xl">
                    <div class="bg-primary-blue500 p-4 rounded-t-xl text-center">
                        <p class="text-white font-bold">Chat với chúng tôi</p>
                    </div>
                </div>
                <div class="p-3 py-6">
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Kênh tiếp nhận thông tin, yêu
                        cầu của khách hàng
                        qua tin nhắn
                    </div>
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Chat với chúng tôi để đặt lịch
                        demo về phần mềm
                    </div>
                    <div class="flex pt-4 items-center justify-center">
                        <a href="https://www.youtube.com/watch?v=aYWhrJOC8H8&list=PL3KWrD8szW3uRzaoqlwwTP_GCAUrcJZtl" target="_blank" class="text-primary-orange500 font-bold border-b ">Xem bộ video demo về phần mềm</a>
                    </div>
                    <div class="flex flex-col pt-32 pb-5 gap-8">
                        <p class="">Để chat với chúng tôi vui lòng click vào biểu tượng chat bên phải phía dưới
                            màn hình</p>
                        <p class="">Chúng tôi sẽ trực trả lời <span class="text-primary-orange500">từ 8:00 đến
                                17:30,</span>
                            ngoài ra giờ hành chính vui lòng để
                            lại thông tin chúng tôi sẽ liên hệ lại
                            sau.</p>
                    </div>
                </div>
            </div>
            <div class="w-96 shadow-xl rounded-xl mobile">
                <div class="border rounded-xl">
                    <div class="bg-primary-blue500 p-4 rounded-t-xl text-center">
                        <p class="text-white font-bold">Liên hệ với bộ phận kinh doanh</p>
                    </div>
                </div>
                <div class="p-3 py-6">
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Đặt lịch demo trực tiếp với tư
                        vấn viên
                    </div>
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Nhận tư vấn từ tư vấn viên
                    </div>
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Nhận thông tin về Getfly sẽ hỗ
                        trợ doanh nghiệp
                        của bạn thế nào
                    </div>
                    <div class="flex pt-4">
                        <a data-modal-target="defaultModal" data-modal-toggle="defaultModal"
                           class="text-primary-orange500 font-bold border-b cursor-pointer">
                            Liên hệ
                        </a>
                    </div>
                </div>
            </div>
            <div class="w-96 shadow-xl rounded-xl mobile">
                <div class="border rounded-xl">
                    <div class="bg-primary-blue500 p-4 rounded-t-xl text-center">
                        <p class="text-white font-bold">Liên hệ với bộ phận chăm sóc khách hàng</p>
                    </div>
                </div>
                <div class="p-3 py-6">
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Giải đáp mọi thắc mắc trong quá
                        trình sử dụng
                        Getfly
                    </div>
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Tiếp nhận hỗ trợ kỹ thuật
                    </div>
                    <div class="flex gap-2 mb-5">
                        <img src="{{asset("site/images/checked.png")}}" class="w-6 h-6"> Phản ảnh các vấn đề liên quan
                        đến phần mềm và
                        dịch vụ
                    </div>
                    <div class="flex pt-4 gap-4 items-end">
                        <a href="#" class="text-primary-orange500 font-bold border-b">Click để chat</a>
                        <a href="https://zalo.me/1353698433294121015" target="_blank" >
                            <img src="{{asset("site/images/zalo.png")}}" style="width: 3rem">
                        </a>
                        <a href="https://www.facebook.com/messages/t/100814384723126" target="_blank">
                            <img src="{{asset("site/images/messenger.png")}}" style="width: 3rem">
                        </a>
                        <a href="https://1.getfly.vn/c3s" target="_blank">
                            <img src="{{asset("site/images/ticket.png")}}" style="width: 3rem">
                        </a>
                    </div>
                    <div class="pt-16">
                        <p>(024) 6262 7662</p>
                        <p class="pt-10">Nhân viên support sẽ trực <span class="text-primary-orange500">từ 8:00 đến
                                17:30 từ thứ 2 đến
                                thứ
                                7</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=" info 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl">Thông tin liên hệ</p>
        <div class="infos">
            <div class="info-detail mobile">
                <p class="font-bold">Bộ phận kinh doanh</p>
                <p>Phone: (024) 6262 7662 - Nhánh 0</p>
                <p class="pl-14">(028) 6285 6395 - Nhánh 0</p>
                <p>Hotline: 0965 593 953</p>
                <p>Email: contact@getflycrm.com</p>
            </div>
            <div class="info-detail mobile">
                <p class="font-bold">Bộ phận chăm sóc khách hàng</p>
                <p>Phone: (024) 6262 7662 - Nhánh 1</p>
                <p class="pl-14">(028) 6285 6395 - Nhánh 1</p>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] my-10 ">
        <div class="info-box">
            <div class="info-group mobile">
                <img src="{{asset("site/images/he-sinh-thai-co-shopee.jpg")}}" class="rounded-tl-lg">
                <div class="py-4 text-center">
                    <p class="font-bold text-primary-blue500 text-base">Hệ sinh thái tích hợp Getfly</p>
                    <a href="{{route('sites.partner')}}"
                       class="text-primary-orange500 font-bold border-b cursor-pointer">
                        Xem ngay
                    </a>
                </div>
            </div>
            <div class="info-group mobile">
                <img src="{{asset("site/images/thong-tin-hoi-thao.png")}}" class="rounded-tl-lg">
                <div class="py-4 text-center">
                    <p class="font-bold text-primary-blue500 text-base">Thông tin hội thảo</p>
                    <a target="_blank" href="https://getfly.vn/i44-goc-bao-chitin-getfly/su-kien.html" class="text-primary-orange500 font-bold border-b cursor-pointer">
                        Xem ngay
                    </a>
                </div>
            </div>
            <div class="info-group mobile">
                <img src="{{asset("site/images/cong-dong-getfly.png")}}" class="rounded-tl-lg">
                <div class="py-4 text-center">
                    <p class="font-bold text-primary-blue500 text-base">Cộng đồng Getfly</p>
                    <a target="_blank" href="https://www.facebook.com/groups/getflycrm/"
                       class="text-primary-orange500 font-bold border-b cursor-pointer">
                        Xem ngay
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="defaultModal" tabindex="-1" aria-hidden="true"
         class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full mobile">
        <div class="relative w-full max-w-2xl max-h-full">
            <!-- Modal content -->
            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                <!-- Modal header -->
                <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                    <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                        Liên hệ tư vấn
                    </h3>
                    <button type="button"
                            class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-hide="defaultModal">
                        <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                  clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Close modal</span>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="p-6 space-y-6">
                    {!!embed_form("lien-he.html")!!}
                </div>
            </div>
        </div>
    </div>
@endsection