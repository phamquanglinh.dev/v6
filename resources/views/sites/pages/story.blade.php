@php
    use App\ViewModels\Sites\Feedback\FeedbackDetailViewModel;
    /**
    * @var FeedbackDetailViewModel $feedbackDetailViewModel
     */
    $feedback = $feedbackDetailViewModel->getFeedback()
@endphp
@extends("sites.shared.layout")
@section("title")
    Câu chuyện khách hàng
@endsection
@section("content")
    <div class="header-story 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div class="md:w-1/2 w-full">
            <div class="flex gap-8 flex-col">
                <p class="font-bold text-primary-orange500 text-xl">{{$feedback->getTitle()}}</p>
                <p class="font-semibold">{!! $feedback->getDescription() !!}</p>
            </div>
        </div>
        <div class="md:w-1/2 w-full">
            <img style="display: flex;justify-content: end;" src="{{$feedback->getAvatar()}}" class="w-100 rounded">
        </div>
    </div>
    <div class="story-border">
        <hr style="border-color: #000;">
    </div>
    <div class="detail-story 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div class="w-2/3 mobile">
            <p class="font-bold text-primary-blue500 text-xl mb-5">{{$feedback->getTitle()}}</p>
            <div class="content bg-white p-3 rounded">
                {!! $feedback->getContent() !!}
            </div>
        </div>
        <div class="w-1/3 mobile">
            <img src="{{asset("site/images/banner-left.png")}}">
        </div>
    </div>
    <style>
        .content p span {
            font-size: 16px !important;
            font-family: Montserrat, sans-serif!important;
        }
    </style>
@endsection