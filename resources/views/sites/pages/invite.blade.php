@php
    use App\ViewModels\Sites\Partner\PartnerListViewModel;
    /**
    * @var PartnerListViewModel $partnerListViewModel
     */
@endphp
@extends("sites.shared.layout")
@section("title")
    Thư mời hợp tác
@endsection
@section("content")
    <div class="relative">
        <div class="box-header">
            <div class="text-thongdiep">
                "Cùng Getfly tạo lên 1 hệ sinh thái giải pháp hỗ trợ quản trị doanh nghiệp nhỏ và vừa phát triển bền
                vững"
            </div>
            <div class="button-header">
                <a href="#doitacchinhthuc" class="dangky-doitac btn bg-btn-color"
                   style="box-shadow: 1px 2px 3px #122552;">
                    Đăng ký trở thành đối tác của Getfly
                </a>
            </div>
        </div>
        <img src="{{asset("site/images/banner-hoptac.png")}}" class="banner-hoptac">
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Thư ngỏ hợp tác</p>
        <div class="thongdiep-box px-2">
            <div class="flex gap-4 flex-col">
                <p class="font-bold">Kính gửi: Quý Khách hàng và Quý đối tác!</p>
                <p>
                    Lời đầu tiên, Công ty Cổ phần Công nghệ Getfly xin gửi lời chúc sức khoẻ và lời chào trân trọng nhất
                    đến Quý khách hàng,
                    Quý đối tác!
                </p>
                <p>Chúc Quý khách hàng, Quý đối tác nhiều sức khỏe, hạnh phúc, thành công và thịnh vượng!</p>
                <p>Công ty Cổ phần Công nghệ Getfly cũng xin gửi lời cảm ơn chân thành nhất tới Quý khách hàng, Quý đối
                    tác, đồng nghiệp đã
                    và đang đồng hành, tin tưởng và ủng hộ chúng tôi trong suốt thời gian qua.</p>
                <p>Chúng tôi cam kết luôn dành cho Quý đối tác những chính sách tốt nhất, sự hỗ trợ nhiệt tình và nhiều
                    các ưu đãi khác.
                    Rất mong nhận được những lời đề nghị hợp tác từ các doanh nghiệp có sản phẩm, dịch vụ phù hợp có thể
                    tích hợp với
                    Getfly.</p>
                <p>Trân trọng!</p>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Lợi ích khi trở thành <br>đối tác
            của
            Getfly</p>
        <div class="flex-loiich px-2">
            <div class="loiich">
                <img src="{{asset("site/images/win-win.png")}}" class="w-20">
                <p class="title-loiich">win-win</p>
                Quan hệ hợp tác dựa trên sự bình đẳng, đôi bên cùng có lợi. Trên cơ sở những nguồn tài nguyên có sẵn,
                Getfly cung cấp cho
                Quý đối tác công nghệ cùng một hệ sinh thái kết nối bền vững để nâng cao chất lượng sản phẩm/ dịch vụ,
                đem lại những giá
                trị thực cho doanh nghiệp và khách hàng
            </div>
            <div class="loiich">
                <img src="{{asset("site/images/support1.png")}}" class="w-20">
                <p class="title-loiich">đội ngũ support
                    chuyên nghiệp</p>
                Getfly coi "nỗi đau" của Quý đối tác là "nỗi đau" của chính mình. Chúng tôi sẵn sàng đồng hành, hỗ trợ
                nhiệt tình cho
                Quý đối tác trong suốt quá trình hợp tác khi có vấn đề hay nhu cầu phát sinh
            </div>
            <div class="loiich">
                <img src="{{asset("site/images/soical.png")}}" class="w-20">
                <p class="title-loiich">phối hợp marketing
                    cùng getfly</p>
                Được Getfly truyền thông về doanh nghiệp của Quý đối tác trên khắp các phương tiện thông tin đại chúng,
                trên website
                chính thức Getfly.vn và trên Cộng đồng Getfly CRM
            </div>
            <div class="loiich">
                <img src="{{asset("site/images/star.png")}}" class="w-20">
                <p class="title-loiich">cơ hội gia tăng
                    khách hàng mới</p>
                Cơ hội kết hợp cùng Getfly mở rộng tệp data, chia sẻ nhu cầu khách hàng, gia tăng cơ hội bán hàng mới
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Nhiệm vụ</p>
        <div class="pb-10 flex flex-wrap justify-center items-center">
            <img class="xl:w-50 rounded w-full px-5 " src="{{asset("site/images/thu-moi-hop-tac-1.webp")}}">
            <div class="xl:w-50 w-full px-2">
                <p class="font-bold text-primary-orange500 mb-2 text-base">Hình thức hợp tác của đối tác:</p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Chia sẻ cơ hội bán hàng cùng Getfly
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Trở thành cộng sự phân phối sản phẩm Getfly
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Cùng hợp tác hỗ trợ khách hàng để duy trì lượng
                    khách hàng trung thành
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Mở ra những cơ hội kinh doanh, truyền thông khác
                </p>
            </div>
        </div>
        <div class="pb-10 flex flex-wrap justify-center items-center">
            <div class="xl:w-50 w-full px-2 ">
                <p class="font-bold text-primary-orange500 mb-2 text-base">Nhiệm vụ của Getfly</p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Hỗ trợ truyền thống, quảng bá thương hiệu của Quý đối tác
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Hỗ trợ triển khai yêu cầu khách hàng của Quý đối tác
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Hỗ trợ chăm sóc khách hàng sau khi ký kết hợp đồng
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Chịu 100% trách nhiệm về hạ tầng sản phẩm, chất lượng và
                    các cập nhật công nghệ
                </p>
                <p class="flex gap-2 mb-2 items-start">
                    <img src="{{asset("site/images/checked2.png")}}">
                    Hỗ trợ khác
                </p>
            </div>
            <img class="xl:w-50 w-full rounded" src="{{asset("site/images/ket-noi.webp")}}">
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Các đối tác tiêu biểu</p>
        <div class="doitac-img">
            @foreach($partnerListViewModel->getPartners() as $partner)
                <a href="{{$partner->getLink()}}" class="text-center">
                    <img src="{{url($partner->getAvatar())}}" style="width: 160px" class="border rounded-xl shadow-lg mb-2">
                    <div class="text-blue-900 font-bold">{{$partner->getName()}}</div>
                </a>
                @if(($loop->index+1)%5==0)
        </div>
        <div class="doitac-img">
            @endif
            @endforeach
        </div>
    </div>
    <div class="doitacchinhthuc" id="doitacchinhthuc">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">đăng ký trở thành <br>
            đối tác chính thức của getfly</p>
        {!! embed_form("thu-moi-hop-tac.html") !!}
    </div>
@endsection