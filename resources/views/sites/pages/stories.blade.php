@php
    use App\ViewModels\Sites\Feedback\FeedbackListViewModel;
    /**
* @var FeedbackListViewModel $feedbackListViewModel
 */
@endphp
@extends("sites.shared.layout")
@section("title")
    Câu chuyện khách hàng
@endsection
@section("content")
    <div class="getfly-story">
        <p class="story-title">Câu chuyện khách hàng</p>
        <p class="story-des">Getfly có thể giúp anh chị gia tăng hiệu suất bán hàng nhờ quản lý
            tốt
            hoạt động
            của
            marketing - sales - chăm
            sóc khách
            hàng. Để có những nhận định khách quan nhất về tính hiệu quả trong ứng dụng, hãy nghe chia sẻ trực tiếp từ
            khách hàng
            của chúng tôi, những anh chị chủ doanh nghiệp, quản lý đã thành công khi ứng dụng công nghệ, công cụ quản lý
            và dịch vụ
            tư vấn chuyển đổi của Getfly</p>
    </div>
    <div class="2xl:px-[25rem] lg:px-[10rem] lg:px-[5rem] bg-gray-200 pt-10">
        <div class="storys-box px-2">
            @foreach($feedbackListViewModel->getFeedbacks() as $feedback)
                <div class="w-1/3 rounded-2xl mobile bg-white mb-5">
                    @if($feedback->getType()!=2)
                        <img src="{{url($feedback->getAvatar())}}" class="w-full rounded-t-lg"
                             style="aspect-ratio: 1033/581">
                    @endif
                    @if($feedback->getType()==2)
                        <iframe src="{{$feedback->getLink()}}" class="w-full rounded-t-lg"
                                style="aspect-ratio: 1033/581"></iframe>
                    @endif
                    <div class="p-4 flex gap-4 flex-col mb-3">
                        <p class="font-bold text-primary-orange500 text-base">{{$feedback->getTitle()}}
                        </p>
                        <p>{{$feedback->getDescription()}}
                        </p>
                        @if($feedback->getType()!=2)
                            <a href="{{route("sites.story",$feedback->getId())}}"
                               class="font-bold text-primary-orange500 text-right">Xem
                                thêm</a>
                        @endif
                        @if($feedback->getType()==2)
                            <a href="{{str_replace("embed","video",$feedback->getLink())}}"
                               class="font-bold text-primary-orange500 text-right">Xem
                                chi tiết</a>
                        @endif
                    </div>
                </div>
                @if( ($loop->index+1)%3==0 && !$loop->last)
        </div>
        <div class="storys-box px-2">
            @endif
            @endforeach
        </div>
    </div>
@endsection