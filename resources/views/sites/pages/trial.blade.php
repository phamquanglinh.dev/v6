@php
    use App\ViewModels\Sites\Pages\ContactViewModel;
    /**
     * @var ContactViewModel $contactViewModel
     */
    $disablePopup = true
@endphp
@extends("version2.layouts.app")
@section("title")
    Đăng ký dùng thử
@endsection
@section("content")
    <div class="box-register 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]" style="padding-bottom: 0!important;">
        <div class="md:w-1/2 mobile text-center">
            <p class="font-bold text-primary-blue500">Vui lòng chọn <span class="text-primary-orange500">ĐỊA ĐIỂM</span>
                phù
                hợp để được hỗ trợ nhanh nhất
            </p>
            <div class="flex justify-evenly my-5">
                <div>
                    <input checked id="default-radio-1" type="radio" value="0" name="default-radio"
                           class="choose_city w-6 h-6 text-primary-blue500 bg-gray-100 border-gray-300 focus:ring-blue-500">
                    <label for="default-radio-1" class="ml-3 font-bold text-primary-orange500">Hà
                        Nội</label>
                </div>
                <div>
                    <input id="default-radio-2" type="radio" value="1" name="default-radio"
                           class="choose_city w-6 h-6 text-primary-blue500 bg-gray-100 border-gray-300 focus:ring-blue-500">
                    <label for="default-radio-2" class="ml-3 font-bold text-primary-orange500">Hồ
                        Chí Minh</label>
                </div>
            </div>
            <div class="flex items-center justify-center">
                <div id="getfly_hn" style="width: 500px;">
                    {!! embed_form("dang_ky_hn") !!}
                </div>
                <div id="getfly_hcm" style="width: 500px;" class="hidden">
                    {!! embed_form("dang_ky_tphcm") !!}
                </div>
            </div>
        </div>
        <div class="md:w-1/2 mobile">
            <p class="font-bold text-primary-blue500 uppercase text-xl text-center xl:text-lg">Phần mềm
                quản lý và chăm
                sóc khách
                hàng <br>toàn diện
                cho SMES
                việt
            </p>
            <p class="text-center text-lg my-5">-----------------------</p>
            <div class="mt-10">
                <p class="mb-5">1. Trải nghiệm <span class="text-primary-orange500">30 ngày miễn phí</span> giải pháp
                    của
                    Getfly CRM</p>
                <p class="mb-5">2. Kinh nghiệm triển khai 4500+ doanh nghiệp với 200+ ngành nghề</p>
                <p class="mb-5">3. Hỗ trợ tư vấn theo từng đặc thù mô hình kinh doanh và quy mô của doanh nghiệp</p>
                <p class="mb-5">4. Cung cấp khóa đào tạo hàng tháng trong suốt quá trình sử dụng hệ thống</p>
                <p class="mb-5">5. Dịch vụ hỗ trợ sau bán đa kênh, đa ngôn ngữ</p>
                <p class="mb-5">6. Kho tàng kiến thức hướng dẫn chi tiết: qua kênh Youtube "<a
                            class="text-blue-600"
                            href="https://www.youtube.com/@hocviengetfly1873" target="_blank">Học viện Getfly</a>", khóa
                    học
                    trực
                    tuyến, Trang
                    tra cứu thông tin <a class="text-blue-600" href="https://helpdesk.getfly.vn/huong-dan-su-dung/" target="_blank"> Getfly helpdesk</a>.
                </p>
                <p class="mb-5">7. Không ngừng nâng cấp và phát triển hệ thống CRM</p>
                <p class="mb-5">8. Đừng ngại, hãy cho phép chúng tôi giúp đỡ bạn!</p>
            </div>
        </div>
    </div>
    @include("version2.layouts.inc.footer")
@endsection
@push("after_scripts")
    <script>
        $(document).ready(function () {
            $('.choose_city').change(function () {
                var id = $(this).val();
                if (parseInt(id) !== 1) {
                    $('#getfly_hn').removeClass('hidden');
                    $('#getfly_hcm').addClass('hidden');
                } else {
                    $('#getfly_hcm').removeClass('hidden');
                    $('#getfly_hn').addClass('hidden');
                }
            });
        });
    </script>
@endpush