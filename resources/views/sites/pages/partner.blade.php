@php
    use App\ViewModels\Sites\Partner\PartnerListViewModel;
    /**
* @var PartnerListViewModel $partnerListViewModel
 */

@endphp
@extends("sites.shared.layout")
@section("title")
    Đối tác
@endsection
@section("content")
    <div class="banner banner-hesinhthai">
        <p class="hesinhthai-des">CRM hub- hệ sinh thái tích
            hợp
            mở,<br>
            kết nối đa dịch vụ all in one</p>
        <p class="hesinhthai-des1">Getfly CRM Hub với API mở, sẵn sàng kết nối với mọi nền tảng, dịch
            vụ để
            chia sẻ, kết
            nối
            tập trung thông tin giúp tối
            ưu mọi quy trình quản lý và chăm sóc khách hàng</p>
    </div>
    <img src="{{asset("site/images/banner-he-sinh-thai-co-shopee.jpg")}}">
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Thông điệp Getfly</p>
        <div class="thongdiep-box">
            <img src="{{asset("site/images/thong-diep.png")}}">
            <div class="flex gap-4 flex-col px-2">
                <p>Getfly CRM Hub đẩy mạnh mở cổng kết nối đa dịch vụ, đa nền tảng, tạo ra một hệ thống quản lý tập
                    trung cho các doanh
                    nghiệp trong thời đại công nghệ 4.0:</p>
                <p class="flex gap-2 items-center">
                    <img src="{{asset("site/images/checklist.png")}}">
                    Chúng tôi cung cấp mã API mở cơ bản cho toàn bộ khách hàng của Getfly và nhữn đơn vị có nhu cầu tích
                    hợp dữ liệu.
                </p>
                <p class="flex gap-2 items-center">
                    <img src="{{asset("site/images/checklist.png")}}">Sẵn sàng tích hợp đa chiều theo yêu cầu của khách
                    hàng.
                </p>
                <p class="flex gap-2 items-center">
                    <img src="{{asset("site/images/checklist.png")}}">Dù doanh nghiệp của bạn có sử dụng bao nhiêu nền
                    tảng hay công cụ
                    phục vụ kinh doanh, với Getfly CRM
                    Hub mọi dữ liệu đều
                    dễ dàng tích hợp trên phần mềm Getfly.
                </p>
                <p class="flex gap-2 items-center">
                    <img src="{{asset("site/images/checklist.png")}}">Giảm thiểu thao tác, dễ dàng tổng hợp dữ liệu cho
                    chủ doanh nghiệp,
                    CEO và cấp quản lý.
                </p>
            </div>
        </div>
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">Đối tác của Getfly</p>
        <div class="">
            <p class="mb-2 font-bold border-b text-base w-60 text-primary-blue500 pb-4 border-primary-blue500">Phân loại
                nền tảng kết nối</p>
            <div class="doitac-box px-2">
                <ul class="flex list-none flex-col flex-wrap w-1/5 mobile" role="tablist" data-te-nav-ref>
                    <li>
                        <a href="#tabs-home03"
                           class="block py-3 text-primary-orange500 data-[te-nav-active]:border-primary data-[te-nav-active]:text-primary"
                           data-te-toggle="pill" data-te-target="#tabs-home03" data-te-nav-active role="tab"
                           aria-controls="tabs-home03" aria-selected="true">Tất cả đối tác</a>
                    </li>
                    @foreach($partnerListViewModel->getPartnerTypes() as $type)
                        <li>
                            <a href="#tabs-{{$type->getPartnerTabId()}}"
                               class="block py-3 text-primary-orange500 data-[te-nav-active]:border-primary data-[te-nav-active]:text-primary"
                               data-te-toggle="pill" data-te-target="#tabs-{{$type->getPartnerTabId()}}" role="tab"
                               aria-controls="tabs-profile03" aria-selected="false">{{$type->getPartnerTypeName()}}</a>
                        </li>
                    @endforeach

                </ul>
                <div class="w-4/5 mobile">
                    <div class="hidden opacity-100 transition-opacity duration-150 ease-linear data-[te-tab-active]:block"
                         id="tabs-home03" role="tabpanel" aria-labelledby="tabs-home-tab03" data-te-tab-active>
                        <div class="doitac-img">
                            @foreach($partnerListViewModel->getPartners() as $partner)
                                <a href="{{$partner->getLink()}}" class="text-center">
                                    <img src="{{$partner->getAvatar()}}" style="max-width: 160px;"
                                         class="border rounded-xl shadow-lg mb-2">
                                    <div class="text-blue-900 font-bold">{{$partner->getName()}}</div>
                                </a>
                                @if(($loop->index+1) %4==0)
                        </div>
                        <div class="doitac-img">
                            @endif
                            @endforeach
                        </div>
                    </div>
                    @foreach($partnerListViewModel->getPartnerTypes() as $type)
                        <div class="hidden opacity-0 transition-opacity duration-150 ease-linear data-[te-tab-active]:block"
                             id="tabs-{{$type->getPartnerTabId()}}" role="tabpanel"
                             aria-labelledby="tabs-profile-tab03">
                            <div class="doitac-img">
                                @foreach($type->getPartnersByType() as $partner)
                                    <a href="{{$partner->getLink()}}" class="text-center">
                                        <img src="{{$partner->getAvatar()}}" style="max-width: 160px;">
                                        <div class="text-blue-900 font-bold">{{$partner->getName()}}</div>
                                    </a>
                                    @if(($loop->index+1) %4==0)
                            </div>
                            <div class="doitac-img">
                                @endif
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="flex justify-center mb-10">
                {{--                <a href="#doitacchinhthuc"--}}
                {{--                   class="btn bg-btn-color flex w-80 rounded-[50px] text-white p-3 uppercase text-center font-bold"--}}
                {{--                   style="box-shadow: 2px 4px 5px #897676;">--}}
                {{--                    Đăng ký trở thành đối tác của getfly crm hub--}}
                {{--                </a>--}}
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]" id="doitacchinhthuc">
        <p class="font-bold text-primary-blue500 text-2xl text-center py-10 uppercase">đăng ký trở thành <br>
            đối tác chính thức của getfly</p>
        {!! embed_form('thu-moi-hop-tac.html') !!}
    </div>
@endsection
@push('after_scripts')
    <script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/tw-elements.umd.min.js"></script>
@endpush