@php
    use App\ViewModels\Sites\News\NewsSearchViewModel;

    /**
     * @var NewsSearchViewModel $newsSearchViewModel
     */

@endphp

@extends('sites.shared.layout')


@section('title')
    Tìm kiếm "{{$newsSearchViewModel->getParams()}}"
@endsection

@section('content')
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] box-customer-crm py-0 mb-5">
        <div class="title-tintuc lg:flex justify-between items-center w-full mb-2">
            <p class="font-bold text-base lg:text-left text-center md:mb-0 mb-4">Kết quả cho:
                "{{$newsSearchViewModel->getParams()}}"</p>
            <div class="search-form md:w-1/5  ml-2 mt-3 mb-3">
                <form action="{{route("sites.newspapers.search")}}">
                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none"
                                 stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                            </svg>
                        </div>
                        <input type="search" name="params" value="{{$newsSearchViewModel->getParams()}}"
                               id="default-search"

                               class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                               placeholder="Tìm kiếm tin tức" required>
                        <button type="submit"
                                class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            Tìm
                        </button>
                    </div>
                </form>
            </div>

        </div>
        <div class="customer-crm">
            @if($newsSearchViewModel->getNews()==[])
                <div class="w-3/4 mobile"></div>
            @else
                <div class="w-3/4 mobile">
                    @foreach($newsSearchViewModel->getNews() as $news)
                        @if($loop->first)
                            <div class="box-news border py-0 mb-5">
                                <img src="{{url($news->getImage())}}" class="w-full">
                                <div class="p-3">
                                    <div class="flex gap-1">
                                        <p class="text-neutral-600">{{$news->getCreatedAt()}}</p> - <a
                                                href="{{route("sites.newspapers.list",['id'=>$news->getCateId(),'slug'=>$news->getCateSlug()])}}"
                                                class="text-primary-blue500 font-semibold">
                                            {{$news->getCateTitle()}}</a>
                                    </div>
                                    <div class="py-2">
                                        <a href="{{route("sites.newspaper.detail",$news->getSlug())}}"
                                           class="font-bold text-primary-blue500 text-lg">
                                            {{$news->getTitle()}}
                                        </a>
                                    </div>
                                    <p>{{$news->getDescription()}}</p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="grid lg:grid-cols-2 gap-6 mb-5" id="newspapers">
                        @foreach($newsSearchViewModel->getNews() as $news)
                            @if(!$loop->first)
                                @include("sites.newspaper.newspaper_item",['news'=>$news])
                            @endif
                        @endforeach
                    </div>
                    <div class="w-full flex justify-center mb-5">
                        <button id="see-more"
                                class="btn bg-btn-color rounded-full shadow-lg text-white p-3 px-8 uppercase mt-3">
                            Xem thêm
                        </button>


                    </div>
                </div>
            @endif
            @include("sites.shared.right_sidebar")
        </div>
    </div>
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection
@push("after_scripts")
    <script>

        $(document).ready(function () {
            let page = 2;
            $("#see-more").click(function () {
                $.post("{{route("sites.newspapers.search-more")}}", {
                    _token: "{{csrf_token()}}",
                    params: "{{$newsSearchViewModel->getParams()}}",
                    page: page,
                }, function (data, status) {
                    console.log(data)
                    $("#newspapers").append(data)
                    page++
                })

            })
        })
    </script>
@endpush