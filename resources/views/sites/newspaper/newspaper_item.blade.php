@php
    use App\ViewModels\Sites\News\Object\NewsListObject;
    /**
* @var  NewsListObject $news
 */
@endphp
<div class="box-news border py-0">
    <img src="{{url($news->getImage())}}" class="w-full" style="aspect-ratio: 368/179">
    <div class="p-3">
        <div class="flex gap-1">
            <p class="text-neutral-600">{{$news->getCreatedAt()}}</p> - <a
                    href="{{route("sites.newspapers.list",["id"=>$news->getCateId(),"slug"=>$news->getCateId()])}}"
                    class="text-primary-blue500 font-semibold">
                {{$news->getCateTitle()}}</a>
        </div>
        <div class="py-2">
            <a href="{{route("sites.newspaper.detail",$news->getSlug())}}"
               class="font-bold text-primary-blue500 text-lg">
                {{$news->getTitle()}}
            </a>
        </div>
        <p>{{$news->getDescription()}}</p>
    </div>
</div>