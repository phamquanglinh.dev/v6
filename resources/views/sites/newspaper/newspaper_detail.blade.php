@php
    use App\ViewModels\Sites\News\NewsDetailViewModel;

    /**
     * @var NewsDetailViewModel $newspaperDetailViewModel
     */
    $news = $newspaperDetailViewModel->getNews();
$newspaper_seo=true;
@endphp

@extends('sites.shared.layout')
@section("newspaper_seo")
    <meta name="author" content="https://getfly.vn/">
    <meta name="description" content="{{$news->getDescription()}}">
    <meta name="keywords" content="{{$news->getKeywordSeo()}}">
    <!-- ogp -->
    <meta property="og:title" content="{{$news->getTitleSeo()??$news->getTitle()}}">
    <meta property="og:type" content="article">
    <meta property="og:image" content="{{url($news->getImage())}}">
    <meta property="og:description" content="{{$news->getDescription()}}">
    <meta property="fb:app_id" content="637630076348771"/>
    <meta property="fb:admins" content="vuhoangduy"/>
    <meta property="fb:pages" content="328467620572169"/>
    <meta name="google-site-verification" content="cjPAARainmNgQA2irOLVfhaOfVCZfCpGQ7EJQm9KKDU">
    <meta name="ahrefs-site-verification" content="11d0f7f6ad53e5445fecfe9b436c65193c5d1bd8c85ccdee11e16d9b73cc99e5">
@endsection
@section('title')
    {{$news->getTitle()}}
@endsection

@section('content')
    <style>
        h1 {
            font-size: 2rem !important;
            color: #dc6803 !important;
        }

        #newspaper_content ul {
            list-style: disc !important;
        }

        #newspaper_content ol {
            list-style: auto !important;
        }

        #newspaper_content h2 {
            font-size: 1.5rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #newspaper_content h3 {
            font-size: 1rem;
            font-weight: bold;
            margin-bottom: 1rem;
            margin-top: 1.5rem;
        }

        #newspaper_content a {
            color: #0b4d75;
        }

        #newspaper_content img {
            padding: 2rem;
        }

        #newspaper_content p {
            margin-bottom: 0.5rem;
        }

        #newspaper_content * {
            font-family: Montserrat, sans-serif !important;
        }
    </style>
    <div class="box-customer-crm 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] py-10">
        <div class="customer-crm">
            <div class="w-3/4 mobile" id="newspaper_content">
                <div class="py-2">
                    <h1 style="line-height: inherit"
                        class="font-bold text-primary-blue500 text-lg">{{$news->getTitle()}}
                    </h1>
                </div>
                <div class="flex gap-1">
                    <p class="text-neutral-600">{{$news->getUpdatedAt()}}</p> - <a
                            href="{{route("sites.newspapers.list",["id"=>$news->getCategoryId(),"slug"=>$news->getCategorySlug()])}}"
                            class="text-primary-blue500 font-semibold">
                        {{$news->getCategoryName()}}</a>
                    <div class="ml-2 italic">
                        <span>{{number_format($news->getView())}} lượt xem</span>
                    </div>
                </div>
                <p class="mt-5">{{$news->getDescription()}}</p>
                <img src="{{url($news->getImage())}}" class="w-full py-4">
                <div class="my-2 pb-5 ">
                    {!! $news->getContent() !!}
                </div>
            </div>
            @include("sites.shared.right_sidebar")
        </div>
    </div>
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection