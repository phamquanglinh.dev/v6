@php
    use App\ViewModels\Sites\News\NewsListViewModel;

    /**
     * @var NewsListViewModel $blogListViewModel
     */
@endphp

@extends('sites.shared.header')

@include('sites.shared.navbar')

@section('title')
    Blogs
@endsection

@section('content')
    BLOGS LIST
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection