@php
    use App\ViewModels\Sites\News\NewsDetailViewModel;

    /**
     * @var NewsDetailViewModel $blogDetailViewModel
     */
@endphp

@extends('sites.shared.header')

@include('sites.shared.navbar')

@section('title')
    Blogs Detail
@endsection

@section('content')
    BLOG DETAIL
@endsection

@section('footer')
    @include('sites.shared.footer')
@endsection