@php
    use App\ViewModels\Sites\Jobs\ListJobsViewModel;
    /**
* @var ListJobsViewModel $listJobsViewModel
 */
    $count = count($listJobsViewModel->getJobs())
@endphp

<div class="duration-700 absolute inset-0 transition-transform transform z-20 translate-x-0"
     @if($count>5)data-carousel-item @endif >
    @foreach($listJobsViewModel->getJobs() as $key => $job)
        <div class="border shadow-sm rounded-lg md:flex block justify-between mb-6">
            <div class="p-3">
                <a href="{{route("sites.jobs.detail",$job->getId())}}">
                    <p class="font-bold text-primary-blue500 text-xl mb-3">{{$job->getJobName()}}</p>
                    <p>Số lượng: {{$job->getQuantity()}}</p>
                </a>
            </div>
            <div class="p-3">
                <p class="flex gap-2 mb-3">
                    <img src="{{asset("site/images/money.png")}}">
                    {{$job->getSalary()}}
                </p>
                <p class="flex gap-2">
                    <img src="{{asset("site/images/time.png")}}">
                    {{$job->getTime()}}
                </p>
            </div>
        </div>
        @if(($key+1)%5==0 && !$loop->last)
</div>
<div class="duration-700 absolute inset-0 transition-transform transform z-10 translate-x-full" data-carousel-item>
    @endif
    @endforeach
</div>

