@php
    use App\ViewModels\Sites\Jobs\ListJobsViewModel;
    /**
    * @var ListJobsViewModel $listJobsViewModel
    */
@endphp
@extends("sites.shared.layout")
@section("title")
    Tuyển dụng
@endsection
@section("content")
    <div class="banner relative">
        <img src="{{asset("site/images/banner-tuyendung.png")}}">
    </div>
    <div class="getfly-tuyendung 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] my-10">
        <div class="w-1/4 mobile">
            <div class="flex items-center mb-5">
                <input checked id="room-all" type="checkbox" value="" name="room"
                       class="select-room w-6 h-6 text-blue-600 bg-white border-gray-300 rounded-sm focus:!outline-0">
                <label for="checked-checkbox" class="ml-2 text-base text-gray-900 dark:text-gray-300">Tất
                    cả</label>
            </div>
            @foreach($listJobsViewModel->getRooms() as $room)
                <div class="flex items-center mb-5">
                    <input id="room-{{$room->getId()}}" type="checkbox" value="" name="room"
                           class="select-room w-6 h-6 text-blue-600 bg-white border-gray-300 rounded-sm focus:!outline-0">
                    <label for="default-checkbox"
                           class="ml-2 text-base text-gray-900 dark:text-gray-300">{{$room->getName()}}</label>
                </div>
            @endforeach
            <div class="flex items-center mb-5">
                <input id="room-other" type="checkbox" value="" name="room"
                       class="select-room w-6 h-6 text-blue-600 bg-white border-gray-300 rounded-sm focus:!outline-0">
                <label for="default-checkbox" class="ml-2 text-base text-gray-900 dark:text-gray-300">Khác</label>
            </div>
        </div>
        <div class="w-3/4 mobile">
            <div id="controls-carousel" class="relative overflow-hidden" data-carousel="slide">
                <div class="banner-box" id="job-items" style="height: 100vh;">
                    @include("sites.jobs.job_items",["listJobsViewModel"=>$listJobsViewModel])
                </div>
                <div class="flex justify-center">
                    @if(count($listJobsViewModel->getJobs())>5)
                        <div class="flex gap-8 justify-center items-center relative w-96">
                            <button type="button" class="btn-next-banner left-10" data-carousel-prev id="previous">
                                <img src="{{asset("site/images/back.png")}}" class="h-5">
                            </button>
                            <div class="px-10 flex gap-8">
                                <div class="p-2 bg-primary-orange500 rounded-full w-10 h-10 text-center">
                                    <span class="text-white font-bold text-base" id="current-page">1</span>
                                </div>
                            </div>
                            <button type="button" class="btn-next-banner right-10" data-carousel-next id="next">
                                <img src="{{asset("site/images/next.png")}}" class="h-5">
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="form flex flex-col justify-center p-20">
        <p class="text-center font-bold text-3xl mb-5 text-white">Đăng ký tuyển dụng</p>
        {!! embed_form('tuyen-dung.html') !!}
    </div>
@endsection
@push("after_scripts")
    <script>
        let page = 1
        let next = 0
        let previous = 0
        const swapSlide = (current, target) => {
            console.log("current:" + current)
            console.log("target:" + target)
            let currentNode = $("[data-carousel-item]:nth-child(" + current + ")")
            let targetNode = $("[data-carousel-item]:nth-child(" + target + ")")
            currentNode.removeClass("z-20")
            currentNode.removeClass("translate-x-0")
            currentNode.addClass("z-10")
            currentNode.addClass("translate-x-full")
            targetNode.removeClass("z-10")
            targetNode.removeClass("translate-x-full")
            targetNode.addClass("z-20")
            targetNode.addClass("translate-x-0")
            $("#current-page").text(target)
        }
        $("#next").click(function () {
            const total = $("[data-carousel-item]").length
            if (page < total) {
                next = page + 1
                swapSlide(page, next)
                page += 1
            } else {
                next = 1
                swapSlide(page, next)
                page = 1
            }
        })
        $("#previous").click(function () {
            const total = $("[data-carousel-item]").length
            if (page > 1) {
                previous = page - 1
                swapSlide(page, previous)
                page -= 1
            } else {
                next = total
                swapSlide(page, next)
                page = total
            }
        })
        $(".select-room").change(function (e) {
            $(".select-room").each(function () {
                $(this).prop("checked", false)
            })
            $(e.currentTarget).prop("checked", true)

            // $("#room-all").prop("checked", false)
            // if ($(this).attr("id") !== "room-all") {
            //     $("#room-all").prop("checked", false)
            // } else {
            //     $(".select-room").each(function (e) {
            //         $(this).prop("checked", false)
            //     })
            //     $("#room-all").prop("checked", true)
            // }
            const selected = []
            $(".select-room:checked").each(function () {
                selected.push($(this).attr("id"))
            })
            $.post("{{route("sites.jobs.ajax")}}", {
                _token: "{{csrf_token()}}",
                rooms: selected
            }, function (data, status) {
                $("#job-items").html(data.data)
                page = 1
                $("#current-page").text(1)
            })
        })
    </script>
@endpush