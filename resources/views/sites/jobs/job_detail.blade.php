@php
    use App\ViewModels\Sites\Jobs\JobDetailViewModel;
    /**
* @var  JobDetailViewModel $jobDetailViewModel
 */
    $recruitment = $jobDetailViewModel->getRecruitment();
    $relate_recruitments = $jobDetailViewModel->getRecruitments();
@endphp
@extends("sites.shared.layout")
@section("title")
    Chi tiết tuyển dụng
@endsection
@section("content")
    <style>
        p ,span{
            font-family: "Montserrat", sans-serif !important;
        }
    </style>
    <div class="banner relative flex justify-center flex-col items-center bg-[#F3F3F3]">
        <p class="tuyendung-title text-center">{{$recruitment->getJobName()}}</p>
        <img src="{{asset("site/images/tuyendung.png")}}">
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] getfly-tuyendung flex-col bg-[#F3F3F3]">
        <div class="bg-white">
            <div class="tuyendung">
                <div class="box-td mobile">
                    <img src="{{asset("site/images/muc-luong.png")}}" class="flex justify-center items-center">
                    <p class="text-white font-light">Mức lương</p>
                    <p class="font-bold text-white">{{$recruitment->getSalary()}}</p>
                </div>
                <div class="box-td mobile">
                    <img src="{{asset("site/images/user.png")}}">
                    <p class="text-white font-light">Số lượng</p>
                    <p class="font-bold text-white">{{$recruitment->getQuantity()}}</p>
                </div>
                <div class="box-td mobile">
                    <img src="{{asset("site/images/thoi-gian.png")}}">
                    <p class="text-white font-light">Hạn ứng tuyển</p>
                    <p class="font-bold text-white">{{$recruitment->getTime()}}</p>
                </div>
                <div class="box-td mobile">
                    <img src="{{asset("site/images/location.png")}}">
                    <p class="text-white font-light">Địa điểm</p>
                    <p class="font-bold text-white">{{$recruitment->getAddress()}}</p>
                </div>
            </div>
            <div class="p-5 px-10">
                {!! $recruitment->getBody() !!}
                <div class="flex">
                    <a href="#register"
                       class="btn bg-btn-color rounded-lg shadow-sm text-white p-3 px-6 lg:px-3 text-sm lg:text-xs mt-5">
                        Ứng tuyển
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] flex flex-col items-center bg-[#DDDEE0]">
        <p class="text-center font-bold text-lg py-5">Các vị trí khác</p>
        <div class="vitri px-2">
            @foreach($relate_recruitments as $recruitment)
                <div class="border p-3 w-1/3 bg-white mobile">
                    <a href="{{route("sites.jobs.detail",$recruitment->getId())}}">
                        <p class="text-primary-blue500 font-bold">{{$recruitment->getJobName()}}</p>
                        <p>Số lượng: {{$recruitment->getQuantity()}}</p>
                        <div class="py-3">
                            <p class="flex gap-2 mb-3 items-center">
                                <img style="height: 100%" src="{{asset("site/images/money.png")}}">
                                {{$recruitment->getSalary()}}
                            </p>
                            <p class="flex gap-2">
                                <img src="{{asset("site/images/time.png")}}">
                                {{$recruitment->getTime()}}
                            </p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <a href="{{route("sites.jobs.list")}}"
           class="text-primary-orange500 font-bold border-b border-primary-orange500 cursor-pointer pt-10 mb-10">
            Xem tất cả
        </a>
    </div>
    <div class="register-form form" id="register">
        <p class="text-center font-bold text-3xl mb-5 text-white">Đăng ký tuyển dụng</p>
        {!! embed_form('tuyen-dung.html') !!}
    </div>
@endsection