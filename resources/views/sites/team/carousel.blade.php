@php
    use App\ViewModels\Sites\TeamInfo\Object\PhotoListViewModel;
    /**
    * @var PhotoListViewModel $photoListViewModel
     */
@endphp
<div class="relative w-full">
    <!-- Carousel wrapper -->
    <div class="h-full w-full">
        @foreach($photoListViewModel->getPhotos() as $key => $photo)
            <img id="photo_{{$key}}" src="{{$photo->getImage()}}"
                 class="{{!$loop->first?"w-0":""}} w-full photo">
        @endforeach
    </div>
    <!-- Slider controls -->
    <button type="button"
            id="prePhotoBtn"
            class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
    >
        <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none"
                 stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M15 19l-7-7 7-7"></path></svg>
            <span class="sr-only">Previous</span>
        </span>
    </button>
    <button type="button"
            id="nextPhotoBtn"
            class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
    >
        <span class="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg aria-hidden="true" class="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800" fill="none"
                 stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
            <span class="sr-only">Next</span>
        </span>
    </button>
</div>
@push("after_scripts")
    <style>
        .w-0 {
            opacity: 0;
            width: 0 !important;
        }

        .photo {
            transition-duration: 500ms;
        }
    </style>
    <script>
        let currentPhoto = 0
        const maxPhotos = {{count($photoListViewModel->getPhotos())-1}};
        const showPhoto = () => {

            $(".photo").addClass("w-0")
            const image = $("#photo_" + currentPhoto)
            image.removeClass("w-0")
        }
        const nextPhoto = () => {
            currentPhoto++
            if (currentPhoto > maxPhotos) {
                currentPhoto = 0
            }
            showPhoto()
        }
        const prePhoto = () => {
            currentPhoto--
            if (currentPhoto < 0) {
                currentPhoto = maxPhotos
            }
            showPhoto()
        }
        const showPhotoWithId = (id) => {
            currentPhoto = id
            showPhoto()
            $("#openModal").click()
        }
        $(document).ready(() => {
            $("#prePhotoBtn").click(() => {
                prePhoto()
            })
            $("#nextPhotoBtn").click(() => {
                nextPhoto()
            })
        })
    </script>
@endpush