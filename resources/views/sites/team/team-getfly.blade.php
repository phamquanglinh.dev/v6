@php
    use App\ViewModels\Sites\TeamInfo\TeamViewModel;
    /**
    * @var TeamViewModel $teamViewModel
     */
@endphp

@extends("sites.shared.layout")
@section("title")
    Cuộc sống GetFly
@endsection
@section("content")
    <div class="2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] getfly-team">
        <p class="getfly-team-title">Hoạt động tại Getfly</p>
        <div class="flex flex-col items-center">
            <img src="{{url($teamViewModel->getTeamInfo()->getThumbnail())}}" class="getfly-team-img mobile">
            <p class="font-bold text-primary-blue500 text-base mt-10 text-center w-1000 mobile">
                {{$teamViewModel->getTeamInfo()->getTitle()}}
            </p>
        </div>
    </div>
    <div class="getfly-team-post py-[5rem] 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem]">
        <div id="controls-carousel" class="relative overflow-hidden px-2" data-carousel="static">
            <div class="banner-box-slide">
                <div class="hidden duration-700" data-carousel-item="active">
                    <div class="post-slide px-2">
                        @foreach($teamViewModel->getNewspaper() as $key => $newspaper)
                            <div class="md:w-1/2 w-full">
                                <a href="{{$newspaper->getUrl()}}">
                                    <div class="post-detail px-2">
                                        <img src="{{$newspaper->getThumbnail()}}" class="mobile post-img">
                                        <div class="flex flex-col">
                                            <p class="font-bold text-base">{{$newspaper->getTitle()}}</p>
                                            <p class="text-neutral-400">{{$newspaper->getDescription()}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if(($key%2==1))
                    </div>
                    @endif
                    @if(($key+1)%6==0)
                </div>
                <div class="hidden duration-700" data-carousel-item>
                    @endif
                    @if($key%2==1)
                        <div class="post-slide px-2">
                            @endif
                            @endforeach
                        </div>
                </div>
            </div>
            <div class="flex justify-center p-3 pb-10">
                <div class="flex gap-8 justify-center items-center relative w-96">
                    <button type="button" class="btn-next-banner left-10 button-h" data-carousel-prev="">
                        <img src="{{url("site/images/back.png")}}" class="h-5">
                    </button>
                    <button type="button" class="btn-next-banner right-10 button-h" data-carousel-next="">
                        <img src="{{asset("site/images/next.png")}}" class="h-5">
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="getfly-team-album py-20 2xl:px-[20rem] lg:px-[10rem] lg:px-[5rem] !pt-0" >
        <div class="slide-col px-2" style="padding-top: 5rem">
            <div class="mb-5 md:w-1/3 w-full py-10 bg-secondary-neutral300 flex justify-center items-center flex-col text-3xl text-center">
                <div>
                    <div class="mb-3 text-primary-orange500 font-bold">
                        Getfly
                    </div>
                    <div style="color: #213f80" class="font-bold">Qua những bức ảnh</div>
                </div>
            </div>
            @foreach($teamViewModel->getPhotos() as $key => $photo)
                @if($loop->last)
                    <div class="mb-5 md:w-1/3 w-full  h-mobile-400">
                        <div class="relative flex justify-center items-center more-photo"
                             style="
                             background: linear-gradient(rgba(14,26,194,0.5),rgba(13,25,190,0.46)),url('{{url($photo->getImage())}}');
                             "
                        >
                            <button id="openModal" class="hidden">

                            </button>
                            <button
                                    onclick="showPhotoWithId({{$key}})"
                                    data-modal-target="photo-modal" data-modal-toggle="photo-modal"
                                    class="text-2xl text-white font-bold flex gap-4 absolute">
                                Xem tiếp
                                <img src="{{asset("site/images/next-white.png")}}" style="width: 16px;"
                                     alt="next-white">
                            </button>
                        </div>
                    </div>
                @else
                    <div class="mb-5 md:w-1/3 w-full cursor-pointer" onclick="showPhotoWithId({{$key}})">
                        <img src="{{url($photo->getImage())}}" class="w-full">
                    </div>
                @endif
                @if($loop->index==1)
        </div>
        <div class="slide-col px-2">
            @endif
            @endforeach
        </div>
    </div>

    <div id="photo-modal" tabindex="-1"
         class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full">
        <div class="relative w-full max-w-7xl max-h-full">
            <!-- Modal content -->
            <div class="relative rounded-lg shadow dark:bg-gray-700">
                @include("sites.team.carousel")
            </div>
        </div>
    </div>

@endsection