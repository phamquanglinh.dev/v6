@php use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
     * @var V2PricingClientViewModel $v2PricingClientViewModel
     */

    $featureTab_1 = $v2PricingClientViewModel->getFeatureCollection(1);

    $featureTab_2 = $v2PricingClientViewModel->getFeatureCollection(2);
@endphp
@extends('version2.layouts.app')
@section('content')
    <div class="w-full bg-price flex justify-center items-center flex-col mt-[84px]">
        <div class="xl:w-1/2 md:w-3/5 w-3/5">
            <p class="xl:text-5xl md:text-5xl text-[30px] font-bold text-white text-center !leading-[60px] mac:text-[44px]"
               data-aos="fade-down" data-aos-duration="1000">
                Lựa chọn giải pháp hoàn hảo ngay cho doanh nghiệp của bạn
            </p>
        </div>
    </div>
    <div id="default-tab-content">
        <div class="rounded-lg pricing-tab" id="pricing1">
            <div class="py-10 text-center">
                <p class="text-xl text-[#4D4D4E] pb-1 font-medium">
                    Tiết kiệm chi phí phần cứng | Truy cập từ bất kì đâu | Bảo mật dữ liệu | Tự động cập nhật miễn phí
                </p>
                <p class="text-xl text-[#4D4D4E] pt-1 font-medium">
                    Khởi động từ 51.000 VNĐ - bạn sẽ được nhiều hơn thế!
                </p>
            </div>
            <div class="flex justify-center" data-aos="fade-down" data-aos-duration="1000">
                <div class="xl:w-3/5 w-4/5 bg-[#FFFCF9] border border-[#ECBC74] rounded-[20px] xl:p-9 md:p-9 p-6 flex gap-8 flex-col">
                    <div class="">
                        <p class="text-[26px] font-semibold flex items-center gap-2 text-[#113DD8]"><img
                                    src="{{v2_url('images/icon-rocket.png')}}"> Mobile App</p>
                        <p class="font-semibold text-base text-[#1C60FA]">Siêu ứng dụng CRM đầu tiên trên thế giới</p>
                    </div>
                    <div class="flex gap-12 xl:flex-row md:flex-row flex-col">
                        <div class="flex gap-5 flex-col">
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon1.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    Phút - Khởi tạo dữ liệu nhanh gọn
                                </span>
                            </p>
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon2.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        0
                                    </span>
                                    Giới hạn tính năng mạng xã hội nội bộ: Bảng tin - Chat - Call-in-App
                                </span>
                            </p>
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon3.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        360º
                                    </span>
                                    - Quản lý toàn bộ thông tin khách hàng
                                </span>
                            </p>
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon4.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    Quy trình bán hàng đồng bộ: Sản phẩm - Kho - Báo giá - Hợp đồng - Đơn hàng - Phiếu xuất kho - Phiếu thu
                                </span>
                            </p>
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon5.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        100%
                                    </span>
                                    Hiệu suất làm việc (KPI) được đo lường chính xác
                                </span>
                            </p>
                            <p class="text-base flex gap-[10px]">
                                <img src="{{v2_url('images/mobile-icon6.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    Hệ thống Mobile ERP
                                </span>
                            </p>
                        </div>
                        <div class="flex flex-col gap-1 xl:w-1/2 md:w-1/2 w-full">
                            <p class="text-[26px] font-bold text-[#1F2A37]">Trải nghiệm ngay</p>
                            <p class="font-medium text-[#064FF4]">Không khả dụng ở trên các thiết bị PC</p>
                            <div class="flex gap-6 pt-5">
                                <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                    <img src="{{v2_url('images/Appstore1.png')}}">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                    <img src="{{v2_url('images/Googleplay1.png')}}">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-center pt-8">
                <div class="xl:w-3/5 w-4/5">
                    <div class="flex xl:gap-8 md:gap-8 gap-8 xl:flex-row md:flex-row flex-col justify-between">
                        <div class="box-pri p-6 flex gap-6 flex-col border border-[#ECBC74]">
                            <div class="flex gap-1 flex-col">
                                <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                                    <img src="{{v2_url('images/icon-rocket.png')}}"> Startup
                                </p>
                                <p class="text-[15px] text-[#1C60FA] font-semibold ">Tất cả trong 1</p>
                            </div>
                            <div class="flex gap-4 flex-col pack-desc">
                                <div class="flex flex-col gap-1">
                                    <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                                    <p class="text-xl text-[#1F2A37] font-semibold">480.000đ <span class="text-base">/ Tháng</span>
                                    </p>
                                </div>
                                <div class="flex gap-3 flex-col ">
                                    <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/users.png')}}"> 03 người dùng</p>
                                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/data.png')}}"> 5,000 data</p>
                                </div>
                                <p class="text-[#3C4048] text-sm">Giải pháp tích hợp các <b>tính năng CRM cho mọi
                                        hoạt động</b> của doanh nghiệp</p>
                            </div>
                            <div>
                                <div class="text-center py-[10px]">
                                    <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank"
                                       class="text-[#FA901B] text-sm font-semibold">Dùng thử miễn phí</a>
                                </div>
                                <a class="button-pri inline-block text-center pt-5"
                                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                    vấn</a>
                            </div>
                        </div>
                        <div class="box-pri-special p-6 flex flex-col border relative">
                            <div class="flex justify-between text-center">
                                <div class="button-special">
                                    <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
                                </div>
                            </div>
                            <div class="flex gap-1 flex-col">
                                <p class="text-[22px] text-white font-semibold flex gap-2 items-center">
                                    <img src="{{v2_url('images/icon-rocket.png')}}"> Professional
                                </p>
                                <p class="text-[15px] text-[#FFA41D] font-semibold">Tiết kiệm chi phí</p>
                            </div>
                            <div class="flex gap-4 flex-col pack-desc pt-6">
                                <div class="flex flex-col gap-1">
                                    <p class="text-sm text-[#EDEFF2] font-semibold">Chỉ từ</p>
                                    <p class="text-xl text-[#F4F4F6] font-semibold">600.000đ <span class="text-base">/ Tháng</span>
                                    </p>
                                </div>
                                <div class="flex gap-3 flex-col ">
                                    <p class="text-sm text-[#F4F4F6] font-semibold">Khởi điểm từ:</p>
                                    <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/users.png')}}"> 10 người dùng</p>
                                    <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/data.png')}}"> 10,000 data</p>
                                </div>
                                <p class="text-[#F0F2F4] text-sm">Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu
                                    với chi phí tối ưu</p>
                            </div>
                            <div class="pt-16">
                                <a class="button-pri inline-block text-center"
                                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                    vấn</a>
                            </div>
                        </div>
                        <div class="box-pri p-6 flex gap-6 flex-col border border-[#ECBC74]">
                            <div class="flex gap-1 flex-col">
                                <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                                    <img src="{{v2_url('images/icon-rocket.png')}}"> Enterprise
                                </p>
                                <p class="text-[15px] text-[#1C60FA] font-semibold">Mạnh mẽ - Linh hoạt</p>
                            </div>
                            <div class="flex gap-4 flex-col pack-desc">
                                <div class="flex flex-col gap-1">
                                    <p class="text-sm text-[#9DA4AE] font-semibold">Đừng ngại ngần</p>
                                    <p class="text-xl text-[#1F2A37] font-semibold ">Liên hệ ngay</p>
                                </div>
                                <div class="flex gap-3 flex-col ">
                                    <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/users.png')}}"> Liên hệ ngay</p>
                                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                src="{{v2_url('images/data.png')}}"> Liên hệ ngay</p>
                                </div>
                                <p class="text-[#3C4048] text-sm">Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu
                                    cầu sử dụng</p>
                            </div>
                            <div class="pt-10">
                                <a class="button-pri inline-block text-center"
                                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                    vấn</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-price1 w-full flex items-center flex-col xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat xl:h-[1260] md:h-[1320px] h-[1200px]">
                <div class="xl:w-3/5 md:w-4/5 w-full pt-28" data-aos="fade-down">
                    <p class="xl:text-[42px] text-[30px] font-bold text-white text-center xl:px-10 md:px-6 px-10 leading-[60px] mac:text-[40px]">
                        Giải pháp CRM toàn diện được 5000++ <br>doanh nghiệp Việt Nam tin dùng</p>
                    <p class="text-white text-xl text-center pt-6 font-normal leading-8">Trải nghiệm quản lý chăm sóc
                        khách hàng đồng bộ & hiệu quả. </br> Xây dựng quy trình chuyên nghiệp & gia tăng doanh số cùng
                        chuyên gia Getfly</p>
                </div>
                <div id="default-carousel" class="relative w-full xl:block md:block hidden" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative h-[790px] overflow-hidden rounded-lg">
                        <!-- Item 1 -->
                        <div class="flex justify-center flex-col items-center h-[700px]" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-5">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm1.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không rào cản chuyển đổi dữ
                                            liệu</p>
                                        <p class="font-normal text-base pt-3">Khởi tạo & chuyển đổi dữ liệu có sẵn chỉ
                                            bằng 01 chiếc điện thoại di động chưa tới 01 phút</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video1.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm2.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Bất kỳ ai cũng có thể sử dụng dễ
                                            dàng</p>
                                        <p class="font-normal text-base pt-3">Giao diện đơn giản - thao tác dễ dàng
                                            trên Mobile app của Getfly</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video2.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm3.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không mất bất kỳ chi phí nào</p>
                                        <p class="font-normal text-base pt-3">Khai thác tối đa mọi tính năng CRM
                                            hoàn toàn miễn phí</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video3.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="flex gap-[30px] pt-[45px]">
                                <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                    <img src="{{v2_url('images/Appstore1.png')}}">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                    <img src="{{v2_url('images/Googleplay1.png')}}">
                                </a>
                            </div>
                        </div>
                        <!-- Item 2 -->
                        <div class="flex justify-center flex-col items-center h-[700px]" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-14 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-5">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm4.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Quản lý khách hàng trên cùng một
                                            giao diện</p>
                                        <p class="font-normal text-base pt-3">Thấu hiểu chân dung từng khách hàng -
                                            Lưu trữ không giới hạn dữ liệu - Xây dựng tệp khách hàng trung
                                            thành</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video4.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm5.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Marketing Automation nuôi dưỡng
                                            khách hàng tiềm năng</p>
                                        <p class="font-normal text-base pt-3">Giải phóng sức lao động với không
                                            giới hạn kịch bản tự động hóa quy trình kết nối khách hàng</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video5.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm6.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Xây dựng hệ thống KPI thông minh -
                                            chính xác</p>
                                        <p class="font-normal text-base pt-3">Đo lường “sức khoẻ doanh nghiệp" thông
                                            qua bộ chỉ số KPI tiêu chuẩn cho mọi phòng ban.</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video6.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="flex gap-[30px] pt-[45px]">
                                <a class="button-pri  inline-block text-center !px-14"
                                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                    vấn</a>
                            </div>
                        </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                                data-carousel-slide-to="0"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                                data-carousel-slide-to="1"></button>
                    </div>
                    <button type="button"
                            class="absolute -top-[40px] start-56 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{v2_url('images/left-icon.png')}}">
                    </button>
                    <button type="button"
                            class="absolute -top-[40px] end-56 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{v2_url('images/right-icon.png')}}">
                    </button>
                </div>
                <!--Mobile-->
                <div id="default-carousel" class="relative w-full xl:hidden md:hidden" data-carousel="static">
                    <div class="relative h-[530px] overflow-hidden rounded-lg">
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm1.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không rào cản chuyển đổi dữ
                                            liệu</p>
                                        <p class="font-normal text-base pt-3">Khởi tạo & chuyển đổi dữ liệu có sẵn chỉ
                                            bằng 01 chiếc điện thoại di động chưa tới 01 phút</p>
                                    </div>
                                    <video autoplay loop muted class="lazy w-[100%] rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video1.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm2.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Bất kỳ ai cũng có thể sử dụng dễ
                                            dàng</p>
                                        <p class="font-medium text-base pt-3">Giao diện đơn giản - thao tác dễ dàng
                                            trên Mobile app của Getfly</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video2.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm3.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không mất bất kỳ chi phí nào</p>
                                        <p class="font-medium text-base pt-3">Khai thác tối đa mọi tính năng CRM
                                            hoàn toàn miễn phí</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video3.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm4.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Quản lý khách hàng trên cùng một
                                            giao diện</p>
                                        <p class="font-medium text-base pt-3">Thấu hiểu chân dung từng khách hàng -
                                            Lưu trữ không giới hạn dữ liệu - Xây dựng tệp khách hàng trung
                                            thành</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video4.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm5.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Marketing Automation nuôi dưỡng
                                            khách hàng tiềm năng</p>
                                        <p class="font-medium text-base pt-3">Giải phóng sức lao động với không
                                            giới hạn kịch bản tự động hóa quy trình kết nối khách hàng</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video5.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{v2_url('images/crm6.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Xây dựng hệ thống KPI thông minh -
                                            chính xác</p>
                                        <p class="font-medium text-base pt-3">Đo lường “sức khoẻ doanh nghiệp" thông
                                            qua bộ chỉ số KPI tiêu chuẩn cho mọi phòng ban</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="lazy w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source data-src="{{v2_url('images/video6.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-full pb-12">
                        <div class="flex justify-center items-center gap-6">
                            <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                <img src="{{v2_url('images/Appstore1.png')}}">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                <img src="{{v2_url('images/Googleplay1.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                                data-carousel-slide-to="0"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                                data-carousel-slide-to="1"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3"
                                data-carousel-slide-to="2"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 4"
                                data-carousel-slide-to="3"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 5"
                                data-carousel-slide-to="4"></button>
                    </div>
                    <button type="button"
                            class="absolute -top-[40px] -start-2 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{v2_url('images/left-icon.png')}}">
                    </button>
                    <button type="button"
                            class="absolute -top-[40px] -end-2 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{v2_url('images/right-icon.png')}}">
                    </button>
                </div>
            </div>
            <div class="w-full flex items-center flex-col bg-[#FAF3F1] pb-10">
                <div class="xl:w-1/2 md:w-4/5 w-5/6 xl:pt-0 md:pt-0 pt-5" data-aos="fade-down">
                    <p class="xl:text-4xl md:text-4xl text-[30px] font-bold text-center text-[#113DD8]">Tối đa tính năng
                        - Tối thiểu chi phí</p>
                    <p class="text-[#37393E] text-xl text-center pt-6 leading-8">Đa dạng gói giải pháp cho nhu cầu & chi
                        phí của bạn </br>Chuyển gói đơn giản - không mất phí</p>
                </div>
                <div class="2xl:w-[64%] 2xl:px-10 xl:w-4/5 md:w-4/5 w-4/5 mac:w-4/5 bg-white rounded-[36px] mt-[45px] pt-10 pb-12 flex gap-9 flex-col xl:px-[50px] md:px-[50px] px-[10px] relative"
                     data-aos="fade-down">
                    <div class="flex xl:flex-row md:flex-row flex-col gap-4 justify-between items-center border-b border-[#EBEBF5] pb-5 xl:sticky md:sticky top-[80px] bg-white py-4">
                        <p class="font-bold xl:text-xl text-xl text-[#313235]">So sánh giữa các loại gói</p>
                        <div class=" xl:block md:block hidden">
                            <div class="flex xl:gap-6 md:gap-2 xl:flex-row md:flex-row flex-col">
                                <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
                                    <div class="flex gap-1 flex-col">
                                        <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
                                        <p class="text-xl text-[#1F2A37] font-semibold">480.000đ <span
                                                    class="text-base">/ Tháng</span></p>
                                    </div>
                                    <div>
                                        <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng thử miễn
                                            phí</p>
                                        <a class="button-pri inline-block text-center"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận
                                            tư vấn</a>
                                    </div>
                                </div>
                                <div class="box-pri-special p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
                                    <div class="button-special">
                                        <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
                                    </div>
                                    <div class="flex gap-1 flex-col">
                                        <p class="text-[22px] text-white font-semibold ">Professional</p>
                                        <div class="flex items-center gap-2 text-white text-xl  font-semibold">
                                            <p id="tr0">1.068.000đ </p><span class="text-base">/ Tháng</span>
                                        </div>
                                    </div>
                                    <div class="pt-8">
                                        <a class="button-pri inline-block text-center"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận
                                            tư vấn</a>
                                    </div>
                                </div>
                                <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
                                    <div class="flex gap-1 flex-col">
                                        <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
                                        <p class="text-xl text-[#575E70] font-semibold">Liên hệ ngay</p>
                                    </div>
                                    <div class="pt-10">
                                        <a class="button-pri inline-block text-center"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận
                                            tư vấn</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="xl:block md:block hidden">
                        <div class="flex flex-col gap-4">
                            <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
                            <table>
                                <tr>
                                    <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl px-5 py-6 text-center">
                                        <p class="text-[#3C4048] text-base font-medium text-left">Chi phí doanh nghiệp
                                            bỏ ra</p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                                        <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp nhỏ</p>
                                        <p class="text-[#296EFA] text-base font-medium">Có đội ngũ nhân sự đa nhiệm
                                            linh hoạt</p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                                        <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp </br>vừa & nhỏ</p>
                                        <p class="text-[#296EFA] text-base font-medium">Đang mở rộng quy mô</p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl px-5 py-6 text-center">
                                        <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp tầm trung &
                                            lớn </p>
                                        <p class="text-[#296EFA] text-base font-medium">Có quy trình hoạt động chuyên
                                            nghiệp bài bản</p>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Số người dùng theo gói
                                            (người)</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">3</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <div class="flex gap-4 justify-center text-center">
                                            <button class="btn-pay" id="btn-pay1">10</button>
                                            <button class="btn-pay active" id="btn-pay2">30</button>
                                            <button class="btn-pay" id="btn-pay3">50</button>
                                        </div>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Phí phần mềm (đồng/năm)</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">5,760,000</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]" id="tr1">12,816,000</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Phí setup (đồng)</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">500,000</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]" id="tr2">1,000,000</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Số buổi đào tạo (online)</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">1</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <div class="flex gap-4 justify-center text-center">
                                            <p class="font-normal text-base text-[#3C4048]" id="tr3">
                                                2
                                            </p>
                                        </div>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Dung lượng data cho phép</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">5,000</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <div class="flex gap-2 justify-center text-center">
                                            <p class="font-normal text-base text-[#3C4048]" id="tr4">
                                                30,000
                                            </p>
                                        </div>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-b border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Nâng cấp gói user
                                            (đồng/user/tháng)</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">200,000đ/user/tháng</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <div class="flex items-center justify-center font-normal text-base text-[#3C4048]">
                                            <p id="tr5">50,000</p> <span>đ/user/tháng</span>
                                        </div>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                                <tr class="border-t border-[#EBEBF5]">
                                    <td class="px-5 py-6">
                                        <p class="font-normal text-base text-[#3C4048]">Thanh toán</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <p class="font-normal text-base text-[#3C4048]">12 tháng</p>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                        <div class=" flex items-center justify-center gap-2 font-normal text-base text-[#3C4048]">
                                            <p id="tr6">12</p> <span>tháng</span>
                                        </div>
                                    </td>
                                    <td class="px-5 py-6 text-center bg-[#F9F9FB] rounded-br-3xl">
                                        <a class="font-normal text-base text-[#3C4048]"
                                           href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html">Nhận tư vấn</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="flex flex-col gap-4">
                            <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
                            <table>
                                <tr>
                                    <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl rounded-bl-3xl px-5 py-6 text-center">
                                        <p class="text-[#3C4048] text-base font-medium">Tính năng doanh nghiệp sở
                                            hữu</p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                                        <p class="text-[#296EFA] text-base font-medium">Giải pháp tích hợp các
                                            tính năng CRM cho mọi hoạt động của doanh nghiệp
                                            <b>(giới hạn user/data)</b>
                                        </p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                                        <p class="text-[#296EFA] text-base font-medium">Dễ dàng lựa chọn tính năng mở
                                            rộng dựa theo nhu cầu với chi phí tối ưu</p>
                                    </td>
                                    <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl rounded-br-3xl px-5 py-6 text-center">
                                        <p class="text-[#296EFA] text-base font-medium">Tuỳ biến hệ thống CRM cho doanh
                                            nghiệp lớn theo nhu cầu sử dụng</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="flex flex-row items-center gap-2 px-5 py-6">
                                            <img src="{{v2_url('images/fea1.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng cốt lõi</p>
                                        </div>
                                    </td>
                                </tr>
                                @foreach($featureTab_1 as $featureShowObject)
                                    <tr class="border-b border-t border-[#EBEBF5]">
                                        <td class="px-5 py-6 border-l-2 border-[#5081FF]">
                                            <p class="font-normal text-base text-[#3C4048] w-fit"
                                               data-tooltip-target="tooltip-right"
                                               data-tooltip-placement="right">{{$featureShowObject->getFeatureName()}}</p>
                                            <div id="tooltip-right" role="tooltip"
                                                 class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                                                {{$featureShowObject->getFeatureTooltip()}}
                                                <div class="tooltip-arrow" data-popper-arrow></div>
                                            </div>
                                        </td>
                                        <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                            <div class="flex justify-center">
                                                @if($featureShowObject->getStartupAvailable() == 1)
                                                    <img src="{{v2_url('images/confirmation.png')}}">
                                                @else
                                                    <img src="{{v2_url('images/refuse.png')}}">
                                                @endif
                                            </div>
                                        </td>
                                        <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                            <div class="flex justify-center">
                                                @if($featureShowObject->getProfessionalAvailable() == 1)
                                                    <img src="{{v2_url('images/confirmation.png')}}">
                                                @else
                                                    <img src="{{v2_url('images/refuse.png')}}">
                                                @endif
                                            </div>
                                        </td>
                                        <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                            <div class="flex justify-center">
                                                @if($featureShowObject->getEnterpriseAvailable()  == 1)
                                                    <img src="{{v2_url('images/confirmation.png')}}">
                                                @else
                                                    <img src="{{v2_url('images/refuse.png')}}">
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                            <div class="h-[270px] overflow-y-hidden" id="table-feature">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="flex flex-row items-center gap-2 px-5 py-6">
                                                <img src="{{v2_url('images/fea2.png')}}" class="w-6 h-6">
                                                <p class="text-base font-bold text-[#3C4048]">Tính năng mở rộng</p>
                                            </div>
                                        </td>
                                    </tr>
                                    @foreach($featureTab_2 as $featureShowObject)
                                        <tr class="border-b border-t border-[#EBEBF5]">
                                            <td class="px-5 py-6 border-l-2 border-[#FFC671] w-[360px]">
                                                <p class="font-normal text-base text-[#3C4048] w-fit"
                                                   data-tooltip-target="tooltip-right10" data-tooltip-placement="right">
                                                    {{$featureShowObject->getFeatureName()}}</p>
                                                <div id="tooltip-right10" role="tooltip"
                                                     class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                                                    {{$featureShowObject->getFeatureTooltip()}}
                                                    <div class="tooltip-arrow" data-popper-arrow></div>
                                                </div>
                                            </td>
                                            <td class="px-5 py-6 text-center bg-[#F9F9FB] w-[270px]">
                                                <div class="flex justify-center">
                                                    @if($featureShowObject->getStartupAvailable() == 1)
                                                        <img src="{{v2_url('images/confirmation.png')}}">
                                                    @else
                                                        <img src="{{v2_url('images/refuse.png')}}">
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-5 py-6 text-center bg-[#F9F9FB] w-[270px]">
                                                <div class="flex justify-center">
                                                    @if($featureShowObject->getProfessionalAvailable() == 1)
                                                        <img src="{{v2_url('images/confirmation.png')}}">
                                                    @else
                                                        <img src="{{v2_url('images/refuse.png')}}">
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="px-5 py-6 text-center bg-[#F9F9FB] w-[270px]">
                                                <div class="flex justify-center">
                                                    @if($featureShowObject->getEnterpriseAvailable() == 1)
                                                        <img src="{{v2_url('images/confirmation.png')}}">
                                                    @else
                                                        <img src="{{v2_url('images/refuse.png')}}">
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="flex justify-center bg-linear-feature">
                        <div class="flex gap-4 cursor-pointer " id="btn-feature">
                            <p class="text-[#064FF4] text-base font-medium">Xem tất cả các tính năng</p>
                            <img src="{{v2_url('images/checkdown.png')}}" class="w-6 h-6">
                        </div>
                    </div>
                    <!--Mobile-->
                    <div class="xl:hidden md:hidden block">
                        <div class="flex flex-col gap-4">
                            <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
                            <div class="bg-[#F2F5FD] p-2">
                                <div id="accordion-flush" data-accordion="collapse"
                                     data-active-classes="bg-white text-gray-900">
                                    <h2 id="accordion-flush-heading-1">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-1">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#0555F8] font-bold text-sm">Doanh nghiệp nhỏ</span>
                                                <span class="text-[#296EFA] font-medium text-sm">Có đội ngũ nhân sự đa nhiệm linh hoạt</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                        <div class="p-2">
                                            <div class="box-pri p-4 flex xl:gap-6 md:gap-6 gap-2 flex-col border !w-full">
                                                <div class="flex gap-1 flex-col">
                                                    <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
                                                    <p class="text-xl text-[#1F2A37] font-semibold ">480.000đ <span
                                                                class="text-base">/ Tháng</span></p>
                                                </div>
                                                <div>
                                                    <p class="text-[#FA901B] text-sm font-semibold xl:px-6 md:px-6 px-0 py-[10px]">
                                                        Dùng thử miễn phí</p>
                                                    <a class="button-pri"
                                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html"
                                                       target="_blank">Nhận tư vấn</a>
                                                </div>
                                            </div>
                                        </div>
                                    </h2>
                                    <div id="accordion-flush-body-1" class="hidden"
                                         aria-labelledby="accordion-flush-heading-1">
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số người dùng theo gói (người)</p>
                                            <p class="text-sm font-normal">3</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí phần mềm (đồng/năm)</p>
                                            <p class="text-sm font-normal">5.760.000</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí setup (đồng)</p>
                                            <p class="text-sm font-normal">500.000</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số buổi đào tạo (online)</p>
                                            <p class="text-sm font-normal">1</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Dung lượng data</br> cho phép (data)</p>
                                            <p class="text-sm font-normal">5.000</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nâng cấp gói user</br> (đồng/user/tháng)</p>
                                            <p class="text-sm font-normal">200.000đ/user/tháng</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thanh toán</p>
                                            <p class="text-sm font-normal">12 tháng</p>
                                        </div>
                                    </div>
                                    <h2 id="accordion-flush-heading-2">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-2">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#0555F8] font-bold text-sm">Doanh nghiệp vừa & nhỏ</span>
                                                <span class="text-[#296EFA] font-medium text-sm">Đang mở rộng quy mô</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                        <div class="p-2">
                                            <div class="box-pri-special p-4 flex xl:gap-6 md:gap-6 gap-2 flex-col border !w-full">
                                                <div class="button-special">
                                                    <p class="text-white uppercase text-[10px] font-semibold">Phổ biến
                                                        nhất</p>
                                                </div>
                                                <div class="flex gap-1 flex-col">
                                                    <p class="text-[22px] text-white font-semibold ">Professional</p>
                                                    <div class="flex items-center gap-2 text-white text-xl font-semibold">
                                                        <p id="m-tr0">1.068.000 </p><span
                                                                class="text-xl text-[#F4F4F6] font-semibold">đ/ Tháng</span>
                                                    </div>
                                                </div>
                                                <div class="pt-10">
                                                    <a class="button-pri"
                                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html"
                                                       target="_blank">Nhận tư vấn</a>
                                                </div>
                                            </div>
                                        </div>
                                    </h2>
                                    <div id="accordion-flush-body-2" class="hidden"
                                         aria-labelledby="accordion-flush-heading-2">
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số người dùng </br>theo gói (người)</p>
                                            <div class="text-sm font-normal">
                                                <div class="flex gap-2 justify-center text-center">
                                                    <button class="btn-pay !px-3" id="m-btn-pay1">10</button>
                                                    <button class="btn-pay !px-3 active" id="m-btn-pay2">30</button>
                                                    <button class="btn-pay !px-3" id="m-btn-pay3">50</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí phần mềm (đồng/năm)</p>
                                            <p class="text-sm font-normal" id="m-tr1">12.816.000</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí setup (đồng)</p>
                                            <p class="text-sm font-normal" id="m-tr2">1.000.000</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số buổi setup</br> (online)</p>
                                            <p class="text-sm font-normal" id="m-tr3">2</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Dung lượng data</br> cho phép (data)</p>
                                            <p class="text-sm font-normal" id="m-tr3">2</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nâng cấp gói user</br> (đồng/user/tháng)</p>
                                            <div class="flex gap-1 items-center justify-center text-sm font-normal">
                                                <p id="m-tr4">50.000</p><span>đ/user/tháng</span>
                                            </div>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thanh toán</p>
                                            <div class="flex gap-1 items-center justify-center text-sm font-normal">
                                                <p class="text-sm font-normal" id="m-tr5">12</p> <span> tháng</span>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 id="accordion-flush-heading-3">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-3">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#0555F8] font-bold text-sm">Doanh nghiệp tầm trung & lớn</span>
                                                <span class="text-[#296EFA] font-medium text-sm">Có quy trình hoạt động chuyên nghiệp bài bản</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                        <div class="p-2">
                                            <div class="box-pri p-4 flex xl:gap-6 md:gap-6 gap-2 flex-col border !w-full">
                                                <div class="flex gap-1 flex-col">
                                                    <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
                                                    <p class="text-[15px] text-[#1C60FA] font-semibold">Mạnh mẽ - Linh
                                                        hoạt</p>
                                                </div>
                                                <div class="pt-10">
                                                    <a class="button-pri inline-block text-center"
                                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html"
                                                       target="_blank">Nhận tư vấn</a>
                                                </div>
                                            </div>
                                        </div>
                                    </h2>
                                    <div id="accordion-flush-body-3" class="hidden"
                                         aria-labelledby="accordion-flush-heading-3">
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số người dùng </br>theo gói (người)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí phần </br>mềm (đồng/năm)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Phí setup (đồng)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Số buổi setup </br>(online)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Dung lượng data</br> cho phép (data)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nâng cấp gói user </br>(đồng/user/tháng)</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thanh toán</p>
                                            <p class="text-sm font-normal">Nhận tư vấn</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex flex-col gap-4 pt-4">
                            <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
                            <div class="bg-[#F2F5FD] p-2">
                                <div id="accordion-flush" data-accordion="collapse"
                                     data-active-classes="bg-white text-gray-900">
                                    <h2 id="accordion-flush-heading-4">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-4">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#296EFA] font-bold text-sm">Giải pháp tích hợp các tính năng CRM cho mọi hoạt động của doanh nghiệp. (giới hạn user/data)</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                    </h2>
                                    <div id="accordion-flush-body-4" class="hidden"
                                         aria-labelledby="accordion-flush-heading-4">
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea1.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng cốt lõi</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Giao và giám sát mục tiêu KPI</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý hành trình khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Automation MKT (Email, SMS,...)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Workstream: MXH nội bộ doanh nghiệp</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Landing pages</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý phản hồi khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý dự án công việc</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Tự động phân lead</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý chiến dịch marketing</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Hỗ trợ kết nối API với phần mềm
                                                khác</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <!--mở rộng-->
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea2.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng mở rộng</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tổng đài chăm sóc KH</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý nhân sự HRM</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nhắn tin & gọi điện liên phòng ban</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Chấm công GPS hoặc IP tĩnh</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Kết nối Facebook, Zalo ghi nhận tiềm năng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý Kho & bán lẻ</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tích điểm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tài chính công nợ khách
                                                hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thiết lập quy trình bán hàng - sản
                                                phẩm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Sàn TMĐT (Shopee)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Bảo mật nâng cao theo nhu cầu</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi lịch đi tuyến</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi bảo hành, bảo trì</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                    </div>
                                    <h2 id="accordion-flush-heading-5">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-5">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#296EFA] font-bold text-sm">Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                    </h2>
                                    <div id="accordion-flush-body-5" class="hidden"
                                         aria-labelledby="accordion-flush-heading-5">
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea1.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng cốt lõi</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Giao và giám sát mục tiêu KPI</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý hành trình khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Automation MKT (Email, SMS,...)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Workstream: MXH nội bộ doanh nghiệp</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Landing pages</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý phản hồi khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý dự án công việc</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Tự động phân lead</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý chiến dịch marketing</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Hỗ trợ kết nối API với phần mềm
                                                khác</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <!--mở rộng-->
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea2.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng mở rộng</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tổng đài chăm sóc KH</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý nhân sự HRM</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nhắn tin & gọi điện liên phòng ban</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Chấm công GPS hoặc IP tĩnh</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Kết nối Facebook, Zalo ghi nhận tiềm năng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý Kho & bán lẻ</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tích điểm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tài chính công nợ khách
                                                hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thiết lập quy trình bán hàng - sản
                                                phẩm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Sàn TMĐT (Shopee)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Bảo mật nâng cao theo nhu cầu</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi lịch đi tuyến</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi bảo hành, bảo trì</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                    </div>
                                    <h2 id="accordion-flush-heading-6">
                                        <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                                                data-accordion-target="#accordion-flush-body-6">
                                            <div class="flex flex-col text-left">
                                                <span class="text-[#296EFA] font-bold text-sm">Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng</span>
                                            </div>
                                            <img src="{{v2_url('images/chevron_up_outline.png')}}" data-accordion-icon>
                                        </button>
                                    </h2>
                                    <div id="accordion-flush-body-6" class="hidden"
                                         aria-labelledby="accordion-flush-heading-6">
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea1.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng cốt lõi</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Giao và giám sát mục tiêu KPI</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý hành trình khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Automation MKT (Email, SMS,...)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Workstream: MXH nội bộ doanh nghiệp</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Landing pages</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý phản hồi khách hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý dự án công việc</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Tự động phân lead</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý chiến dịch marketing</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}" class="w-5 h-5">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#5081FF] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Hỗ trợ kết nối API với phần mềm
                                                khác</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/check.png')}}">
                                            </p>
                                        </div>
                                        <!--mở rộng-->
                                        <div class="flex flex-row items-center gap-2 px-2 py-6">
                                            <img src="{{v2_url('images/fea2.png')}}" class="w-6 h-6">
                                            <p class="text-base font-bold text-[#3C4048]">Tính năng mở rộng</p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tổng đài chăm sóc KH</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý nhân sự HRM</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Nhắn tin & gọi điện liên phòng ban</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Chấm công GPS hoặc IP tĩnh</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Kết nối Facebook, Zalo ghi nhận tiềm năng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý Kho & bán lẻ</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tích điểm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Quản lý tài chính công nợ khách
                                                hàng</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Thiết lập quy trình bán hàng - sản
                                                phẩm</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Sàn TMĐT (Shopee)</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Bảo mật nâng cao theo nhu cầu</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi lịch đi tuyến</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                        <div class="py-3 px-1 border-b border-l-2 border-l-[#FFC671] flex flex-row justify-between">
                                            <p class="text-sm font-normal">Theo dõi bảo hành, bảo trì</p>
                                            <p class="text-sm font-normal">
                                                <img src="{{v2_url('images/refuse.png')}}">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full flex items-center">
                <div class="bg-price-footer w-full flex items-center flex-col xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat xl:h-[1000px] md:h-[1000px] h-[1550px]">
                    <div class="xl:w-1/2 md:w-1/2 w-full pt-28 aos-init aos-animate" data-aos="fade-down">
                        <p class="xl:text-[42px] text-[30px] font-bold text-white text-center leading-[60px] mac:text-[38px]">
                            Doanh nghiệp lớn cấu hình hệ thống không giới hạn với CRM on Premise</p>
                        <p class="text-white text-xl text-center pt-6 font-normal mac:text-[18px]">Chủ động cấu hình
                            trên server riêng - Trọn bộ 100++ tính năng của Getfly CRM - Thiết kế riêng cho từng đặc
                            thù doanh nghiệp</p>
                    </div>
                    <div class="flex justify-center pt-8">
                        <div class="flex xl:gap-8 md:gap-8 gap-8 xl:flex-row md:flex-row flex-col justify-between">
                            <div class="box-pri !w-[350px] p-4 flex gap-6 flex-col border border-[#ECBC74] shadow-lg"
                                 data-aos="fade-down">
                                <div class="flex gap-1 flex-col">
                                    <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                                        <img src="{{v2_url('images/icon-rocket.png')}}"> Master
                                    </p>
                                    <p class="text-[15px] text-[#1C60FA] font-semibold ">Tối ưu chi phí</p>
                                </div>
                                <div class="flex gap-4 flex-col pack-desc">
                                    <div class="flex flex-col gap-1">
                                        <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                                        <p class="text-xl text-[#1F2A37] font-semibold">40.000.000đ</p>
                                    </div>
                                    <div class="flex gap-3 flex-col ">
                                        <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                                        <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                    src="{{v2_url('images/users.png')}}"> 20 người dùng</p>
                                        <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                    src="{{v2_url('images/data.png')}}"> Không giới hạn data</p>
                                    </div>
                                    <p class="text-[#3C4048] text-sm">Tư vấn tuỳ chỉnh theo quy mô doanh nghiệp</p>
                                </div>
                                <div class="pt-16">
                                    <a class="button-pri inline-block text-center"
                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                        vấn</a>
                                </div>
                            </div>
                            <div class="box-pri !w-[350px] p-4 flex gap-6 flex-col border border-[#ECBC74] shadow-lg"
                                 data-aos="fade-down">
                                <div class="flex gap-1 flex-col">
                                    <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                                        <img src="{{v2_url('images/icon-rocket.png')}}"> Enterprise
                                    </p>
                                    <p class="text-[15px] text-[#1C60FA] font-semibold">Không giới hạn sức mạnh </p>
                                </div>
                                <div class="flex gap-4 flex-col pack-desc">
                                    <div class="flex flex-col gap-1">
                                        <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                                        <p class="text-xl text-[#1F2A37] font-semibold ">Liên hệ ngay</p>
                                    </div>
                                    <div class="flex gap-3 flex-col ">
                                        <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                                        <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                    src="{{v2_url('images/users.png')}}"> Không giới hạn data</p>
                                        <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                                    src="{{v2_url('images/data.png')}}"> Không giới hạn data</p>
                                    </div>
                                    <p class="text-[#3C4048] text-sm">Giải pháp chuyển đổi số vượt bậc cho doanh
                                        nghiệp lớn</p>
                                </div>
                                <div class="pt-10">
                                    <a class="button-pri inline-block text-center"
                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                        vấn</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-pric flex justify-center xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat xl:h-[1650px] md:h-[1650px] h-[2250px]">
                <div class="xl:w-3/5 w-4/5 bg-[#FFFCF9 flex gap-8 flex-col">
                    <div class="text-center pt-[106px]">
                        <p class="text-[#113DD8] xl:text-[38px] md:text-[38px] text-[30px] font-bold leading-normal mac:text-[44px]">
                            Giải pháp tư vấn chuyển đổi số giúp doanh nghiệp vận hành tự động với quy trình
                            bài bản</p>
                        <p class="text-[#37393E] text-xl font-medium pt-6">Gói chuyển đổi số - Chìa khóa khởi
                            động quy trình làm việc tự động & chuyên nghiệp</p>
                    </div>

                    <div class="xl:block md:block hidden">
                        <div class="flex pt-[70px] relative">
                            <div class="w-1/3 ">
                                <div class="xl:w-[427px] md:w-[427px] h-[234px] mac:w-[410px] flex flex-col gap-4 border rounded-[20px] border-[#ECBC74] bg-[#FFFCF9] p-10 sticky top-[90px] left-0">
                                    <p class="text-[#454545] text-[26px] font-semibold">Chuyên gia chuyển đổi số của
                                        chúng tôi đã sẵn sàng</p>
                                    <a class="button-pri !w-32 text-center shadow-none"
                                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Tư vấn
                                        ngay</a>
                                </div>
                            </div>
                            <div class="w-2/3 flex flex-col items-end gap-10">
                                <div class="flex flex-col gap-4 rounded-[20px] bg-[#E9F0FA] p-10 xl:w-[427px] md:w-[427px] mac:w-[410px]">
                                    <img class="w-10 h-10" src="{{v2_url('images/gp1.png')}}">
                                    <p class="text-2xl text-[#484848] font-medium">Chiến lược kinh doanh bài bản -
                                        ra quyết định quản lý dựa trên số liệu</p>
                                </div>
                                <div class="flex flex-col gap-4 rounded-[20px] bg-[#FAEED7] p-10 xl:w-[427px] md:w-[427px] mac:w-[410px]">
                                    <img class="w-10 h-10" src="{{v2_url('images/gp1.png')}}">
                                    <p class="text-2xl text-[#484848] font-medium">Xây dựng đội ngũ nhân sự làm việc chủ
                                        động, giảm gánh nặng quản lý</p>
                                </div>
                                <div class="flex flex-col gap-4 rounded-[20px] bg-[#E5F5F4] p-10 xl:w-[427px] md:w-[427px] mac:w-[410px]">
                                    <img class="w-10 h-10" src="{{v2_url('images/gp3.png')}}">
                                    <p class="text-2xl text-[#484848] font-medium">Khai thác dữ liệu khách hàng -
                                        gia tăng hiệu quả chiến dịch marketing</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!--mobile-->
                    <div class="flex flex-col gap-10 pt-[70px] relative xl:hidden md:hidden block">
                        <div class="w-full h-[294px] flex flex-col gap-4 border rounded-[20px] border-[#ECBC74] bg-[#FFFCF9] p-10">
                            <p class="text-[#454545] text-[26px] font-semibold">Chuyên gia chuyển đổi số của chúng
                                tôi đã sẵn sàng</p>
                            <a class="button-pri !w-32 text-center"
                               href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Tư vấn ngay</a>
                        </div>
                        <div class="flex flex-col gap-4 border rounded-[20px] bg-[#E9F0FA] p-10 w-full shadow-md">
                            <img class="w-10 h-10" src="{{v2_url('images/gp1.png')}}">
                            <p class="text-2xl text-[#484848] font-medium">Chiến lược kinh doanh bài bản - ra quyết
                                định quản lý dựa trên số liệu</p>
                        </div>
                        <div class="flex flex-col gap-4 border rounded-[20px] bg-[#FAEED7] p-10 w-full shadow-md">
                            <img class="w-10 h-10" src="{{v2_url('images/gp1.png')}}">
                            <p class="text-2xl text-[#484848] font-medium">Xây dựng đội ngũ nhân sự làm việc chủ động,
                                giảm gánh nặng quản lý</p>
                        </div>
                        <div class="flex flex-col gap-4 border rounded-[20px] bg-[#E5F5F4] p-10 w-full shadow-md">
                            <img class="w-10 h-10" src="{{v2_url('images/gp3.png')}}">
                            <p class="text-2xl text-[#484848] font-medium">Khai thác dữ liệu khách hàng - gia tăng
                                hiệu quả chiến dịch marketing</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full flex justify-center relative">
                <div class="xl:w-3/5 w-4/5 flex gap-2 relative">
                    <div class="box-pri2 absolute xl:h-[300px] md:h-[300px] h-[420px] xl:!top-[-370px] md:!top-[-370px] !-top-[430px] xl:p-[30px] md:p-[30px] p-[20px] flex gap-8 items-center xl:flex-row md:flex-row flex-col">
                        <div class="flex flex-col gap-4 w-full">
                            <p class="text-[#313235] font-semibold text-[26px] mac:text-[24px]">Giải đáp ngay mọi thắc
                                mắc về lựa chọn gói với chuyên gia Getfly</p>
                            <p class="text-[#3C4048] text-[18px]">Quản lý khép kín toàn bộ quy trình Trước - Trong -
                                Sau bán hàng ngay hôm nay với chúng tôi!</p>
                            <div class="flex justify-center items-center">
                                <a class="button-pri text-center xl:!w-32 md:!w-32 w-full"
                                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                    vấn</a>
                            </div>
                        </div>
                        <img src="{{v2_url('images/fly.png')}}" class="fly">
                    </div>
                </div>
            </div>
            @include('version2.layouts.inc.footer')
        </div>
    </div>
@endsection
@section('page_scripts')
    @include("version2.layouts.inc.pricing-scripts")
@endsection