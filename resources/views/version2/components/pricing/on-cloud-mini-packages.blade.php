@php use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php

    /**
         * @var V2PricingClientViewModel $v2PricingClientViewModel
         */

        $startUp = $v2PricingClientViewModel->getOnCloudPackage(0);
        $professional = $v2PricingClientViewModel->getOnCloudPackage(1);
        $enterprise = $v2PricingClientViewModel->getOnCloudPackage(2);
@endphp
{{--<div class="flex xl:gap-6 md:gap-2 xl:flex-row md:flex-row flex-col">--}}
{{--    <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">--}}
{{--        <div class="flex gap-1 flex-col">--}}
{{--            <p class="text-[22px] text-[#113DD8] font-semibold">{{$startUp->getName()}}</p>--}}
{{--            <p class="text-xl text-[#1F2A37] font-semibold">{{$startUp->getAmountPerMonth()}}--}}
{{--        </div>--}}
{{--        <div>--}}
{{--            <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng thử miễn--}}
{{--                phí</p>--}}
{{--            <a href="{{route('sites.trial')}}" target="_blank">--}}
{{--                <button class="button-pri">--}}
{{--                    Nhận--}}
{{--                    tư vấn--}}
{{--                </button>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="box-pri-special p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">--}}
{{--        <div class="button-special" style="left: 56px">--}}
{{--            <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>--}}
{{--        </div>--}}
{{--        <div class="flex gap-1 flex-col">--}}
{{--            <p class="text-[22px] text-white font-semibold ">{{$professional->getName()}}</p>--}}
{{--            <p class="text-xl text-white font-semibold">{{$professional->getAmountPerMonth()}}--}}
{{--            </p>--}}
{{--        </div>--}}
{{--        <div class="pt-10">--}}
{{--            <a href="{{route('sites.trial')}}" target="_blank">--}}
{{--                <button class="button-pri">--}}
{{--                    Nhận--}}
{{--                    tư vấn--}}
{{--                </button>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">--}}
{{--        <div class="flex gap-1 flex-col">--}}
{{--            <p class="text-[22px] text-[#113DD8] font-semibold">{{$enterprise->getName()}}</p>--}}
{{--            <p class="text-xl text-[#575E70] font-semibold">{{$enterprise->getAmountPerMonth()}}</p>--}}
{{--        </div>--}}
{{--        <div class="pt-10">--}}
{{--            <a href="{{route('sites.trial')}}" target="_blank">--}}
{{--                <button class="button-pri">--}}
{{--                    Nhận--}}
{{--                    tư vấn--}}
{{--                </button>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="flex xl:gap-6 md:gap-2 xl:flex-row md:flex-row flex-col">
    <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
        <div class="flex gap-1 flex-col">
            <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
            <p class="text-xl text-[#1F2A37] font-semibold">480.000đ <span class="text-base">/ Tháng</span></p>
        </div>
        <div>
            <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng thử miễn phí</p>
            <button class="button-pri">
                <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư vấn</a>
            </button>
        </div>
    </div>
    <div class="box-pri-special p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
        <div class="button-special !left-[50px]">
            <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
        </div>
        <div class="flex gap-1 flex-col">
            <p class="text-[22px] text-white font-semibold ">Professional</p>
            <div class="flex items-center gap-2 text-white text-xl  font-semibold">
                <p id="tr0">1.068.000 </p><span class="text-base">đ/ Tháng</span>
            </div>
        </div>
        <div class="pt-10">
            <button class="button-pri">
                <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư vấn</a>
            </button>
        </div>
    </div>
    <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[226px] md:w-[226px] w-[230px]">
        <div class="flex gap-1 flex-col">
            <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
            <p class="text-xl text-[#575E70] font-semibold">Liên hệ ngay</p>
        </div>
        <div class="pt-10">
            <button class="button-pri">
                <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư vấn</a>
            </button>
        </div>
    </div>
</div>