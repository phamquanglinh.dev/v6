@php
    use App\ViewModels\V2\V2PricingClientViewModel;
    /**
* @var V2PricingClientViewModel $v2PricingClientViewModel
 */
@endphp
<div class="flex flex-col gap-4">
    <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
    <table>
        <tr>
            <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl px-5 py-6 text-center">
                <p class="text-[#3C4048] text-base font-medium">Tính năng doanh nghiệp sở
                    hữu</p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                <p class="text-[#296EFA] text-base font-medium">Giải pháp tích hợp các
                    tính năng CRM cho mọi hoạt động của doanh nghiệp.
                    <b>(giới hạn user/data)</b>
                </p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                <p class="text-[#296EFA] text-base font-medium">Dễ dàng lựa chọn tính năng mở
                    rộng dựa theo nhu cầu với chi phí tối ưu</p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl px-5 py-6 text-center">
                <p class="text-[#296EFA] text-base font-medium">Tuỳ biến hệ thống CRM cho doanh
                    nghiệp lớn theo nhu cầu sử dụng</p>
            </td>
        </tr>
        @foreach($v2PricingClientViewModel->getFeatureCollection() as $feature)
            {{--        @dd($feature);--}}
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right"
                       data-tooltip-placement="right">{{$feature->getFeatureName()}}</p>
                    <div id="tooltip-right" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        {{--                    Thiết lập/theo dõi/đo lường KPI toàn diện - chính xác--}}
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        @if($feature->getStartupAvailable())
                            <img src="{{version2_url('images/confirmation.png')}}">
                        @else
                            <img src="{{version2_url('images/refuse.png')}}">
                        @endif
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        @if($feature->getProfessionalAvailable())
                            <img src="{{version2_url('images/confirmation.png')}}">
                        @else
                            <img src="{{version2_url('images/refuse.png')}}">
                        @endif
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        @if($feature->getEnterpriseAvailable())
                            <img src="{{version2_url('images/confirmation.png')}}">
                        @else
                            <img src="{{version2_url('images/refuse.png')}}">
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>