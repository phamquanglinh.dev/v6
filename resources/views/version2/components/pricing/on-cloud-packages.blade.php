@php use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
     * @var V2PricingClientViewModel $v2PricingClientViewModel
     */

    $startUp = $v2PricingClientViewModel->getOnCloudPackage(0);
    $professional = $v2PricingClientViewModel->getOnCloudPackage(1);
    $enterprise = $v2PricingClientViewModel->getOnCloudPackage(2);
@endphp
<div class="box-pri p-4 flex gap-6 flex-col border border-[#ECBC74]" data-aos="fade-up"
     data-aos-duration="1500">
    <div class="flex gap-1 flex-col">
        <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
            <img src="{{version2_url('images/icon-rocket.png')}}"> {{$startUp->getName()}}
        </p>
        <p class="text-[15px] text-[#1C60FA] font-semibold ">{{$startUp->getDescription()}}</p>
    </div>
    <div class="flex gap-4 flex-col pack-desc">
        <div class="flex flex-col gap-1">
            <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
            <p class="text-xl text-[#1F2A37] font-semibold">{{$startUp->getAmountPerMonth()}} <span
                        class="text-base"></span>
            </p>
        </div>
        <div class="flex gap-3 flex-col ">
            <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
            <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/users.png')}}"> {{$startUp->getUser()}}</p>
            <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/data.png')}}">{{$startUp->getData()}}</p>
        </div>
        <p class="text-[#3C4048] text-sm">{!! $startUp->getBenefitText() !!}</p>
    </div>
    <div>
        <p class="text-[#FA901B] text-sm font-semibold py-[10px]">Dùng thử miễn phí</p>
        <a href="{{route('sites.trial')}}" target="_blank">
            <button class="button-pri">
                Nhận tư
                vấn
            </button>
        </a>
    </div>
</div>
<div class="box-pri-special p-4 flex flex-col border" data-aos="fade-up"
     data-aos-duration="800">
    <div class="flex justify-between text-center">
        <div class="button-special">
            <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
        </div>
    </div>
    <div class="flex gap-1 flex-col">
        <p class="text-[22px] text-white font-semibold flex gap-2 items-center">
            <img src="{{version2_url('images/icon-rocket.png')}}"> {{$professional->getName()}}
        </p>
        <p class="text-[15px] text-[#FFA41D] font-semibold">{{$professional->getDescription()}}</p>
    </div>
    <div class="flex gap-4 flex-col pack-desc pt-12">
        <div class="flex flex-col gap-1">
            <p class="text-sm text-[#EDEFF2] font-semibold">Chỉ từ</p>
            <p class="text-xl text-[#F4F4F6] font-semibold"> {{$professional->getAmountPerMonth()}}
            </p>
        </div>
        <div class="flex gap-3 flex-col ">
            <p class="text-sm text-[#F4F4F6] font-semibold">Khởi điểm từ:</p>
            <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/users.png')}}"> {{$professional->getUser()}}</p>
            <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/data.png')}}"> {{$professional->getData()}}</p>
        </div>
        <p class="text-[#F0F2F4] text-sm">{!! $professional->getBenefitText() !!}</p>
    </div>
    <div class="pt-10">
        <a href="{{route('sites.trial')}}" target="_blank">
            <button class="button-pri">
                Nhận tư
                vấn
            </button>
        </a>
    </div>
</div>
<div class="box-pri p-4 flex gap-6 flex-col border border-[#ECBC74]" data-aos="fade-up"
     data-aos-duration="1500">
    <div class="flex gap-1 flex-col">
        <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
            <img src="{{version2_url('images/icon-rocket.png')}}"> {{$enterprise->getName()}}
        </p>
        <p class="text-[15px] text-[#1C60FA] font-semibold">{{$enterprise->getDescription()}}</p>
    </div>
    <div class="flex gap-4 flex-col pack-desc">
        <div class="flex flex-col gap-1">
            <p class="text-sm text-[#9DA4AE] font-semibold">Đừng ngại ngần</p>
            <p class="text-xl text-[#1F2A37] font-semibold ">{{$enterprise->getAmountPerMonth()}}</p>
        </div>
        <div class="flex gap-3 flex-col ">
            <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
            <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/users.png')}}"> {{$enterprise->getUser()}}</p>
            <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                        src="{{version2_url('images/data.png')}}"> {{$enterprise->getData()}}</p>
        </div>
        <p class="text-[#3C4048] text-sm">{!! $enterprise->getBenefitText() !!}</p>
    </div>
    <div class="pt-10">
        <a href="{{route('sites.trial')}}" target="_blank">
            <button class="button-pri">
                Nhận tư
                vấn
            </button>
        </a>
    </div>
</div>