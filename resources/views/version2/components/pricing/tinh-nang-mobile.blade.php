<div class="flex flex-col gap-4 pt-4">
    <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
    <div class="bg-[#F2F5FD] p-2">
        <div id="accordion-flush" data-accordion="collapse"
             data-active-classes="bg-white text-gray-900">
            <h2 id="accordion-flush-heading-4">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-4">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Giải pháp tích hợp các tính năng CRM cho mọi hoạt động của doanh nghiệp. (giới hạn user/data)</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-4" class="hidden"
                 aria-labelledby="accordion-flush-heading-4">
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Báo cáo KPI</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Automation MKT</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Chat & call</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Kho + Bán lẻ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Điểm thưởng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Tổng đài</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">HRM</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo hành</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Đi tuyến</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối
                        quan hệ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                        định</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của
                        người dùng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </p>
                </div>
            </div>
            <h2 id="accordion-flush-heading-5">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-5">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-5" class="hidden"
                 aria-labelledby="accordion-flush-heading-5">
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Báo cáo KPI</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Automation MKT</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Chat & call</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Kho + Bán lẻ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Điểm thưởng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Tổng đài</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">HRM</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo hành</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Đi tuyến</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối
                        quan hệ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                        định</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của
                        người dùng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
            </div>
            <h2 id="accordion-flush-heading-6">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-6">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-6" class="hidden"
                 aria-labelledby="accordion-flush-heading-6">
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Báo cáo KPI</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Automation MKT</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Chat & call</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Kho + Bán lẻ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Điểm thưởng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Tổng đài</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">HRM</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo hành</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Đi tuyến</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối
                        quan hệ</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                        định</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
                <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                    <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của
                        người dùng</p>
                    <p class="text-sm font-medium">
                        <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>