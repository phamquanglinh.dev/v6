<div class="w-full bg-price flex justify-center items-center flex-col">
    <div class="sm:w-3/4 xl:w-1/2 pt-20">
        <p class="xl:text-5xl md:text-4xl text-3xl font-bold text-white text-center !leading-[60px] px-2" data-aos="fade-down" data-aos-duration="1000">Lựa chọn giải pháp hoàn hảo ngay cho doanh nghiệp của bạn</p>
    </div>
    <div class="xl:pt-24 md:pt-[30px] pt-[30px]">
        <ul data-aos="fade-up" class="flex flex-wrap text-sm font-medium text-center xl:gap-4  md:gap-4 gap-2" id="default-tab" data-tabs-toggle="#default-tab-content" role="tablist">
            <li role="presentation">
                <button class="inline-block text-base p-4 rounded-t-lg text-white font-bold uppercase xl:border md:border border border-white active-pricing" id="btn-1" onclick="openPricingTab('pricing1')">CRM ON CLOUD</button>
            </li>
            <li role="presentation">
                <button class="inline-block text-base p-4 rounded-t-lg text-white font-bold uppercase xl:border md:border border border-white" id="btn-2" onclick="openPricingTab('pricing2')">CRM ON PREMISE</button>
            </li>
        </ul>
    </div>
</div>