@php use App\Models\V2Pricing;use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
* @var V2PricingClientViewModel $v2PricingClientViewModel
*/
$mobilePricingData = $v2PricingClientViewModel->getMobilePricingData();
$heading = $v2PricingClientViewModel->definePricingTableHead();
@endphp
<div class="flex flex-col gap-4">
    <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
    <div class="bg-[#F2F5FD] p-2">
        <div id="accordion-flush" data-accordion="collapse"
             data-active-classes="bg-white text-gray-900">
            <h2 id="accordion-flush-heading-1">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-1">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Doanh nghiệp nhỏ</span>
                        <span class="font-medium text-sm">Có đội ngũ nhân sự đa nhiệm linh hoạt</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-1" class="hidden"
                 aria-labelledby="accordion-flush-heading-1">
                <div class="p-2">
                    <div class="box-pri p-4 flex xl:gap-6 md:gap-6 gap-2 flex-col border !w-full">
                        <div class="flex gap-1 flex-col">
                            <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
                            <p class="text-xl text-[#1F2A37] font-semibold ">480.000đ <span
                                        class="text-base">/ Tháng</span></p>
                        </div>
                        <div>
                            <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng
                                thử miễn phí</p>
                            <a href="{{route('sites.trial')}}"
                               target="_blank">
                                <button class="button-pri">
                                    Nhận tư vấn
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

                @foreach($mobilePricingData[V2Pricing::SMALL_BUSINESS] as $key => $value)
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">{{$heading[$key]}}</p>
                        <p class="text-sm font-medium">{{$value}}</p>
                    </div>
                @endforeach
            </div>
            <h2 id="accordion-flush-heading-2">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-2">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Doanh nghiệp vừa & nhỏ</span>
                        <span class="font-medium text-sm">Đang mở rộng quy mô</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-2" class="hidden" aria-labelledby="accordion-flush-heading-2">

                @foreach($mobilePricingData[V2Pricing::MEDIUM_BUSINESS] as $key => $column)
                    @if($key!='number_of_user')
                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                            <p class="text-sm font-medium">{{$heading[$key]}}</p>
                            <div class="flex gap-1 items-center justify-center text-sm font-medium">
                                <p class="text-sm font-medium" id="m-{{$column['id']}}">{{number_format($column['data'][1])}}</p> <span> {{$column['suffix']}}</span>
                            </div>
                        </div>
                    @else
                        <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                            <p class="text-sm font-medium">Số người dùng </br>theo gói (người)</p>
                            <div class="text-sm font-medium">
                                <div class="flex gap-2 justify-center text-center">
                                    <button class="btn-pay !px-3" id="m-btn-pay1">{{$column['data'][0]}}</button>
                                    <button class="btn-pay !px-3 active" id="m-btn-pay2">{{$column['data'][1]}}</button>
                                    <button class="btn-pay !px-3" id="m-btn-pay3">{{$column['data'][2]}}</button>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <h2 id="accordion-flush-heading-3">
                <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                        data-accordion-target="#accordion-flush-body-3">
                    <div class="flex flex-col text-left">
                        <span class="font-bold text-sm">Doanh nghiệp tầm trung & lớn</span>
                        <span class="font-medium text-sm">Có quy trình hoạt động chuyên nghiệp bài bản</span>
                    </div>
                    <img src="{{version2_url('images/chevron_up_outline.png')}}"
                         data-accordion-icon>
                </button>
            </h2>
            <div id="accordion-flush-body-3" class="hidden"
                 aria-labelledby="accordion-flush-heading-3">
                <div class="p-2">
                    <div class="box-pri p-4 flex xl:gap-6 md:gap-6 gap-2 flex-col border !w-full">
                        <div class="flex gap-1 flex-col">
                            <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
                            <p class="text-[15px] text-[#1C60FA] font-semibold">Mạnh mẽ - Linh
                                hoạt</p>
                        </div>
                        <div class="pt-10">
                            <a href="{{route('sites.trial')}}" target="_blank">
                                <button class="button-pri">
                                    Nhận tư vấn
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                @foreach($mobilePricingData[V2Pricing::BIG_BUSINESS] as $key => $value)
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">{{$heading[$key]}}</p>
                        <p class="text-sm font-medium">
                            <a href="{{asset('sites.trial')}}">{{$value}}</a>
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>