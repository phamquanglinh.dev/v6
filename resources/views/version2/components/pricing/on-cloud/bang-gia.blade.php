<div class="xl:block md:block hidden">
    <div class="flex flex-col gap-4">
        <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
        <table>
            <tr>
                <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl px-5 py-6 text-center">
                    <p class="text-[#3C4048] text-base font-medium">Chi phí doanh nghiệp bỏ ra</p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                    <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp nhỏ</p>
                    <p class="text-[#296EFA] text-base font-medium">Có đội ngũ nhân sự đa nhiệm linh
                        hoạt</p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                    <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp vừa & nhỏ</p>
                    <p class="text-[#296EFA] text-base font-medium">Đang mở rộng quy mô</p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl px-5 py-6 text-center">
                    <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp tầm trung & lớn </p>
                    <p class="text-[#296EFA] text-base font-medium">Có quy trình hoạt động chuyên nghiệp
                        bài bản</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Phí phần mềm (đồng/năm)</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">5,760,000</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">18,816,000</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Phí setup (đồng)</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">500,000</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">1,000,000</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Số buổi setup (online)</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">1</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex gap-4 justify-center text-center">
                        <p class="font-medium text-base text-[#3C4048]">
                            1
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            2
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            2
                        </p>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Số người dùng theo gói (người)</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">3</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex gap-4 justify-center text-center">
                        <p class="font-medium text-base text-[#3C4048]">
                            10
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            30
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            50
                        </p>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Dung lượng data cho phép</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">5,000</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex gap-2 justify-center text-center">
                        <p class="font-medium text-base text-[#3C4048]">
                            10,000
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            30,000
                        </p>
                        <p class="text-[#D6D9E1]">|</p>
                        <p class="font-medium text-base text-[#3C4048]">
                            30,000
                        </p>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
            <tr class="border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">Thanh toán</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">12 tháng</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <p class="font-medium text-base text-[#3C4048]">12 tháng</p>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB] rounded-br-3xl">
                    <p class="font-medium text-base text-[#3C4048]">Nhận tư vấn</p>
                </td>
            </tr>
        </table>
    </div>
    <div class="flex flex-col gap-4">
        <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
        <table>
            <tr>
                <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl px-5 py-6 text-center">
                    <p class="text-[#3C4048] text-base font-medium">Tính năng doanh nghiệp sở hữu</p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                    <p class="text-[#296EFA] text-base font-medium">Giải pháp tích hợp các tính
                        năng CRM cho mọi hoạt động của doanh nghiệp.
                        <b>(giới hạn user/data)</b>
                    </p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                    <p class="text-[#296EFA] text-base font-medium">Dễ dàng lựa chọn tính năng mở rộng
                        dựa theo nhu cầu với chi phí tối ưu</p>
                </td>
                <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl px-5 py-6 text-center">
                    <p class="text-[#296EFA] text-base font-medium">Tuỳ biến hệ thống CRM cho doanh
                        nghiệp lớn theo nhu cầu sử dụng</p>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right" data-tooltip-placement="right">Báo cáo
                        KPI</p>
                    <div id="tooltip-right" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Thiết lập/theo dõi/đo lường KPI toàn diện - chính xác
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right1" data-tooltip-placement="right">Quản lý hành
                        trình khách hàng</p>
                    <div id="tooltip-right1" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Phân loại theo mối quan hệ khách hàng giúp tăng tỷ lệ khai thác thành
                        công
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right2" data-tooltip-placement="right">Automation
                        MKT</p>
                    <div id="tooltip-right2" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Nuôi dưỡng khách hàng tiềm năng tự động nhờ bộ công cụ mạnh mẽ
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right3" data-tooltip-placement="right">Chat &
                        call</p>
                    <div id="tooltip-right3" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Nhắn tin - gọi điện - call video liên phòng ban
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right4" data-tooltip-placement="right">Kho + Bán
                        lẻ</p>
                    <div id="tooltip-right4" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Quản lý đồng bộ hoạt động kho hàng & liên kết với máy POS
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right5" data-tooltip-placement="right">Social CRM
                        (Zalo, FB,..)</p>
                    <div id="tooltip-right5" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Quản lý chat/comment trên Fanpge/ Zalo OA/ZNS
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right6" data-tooltip-placement="right">Điểm
                        thưởng</p>
                    <div id="tooltip-right6" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Thiết lập hệ thống tích lũy & quy đổi điểm thưởng cho khách hàng
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right7" data-tooltip-placement="right">Tổng đài</p>
                    <div id="tooltip-right7" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Ghi âm hội thoại tư vấn, nghe gọi trực tiếp trên CRM, phân máy nhánh,...
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right8" data-tooltip-placement="right">HRM</p>
                    <div id="tooltip-right8" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Quản lý chấm công, hồ sơ, hợp đồng & chính sách của từng nhân
                        sự/phòng ban
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/confirmation.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right9" data-tooltip-placement="right">Sàn TMĐT
                        (Shopee)</p>
                    <div id="tooltip-right9" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Đồng bộ thông tin khách hàng/ dữ liệu đơn hàng & không giới hạn số
                        lượng shop
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right10" data-tooltip-placement="right">Bảo hành</p>
                    <div id="tooltip-right10" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Tạo/sửa/xóa phiếu bảo hành đơn giản - nhanh chóng
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right11" data-tooltip-placement="right">Đi tuyến</p>
                    <div id="tooltip-right11" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Xây dựng tuyến đi đa dạng, quản lý và đánh giá điểm Check in – Check
                        out
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right13" data-tooltip-placement="right">Bảo mật dữ
                        liệu riêng tư</p>
                    <div id="tooltip-right12" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Bảo mật tuyệt đối thông tin khách hàng theo người phụ trách
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right13" data-tooltip-placement="right">Bảo mật phân
                        quyền khách hàng theo mối quan hệ</p>
                    <div id="tooltip-right13" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Phân quyền xem user với mối quan hệ khách hàng được nhất định
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-b border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right14" data-tooltip-placement="right">Bảo mật
                        trường dữ liệu nhạy cảm được chỉ định</p>
                    <div id="tooltip-right14" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Chỉ người phụ trách khách hàng và người phân quyền xem được dữ liệu
                        nhạy cảm được chỉ định
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
            <tr class="border-t border-[#EBEBF5]">
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048] w-fit"
                       data-tooltip-target="tooltip-right15" data-tooltip-placement="right">Thống kê
                        logs ghi nhận thao tác của người dùng</p>
                    <div id="tooltip-right15" role="tooltip"
                         class="w-72 absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
                        Phân quyền hiển thị với lịch sử tương tác khách hàng
                        <div class="tooltip-arrow" data-popper-arrow></div>
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
                <td class="px-5 py-6 text-center bg-[#F9F9FB] rounded-br-3xl">
                    <div class="flex justify-center">
                        <img src="{{version2_url('images/refuse.png')}}">
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="xl:hidden md:hidden block">
    <div class="flex flex-col gap-4">
        <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
        <div class="bg-[#F2F5FD] p-2">
            <div id="accordion-flush" data-accordion="collapse"
                 data-active-classes="bg-white text-gray-900">
                <h2 id="accordion-flush-heading-1">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-1">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Doanh nghiệp nhỏ</span>
                            <span class="font-medium text-sm">Có đội ngũ nhân sự đa nhiệm linh hoạt</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-1" class="hidden"
                     aria-labelledby="accordion-flush-heading-1">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí phần mềm (đồng/năm)</p>
                        <p class="text-sm font-medium">5,760,000</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí setup (đồng)</p>
                        <p class="text-sm font-medium">500,000</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số buổi setup (online)</p>
                        <p class="text-sm font-medium">1</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng theo gói (người)</p>
                        <p class="text-sm font-medium">3</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng theo gói (người)</p>
                        <p class="text-sm font-medium">5,000</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thanh toán</p>
                        <p class="text-sm font-medium">12 tháng</p>
                    </div>
                </div>
                <h2 id="accordion-flush-heading-2">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-2">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Doanh nghiệp vừa & nhỏ</span>
                            <span class="font-medium text-sm">Đang mở rộng quy mô</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-2" class="hidden"
                     aria-labelledby="accordion-flush-heading-2">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí phần mềm (đồng/năm)</p>
                        <p class="text-sm font-medium">18,816,000</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí setup (đồng)</p>
                        <p class="text-sm font-medium">18,816,000</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số buổi setup</br> (online)</p>
                        <p class="text-sm font-medium">
                        <div class="flex gap-2 justify-center text-center">
                            <p class="font-medium text-base text-[#3C4048]">
                                1
                            </p>
                            <p class="text-[#D6D9E1]">|</p>
                            <p class="font-medium text-base text-[#3C4048]">
                                2
                            </p>
                            <p class="text-[#D6D9E1]">|</p>
                            <p class="font-medium text-base text-[#3C4048]">
                                2
                            </p>
                        </div>
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng </br>theo gói (người)</p>
                        <p class="text-sm font-medium">
                        <div class="flex gap-2 justify-center text-center">
                            <p class="font-medium text-base text-[#3C4048]">
                                10
                            </p>
                            <p class="text-[#D6D9E1]">|</p>
                            <p class="font-medium text-base text-[#3C4048]">
                                30
                            </p>
                            <p class="text-[#D6D9E1]">|</p>
                            <p class="font-medium text-base text-[#3C4048]">
                                50
                            </p>
                        </div>
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng </br>theo gói (người)</p>
                        <p class="text-sm font-medium">
                        <div class="flex gap-2 justify-center text-center flex-col">
                            <p class="font-medium text-base text-[#3C4048] border-b">
                                10,000
                            </p>
                            <p class="font-medium text-base text-[#3C4048] border-b">
                                30,000
                            </p>
                            <p class="font-medium text-base text-[#3C4048]">
                                30,000
                            </p>
                        </div>
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thanh toán</p>
                        <p class="text-sm font-medium">12 tháng</p>
                    </div>
                </div>
                <h2 id="accordion-flush-heading-3">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-3">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Doanh nghiệp tầm trung & lớn</span>
                            <span class="font-medium text-sm">Có quy trình hoạt động chuyên nghiệp bài bản</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-3" class="hidden"
                     aria-labelledby="accordion-flush-heading-3">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí phần </br>mềm (đồng/năm)</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Phí setup (đồng)</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số buổi setup </br>(online)</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng </br>theo gói (người)</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Số người dùng </br>theo gói (người)</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thanh toán</p>
                        <p class="text-sm font-medium">Nhận tư vấn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flex flex-col gap-4 pt-4">
        <p class="text-2xl font-semibold text-[#113DD8]">Tính năng</p>
        <div class="bg-[#F2F5FD] p-2">
            <div id="accordion-flush" data-accordion="collapse"
                 data-active-classes="bg-white text-gray-900">
                <h2 id="accordion-flush-heading-4">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-4">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Giải pháp tích hợp các tính năng CRM cho mọi hoạt động của doanh nghiệp. (giới hạn user/data)</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-4" class="hidden"
                     aria-labelledby="accordion-flush-heading-4">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Báo cáo KPI</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Automation MKT</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Chat & call</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Kho + Bán lẻ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Điểm thưởng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Tổng đài</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">HRM</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo hành</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Đi tuyến</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối quan
                            hệ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                            định</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của người
                            dùng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}">
                        </p>
                    </div>
                </div>
                <h2 id="accordion-flush-heading-5">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-5">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-5" class="hidden"
                     aria-labelledby="accordion-flush-heading-5">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Báo cáo KPI</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Automation MKT</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Chat & call</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Kho + Bán lẻ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Điểm thưởng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Tổng đài</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">HRM</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo hành</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Đi tuyến</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối quan
                            hệ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                            định</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của người
                            dùng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                </div>
                <h2 id="accordion-flush-heading-6">
                    <button type="button" class="flex justify-between w-full p-2 border-b gap-3"
                            data-accordion-target="#accordion-flush-body-6">
                        <div class="flex flex-col text-left">
                            <span class="font-bold text-sm">Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng</span>
                        </div>
                        <img src="{{version2_url('images/refuse.png')}}" data-accordion-icon>
                    </button>
                </h2>
                <div id="accordion-flush-body-6" class="hidden"
                     aria-labelledby="accordion-flush-heading-6">
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Báo cáo KPI</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Quản lý hành trình khách hàng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Automation MKT</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/check.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Chat & call</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Kho + Bán lẻ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Social CRM (Zalo, FB,..)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Điểm thưởng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Tổng đài</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">HRM</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Sàn TMĐT (Shopee)</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo hành</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Đi tuyến</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật dữ liệu riêng tư</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật phân quyền khách </br>hàng theo mối quan
                            hệ</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Bảo mật trường dữ liệu </br>nhạy cảm được chỉ
                            định</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                    <div class="py-3 px-1 border-b border-gray-200 flex flex-row justify-between">
                        <p class="text-sm font-medium">Thống kê logs ghi nhận </br>thao tác của người
                            dùng</p>
                        <p class="text-sm font-medium">
                            <img src="{{version2_url('images/refuse.png')}}" class="w-5 h-5">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>