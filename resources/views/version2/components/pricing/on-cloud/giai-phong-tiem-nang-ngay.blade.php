<div class="w-full flex items-center flex-col bg-price2 pt-16 pb-16">
    <div class="xl:w-3/5 w-4/5 flex gap-2 ">
        <div class="box-pri2 xl:p-[60px] md:p-[60px] p-[20px] flex gap-8 items-center relative xl:flex-row md:flex-row flex-col !top-0 justify-content-between">
            <div class="flex flex-col gap-4 xl:w-3/5 md:w-3/5 w-full" data-aos="fade-right">
                <p class="text-[#313235] font-semibold text-[28px]">Giải phóng tiềm năng doanh nghiệp ngay
                    với Getfly CRM</p>
                <p class="text-[#3C4048] text-[18px]">Quản lý khép kín toàn bộ quy trình Trước - Trong - Sau
                    bán hàng ngay hôm nay với chúng tôi!</p>
            </div>
            <a href="{{route('sites.trial')}}" class="button-pri !w-32"
               style=" width: 200px!important;text-align: center;">
                Nhận tư vấn
            </a>
        </div>
    </div>
</div>