<div class="flex gap-4 justify-between items-center border-b border-[#EBEBF5] pb-5 xl:flex-row sticky top-0 h-[469px]"
     id="packPrice1">
    <p class="font-bold xl:text-xl text-xl text-[#313235]">So sánh giữa các loại gói</p>
    <div class="flex xl:gap-6 md:gap-2 xl:flex-row md:flex-row flex-col">
        <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]"
             data-aos="fade-right">
            <div class="flex gap-1 flex-col">
                <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
                <p class="text-[15px] text-[#1C60FA] font-semibold ">Tất cả trong 1</p>

            </div>
            <div class="flex gap-4 flex-col pack-desc">
                <div class="flex flex-col gap-1">
                    <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                    <p class="text-xl text-[#1F2A37] font-semibold">480.000đ <span class="text-base">/ Tháng</span>
                    </p>
                </div>
                <div class="flex gap-3 flex-col ">
                    <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/users.png')}}"> 03 người dùng</p>
                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/data.png')}}"> 5,000 data</p>
                </div>
                <p class="text-[#3C4048] text-sm">Giải pháp tích hợp các <b>tính năng CRM cho mọi
                        hoạt động</b> của doanh nghiệp.</p>
            </div>
            <div>
                <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng thử miễn phí</p>
                <button class="button-pri">
                    Nhận tư vấn
                </button>
            </div>
        </div>
        <div class="box-pri-special p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]"
             data-aos="fade-up">
            <div class="button-special">
                <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
            </div>
            <div class="flex gap-1 flex-col">
                <p class="text-[22px] text-white font-semibold ">Professional</p>
                <p class="text-[15px] text-[#FFA41D] font-semibold">Tiết kiệm chi phí</p>


            </div>
            <div class="flex gap-4 flex-col pack-desc">
                <div class="flex flex-col gap-1">
                    <p class="text-sm text-[#EDEFF2] font-semibold">Chỉ từ</p>
                    <p class="text-xl text-[#F4F4F6] font-semibold">600.000đ <span class="text-base">/ Tháng</span>
                    </p>
                </div>
                <div class="flex gap-3 flex-col ">
                    <p class="text-sm text-[#F4F4F6] font-semibold">Khởi điểm từ:</p>
                    <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/users.png')}}"> 10 người dùng</p>
                    <p class="text-[#E0E8FC] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/data.png')}}"> 10,000 data</p>
                </div>
                <p class="text-[#F0F2F4] text-sm">Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu
                    với chi phí tối ưu</p>
            </div>
            <div class="pt-10">
                <button class="button-pri">
                    Nhận tư vấn
                </button>
            </div>
        </div>
        <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]"
             data-aos="fade-left">
            <div class="flex gap-1 flex-col">
                <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
                <p class="text-[15px] text-[#1C60FA] font-semibold">Mạnh mẽ - Linh hoạt</p>
            </div>
            <div class="flex gap-4 flex-col pack-desc">
                <div class="flex flex-col gap-1">
                    <p class="text-sm text-[#9DA4AE] font-semibold">Đừng ngại ngần</p>
                    <p class="text-xl text-[#1F2A37] font-semibold ">Liên hệ ngay</p>
                </div>
                <div class="flex gap-3 flex-col ">
                    <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/users.png')}}"> Liên hệ ngay</p>
                    <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                                src="{{version2_url('images/data.png')}}"> Liên hệ ngay</p>
                </div>
                <p class="text-[#3C4048] text-sm">Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu
                    cầu sử dụng</p>
            </div>
            <div class="pt-10">
                <button class="button-pri">
                    Nhận tư vấn
                </button>
            </div>
        </div>
    </div>
</div>
<div class="sticky top-0 h-[469px] hidden" id="packPrice2">
    <div style="padding-top: 6rem" class="flex gap-4  justify-between items-center border-b border-[#EBEBF5] pb-5 xl:flex-row md:flex-row flex-col  bg-white py-5 h-max">
        <p class="font-bold xl:text-xl text-xl text-[#313235]">So sánh giữa các loại gói</p>
        <div class="flex gap-6 xl:flex-row md:flex-row flex-col">
            <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]">
                <div class="flex gap-1 flex-col">
                    <p class="text-[22px] text-[#113DD8] font-semibold">Startup</p>
                    <p class="text-xl text-[#1F2A37] font-semibold ">480.000đ <span class="text-base">/ Tháng</span>
                    </p>

                </div>
                <div>
                    <p class="text-[#FA901B] text-sm font-semibold px-6 py-[10px]">Dùng thử miễn phí</p>
                    <button class="button-pri">
                        Nhận tư vấn
                    </button>
                </div>
            </div>
            <div class="box-pri-special p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]">
                <div class="button-special">
                    <p class="text-white uppercase text-[10px] font-semibold">Phổ biến nhất</p>
                </div>
                <div class="flex gap-1 flex-col">
                    <p class="text-[22px] text-white font-semibold ">Professional</p>
                    <p class="text-xl text-[#F4F4F6] font-semibold">600.000đ <span class="text-base">/ Tháng</span>
                    </p>
                </div>
                <div class="pt-10">
                    <button class="button-pri">
                        Nhận tư vấn
                    </button>
                </div>
            </div>
            <div class="box-pri p-4 flex gap-6 flex-col border xl:w-[256px] md:w-[256px] w-[230px]">
                <div class="flex gap-1 flex-col">
                    <p class="text-[22px] text-[#113DD8] font-semibold">Enterprise</p>
                    <p class="text-[15px] text-[#1C60FA] font-semibold">Mạnh mẽ - Linh hoạt</p>
                </div>
                <div class="pt-10">
                    <button class="button-pri">
                        Nhận tư vấn
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>