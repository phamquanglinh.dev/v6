@php use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
     * @var V2PricingClientViewModel $v2PricingClientViewModel
     */

    $master = $v2PricingClientViewModel->getOnPremisePackage(0);
    $enterprise = $v2PricingClientViewModel->getOnPremisePackage(1);
@endphp

<div class="flex xl:gap-8 md:gap-8 gap-8 xl:flex-row md:flex-row flex-col justify-between">
    <div class="box-pri !max-w-[350px] p-4 flex gap-6 flex-col border border-[#ECBC74]"
         data-aos="fade-up">
        <div class="flex gap-1 flex-col">
            <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                <img src="{{version2_url('images/icon-rocket.png')}}"> {{$master->getName()}}
            </p>
            <p class="text-[15px] text-[#1C60FA] font-semibold ">{{$master->getDescription()}}</p>
        </div>
        <div class="flex gap-4 flex-col pack-desc">
            <div class="flex flex-col gap-1">
                <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                <p class="text-xl text-[#1F2A37] font-semibold">{{$master->getAmountPerMonth()}}</p>
            </div>
            <div class="flex gap-3 flex-col ">
                <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                            src="{{version2_url('images/users.png')}}"> {{$master->getUser()}}</p>
                <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                            src="{{version2_url('images/data.png')}}"> {{$master->getData()}}</p>
            </div>
            <p class="text-[#3C4048] text-sm">{!!  $master->getBenefitText() !!}</p>
        </div>
        <div class="pt-16">
            <button class="button-pri">
                <a href="{{route('sites.trial')}}" target="_blank">Nhận
                    tư vấn</a>
            </button>
        </div>
    </div>
    <div class="box-pri !max-w-[350px] p-4 flex gap-6 flex-col border border-[#ECBC74]"
         data-aos="fade-up">
        <div class="flex gap-1 flex-col">
            <p class="text-[22px] text-[#113DD8] font-semibold flex gap-2 items-center">
                <img src="{{version2_url('images/icon-rocket.png')}}"> {{$enterprise->getName()}}
            </p>
            <p class="text-[15px] text-[#1C60FA] font-semibold">{{$enterprise->getDescription()}} </p>
        </div>
        <div class="flex gap-4 flex-col pack-desc">
            <div class="flex flex-col gap-1">
                <p class="text-sm text-[#9DA4AE] font-semibold">Chỉ từ</p>
                <p class="text-xl text-[#1F2A37] font-semibold ">{{$enterprise->getAmountPerMonth()}}</p>
            </div>
            <div class="flex gap-3 flex-col ">
                <p class="text-sm text-[#424652] font-semibold">Khởi điểm từ:</p>
                <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                            src="{{version2_url('images/users.png')}}"> {{$enterprise->getUser()}}</p>
                <p class="text-[#064FF4] font-medium flex items-center gap-2"><img
                            src="{{version2_url('images/data.png')}}"> {{$enterprise->getData()}}</p>
            </div>
            <p class="text-[#3C4048] text-sm">{!! $enterprise->getBenefitText() !!}</p>
        </div>
        <div class="pt-10">
            <button class="button-pri">
                <a href="{{route('sites.trial')}}" target="_blank">Nhận
                    tư vấn</a>
            </button>
        </div>
    </div>
</div>