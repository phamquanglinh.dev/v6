@php use App\Models\V2Pricing;use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
     * @var V2PricingClientViewModel $v2PricingClientViewModel
     */
    $dataTables  = $v2PricingClientViewModel->getTablePricingData();
@endphp
<div class="flex flex-col gap-4">
    <p class="text-2xl font-semibold text-[#113DD8]">Bảng giá</p>
    <table>
        <tr>
            <td class="bg-[#F2F5FD] w-[360px] h-[154px] rounded-tl-3xl px-5 py-6 text-center">
                <p class="text-[#3C4048] text-base font-medium text-left">Chi phí doanh nghiệp bỏ ra</p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp nhỏ</p>
                <p class="text-[#296EFA] text-base font-medium">Có đội ngũ nhân sự đa nhiệm linh hoạt</p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] px-5 py-6 text-center">
                <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp </br>vừa & nhỏ</p>
                <p class="text-[#296EFA] text-base font-medium">Đang mở rộng quy mô</p>
            </td>
            <td class="bg-[#E9EFFC] w-[270px] h-[154px] rounded-tr-3xl px-5 py-6 text-center">
                <p class="text-[#0555F8] text-lg font-semibold">Doanh nghiệp tầm trung & lớn </p>
                <p class="text-[#296EFA] text-base font-medium">Có quy trình hoạt động chuyên nghiệp bài bản</p>
            </td>
        </tr>

        @foreach($dataTables as $rowKey => $dataTable)
            <tr>
                <td class="px-5 py-6">
                    <p class="font-medium text-base text-[#3C4048]">{{$dataTable['heading']}}</p>
                </td>
                @foreach($dataTable['row'] as $size => $column)
                    @if($size != V2Pricing::MEDIUM_BUSINESS)
                        <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                            <p class="font-medium text-base text-[#3C4048]">{{$column}}</p>
                        </td>
                    @else
                        @if($rowKey != 'number_of_user')
                            <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                <div class="flex items-center justify-center font-medium text-base text-[#3C4048]">
                                    <p class="font-medium text-base text-[#3C4048]"
                                       id="{{$column['id']}}">{{number_format($column['data'][1])}}@if($column['suffix']!='')@endif</p>
                                    <span class="ml-1">{{$column['suffix']}}</span>
                                </div>
                            </td>
                        @else
                            <td class="px-5 py-6 text-center bg-[#F9F9FB]">
                                <div class="flex gap-4 justify-center text-center">
                                    <button class="btn-pay" id="btn-pay1">{{$column['data'][0]}}</button>
                                    <button class="btn-pay active" id="btn-pay2">{{$column['data'][1]}}</button>
                                    <button class="btn-pay" id="btn-pay3">{{$column['data'][2]}}</button>
                                </div>
                            </td>
                        @endif
                    @endif
                @endforeach
            </tr>
        @endforeach
    </table>
</div>