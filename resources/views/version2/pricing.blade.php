@php use App\ViewModels\V2\V2PricingClientViewModel; @endphp
@php
    /**
     * @var V2PricingClientViewModel $v2PricingClientViewModel
     */
@endphp
@extends('version2.layouts.app')
@section('title')
    Báo giá
@endsection
@section('content')
    <div class="w-full bg-price flex justify-center items-center flex-col mt-[84px]">
        <div class="sm:w-3/4 xl:w-1/2">
            <p class="xl:text-5xl md:text-4xl text-3xl font-bold text-white text-center !leading-[60px] px-2"
               data-aos="fade-down" data-aos-duration="1000">Lựa chọn giải pháp hoàn hảo ngay cho doanh nghiệp của
                bạn</p>
        </div>
    </div>
    <div id="default-tab-content">
        <div class="rounded-lg pricing-tab" id="pricing1">
            <div class="py-10 text-center" data-aos="fade-up" data-aos-duration="1000">
                <p class="text-xl text-[#4D4D4E] pb-1">
                    Tiết kiệm chi phí phần cứng | Truy cập từ bất kì đâu | Bảo mật dữ liệu | Tự động cập nhật miễn phí.
                </p>
                <p class="text-xl text-[#4D4D4E] pt-1">
                    Khởi động từ 31.360 VNĐ - bạn sẽ được nhiều hơn thế!
                </p>
            </div>
            <div class="flex justify-center" data-aos="flip-down" data-aos-duration="1000">
                <div class="xl:w-3/5 w-4/5 bg-[#FFFCF9] border border-[#ECBC74] rounded-[20px] xl:p-9 md:p-9 p-6 flex gap-8 flex-col">
                    <div class="">
                        <p class="text-[26px] font-semibold flex items-center gap-2 text-[#113DD8]"><img
                                    src="{{version2_url('images/icon-rocket.png')}}"> Mobile App</p>
                        <p class="font-semibold text-base text-[#1C60FA]">Siêu ứng dụng CRM đầu tiên trên thế giới</p>
                    </div>
                    <div class="flex gap-16 xl:flex-row md:flex-row flex-col">
                        <div class="flex gap-5 flex-col">
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon1.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    Phút - Khởi tạo dữ liệu nhanh gọn
                                </span>
                            </p>
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon2.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        0
                                    </span>
                                    giới hạn tính năng mạng xã hội nội bộ: Bảng tin - Chat - Call-in-App
                                </span>
                            </p>
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon3.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        360º
                                    </span>
                                    - Quản lý toàn bộ thông tin khách hàng
                                </span>
                            </p>
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon4.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    quy trình bán hàng đồng bộ: Sản phẩm - Kho - Báo giá - Hợp đồng - Đơn hàng - Phiếu xuất kho - Phiếu thu
                                </span>
                            </p>
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon5.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        100%
                                    </span>
                                    hiệu suất làm việc (KPI) được đo lường chính xác
                                </span>
                            </p>
                            <p class="text-base font-medium flex gap-[10px]">
                                <img src="{{version2_url('images/mobile-icon6.png')}}" class="w-6 h-6">
                                <span class="flex gap-1">
                                    <span class="text-[#FE9906]">
                                        01
                                    </span>
                                    hệ thống Mobile ERP
                                </span>
                            </p>
                        </div>
                        <div class="flex flex-col gap-1 xl:w-1/2 md:w-1/2 w-full">
                            <p class="text-[26px] font-bold text-[#1F2A37]">Trải nghiệm ngay</p>
                            <p class="font-medium text-[#064FF4]">Không khả dụng ở trên các thiết bị PC</p>
                            <div class="flex gap-6 pt-5">
                                <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                    <img src="{{version2_url('images/Appstore1.png')}}">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                    <img src="{{version2_url('images/Googleplay1.png')}}">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex justify-center pt-8">
                <div class="xl:w-3/5 w-4/5">
                    <div class="flex xl:gap-8 md:gap-8 gap-8 xl:flex-row md:flex-row flex-col justify-between">
                        @include("version2.components.pricing.on-cloud-packages",['v2PricingClientViewModel' => $v2PricingClientViewModel])
                    </div>
                </div>
            </div>
            <div class="bg-price1 w-full flex items-center flex-col xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat xl:h-[1260] md:h-[1320px] h-[1200px]">
                <div class="xl:w-1/2 md:w-1/2 w-full pt-28" data-aos="fade-down">
                    <p class="xl:text-[42px] text-4xl font-bold text-white text-center xl:px-10 md:px-6 px-10 leading-[60px]">
                        Giải pháp CRM toàn diện được 5000++ doanh nghiệp Việt Nam tin dùng</p>
                    <p class="text-white text-xl text-center pt-6">CRM mang đến trải nghiệm quản lý chăm sóc
                        khách hàng đồng bộ & hiệu quả cho doanh nghiệp. Xây dựng quy trình chuyên nghiệp & gia
                        tăng doanh số đáng mơ ước cùng chuyên gia Getfly.</p>
                </div>
                <div id="default-carousel" class="relative w-full xl:block md:block hidden" data-carousel="static">
                    <!-- Carousel wrapper -->
                    <div class="relative h-[790px] overflow-hidden rounded-lg">
                        <!-- Item 1 -->
                        <div class="flex justify-center flex-col items-center h-[700px]" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-14 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-5">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm1.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không rào cản chuyển đổi dữ
                                            liệu</p>
                                        <p class="font-medium text-base pt-3">Khởi tạo & chuyển đổi dữ liệu có sẵn chỉ
                                            bằng 01 chiếc điện thoại di động chưa tới 01 phút</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video1.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm2.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Bất kỳ ai cũng có thể sử dụng dễ
                                            dàng</p>
                                        <p class="font-medium text-base pt-3">Giao diện đơn giản - thao tác dễ dàng
                                            trên Mobile app của Getfly.</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video2.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm3.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không mất bất kỳ chi phí nào</p>
                                        <p class="font-medium text-base pt-3">Khai thác tối đa mọi tính năng CRM
                                            hoàn toàn miễn phí</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video3.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="flex gap-[30px] pt-[45px]">
                                <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                    <img src="{{version2_url('images/Appstore1.png')}}">
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                    <img src="{{version2_url('images/Googleplay1.png')}}">
                                </a>
                            </div>
                        </div>
                        <!-- Item 2 -->
                        <div class="flex justify-center flex-col items-center h-[700px]" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-14 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-5">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm4.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Quản lý khách hàng trên cùng một
                                            giao diện</p>
                                        <p class="font-medium text-base pt-3">Thấu hiểu chân dung từng khách hàng -
                                            Lưu trữ không giới hạn dữ liệu - Xây dựng tệp khách hàng trung
                                            thành</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video4.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm5.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Marketing Automation nuôi dưỡng
                                            khách hàng tiềm năng</p>
                                        <p class="font-medium text-base pt-3">Giải phóng sức lao động với không
                                            giới hạn kịch bản tự động hóa quy trình kết nối khách hàng</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video5.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm6.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Xây dựng hệ thống KPI thông minh -
                                            chính xác</p>
                                        <p class="font-medium text-base pt-3">Đo lường “sức khoẻ doanh nghiệp" thông
                                            qua bộ chỉ số KPI tiêu chuẩn cho mọi phòng ban.</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video6.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                            <div class="flex gap-[30px] pt-[45px]">
                                <button class="button-pri !px-14">
                                    <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Nhận tư
                                        vấn</a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Slider indicators -->
                    <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                                data-carousel-slide-to="0"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                                data-carousel-slide-to="1"></button>
                    </div>
                    <button type="button"
                            class="absolute -top-[40px] start-56 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{version2_url('images/left-icon.png')}}">
                    </button>
                    <button type="button"
                            class="absolute -top-[40px] end-56 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{version2_url('images/right-icon.png')}}">
                    </button>
                </div>
                <!--Mobile-->
                <div id="default-carousel" class="relative w-full xl:hidden md:hidden" data-carousel="static">
                    <div class="relative h-[530px] overflow-hidden rounded-lg">
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm1.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không rào cản chuyển đổi dữ
                                            liệu</p>
                                        <p class="font-medium text-base pt-3">Khởi tạo & chuyển đổi dữ liệu có sẵn chỉ
                                            bằng 01 chiếc điện thoại di động chưa tới 01 phút</p>
                                    </div>
                                    <video autoplay muted loop class="w-[100%] rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video1.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm2.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Bất kỳ ai cũng có thể sử dụng dễ
                                            dàng</p>
                                        <p class="font-medium text-base pt-3">Giao diện đơn giản - thao tác dễ dàng
                                            trên Mobile app của Getfly.</p>
                                    </div>
                                    <video autoplay muted loop
                                           class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video2.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm3.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Không mất bất kỳ chi phí nào</p>
                                        <p class="font-medium text-base pt-3">Khai thác tối đa mọi tính năng CRM
                                            hoàn toàn miễn phí</p>
                                    </div>
                                    <video autoplay loop muted
                                           class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video3.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] overflow-hidden relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm4.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Quản lý khách hàng trên cùng một
                                            giao diện</p>
                                        <p class="font-medium text-base pt-3">Thấu hiểu chân dung từng khách hàng -
                                            Lưu trữ không giới hạn dữ liệu - Xây dựng tệp khách hàng trung
                                            thành</p>
                                    </div>
                                    <video autoplay muted loop
                                           class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video4.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm5.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Marketing Automation nuôi dưỡng
                                            khách hàng tiềm năng</p>
                                        <p class="font-medium text-base pt-3">Giải phóng sức lao động với không
                                            giới hạn kịch bản tự động hóa quy trình kết nối khách hàng</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video5.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                        <div class="hidden duration-700 ease-in-out" data-carousel-item>
                            <div class="flex gap-[30px] xl:w-3/5 md:w-4/5 w-full pt-6 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-14">
                                <div class="xl:w-1/3 md:w-1/3 text-2xl w-full bg-white rounded-[20px] relative">
                                    <div class="p-[20px] h-[470px]">
                                        <img src="{{version2_url('images/crm6.png')}}">
                                        <p class="font-semibold text-[20px] pt-4">Xây dựng hệ thống KPI thông minh -
                                            chính xác</p>
                                        <p class="font-medium text-base pt-3">Đo lường “sức khoẻ doanh nghiệp" thông
                                            qua bộ chỉ số KPI tiêu chuẩn cho mọi phòng ban.</p>
                                    </div>
                                    <video autoplay loop class="w-auto h-auto object-none rounded-xl absolute bottom-0">
                                        <source src="{{version2_url('images/video6.mp4')}}" type="video/mp4">
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-full pb-12">
                        <div class="flex justify-center items-center gap-6">
                            <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                                <img src="{{version2_url('images/Appstore1.png')}}">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                                <img src="{{version2_url('images/Googleplay1.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                                data-carousel-slide-to="0"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                                data-carousel-slide-to="1"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3"
                                data-carousel-slide-to="2"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 4"
                                data-carousel-slide-to="3"></button>
                        <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 5"
                                data-carousel-slide-to="4"></button>
                    </div>
                    <button type="button"
                            class="absolute -top-[40px] -start-2 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{version2_url('images/left-icon.png')}}">
                    </button>
                    <button type="button"
                            class="absolute -top-[40px] -end-2 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{version2_url('images/right-icon.png')}}">
                    </button>
                </div>
            </div>
            <div class="w-full flex items-center flex-col bg-[#FAF3F1] pb-10">
                <div class="xl:w-1/2 md:w-4/5 w-5/6 xl:pt-0 md:pt-0 pt-5" data-aos="fade-down">
                    <p class="text-4xl font-bold text-center text-[#113DD8]">Tối đa tính năng - Tối thiểu chi phí</p>
                    <p class="text-[#37393E] text-xl text-center pt-6">Khám phá & lựa chọn ngay các gói giải pháp của
                        Getfly giúp doanh nghiệp chuyển đổi số chuyên nghiệp chỉ trong 01 click!</p>
                </div>
                <div class="xl:w-3/5 w-4/5 bg-white rounded-[36px] mt-[45px] pt-10 pb-12 flex gap-9 flex-col xl:px-[50px] md:px-[50px] px-[10px] relative"
                     data-aos="fade-down">

                    <div class="flex xl:flex-row md:flex-row flex-col gap-4 justify-between items-center border-b border-[#EBEBF5] pb-5 xl:sticky md:sticky top-[80px] bg-white py-4">
                        <p class="font-bold xl:text-xl text-xl text-[#313235]">So sánh giữa các loại gói</p>
                        <div class=" xl:block md:block hidden">
                            @include('version2.components.pricing.on-cloud-mini-packages',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                        </div>
                    </div>

                    <div class="xl:block md:block hidden">
                            @include('version2.components.pricing.bang-gia-pc',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                            @include('version2.components.pricing.tinh-nang-pc',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                    </div>
                    <!--Mobile-->
                    <div class="xl:hidden md:hidden block">
                        @include('version2.components.pricing.bang-gia-mobile',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                        @include('version2.components.pricing.tinh-nang-mobile',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                    </div>

                </div>
            </div>
            <div class="w-full flex items-center">
                <div class="bg-price-footer w-full flex items-center flex-col xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat xl:h-[1260] md:h-[1320px] h-[1950px]">
                    <div class="xl:w-1/2 md:w-1/2 w-full pt-28 aos-init aos-animate" data-aos="fade-down">
                        <p class="xl:text-[42px] text-4xl font-bold text-white text-center xl:px-10 md:px-6 px-10 leading-[60px]">
                            Chuyển đổi số mạnh mẽ, linh hoạt cho doanh nghiệp lớn cùng CRM On Premise</p>
                        <p class="text-white text-xl text-center pt-6">Chủ động cấu hình trên server riêng - Trọn
                            bộ 100++ tính năng của Getfly CRM - Thiết kế riêng cho từng đặc thù doanh nghiệp</p>
                    </div>
                    <div class="flex justify-center pt-8">
                        @include('version2.components.pricing.on-premise-package',['v2PricingClientViewModel' => $v2PricingClientViewModel])
                    </div>
                </div>
            </div>
            <div class="w-full flex justify-center relative" data-aos="fade-up">
                <div class="xl:w-3/5 w-4/5 flex gap-2 relative">
                    <!-- <div class="box-pri2 absolute xl:h-[300px] md:h-[300px] h-[420px] xl:!w-3/5 md:!w-3/5 !w-4/5 xl:!top-[-370px] md:!top-[-370px] !-top-[430px] xl:p-[60px] md:p-[60px] p-[20px] flex gap-8 items-center xl:flex-row md:flex-row flex-col">
                        <div class="flex flex-col gap-4 xl:w-3/5 md:w-3/5 w-full">
                            <p class="text-[#313235] font-semibold text-[28px]">Giải phóng tiềm năng doanh nghiệp ngay với Getfly CRM</p>
                            <p class="text-[#3C4048] text-[18px]">Quản lý khép kín toàn bộ quy trình Trước - Trong - Sau bán hàng ngay hôm nay với chúng tôi!</p>
                        </div>
                        <div class="flex items-center justify-between gap-2 xl:w-2/5 md:w-2/5 w-full border rounded-[20px] px-4 bg-white xl:h-16 md:h-16 h-auto z-10 xl:flex-row md:flex-row flex-row xl:py-0 md:py-0 py-2">
                            <button class="button-pri !w-32">
                                Nhận tư vấn
                            </button>
                        </div>
                        <img src="images/fly.png" class="fly">
                    </div> -->
                    <div class="box-pri2 absolute h-auto xl:!top-[-370px] md:!top-[-370px] !-top-[430px] xl:p-[60px] md:p-[60px] p-[20px] flex gap-8 items-center xl:flex-row md:flex-row flex-col">
                        <div class="flex flex-col gap-4 w-full">
                            <p class="text-[#313235] font-semibold text-[28px]">Giải đáp ngay mọi thắc mắc về lựa chọn gói với chuyên gia Getfly</p>
                            <p class="text-[#3C4048] text-[18px]">Quản lý khép kín toàn bộ quy trình Trước - Trong -
                                Sau bán hàng ngay hôm nay với chúng tôi!</p>
                            <a href="{{route('sites.trial')}}" target="_blank" class="md:text-center">
                                <button class="button-pri xl:!w-32 md:!w-full" style="width: 11rem;">
                                    Nhận tư vấn
                                </button>
                            </a>
                        </div>
                        <img src="{{version2_url('images/fly.png')}}" class="fly">
                    </div>
                </div>
            </div>
{{--            @include('version2.layouts.inc.footer')--}}
        </div>
    </div>
@endsection

@push('after_scripts')
    <script>
        function openPricingTab(tab_id) {
            var i;
            var x = document.getElementsByClassName("pricing-tab");
            const btnPricing1 = document.getElementById("btn-1")
            const btnPricing2 = document.getElementById("btn-2")
            if (tab_id == "pricing1") {
                if (!btnPricing1.classList.contains("active-pricing")) {
                    btnPricing1.classList.add("active-pricing");
                    btnPricing2.classList.remove("active-pricing");
                }

            } else {
                if (!btnPricing2.classList.contains("active-pricing")) {
                    btnPricing2.classList.add("active-pricing");
                    btnPricing1.classList.remove("active-pricing");
                }

            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(tab_id).style.display = "block";
            document.getElementById(tab_id).classList.add = "active_pricing";
        }
    </script>
    <script>
        //PC
        const btnPay1 = document.getElementById("btn-pay1")//10
        const btnPay2 = document.getElementById("btn-pay2")//30
        const btnPay3 = document.getElementById("btn-pay3")//50
        const tr0 = document.getElementById("tr0")
        const tr1 = document.getElementById("tr1")
        const tr2 = document.getElementById("tr2")
        const tr3 = document.getElementById("tr3")
        const tr4 = document.getElementById("tr4")
        const tr5 = document.getElementById("tr5")
        const tr6 = document.getElementById("tr6")


        btnPay1.addEventListener("click", () => {
            btnPay1.classList.add("active")
            btnPay2.classList.remove("active")
            btnPay3.classList.remove("active")

            animateNumber(600000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr0.textContent = formattedNumber
            })
            animateNumber(7200000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr1.textContent = formattedNumber
            })

            animateNumber(500000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr2.textContent = formattedNumber
            })

            animateNumber(1, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr3.textContent = formattedNumber
            })

            animateNumber(10000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr4.textContent = formattedNumber
            })

            animateNumber(100000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr5.textContent = formattedNumber
            })

            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr6.textContent = formattedNumber
            })
        })
        btnPay2.addEventListener("click", () => {
            btnPay2.classList.add("active")
            btnPay1.classList.remove("active")
            btnPay3.classList.remove("active")
            animateNumber(1068000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr0.textContent = formattedNumber
            })
            animateNumber(12816000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr1.textContent = formattedNumber
            })
            animateNumber(1000000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr2.textContent = formattedNumber
            })
            animateNumber(2, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr3.textContent = formattedNumber
            })
            animateNumber(30000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr4.textContent = formattedNumber
            })
            animateNumber(50000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr5.textContent = formattedNumber
            })
            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr6.textContent = formattedNumber
            })
        })
        btnPay3.addEventListener("click", () => {
            btnPay3.classList.add("active")
            btnPay1.classList.remove("active")
            btnPay2.classList.remove("active")
            animateNumber(1568000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr0.textContent = formattedNumber
            })
            animateNumber(18816000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr1.textContent = formattedNumber
            })
            animateNumber(1000000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr2.textContent = formattedNumber
            })
            animateNumber(2, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr3.textContent = formattedNumber
            })
            animateNumber(50000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr4.textContent = formattedNumber
            })
            animateNumber(35000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr5.textContent = formattedNumber
            })
            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                tr6.textContent = formattedNumber
            })
        })

        //MOBILE
        const mBtnPay1 = document.getElementById("m-btn-pay1")//10
        const mBtnPay2 = document.getElementById("m-btn-pay2")//30
        const mBtnPay3 = document.getElementById("m-btn-pay3")//50
        const mTr0 = document.getElementById("m-tr0")
        const mTr1 = document.getElementById("m-tr1")
        const mTr2 = document.getElementById("m-tr2")
        const mTr3 = document.getElementById("m-tr3")
        const mTr4 = document.getElementById("m-tr4")
        const mTr5 = document.getElementById("m-tr5")
        const mTr6 = document.getElementById("m-tr6")
        mBtnPay1.addEventListener("click", () => {
            mBtnPay1.classList.add("active")
            mBtnPay2.classList.remove("active")
            mBtnPay3.classList.remove("active")

            animateNumber(600000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr0.textContent = formattedNumber
            })
            animateNumber(7200000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr1.textContent = formattedNumber
            })

            animateNumber(500000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr2.textContent = formattedNumber
            })

            animateNumber(1, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr3.textContent = formattedNumber
            })

            animateNumber(10000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr4.textContent = formattedNumber
            })

            animateNumber(100000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr5.textContent = formattedNumber
            })

            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr6.textContent = formattedNumber
            })
        })
        mBtnPay2.addEventListener("click", () => {
            mBtnPay2.classList.add("active")
            mBtnPay1.classList.remove("active")
            mBtnPay3.classList.remove("active")
            animateNumber(1068000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr0.textContent = formattedNumber
            })
            animateNumber(12816000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr1.textContent = formattedNumber
            })
            animateNumber(1000000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr2.textContent = formattedNumber
            })
            animateNumber(2, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr3.textContent = formattedNumber
            })
            animateNumber(30000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr4.textContent = formattedNumber
            })
            animateNumber(50000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr5.textContent = formattedNumber
            })
            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr6.textContent = formattedNumber
            })
        })
        mBtnPay3.addEventListener("click", () => {
            mBtnPay3.classList.add("active")
            btnPay1.classList.remove("active")
            mBtnPay2.classList.remove("active")
            animateNumber(1568000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr0.textContent = formattedNumber
            })
            animateNumber(18816000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr1.textContent = formattedNumber
            })
            animateNumber(1000000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr2.textContent = formattedNumber
            })
            animateNumber(2, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr3.textContent = formattedNumber
            })
            animateNumber(50000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr4.textContent = formattedNumber
            })
            animateNumber(35000, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr5.textContent = formattedNumber
            })
            animateNumber(12, 500, 0, function (number) {
                const formattedNumber = number.toLocaleString()
                mTr6.textContent = formattedNumber
            })
        })


    </script>
    <script>
        function animateNumber(finalNumber, duration = 5000, startNumber = 0, callback) {
            let currentNumber = startNumber
            const interval = window.setInterval(updateNumber, 17)

            function updateNumber() {
                if (currentNumber >= finalNumber) {
                    clearInterval(interval)
                } else {
                    let inc = Math.ceil(finalNumber / (duration / 17))
                    if (currentNumber + inc > finalNumber) {
                        currentNumber = finalNumber
                        clearInterval(interval)
                    } else {
                        currentNumber += inc
                    }
                    callback(currentNumber)
                }
            }
        }


    </script>
@endpush