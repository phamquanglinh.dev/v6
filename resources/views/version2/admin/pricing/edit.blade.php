@php use App\ViewModels\Admin\V2FeatureShowViewModel;use App\ViewModels\Admin\V2OnCloudPackageShowViewModel;use App\ViewModels\Admin\V2OnPremisePackageShowViewModel;use App\ViewModels\Admin\V2PricingShowViewModel; @endphp
@php
    /**
     * @var V2PricingShowViewModel $v2PricingShowViewModel
    * @var V2FeatureShowViewModel $v2FeatureShowViewModel
 * @var V2OnCloudPackageShowViewModel $v2OnCloudPackageShowViewModel
 * @var V2OnPremisePackageShowViewModel $v2OnPremisePackageShowViewModel
     */

    $pricings = $v2PricingShowViewModel->getPricingCollection();
    $onCloudPackages = $v2OnCloudPackageShowViewModel->getMappedPackageByName();
    $onPremisePackages = $v2OnPremisePackageShowViewModel->getMappedPackageByName();
    $features = $v2FeatureShowViewModel->getFeatures();
@endphp

@extends(backpack_view('blank'))
@section('content')
    <div class="container-fluid mb-5">
        <div class="my-2 font-weight-bold">
            Bảng giá
        </div>
        <form action="{{url('admin/v2/pricing/update/pricing')}}" method="POST">
            @csrf
            @foreach($pricings as $key => $pricing)
                <input name="business_size[{{$key}}]" hidden value="{{$pricing->getBusinessSize()}}">
            @endforeach
            <table class="table table-bordered">
                <tr>
                    <th>Chi phí doanh nghiệp bỏ ra</th>
                    @foreach($pricings as $pricing)
                        <th>
                            <div>{{ $pricing->getBusinessTitle() }}</div>
                            <div class="small font-weight-normal">{{ $pricing->getBusinessDescription() }}</div>
                        </th>
                    @endforeach
                </tr>
                <tr>
                    <th>Số người dùng theo gói (người)</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="number_of_user[{{$key}}]"
                                   class="form-control @error('number_of_user.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getNumberOfUser()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Phí phần mềm (đồng/năm)</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="production_fee[{{$key}}]"
                                   class="form-control @error('production_fee.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getProductionFee()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Phí setup (đồng)</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="setup_fee[{{$key}}]"
                                   class="form-control @error('setup_fee.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getSetupFee()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Số buổi setup (online)</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="setup_number[{{$key}}]"
                                   class="form-control @error('setup_number.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getSetupNumber()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Nâng cấp gói user (đồng/user/tháng)(online)</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="upgrade_fee[{{$key}}]"
                                   class="form-control @error('upgrade_fee.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getUpgradeFee()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Dung lượng data cho phép</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="data_limit[{{$key}}]"
                                   class="form-control @error('data_limit.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getDataLimit()}}"/>
                        </td>
                    @endforeach
                </tr>
                <tr>
                    <th>Thanh toán</th>
                    @foreach($pricings as $key => $pricing)
                        <td>
                            <input name="payment_cycle[{{$key}}]"
                                   class="form-control @error('payment_cycle.'.$key) border-danger text-danger @endif"
                                   value="{{$pricing->getPaymentCycle()}}"/>
                        </td>
                    @endforeach
                </tr>
            </table>
            <button class="btn btn-success">Cập nhật</button>
        </form>
    </div>
    <div class="container-fluid mb-5">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">Tính năng</span>
        </div>
        <form action="{{url('admin/v2/pricing/update/feature')}}" method="POST">
            @csrf
            <table class="table table-bordered">
                <tr>
                    <th style="min-width: 25rem">Tính năng doanh nghiệp sở hữu</th>
                    <th style="min-width: 20rem">Tooltip</th>
                    <th>Giải pháp tích hợp các tính năng CRM cho mọi hoạt động của doanh nghiệp. (giới hạn
                        user/data)
                    </th>
                    <th>Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu</th>
                    <th>Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng</th>
                </tr>
                @foreach($features as $feature)
                    <tr>
                        <td>
                            <input class="form-control" name="feature[{{$feature->getId()}}][feature_name]"
                                   value="{{$feature->getFeatureName()}}">
                        </td>
                        <td>
                            <input class="form-control" name="feature[{{$feature->getId()}}][feature_tooltip]"
                                   value="{{$feature->getFeatureTooltip()}}">
                        </td>
                        <td>
                            <div class="text-center">
                                <input class="form-check-input h3" type="checkbox"
                                       name="feature[{{$feature->getId()}}][startup_available]"
                                       @if($feature->getStartupAvailable()) checked @endif
                                >
                            </div>
                        </td>
                        <td>
                            <div class="text-center">
                                <input class="form-check-input h3" type="checkbox"
                                       name="feature[{{$feature->getId()}}][professional_available]"
                                       @if($feature->getProfessionalAvailable()) checked @endif
                                >
                            </div>
                        </td>
                        <td>
                            <div class="text-center">
                                <input class="form-check-input h3" type="checkbox"
                                       name="feature[{{$feature->getId()}}][enterprise_available]"
                                       @if($feature->getEnterpriseAvailable()) checked @endif
                                >
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
            <button class="btn btn-success">Cập nhật</button>
        </form>
    </div>
    <div class="container-fluid mb-5">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">So sánh giữa các loại gói</span>
        </div>
        <form action="{{url('admin/v2/pricing/update/on-cloud-package')}}" method="POST">
            @csrf
            <div class="row">
                @foreach($onCloudPackages as $key => $package)
                    <div class="col-md-4 p-1">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title font-weight-bold">
                                    <span>{{$package->getName()}}</span>
                                    <input class="form-control" name="name[{{$key}}]" value="{{$package->getName()}}"
                                           hidden="">
                                </h5>
                                <p class="card-text">
                                    <input class="form-control" value="{{$package->getDescription()}}"
                                           name="description[{{$key}}]">
                                </p>
                                <div class="h4">
                                    <input value="{{$package->getAmountPerMonth()}}" name="amount_per_month[{{$key}}]"
                                           class="form-control">
                                </div>
                                <div class="align-items-center">
                                    <p>Khởi điểm từ : </p>
                                    <p>
                                        <input value="{{$package->getUser()}}" class="form-control"
                                               name="user[{{$key}}]">
                                    </p>
                                    <p>
                                        <input value="{{$package->getData()}}" class="form-control"
                                               name="data[{{$key}}]">
                                    </p>
                                </div>
                                <textarea class="form-control"
                                          name="benefit_text[{{$key}}]">{!! $package->getBenefitText() !!}</textarea>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-12 mt-2">
                    <button class="btn btn-success" type="submit">
                        Cập nhật
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid mb-5">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">So sánh giữa các loại gói[ON PREMISE]</span>
        </div>
        <form action="{{url('admin/v2/pricing/update/on-premise-package')}}" method="POST">
            @csrf
            <div class="row">
                @foreach($onPremisePackages as $key => $package)
                    <div class="col-md-4 p-1">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title font-weight-bold">
                                    <span>{{$package->getName()}}</span>
                                    <input class="form-control" name="name[{{$key}}]" value="{{$package->getName()}}"
                                           hidden="">
                                </h5>
                                <p class="card-text">
                                    <input class="form-control" value="{{$package->getDescription()}}"
                                           name="description[{{$key}}]">
                                </p>
                                <div class="h4">
                                    <input value="{{$package->getAmountPerMonth()}}" name="amount_per_month[{{$key}}]"
                                           class="form-control">
                                </div>
                                <div class="align-items-center">
                                    <p>Khởi điểm từ : </p>
                                    <p>
                                        <input value="{{$package->getUser()}}" class="form-control"
                                               name="user[{{$key}}]">
                                    </p>
                                    <p>
                                        <input value="{{$package->getData()}}" class="form-control"
                                               name="data[{{$key}}]">
                                    </p>
                                </div>
                                <textarea class="form-control"
                                          name="benefit_text[{{$key}}]">{!! $package->getBenefitText() !!}</textarea>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-12">
                    <button class="btn btn-success" type="submit">
                        Cập nhật
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection