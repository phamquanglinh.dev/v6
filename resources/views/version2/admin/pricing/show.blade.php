@php
    use App\ViewModels\Admin\V2FeatureShowViewModel;
    use App\ViewModels\Admin\V2OnCloudPackageShowViewModel;
    use App\ViewModels\Admin\V2OnPremisePackageShowViewModel;
    use App\ViewModels\Admin\V2PricingShowViewModel; @endphp
@php
    /**
     * @var V2PricingShowViewModel $v2PricingShowViewModel
     * @var V2FeatureShowViewModel $v2FeatureShowViewModel
    * @var V2OnCloudPackageShowViewModel $v2OnCloudPackageShowViewModel
    * @var V2OnPremisePackageShowViewModel $v2OnPremisePackageShowViewModel
     */

    $pricings = $v2PricingShowViewModel->getPricingCollection();
    $features = $v2FeatureShowViewModel->getFeatures();
    $onCloudPackage = $v2OnCloudPackageShowViewModel->getMappedPackageByName();
    $onPremisePackage = $v2OnPremisePackageShowViewModel->getMappedPackageByName();
@endphp

@extends(backpack_view('blank'))
@section('content')
    <div class="container-fluid">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">Bảng giá</span>
            <a href="{{url('admin/v2/pricing/edit')}}" class="p-0"><i class="la la-pen"></i> Sửa</a>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>Chi phí doanh nghiệp bỏ ra</th>
                @foreach($pricings as $pricing)
                    <th>
                        <div>{{ $pricing->getBusinessTitle() }}</div>
                        <div class="small font-weight-normal">{{ $pricing->getBusinessDescription() }}</div>
                    </th>
                @endforeach
            </tr>
            <tr>
                <th>Số người dùng theo gói (người)</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getNumberOfUser()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Phí phần mềm (đồng/năm)</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getProductionFee()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Phí setup (đồng)</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getSetupFee()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Số buổi setup (online)</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getSetupNumber()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Dung lượng data cho phép</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getDataLimit()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Thanh toán</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getPaymentCycle()}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Nâng cấp gói user (đồng/user/tháng)</th>
                @foreach($pricings as $pricing)
                    <td>{{$pricing->getUpgradeFee()}}</td>
                @endforeach
            </tr>
        </table>
    </div>

    <div class="container-fluid">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">Tính năng</span>
            <a href="{{url('admin/v2/pricing/edit')}}" class="p-0"><i class="la la-pen"></i> Sửa</a>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>Tính năng doanh nghiệp sở hữu</th>
                <th>Tooltip</th>
                <th>Giải pháp tích hợp các tính năng CRM cho mọi hoạt động của doanh nghiệp. (giới hạn
                    user/data)
                </th>
                <th>Dễ dàng lựa chọn tính năng mở rộng dựa theo nhu cầu với chi phí tối ưu</th>
                <th>Tuỳ biến hệ thống CRM cho doanh nghiệp lớn theo nhu cầu sử dụng</th>
            </tr>
            @foreach($features as $feature)
                <tr>
                    <td>
                        <div>
                            {{$feature->getFeatureName()}}
                        </div>
                    </td>
                    <td>
                        <div>{{$feature->getFeatureTooltip()}}</div>
                    </td>
                    <td>
                        <div class="text-center">
                            @if($feature->getStartupAvailable())
                                <img src="{{version2_url('images/confirmation.png')}}" style="width: 1rem" alt="">
                            @else
                                <img src="{{version2_url('images/refuse.png')}}" style="width: 1rem" alt="">
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="text-center">
                            @if($feature->getProfessionalAvailable())
                                <img src="{{version2_url('images/confirmation.png')}}" style="width: 1rem" alt="">
                            @else
                                <img src="{{version2_url('images/refuse.png')}}" style="width: 1rem" alt="">
                            @endif
                        </div>
                    </td>
                    <td>
                        <div class="text-center">
                            @if($feature->getProfessionalAvailable())
                                <img src="{{version2_url('images/confirmation.png')}}" style="width: 1rem" alt="">
                            @else
                                <img src="{{version2_url('images/refuse.png')}}" style="width: 1rem" alt="">
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <div class="container-fluid">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">So sánh giữa các loại gói</span>
            <a href="{{url('admin/v2/pricing/edit')}}" class="p-0"><i class="la la-pen"></i> Sửa</a>
        </div>
        <div class="row">
            @foreach($onCloudPackage as $package)
                <div class="col-md-4 p-1">
                    <div class="card h-100">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">{{$package->getName()}}</h5>
                            <p class="card-text">{{$package->getDescription()}}</p>
                            <div class="h4">{{$package->getAmountPerMonth()}}</div>
                            <div class="d-flex justify-content-between">
                                <p>Khởi điểm từ : </p>
                                <p>
                                    <i class="la la-user"></i>
                                    <span>{{$package->getUser()}}</span>
                                </p>
                                <p>
                                    <i class="la la-database"></i>
                                    <span>{{$package->getData()}}</span>
                                </p>
                            </div>
                            <div>{!! $package->getBenefitText() !!}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container-fluid">
        <div class="my-2 d-flex">
            <span class="font-weight-bold mr-2">So sánh giữa các loại gói</span>
            <a href="{{url('admin/v2/pricing/edit')}}" class="p-0"><i class="la la-pen"></i> Sửa</a>
        </div>
        <div class="row">
            @foreach($onPremisePackage as $package)
                <div class="col-md-4 p-1">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold">{{$package->getName()}}</h5>
                            <p class="card-text">{{$package->getDescription()}}</p>
                            <div class="h4">{{$package->getAmountPerMonth()}}</div>
                            <div class="d-flex justify-content-between">
                                <p>Khởi điểm từ : </p>
                                <p>
                                    <i class="la la-user"></i>
                                    <span>{{$package->getUser()}}</span>
                                </p>
                                <p>
                                    <i class="la la-database"></i>
                                    <span>{{$package->getData()}}</span>
                                </p>
                            </div>
                            <div>{!! $package->getBenefitText() !!}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection