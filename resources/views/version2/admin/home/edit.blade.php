@php use App\ViewModels\Admin\V2CrudHomeViewModel; @endphp
@php
    /**
     * @var V2CrudHomeViewModel $v2CrudHomeViewModel
     */
    $newspaperSelectList = $v2CrudHomeViewModel->getNewspapersSelectList();
    $topPartners = $v2CrudHomeViewModel->getTopPartners();
    $bottomPartner = $v2CrudHomeViewModel->getBottomPartners();
    $delegateNewsIds = $v2CrudHomeViewModel->getDelegateNewsIds();
@endphp

@extends(backpack_view('blank'))
@section('content')
    <div class="container-fluid">
        <div class="border p-2 rounded bg-white">
            <div class="uppercase h3 mb-3 font-weight-bold">HỆ SINH THÁI CÔNG NGHỆ KẾT NỐI MỞ VỚI HÀNG NGÀN
                ĐỐI TÁC
            </div>
            <div class="d-flex my-2">
                @foreach($topPartners as $topPartner)
                    <img style="max-width: 10rem" src="{{url($topPartner->getPartnerLogo())}}" class="w-100">
                @endforeach
            </div>
            <div class="uppercase h3 mt-5 font-weight-bold">Luôn Trân Quý Niềm Tin Của Khách Hàng
                Đặt Vào Chúng Tôi
            </div>
            <div class="row my-2">
                @foreach($bottomPartner as $topPartner)
                    <div class="col-md-3 mb-3">
                        <img style="max-width: 15rem" src="{{url($topPartner->getPartnerLogo())}}"
                             class="w-100 border rounded">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="my-3">
            <a class="btn btn-success" href="{{backpack_url('/v2-partner')}}">Sửa - Sắp xếp</a>
        </div>
        <div>
            <div class="my-3 h3 font-weight-bold">Bài viết hiển thị ở trang chủ</div>
            <form action="{{backpack_url('v2/home-page/newspaper')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <label for="delegate_type[1]">Bài viết 01</label>
                        <select class="js-example-basic-multiple form-control p-2" id="delegate_type[1]"
                                name="delegate_type[1]">
                            @foreach($newspaperSelectList as $newId => $title)
                                <option
                                        @if($newId == $delegateNewsIds[1]) selected @endif
                                value="{{$newId}}">{{$title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="delegate_type[2]">Bài viết 02</label>
                        <select class="js-example-basic-multiple form-control p-2" id="delegate_type[2]"
                                name="delegate_type[2]">
                            @foreach($newspaperSelectList as $newId => $title)
                                <option
                                        @if($newId == $delegateNewsIds[2]) selected @endif
                                value="{{$newId}}">{{$title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="delegate_type[3]">Bài viết 02</label>
                        <select class="js-example-basic-multiple form-control p-2" id="delegate_type[3]"
                                name="delegate_type[3]">
                            @foreach($newspaperSelectList as $newId => $title)
                                <option
                                        @if($newId == $delegateNewsIds[3]) selected @endif
                                value="{{$newId}}">{{$title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button class="btn btn-primary my-2">Cập nhật</button>
            </form>
        </div>
    </div>
@endsection
@push('after_scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endpush