@php use App\ViewModels\V2\V2HomeViewModel; @endphp
@php
    /**
     * @var V2HomeViewModel $v2HomeViewModel
     */

    /**
* feedbacks
* newspapers
* topPlacePartners
* bottomPlacePartners
 */

    $feedbacks = $v2HomeViewModel->getFeedbacks();
    $newspapers = $v2HomeViewModel->getNewspaper();
    $topPlacePartners = $v2HomeViewModel->getTopPlacePartners();
    $bottomPlacePartners = $v2HomeViewModel->getBottomPlacePartners();

@endphp
@extends('version2.layouts.app')
@section("content")
    {{--    Banner TT#1 --}}
    <div class="2xl:px-10 md:px-10 px-0 mt-[84px] relative">
        <div
                class="bg-animation xl:w-[95%] md:w-[95%] w-full z-0 xl:!bg-repeat-round md:!bg-repeat-round !bg-no-repeat rounded-[50px] absolute top-0 xl:h-[1016px] md:h-[1016px] h-[1000px]">
        </div>
        <div
                class="bg-fadeup xl:w-[95%] md:w-[95%] w-full z-[11] xl:!bg-repeat-round md:!bg-repeat-round !bg-no-repeat rounded-[50px] absolute top-0 xl:h-[1016px] md:h-[1016px] h-[1000px]">
        </div>
        <div
                class="bg-star z-10 xl:!bg-repeat-round md:!bg-repeat-round !bg-no-repeat rounded-[50px] xl:h-[1016px] md:h-[1016px] h-[1000px]">
        </div>
        <div
                class="bg-head flex justify-start items-center flex-col px-2 absolute top-0 xl:h-[1016px] md:h-[1016px] h-[1000px] xl:!bg-repeat-round md:!bg-repeat-round !bg-no-repeat">
            <div class="pt-[72px] xl:w-3/5 md:w-4/5 relative xl:px-5 md:px-0" data-aos="fade-down">
                <p class="  xl:text-5xl md:text-[40px] text-[30px] mac:text-[36px] text-white font-bold text-center xl:leading-[70px] md:leading-[60px] leading-[54px] capitalize">
                    <span class="text-[#FE7F2D]">Getfly CRM</span> - Giải pháp quản lý & chăm sóc khách hàng toàn
                    diện cho SMEs
                </p>

                <img src="{{v2_url('images/round.png')}}" class="roundimg">
                <p class="text-white text-xl text-center pt-6">
                    Quản lý khép kín quy trình Trước - Trong - Sau bán hàng. <br> Nền tảng CRM đầu tiên mở rộng
                    tính
                    năng đa nhiệm kiểm soát mọi hoạt động của doanh nghiệp.
                </p>

                <div class="text-center pt-10">
                    <a class="button-pri !w-auto border-white border"
                       href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Trải nghiệm 30 ngày</a>
                </div>

                <div class="flex gap-5 justify-center xl:items-center md:items-center items-start pt-3 mt-3 xl:flex-row md:flex-row flex-col">
                    <div class="flex text-white font-medium gap-[6px]">
                        <img src="{{v2_url('images/check-green.png')}}" class="w-5 h-5">
                        Quản lý khách hàng
                    </div>
                    <div class="flex text-white font-medium gap-[6px]">
                        <img src="{{v2_url('images/check-green.png')}}" class="w-5 h-5">
                        Marketing Automation
                    </div>
                    <div class="flex text-white font-medium gap-[6px]">
                        <img src="{{v2_url('images/check-green.png')}}" class="w-5 h-5">
                        Đo lường KPI
                    </div>
                    <div class="flex text-white font-medium gap-[6px]">
                        <img src="{{v2_url('images/check-green.png')}}" class="w-5 h-5">
                        Mobile App
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Danh sách chữ chạy TT#2--}}
    <div class="flex flex-col justify-center text-center pt-24 gap-8">
        <p class="text-lg uppercase text-[#555463]">Hệ sinh thái công nghệ kết nối mở với hàng ngàn đối tác
        </p>
        <div class="w-[90vw] mx-auto">
            <div class="marquee">
                <div class="marquee-content">
                    @foreach($topPlacePartners as $topPlacePartner)
                        <div class="marquee-item"><img src="{{$topPlacePartner->getPartnerLogo()}}"
                                                       class="image-container w-[180px]"></div>
                    @endforeach
                    @foreach($topPlacePartners as $topPlacePartner)
                        <div class="marquee-item"><img src="{{$topPlacePartner->getPartnerLogo()}}"
                                                       class="image-container w-[180px]"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {{--Phần video của a Hoàng TT#3--}}
    <div class="py-[72px] flex justify-center xl:px-[120px] md:px-[10px] px-2">
        <div
                class="xl:w-full md:w-4/5 w-full xl:p-16 md:p-9 p-2 flex flex-col  bg-[#FAEEEB] rounded-3xl relative xl:h-[933px] md:h-[933px] h-full xl:gap-32 md:gap-32 gap-8">
            <div class="flex flex-col">
                <div class="flex flex-col xl:w-[500px] md:w-[490px] w-full gap-6">
                    <p class="xl:text-5xl md:text-4xl text-[30px] text-[#212F51] font-bold leading-[60px] capitalize">
                        Đối
                        tác tin cậy của mọi doanh nghiệp</p>
                    <p class="text-lg text-[#3B4A6D]">11+ năm đồng hành cùng doanh nghiệp Việt Nam giữ vững niềm tin của
                        khách hàng.</p>
                    <div class="pt-0">
                        <a class="button-pri !w-auto" href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html"
                           target="_blank">Trải nghiệm 30 ngày</a>
                    </div>
                </div>
                <div class="flex">
                    <img src="{{v2_url('images/tro-ly.png')}}"
                         class="xl:absolute md:absolute relative xl:top-0 md:top-0 top-5 xl:right-0 md:right-0 right-auto rounded-3xl z-10 xl:w-auto md:w-500"
                         data-aos="zoom-in">
                    <div
                            class="absolute z-10 xl:right-[270px] md:right-[220px] right-[120px] xl:top-[220px] md:top-[200px] top-[500px]   ">
                        <div class="relative w-[82px] h-[82px]">

                            <div class="animate-ping bg-[#2761FA] absolute inset-2 rounded-full">
                            </div>
                            <button data-modal-target="default-modal" data-modal-toggle="default-modal"
                                    class="z-10 cursor-pointer absolute top-0 left-0">
                                <img src="{{v2_url('images/plays.png')}}" class="">
                            </button>
                        </div>

                    </div>
                    <div id="default-modal" tabindex="-1" aria-hidden="true"
                         class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
                        <div class="relative p-4 w-full max-w-2xl max-h-full">
                            <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                                <div class="p-4 md:p-5 space-y-4">
                                    <iframe width="100%" height="315"
                                            src="https://www.youtube.com/embed/Nt00L2cj6cQ?si=VGL-x4Y4iMx-yA7b?rel=0&autoplay=1"
                                            allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex flex-col xl:gap-10 md:gap-8 gap-8 pt-[90px]">
                <div class="flex xl:gap-20 lg:gap-[20px] md:gap-8 gap-8 xl:flex-row md:flex-row flex-col">
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon1.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Bảo mật dữ liệu khách hàng</p>
                        <p class="text-lg text-[#3B4A6D]">Nâng cao uy tín thương hiệu trong mắt khách hàng của
                            bạn (Tiêu chuẩn ISO 27001:2013)</p>
                    </div>
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon2.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Dễ sử dụng</p>
                        <p class="text-lg text-[#3B4A6D]">Giao diện thân thiện hơn - trực quan hơn - hiện đại hơn
                        </p>
                    </div>
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon3.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Đồng bộ Thiết bị</p>
                        <p class="text-lg text-[#3B4A6D]">Sử dụng CRM đồng bộ 100% App & PC</p>
                    </div>
                </div>
                <div class="flex xl:gap-20 lg:gap-[20px] md:gap-8 gap-8 xl:flex-row md:flex-row flex-col">
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon4.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Dễ dàng liên lạc</p>
                        <p class="text-lg text-[#3B4A6D]">Giải quyết mọi thắc mắc nhanh chóng</p>
                    </div>
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon5.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Tối ưu chi phí đầu tư</p>
                        <p class="text-lg text-[#3B4A6D]">On-boarding dễ dàng - Chỉ mất 1 phút để khởi tạo CRM
                        </p>
                    </div>
                    <div class="box-part xl:w-96 md:w-96 w-full flex flex-col gap-4">
                        <img src="{{v2_url('images/tro-ly-icon3.png')}}" class="w-12 h-12">
                        <p class="font-bold text-xl text-[#212F51] capitalize">Liên tục cập nhật</p>
                        <p class="text-lg text-[#3B4A6D]">Miễn phí cập nhật & tối ưu tính năng hàng tháng</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Phần 3 tab KH automation kpi TT#4--}}
    <div class="flex justify-center text-center">
        <div class="xl:w-3/5 w-4/5" data-aos="fade-down">
            <p class="xl:text-4xl md:text-4xl text-[30px] font-bold text-[#212F51] !leading-[60px] capitalize">Quản lý
                toàn bộ danh sách khách hàng giúp doanh nghiệp <span class="text-[#FE7F2D]">khai thác tối đa tiềm năng data</span>
            </p>
            <p class="text-[#3B4A6D] text-lg pt-4">Giúp bạn nắm rõ chân dung của từng khách hàng đồng thời duy
                trì được mối quan hệ lâu dài của </br>doanh nghiệp và khách hàng.</p>

        </div>
    </div>
    <div class="relative pt-10">
        <div class="flex justify-center text-center">
            <div class="relative">
                <a href="https://getfly.vn/bang-gia.html"
                   class="text-[#003BD2] text-xl font-semibold flex gap-2 capitalize">
                    Tìm hiểu thêm gói dịch vụ
                    <img src="{{v2_url('images/dichvu.png')}}" class="w-6 h-6">
                </a>
                <img src="{{v2_url('images/round2.png')}}" class="absolute top-7 left-0">
            </div>
        </div>
        <div class="border rounded-[32px] p-6 xl:w-3/5 w-4/5 xl:block md:block hidden mx-auto mt-14">
            <div class="mb-4 flex justify-center">
                <ul class="flex flex-wrap gap-6 -mb-px text-sm font-medium text-center" id="default-tab"
                    data-tabs-toggle="#default-tab-content" role="tablist">
                    <li class="me-2" role="presentation">
                        <button
                                class="flex gap-2 p-[13px] w-[172px] justify-center font-medium text-lg border rounded-xl button-tab btn1-tab-active">
                            <img src="{{v2_url('images/customer.svg')}}"> Khách hàng
                        </button>
                    </li>
                    <li class="me-2" role="presentation">
                        <button
                                class="flex gap-2 p-[13px] w-[172px] justify-center font-medium text-lg border rounded-xl button-tab  hover:text-gray-600 hover:border-gray-300">
                            <img src="{{v2_url('images/auto-deactive.svg')}}"> Automation
                        </button>
                    </li>
                    <li class="me-2" role="presentation">
                        <button
                                class="flex gap-2 p-[13px] w-[172px] justify-center font-medium text-lg border rounded-xl button-tab  hover:text-gray-600 hover:border-gray-300">
                            <img src="{{v2_url('images/kpi-deactive.svg')}}"> KPI
                        </button>
                    </li>
                </ul>
            </div>
            <div id="default-tab-content" class="bg-home1 flex justify-center items-center"
                 style="background-image:url({{v2_url('images/bg-customer.png')}});">
                <div class="p-4 xl:px-10 md:px-2 px-0 rounded-lg flex h-[510px] service-tab">
                    <div class="flex gap-6 justify-between items-center relative">
                        <div class="w-1/2">
                            <video autoplay loop muted playsinline class="lazy w-auto h-auto object-none rounded-xl"
                                   id="video-1">
                                <source data-src="{{v2_url('images/videokh1.mp4')}}" type="video/mp4">
                            </video>
                        </div>
                        <div class="w-1/2 flex flex-col gap-5">
                            <div class="flex gap-4 flex-col" id="accordion-collapse" data-accordion="collapse">
                                <div class=" rounded-2xl border active-border active collapse-service">
                                    <h2 id="accordion-collapse-heading-1">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-1" aria-expanded="true"
                                                aria-controls="accordion-collapse-body-1">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Quản lý khách hàng đơn giản - tập trung trên một giao diện</p>
                                            <img src="{{v2_url('images/close2.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-1"
                                         class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-1">
                                        <p class="text-base text-[#02060F]">Quản lý dữ liệu không giới hạn, kiểm
                                            soát
                                            cuộc gọi & hoạt động tương tác khách hàng theo thời gian thực.</p>
                                    </div>
                                </div>

                                <div class=" rounded-2xl active-border collapse-service">
                                    <h2 id="accordion-collapse-heading-8">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-2" aria-expanded="false"
                                                aria-controls="accordion-collapse-body-2">
                                            <p class="text-xl mac:text-[18px] text-[#3B4A6D] font-semibold ">
                                                Giao dữ liệu nhanh - chính xác - hiệu quả</p>
                                            <img src="{{v2_url('images/plus.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-2" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-2">
                                        <p class="text-base text-[#3B4A6D]">Quản lý data khách hàng tập trung -
                                            tối ưu hóa chi phí & thời gian</p>
                                    </div>
                                </div>
                                <div class="rounded-2xl active-border collapse-service">
                                    <h2 id="accordion-collapse-heading-3">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-3" aria-expanded="false"
                                                aria-controls="accordion-collapse-body-3">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Tăng chất lượng khai thác data nhờ việc phân loại khách hàng tự
                                                động</p>
                                            <img src="{{v2_url('images/plus.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-3" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-3">
                                        <p class="text-base text-[#3B4A6D]">Quản lý data khách hàng tự động phân
                                            loại theo nhu cầu - hành vi - nguồn.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden p-4 xl:px-10 md:px-2 px-0 rounded-lg flex h-[555px] service-tab">
                    <div class="flex gap-6 justify-between items-center relative">
                        <div class="w-1/2">
                            <video autoplay loop muted class="lazy w-auto h-auto object-none rounded-xl" id="video-2">
                                <source data-src="{{v2_url('images/videoauto1.mp4')}}" type="video/mp4">
                            </video>
                        </div>
                        <div class="w-1/2 flex flex-col gap-5">
                            <div class="flex gap-4 flex-col" id="accordion-collapse" data-accordion="collapse">
                                <div class=" rounded-2xl border active-border active collapse-service1">
                                    <h2 id="accordion-collapse-heading-4">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-4" aria-expanded="true"
                                                aria-controls="accordion-collapse-body-4">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Chăm Sóc Tự
                                                Động Từng Khách Hàng Bằng Hệ Thống Email/SMS/ZNS</p>
                                            <img src="{{v2_url('images/close2.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-4" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-4">
                                        <p class="text-base text-[#3B4A6D]">Cá nhân hóa kịch bản nuôi dưỡng khách hàng
                                            theo luồng có sẵn. Kích thích nhu cầu mở & phản hồi của khách hàng.</p>
                                    </div>
                                </div>
                                <div class=" rounded-2xl active-border collapse-service1">
                                    <h2 id="accordion-collapse-heading-5">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-5" aria-expanded="false"
                                                aria-controls="accordion-collapse-body-5">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Cá nhân hóa kịch
                                                bản nuôi dưỡng khách hàng theo luồng có sẵn. Kích thích nhu cầu mở &
                                                phản hồi của khách hàng.</p>
                                            <img src="{{v2_url('images/plus.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-5" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-5">
                                        <p class="text-base text-[#3B4A6D]">Tư vấn viên sắp xếp các công việc/lịch
                                            liên hệ khách hàng khoa học với hệ thống tự động gửi thông báo/
                                            SMS</p>
                                    </div>
                                </div>
                                <div class=" rounded-2xl active-border collapse-service1">
                                    <h2 id="accordion-collapse-heading-6">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-6" aria-expanded="false"
                                                aria-controls="accordion-collapse-body-6">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Chăm Sóc Khách
                                                Hàng Tự Động Với Luồng (Workflow)</p>
                                            <img src="{{v2_url('images/plus.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-6" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-6">
                                        <p class="text-base text-[#3B4A6D]">Lên kịch bản nhận thông điệp tự động
                                            dễ dàng bằng cách thiết lập quy trình tùy chỉnh. Ngay lập tức
                                            kích hoạt Email/SMS/ZNS... ngay khi khách hành tương tác.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden p-4 xl:px-10 md:px-2 px-0 rounded-2xl flex h-[510px] service-tab">
                    <div class="flex gap-6 justify-between items-center relative">
                        <div class="w-1/2">
                            <video autoplay loop muted class="lazy w-auto h-auto object-none rounded-xl" id="video-3">
                                <source data-src="{{v2_url('images/videokpi2.mp4')}}" type="video/mp4">
                            </video>
                        </div>
                        <div class="w-1/2 flex flex-col gap-5">
                            <div class="flex gap-4 flex-col" id="accordion-collapse" data-accordion="collapse">
                                <div class=" rounded-2xl border active-border active collapse-service2">
                                    <h2 id="accordion-collapse-heading-7">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-7" aria-expanded="true"
                                                aria-controls="accordion-collapse-body-7">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Thiết Lập KPI
                                                Theo Từng Phòng Ban - Kiểm Soát Nhận Diện Vấn Đề - Điều Chỉnh
                                                Kịp Thời Mọi Lúc Mọi Nơi</p>
                                            <img src="{{v2_url('images/close2.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-7" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-7">
                                        <p class="text-base text-[#3B4A6D]">Dễ dàng tạo chỉ tiêu KPI cho nhân viên
                                            theo các mẫu gợi ý có sẵn. Xem báo KPI tự động (khách hàng, doanh
                                            thu, doanh số,...) từ dữ liệu chính xác trên hệ thống. Phát hiện
                                            và tập trung xử lý vấn đề trong hoạt động bán hàng thông qua các
                                            “con số biết nói”.</p>
                                    </div>
                                </div>
                                <div class=" rounded-2xl active-border collapse-service2">
                                    <h2 id="accordion-collapse-heading-8">
                                        <button type="button"
                                                class="flex gap-3 bg-inherit p-5 rounded-2xl text-left items-center"
                                                data-accordion-target="#accordion-collapse-body-8" aria-expanded="false"
                                                aria-controls="accordion-collapse-body-8">
                                            <p class="text-xl mac:text-[18px] text-[#212F51] font-semibold ">
                                                Không Còn “Bỏ
                                                Quên” Khách Hàng Khi Hệ Thống Thông Minh Tự Động Nhắc Lịch Hẹn
                                            </p>
                                            <img src="{{v2_url('images/plus.png')}}" class="w-10 h-10">
                                        </button>
                                    </h2>
                                    <div id="accordion-collapse-body-8" class="hidden bg-inherit px-5 pb-5 rounded-2xl"
                                         aria-labelledby="accordion-collapse-heading-8">
                                        <p class="text-base text-[#3B4A6D]">Tư vấn viên sắp xếp các công việc/lịch
                                            liên hệ khách hàng khoa học với hệ thống tự động gửi thông báo/
                                            SMS</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile -->
        <div class="p-2 pt-[50px]">
            <div class="border rounded-[32px] p-2 w-full xl:hidden md:hidden">
                <div class="mb-4 flex justify-center">
                    <ul class="flex flex-row gap-2 -mb-px text-sm font-medium text-center" id="default-tab"
                        data-tabs-toggle="#default-tab-content" role="tablist" data-aos="fade-down">
                        <li role="presentation">
                            <button
                                    class="flex gap-2 p-2 justify-center font-medium text-sm border rounded-xl bg-[#EBF0FC]"
                                    id="profile-tab1" data-tabs-target="#profile1" type="button" role="tab"
                                    aria-controls="profile" aria-selected="false">
                                <img src="{{v2_url('images/customer.svg')}}"> Khách hàng
                            </button>
                        </li>
                        <li role="presentation">
                            <button
                                    class="flex gap-2 p-2 justify-center font-medium text-sm border rounded-xl bg-[#EFF1F5] hover:text-gray-600 hover:border-gray-300"
                                    id="dashboard-tab1" data-tabs-target="#dashboard1" type="button" role="tab"
                                    aria-controls="dashboard" aria-selected="false">
                                <img src="{{v2_url('images/auto.svg')}}"> Automation
                            </button>
                        </li>
                        <li role="presentation">
                            <button
                                    class="flex gap-2 p-2 justify-center font-medium text-sm border rounded-xl bg-[#EFF1F5] hover:text-gray-600 hover:border-gray-300"
                                    id="settings-tab1" data-tabs-target="#settings1" type="button" role="tab"
                                    aria-controls="settings" aria-selected="false">
                                <img src="{{v2_url('images/kpi.svg')}}"> KPI
                            </button>
                        </li>
                    </ul>
                </div>
                <div id="default-tab-content">
                    <div class="hidden p-4 xl:px-14 md:px-2 px-0 rounded-lg" id="profile1" role="tabpanel"
                         aria-labelledby="profile-tab">
                        <div id="default-carousel" class="relative w-full" data-carousel="slide">
                            <!-- Carousel wrapper -->
                            <div class="relative h-[550px] overflow-hidden rounded-lg">
                                <!-- Item 1 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay loop muted class="lazy w-auto h-auto object-none rounded-xl"
                                                   id="video-1">
                                                <source data-src="{{v2_url('images/videokh1.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Quản lý
                                                    khách hàng đơn giản - tập trung trên một giao diện</p>
                                                <p class="text-base text-[#3B4A6D]">Quản lý dữ liệu không giới hạn, kiểm
                                                    soát cuộc gọi & hoạt động tương tác khách hàng theo thời gian thực.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item 2 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videokh2.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Giao dữ
                                                    liệu nhanh - chính xác - hiệu quả</p>
                                                <p class="text-base text-[#3B4A6D]">Quản lý data khách hàng tập
                                                    trung - tối ưu hóa chi phí & thời gian</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videokh3.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Tăng chất
                                                    lượng khai thác data nhờ việc phân loại khách hàng tự động
                                                </p>
                                                <p class="text-base text-[#3B4A6D]">Quản lý data khách hàng tự
                                                    động phân loại theo nhu cầu - hành vi - nguồn.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Slider indicators -->
                            <div
                                    class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="true"
                                        aria-label="Slide 1" data-carousel-slide-to="0"></button>
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="false"
                                        aria-label="Slide 2" data-carousel-slide-to="1"></button>
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="false"
                                        aria-label="Slide 3" data-carousel-slide-to="2"></button>
                            </div>
                        </div>
                    </div>
                    <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="dashboard1" role="tabpanel"
                         aria-labelledby="dashboard-tab">
                        <div id="default-carousel" class="relative w-full" data-carousel="slide">
                            <!-- Carousel wrapper -->
                            <div class="relative h-[550px] overflow-hidden rounded-lg">
                                <!-- Item 1 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videoauto1.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Chăm Sóc Tự
                                                    Động Từng Khách Hàng Bằnh Hệ Thống Email/SMS/ZNS</p>
                                                <p class="text-base text-[#3B4A6D]">Cá nhân hóa kịch bản nuôi dưỡng
                                                    khách hàng theo luồng có sẵn. Kích thích nhu cầu mở & phản hồi của
                                                    khách hàng.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item 2 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="w-auto lazy h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videoauto2.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Không Còn
                                                    “Bỏ Quên” Khách Hàng Khi Hệ Thống Thông Minh Tự Động Nhắc
                                                    Lịch Hẹn</p>
                                                <p class="text-base text-[#3B4A6D]">Tư vấn viên sắp xếp các công
                                                    việc/lịch liên hệ khách hàng khoa học với hệ thống tự
                                                    động gửi thông báo/ SMS</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videoauto3.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Chăm Sóc
                                                    Khách Hàng Tự Động Với Luồng (Workflow)</p>
                                                <p class="text-base text-[#3B4A6D]">Lên kịch bản nhận thông điệp tự
                                                    động dễ dàng bằng cách thiết lập quy trình tùy chỉnh. Ngay
                                                    lập tức kích hoạt Email/SMS/ZNS... ngay khi khách hành tương
                                                    tác.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Slider indicators -->
                            <div
                                    class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="true"
                                        aria-label="Slide 1" data-carousel-slide-to="0"></button>
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="false"
                                        aria-label="Slide 2" data-carousel-slide-to="1"></button>
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="false"
                                        aria-label="Slide 3" data-carousel-slide-to="2"></button>
                            </div>
                        </div>
                    </div>
                    <div class="hidden p-4 rounded-lg bg-gray-50 dark:bg-gray-800" id="settings1" role="tabpanel"
                         aria-labelledby="settings-tab">
                        <div id="default-carousel" class="relative w-full" data-carousel="slide">
                            <!-- Carousel wrapper -->
                            <div class="relative h-[700px] overflow-hidden rounded-lg">
                                <!-- Item 1 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videokpi1.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Thiết Lập
                                                    KPI Theo Từng Phòng Ban - Kiểm Soát Nhận Diện Vấn Đề - Điều
                                                    Chỉnh Kịp Thời Mọi Lúc Mọi Nơi</p>
                                                <p class="text-base text-[#3B4A6D]">Dễ dàng tạo chỉ tiêu KPI cho
                                                    nhân viên theo các mẫu gợi ý có sẵn. Xem báo KPI tự động
                                                    (khách hàng, doanh thu, doanh số,...) từ dữ liệu chính xác
                                                    trên hệ thống. Phát hiện và tập trung xử lý vấn đề trong
                                                    hoạt động bán hàng thông qua các “con số biết nói”.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Item 2 -->
                                <div class="hidden duration-700 ease-in-out" data-carousel-item>
                                    <div class="relative rounded-lg flex flex-col">
                                        <div class="flex flex-col p-2 w-full">
                                            <video autoplay="" loop="" muted=""
                                                   class="lazy w-auto h-auto object-none rounded-xl" id="video-1">
                                                <source data-src="{{v2_url('images/videokpi2.mp4')}}" type="video/mp4">
                                            </video>
                                            <div
                                                    class="flex flex-col gap-2 border border-[#2670FF] bg-[#F9FBFF] rounded-2xl p-2">
                                                <p class="text-xl text-[#212F51] font-semibold ">Không Còn
                                                    “Bỏ Quên” Khách Hàng Khi Hệ Thống Thông Minh Tự Động Nhắc
                                                    Lịch Hẹn</p>
                                                <p class="text-base text-[#3B4A6D]">Tư vấn viên sắp xếp các công
                                                    việc/lịch liên hệ khách hàng khoa học với hệ thống tự
                                                    động gửi thông báo/ SMS</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Slider indicators -->
                            <div
                                    class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="true"
                                        aria-label="Slide 1" data-carousel-slide-to="0"></button>
                                <button type="button" class="w-3 h-3 rounded-full !bg-neutral-400" aria-current="false"
                                        aria-label="Slide 2" data-carousel-slide-to="1"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Phần đánh giá khách hàng TT#5 --}}
    <div class="flex bg-scroll-rocket relative xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat">
        <img src="{{v2_url('images/rocket.png')}}" class="absolute right-[270px] -top-[12px] rotate-[-20deg]">
        <div class="flex flex-col items-center justify-center w-full" data-aos="fade-down">
            <div class="xl:w-3/5 w-4/5 text-center flex gap-4 flex-col">
                <p class="text-white xl:text-[42px] md:text-[40px] text-[30px] xl:pt-0 md:pt-0 pt-10 mac:text-[40px] font-bold leading-[60px] capitalize">
                    Sự hài lòng của khách hàng là động lực của Getfly</p>
                <p class="text-[#B2C5F9] text-lg font-normal">Phần mềm Getfly đã giúp 5000++ doanh nghiệp SMEs
                    chuyển đổi số thành công</p>
            </div>
            <a class="text-white text-sm border border-white rounded-xl p-3 mt-10" href="https://getfly.vn/feedback"
               target="_blank">Khám phá câu chuyện khách hàng</a>
            <div id="default-carousel" class="relative w-full pt-[52px] xl:block md:block hidden"
                 data-carousel="static">
                <!-- Carousel wrapper -->
                <div class="relative h-64 overflow-hidden rounded-lg">
                    <!-- Item 1 -->
                    <div class="hidden duration-700 ease-in-out" data-carousel-item>
                        <div class="flex gap-6 justify-center xl:flex-row md:flex-row flex-col">
                            @foreach($feedbacks as $index => $feedback)
                                <div
                                        class="flex gap-4 border bg-[#0054DB] rounded-2xl border-[#7097FB] px-4 py-[22px] w-500">
                                    <div class="flex w-full">
                                        <img src="{{url('assets/uploads/feedback').'/'.$feedback->getAvatar()}}"
                                             class="slide-img">
                                    </div>
                                    <div class="flex flex-col">
                                        <p class="text-white font-medium text-[13px] line-clamp-4">{{$feedback->getTitle()}}</p>
                                        <p class="text-white font-medium text-[11x] pt-5">{{$feedback->getName()}}</p>
                                        <p class="text-[#E5E7E9] font-medium text-[11x]">{{$feedback->getJobTitle()}}</p>
                                    </div>
                                </div>
                                @if(($index+1) % 3 == 0)
                        </div>
                    </div>
                    <div class="hidden duration-700 ease-in-out" data-carousel-item>
                        <div class="flex gap-6 justify-center xl:flex-row md:flex-row flex-col">
                            @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                    <button type="button"
                            class="absolute top-0 z-30 left-[-60px] flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{v2_url('images/left1.png')}}">
                    </button>
                    <button type="button" class="w-3 h-3 rounded-full" aria-current="true" aria-label="Slide 1"
                            data-carousel-slide-to="0"></button>
                    <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 2"
                            data-carousel-slide-to="1"></button>
                    <button type="button" class="w-3 h-3 rounded-full" aria-current="false" aria-label="Slide 3"
                            data-carousel-slide-to="2"></button>
                    <button type="button"
                            class="absolute top-0 z-30 right-[-70px] flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{v2_url('images/right1.png')}}">
                    </button>
                </div>
            </div>

            <!-- mobile -->
            <div id="default-carousel" class="relative w-full pt-[52px] xl:hidden md:hidden" data-carousel="slide">
                <!-- Carousel wrapper -->
                <div class="relative h-[270px] overflow-hidden rounded-lg">
                    <!-- Item 1 -->
                    @foreach($feedbacks as $feedback)
                        <div class="hidden duration-700 ease-in-out px-4" data-carousel-item>
                            <div class="flex gap-4 border bg-[#0054DB] rounded-2xl border-[#7097FB] px-4 py-[22px] w-full">
                                <div class="flex min-w-[80px] h-[80px]">
                                    <img src="{{url('assets/uploads/feedback').'/'.$feedback->getAvatar()}}"
                                         class="slide-img">
                                </div>
                                <div class="flex flex-col">
                                    <p class="text-white font-medium text-[13px] line-clamp-4">{{$feedback->getTitle()}}</p>
                                    <p class="text-white font-medium text-[11x] pt-5">{{$feedback->getName()}}</p>
                                    <p class="text-[#E5E7E9] font-medium text-[11x]">{{$feedback->getJobTitle()}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Slider indicators -->
                <div class="absolute z-30 flex -translate-x-1/2 bottom-5 left-1/2 space-x-3 rtl:space-x-reverse">
                    <button type="button"
                            class="absolute top-0 z-30 left-[-60px] flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-prev>
                        <img src="{{v2_url('images/left1.png')}}">
                    </button>
                    <button type="button"
                            class="absolute top-0 z-30 right-[-70px] flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
                            data-carousel-next>
                        <img src="{{v2_url('images/right1.png')}}">
                    </button>
                </div>
            </div>
        </div>
    </div>
    {{--Phần có video 8 module TT#6--}}
    <div class="flex justify-center xl:py-[104px] md:py-[104px] py-12 xl:px-0 md-px-0 px-9" data-aos="fade-down">
        <div class="xl:w-3/5 w-4/5 items-center justify-between flex gap-8 xl:flex-row md:flex-row flex-col">
            <div class="xl:w-700 md:w-700 w-full">
                <div class="flex flex-col gap-4">
                    <p class="xl:text-5xl md:text-5xl text-[30px] text-[#212F51] font-bold !leading-[60px] capitalize mac:text-[40px]">
                        Gia tăng trải nghiệm khách hàng với 12 module mở rộng của getfly CRM</p>
                    <p class="text-2xl text-[#3B4A6D]">Doanh nghiệp của bạn dễ dàng tùy chỉnh theo nhu cầu và
                        cá nhân hóa vô tận</p>
                </div>
                <a href="https://getfly.vn/bang-gia.html" target="_blank"
                   class="text-xl text-[#003BD2] font-semibold flex gap-2 pt-[52px] capitalize">
                    Tìm hiểu thêm gói dịch vụ <img src="{{v2_url('images/dichvu.png')}}" class="w-6 h-6">
                </a>
            </div>
            <div>
                <video width="320" height="240" autoplay muted loop class="lazy w-auto h-auto">
                    <source data-src="{{v2_url('images/getfly.mp4')}}" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
    {{--Phần background đen TT#7 --}}
    <div class="flex flex-col items-center xl:py-[100px] md:py-[100px] py-10 bg-section gap-8 xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat"
         data-aos="fade-down">
        <div class="xl:w-3/5 w-4/5">
            <div class="flex flex-col text-center gap-6 xl:px-0 md:px-0 px-5">
                <p class="xl:text-5xl md:text-5xl text-[30px] text-white font-bold xl:leading-[60px] md:leading-[60px] leading-10 capitalize mac:text-[42px]">
                    CRM G - version mang lại trải nghiệm</br>
                    <span class="text-[#FE7F2D]"> hiệu suất vượt trội mạnh mẽ</span>
                </p>
                <p class="text-base mac:text-[14px] text-white">Sứ mệnh của Getfly - Cung cấp nền tảng công nghệ thay
                    đổi phương pháp quản trị Khoa học - Bài bản - Vững chắc</p>
            </div>
            <div class="flex justify-center py-10">
                <a class="rounded-xl bg-transparent border border-white text-white font-semibold text-sm px-6 py-3 hover:bg-white hover:text-[#071330]"
                   href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Đăng ký ngay</a>
            </div>
            <div class="flex flex-col xl:gap-12 md:gap-8 gap-4">
                <div class="flex justify-center text-white xl:gap-10 md:gap-10 gap-4 xl:flex-row md:flex-row flex-col items-center">
                    <div class="border rounded-2xl border-[#384052] text-center xl:p-8 md:p-8 p-4 flex flex-col gap-10 w-80 h-full hover:bg-white hover:text-[#071330]">
                        <p class="text-5xl font-bold">x 1,5</p>
                        <p class="text-sm">Tốc độ tải trang & tải dữ liệu</p>
                    </div>
                    <div class="border rounded-2xl border-[#384052] text-center xl:p-8 md:p-8 p-4 flex flex-col gap-6 w-80 h-full hover:bg-white hover:text-[#071330]">
                        <p class="text-5xl font-bold">90+</p>
                        <p class="text-sm">Công cụ kết nối mở API. Tích hợp đa công cụ & nền tảng</p>
                    </div>
                </div>
                <div class="flex justify-center text-white xl:gap-10 md:gap-10 gap-4 xl:flex-row md:flex-row flex-col items-center">
                    <div class="border rounded-2xl border-[#384052] text-center xl:p-8 md:p-8 p-4 flex flex-col gap-6 w-80 h-full hover:bg-white hover:text-[#071330]">
                        <p class="text-5xl font-bold">100++</p>
                        <p class="text-sm">Tính năng nghiên cứu phát triển dựa trên trải nghiệm người dùng</p>
                    </div>
                    <div class="border rounded-2xl border-[#384052] text-center xl:p-8 md:p-8 p-4 flex flex-col gap-6 w-80 h-full hover:bg-white hover:text-[#071330]">
                        <p class="text-5xl font-bold">0đ</p>
                        <p class="text-sm">Miễn phí hoàn toàn tính năng khi cập nhật phiên bản G-verson</p>
                    </div>
                    <div class="border rounded-2xl border-[#384052] text-center xl:p-8 md:p-8 p-4 flex flex-col gap-6 w-80 h-full hover:bg-white hover:text-[#071330]">
                        <p class="text-5xl font-bold">1 phút</p>
                        <p class="text-sm">Di chuyển và động bộ dữ liệu khách hàng từ hệ thống khác</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="xl:w-3/5 w-4/5 flex justify-center !pt-32">
            <div class="flex items-center justify-center bg-[#FFFBF5] border border-[#FFBF1A] rounded-3xl md:px-[50px] py-5"
                 data-aos="fade-down">
                <div class="flex items-center xl:gap-10 md:gap-1 gap-1 xl:flex-row md:flex-row flex-col xl:px-0 md:px-0 px-5 py-5">
                    <div class="flex flex-col gap-7 xl:w-[600px] md:w-[600px] w-full">
                        <button class="text-[#2561FC] text-sm font-semibold btn-app w-fit">MOBILE APP</button>
                        <p class="xl:text-4xl md:text-4xl text-[30px] text-[#212F51] font-bold capitalize">Siêu ứng dụng
                            CRM miễn phí trong lòng bàn tay</p>
                        <p class="text-base text-[#3B4A6D]">Trải nghiệm
                            Getfly CRM đầy đủ trong lòng bàn tay của bạn. Mang đến cho nhóm của bạn sự linh hoạt để làm
                            việc mọi lúc mọi nơi trong khi khách hàng của bạn được tận hưởng dịch vụ nhanh chóng, 24/24.
                        </p>
                        <div class="flex gap-6">
                            <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi" target="_blank">
                                <img src="{{v2_url('images/Appstore1.png')}}">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6" target="_blank">
                                <img src="{{v2_url('images/Googleplay1.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="flex">
                        <img src="{{v2_url('images/Logo Horizontal.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Phần tin tức getfly TT#8--}}
    <div class="flex justify-center">
        <div class="flex xl:py-16 md:py-16 py-5 flex-col xl:w-3/5 w-4/5" data-aos="fade-down">
            <div class="flex flex-col w-full">
                <p class="xl:text-4xl md:text-4xl text-[30px] text-[#212F51] font-bold leading-[60px] capitalize">Bản
                    Tin Getfly</p>
                <p class="text-lg text-[#212F51] pt-4">Khám phá ngay kiến thức & kinh nghiệm từ các chuyên gia
                    của Getfly</p>
                <a href="https://getfly.vn/i4-blog.html" target="_blank"
                   class="text-xl text-[#003BD2] font-semibold flex gap-2 pt-6 capitalize">
                    Xem thêm tin tức <img src="{{v2_url('images/dichvu.png')}}" class="w-6 h-6" alt="">
                </a>
            </div>
            <div class="flex xl:flex-row md:flex-row flex-col gap-10 xl:py-16 md:py-16 py-5">
                @foreach($newspapers as $newspaper)
                    <div class="flex flex-col gap-5">
                        <img class="rounded-[10px]" src="{{$newspaper->getImage()}}"
                             style="aspect-ratio: 2;max-width: 353px">
                        <div class="flex flex-col gap-4">
                            <p class="xl:text-[32px] md:text-[20px] text-[#323539] font-medium line-clamp-2 leading-10 tracking-tighter">
                                {{$newspaper->getTitle()}}
                            </p>
                            <p class="text-base text-[#858C95] line-clamp-3">{{$newspaper->getDescription()}}</p>
                        </div>
                        <a class="text-xl text-[#003BD2] font-semibold flex gap-2 pt-5" href="#" target="_blank">Đọc
                            thêm
                            <img src="{{v2_url('images/dichvu.png')}}" class="w-6 h-6"></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    {{--    Phần danh sách nhiều khách hàng TT#9 --}}
    <div class="flex flex-col justify-center gap-20 items-center">
        <div class="xl:w-3/5 w-4/5 py-[100px]">
            <div class="flex gap-4 flex-col text-center items-center">
                <button class="text-[#2561FC] text-sm font-semibold btn-app w-fit">KHÁCH HÀNG</button>
                <p class="xl:text-5xl md:text-5xl text-[30px] text-[#212F51] font-bold capitalize leading-[60px]"
                   style="line-height: 1.2!important;">Luôn trân quý niềm tin của
                    <br><span class="text-[#FE7F2D]">khách hàng đặt vào chúng tôi</span>
                </p>
                <p class="text-base mac:text-[17px] text-[#353535]">Thiết lập quy trình chăm sóc khách hàng chuyên
                    nghiệp đồng thời duy trì được mối quan hệ lâu dài của doanh nghiệp và khách hàng</p>
            </div>
            <div id="default-carousel" class="relative w-full pt-[52px] xl:block md:block hidden" data-carousel="slide">
                <div class="relative h-[400px] overflow-hidden rounded-lg">
                    <div class="bg-white flex flex-col gap-10" data-carousel-item>
                        <div class="flex justify-between align-items-center">
                            @foreach($bottomPlacePartners as $index => $partner)
                                <img style="max-height: 80px" src="{{$partner->getPartnerLogo()}}"
                                     class="image-container" alt="">
                                @if(($index+1) % 4 == 0)
                        </div>
                        @if(($index+1) % 12 == 0)
                    </div>
                    <div class="bg-white flex flex-col gap-10" data-carousel-item>
                        @endif
                        <div class="flex justify-between align-items-center">
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="absolute z-30 flex -translate-x-1/2 bottom-5 pt-5 left-1/2 space-x-3 rtl:space-x-reverse"
                     id="slide-interact">
                    @foreach($bottomPlacePartners as $index => $topPlacePartner)
                        @if($index % 12 == 0)
                            <button type="button" class="w-3 h-3 rounded-full !bg-neutral-200 !hover:bg-neutral-300"
                                    aria-current="true" aria-label="Slide 1" data-carousel-slide-to="{{$index/3}}"></button>
                        @endif
                    @endforeach
                </div>
            </div>
            <!--mobile-->
            <div id="default-carousel" class="relative w-full pt-[52px] xl:hidden md:hidden block"
                 data-carousel="slide">
                <div class="relative h-[400px] overflow-hidden rounded-lg">
                    <div class="bg-white flex flex-col gap-10" data-carousel-item>
                        @foreach($bottomPlacePartners as $index => $partner)
                            <div class="flex justify-between">
                                <img src="{{$partner->getPartnerLogo()}}"
                                     class="image-container" alt="">
                            </div>
                            @if(($index+1) % 3 == 0)
                    </div>
                    <div class="bg-white flex flex-col gap-10" data-carousel-item>
                        @endif
                        @endforeach
                    </div>
                </div>
                <div class="absolute z-30 flex -translate-x-1/2 bottom-5 pt-5 left-1/2 space-x-3 rtl:space-x-reverse"
                     id="slide-interact">
                    @foreach($bottomPlacePartners as $index => $bottomPlacePartner)
                        @if($index % 3 == 0)
                            <button type="button" class="w-3 h-3 rounded-full !bg-neutral-200 !hover:bg-neutral-300"
                                    aria-current="true" aria-label="Slide 1" data-carousel-slide-to="{{$index}}"></button>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {{--  Phần nút chuyển đổi doanh nghiệp TT#10 --}}
    <div class="flex flex-col footer-text justify-center text-center xl:py-12 md:py-12 py-16 items-center xl:!bg-repeat-round md:!bg-repeat-round bg-no-repeat">
        <div class="xl:w-3/5 w-4/5 flex gap-8 flex-col items-center">
            <p class="xl:text-[44px] md:text-[44px] text-[30px] text-[#004DFF] font-bold leading-[60px] capitalize mac:text-[40px]">
                Chuyển đổi doanh nghiệp của bạn ngay</p>
            <button class="button-pri !w-fit">
                <a href="https://getfly.vn/dang-ky-dung-thu-phan-mem.html" target="_blank">Trải nghiệm 30 ngày</a>
            </button>
        </div>
    </div>
    {{--    Footer chung --}}
    @include("version2.layouts.inc.footer")
@endsection
@section('page_scripts')
    @include("version2.layouts.inc.home-scripts")
@endsection