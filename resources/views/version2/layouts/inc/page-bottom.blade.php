<script async src="https://www.googletagmanager.com/gtag/js?id=G-3RDFHX6J42"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-3RDFHX6J42');
</script>
<div class="phone">
    <a href="tel:02462627662"> <img src="{{url("site/images/phone.png")}}"></a>
    <div class="hotline">
        <p>Hotline 24/7: (024) 6262 7662</p>
    </div>
</div>
<div class="fixed bottom-5 right-3" style="z-index: 99999999">
    {{--    <a onclick="openChat()" class="cursor-pointer">--}}
    {{--        <img class="mb-3" style="width: 3rem;height: 3rem" src="{{url("site/images/subiz.webp")}}">--}}
    {{--    </a>--}}
    <a href="https://zalo.me/2642095819923952061" target="_blank">
        <img class="mb-3" src="{{url("site/images/zalo.png")}}">
    </a>
    <a href="https://www.facebook.com/messages/t/328467620572169" target="_blank">
        <img src="{{url("site/images/messenger.png")}}">
    </a>
</div>
<script>
    !function (s, u, b, i, z) {
        var o, t, r, y;
        s[i] || (s._sbzaccid = z, s[i] = function () {
            s[i].q.push(arguments)
        }, s[i].q = [], s[i]("setAccount", z), r = ["widget.subiz.net", "storage.googleapis" + (t = ".com"), "app.sbz.workers.dev", i + "a" + (o = function (k, t) {
            var n = t <= 6 ? 5 : o(k, t - 1) + o(k, t - 3);
            return k !== t ? n : n.toString(32)
        })(20, 20) + t, i + "b" + o(30, 30) + t, i + "c" + o(40, 40) + t], (y = function (k) {
            var t, n;
            s._subiz_init_2094850928430 || r[k] && (t = u.createElement(b), n = u.getElementsByTagName(b)[0], t.async = 1, t.src = "https://" + r[k] + "/sbz/app.js?accid=" + z, n.parentNode.insertBefore(t, n), setTimeout(y, 2e3, k + 1))
        })(0))
    }(window, document, "script", "subiz", "acrrnsgtisduismngghn")
</script>