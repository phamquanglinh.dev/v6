<script>
    // Kéo trang lên đầu sau khi trang đã tải lại hoàn toàn
    // window.addEventListener('load', function () {
    //     window.scrollTo(0, 0);
    // });

    // Lắng nghe sự kiện scroll và hiển thị nút khi cần
    window.addEventListener('scroll', function () {
        var mybutton = document.getElementById("myBtn");
        var scrollPosition = window.scrollY || document.documentElement.scrollTop;

        if (scrollPosition > 2000) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    });


    const collapseService = document.querySelectorAll(".collapse-service");
    const collapseService1 = document.querySelectorAll(".collapse-service1");
    const collapseService2 = document.querySelectorAll(".collapse-service2");
    const video1 = document.getElementById("video-1")
    const video2 = document.getElementById("video-2")
    const video3 = document.getElementById("video-3")

    // Hàm xử lý sự kiện click
    function handleClick(index, topValue, elementTarget) {

        elementTarget.forEach((element, i) => {
            if (i === index) {
                if (!element.classList.contains("border")) {
                    element.classList.add("border", "active");
                }
                element.querySelector("img").src = "{{v2_url('images/close2.png')}}";
            } else {
                element.classList.remove("border", "active");
                element.querySelector("img").src = "{{v2_url('images/plus.png')}}";
            }
        });

    }

    // Gắn sự kiện click cho mỗi phần tử collapseService
    collapseService.forEach((element, index) => {
        element.addEventListener("click", () => {
            var randomNumber = Math.floor(Math.random() * 10000);
            console.log(index);

            switch (index) {
                case 0:
                    handleClick(0, 40, collapseService);
                    video1.src = "{{v2_url('images/videokh1.mp4')}}"
                    break;
                case 1:
                    handleClick(1, 223, collapseService);
                    video1.src = "{{v2_url('images/videokh2.mp4')}}"
                    break;
                case 2:
                    handleClick(2, 398, collapseService);
                    video1.src = "{{v2_url('images/videokh3.mp4')}}"
                    break;
                default:
                    break;
            }
        });
    });
    collapseService1.forEach((element, index) => {
        element.addEventListener("click", () => {
            switch (index) {
                case 0:
                    handleClick(0, 40, collapseService1);
                    video2.src = "{{v2_url('images/videoauto1.mp4')}}"
                    break;
                case 1:
                    handleClick(1, 223, collapseService1);
                    video2.src = "{{v2_url('images/videoauto2.mp4')}}"
                    break;
                case 2:
                    handleClick(2, 398, collapseService1);
                    video2.src = "{{v2_url('images/videoauto3.mp4')}}"
                    break;
                default:
                    break;
            }
        });
    });
    collapseService2.forEach((element, index) => {
        element.addEventListener("click", () => {
            switch (index) {
                case 0:
                    handleClick(0, 40, collapseService2);
                    video3.src = "{{v2_url('images/videokpi1.mp4')}}"
                    break;
                case 1:
                    handleClick(1, 223, collapseService2);
                    video3.src = "{{v2_url('images/videokpi2.mp4')}}"
                    break;

                default:
                    break;
            }
        });
    });


    const btnTab = document.querySelectorAll(".button-tab")
    const serviceTab = document.querySelectorAll(".service-tab")
    const srcActive = [
        {active: "{{v2_url('images/customer.svg')}}", deactive: "{{v2_url('images/customer-deactive.svg')}}"},
        {active: "{{v2_url('images/auto.svg')}}", deactive: "{{v2_url('images/auto-deactive.svg')}}"},
        {active: "{{v2_url('images/kpi.svg')}}", deactive: "{{v2_url('images/kpi-deactive.svg')}}"}
    ]
    const bgHomeDiv = document.querySelector(".bg-home1");


    const handleBtnClick = (element, index) => {
        if (index == 0) {
            element.classList.add("btn1-tab-active")
            btnTab[1].classList.remove("btn2-tab-active")
            btnTab[1].classList.remove("btn2-tab-active")
            element.querySelector("img").src = srcActive[0]?.active
            btnTab[1].querySelector("img").src = srcActive[1]?.deactive
            btnTab[2].querySelector("img").src = srcActive[2]?.deactive
            bgHomeDiv.style.backgroundImage = "url({{v2_url('images/bg-customer.png')}})"

        } else if (index == 1) {
            element.classList.add("btn2-tab-active")
            btnTab[0].classList.remove("btn1-tab-active")
            btnTab[2].classList.remove("btn3-tab-active")
            element.querySelector("img").src = srcActive[1]?.active
            btnTab[0].querySelector("img").src = srcActive[0]?.deactive
            btnTab[2].querySelector("img").src = srcActive[2]?.deactive
            bgHomeDiv.style.backgroundImage = "url({{v2_url('images/bg-auto.png')}})"
        } else {
            element.classList.add("btn3-tab-active")
            btnTab[1].classList.remove("btn2-tab-active")
            btnTab[0].classList.remove("btn1-tab-active")
            element.querySelector("img").src = srcActive[2]?.active
            btnTab[1].querySelector("img").src = srcActive[1]?.deactive
            btnTab[0].querySelector("img").src = srcActive[0]?.deactive
            bgHomeDiv.style.backgroundImage = "url({{v2_url('images/bg-kpi.png')}})"
        }
        serviceTab.forEach((elm, idx) => {
                if (idx == index) {
                    elm.classList.remove("hidden")
                } else {
                    elm.classList.add("hidden")
                }
            }
        )
    }

    btnTab.forEach((btn, index) => {
        btn.addEventListener("click", () => {
            document.documentElement.style.setProperty('--border-color', `${index == 0 ? "#2670FF" : index == 1 ? "#1DA68D" : "#FFA37A"}`);
            document.documentElement.style.setProperty('--bg-color', `${index == 0 ? "#F9FBFF" : index == 1 ? "#DEFCF6" : "#FFF7F3"}`);
            handleBtnClick(btn, index)
        })
    })


    function onScrollToTop(duration) {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    }

</script>