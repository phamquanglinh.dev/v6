@php
    use App\ViewModels\Sites\Header\HeaderListViewModel;
    /**
     * @var HeaderListViewModel $headerListViewModel
     */
@endphp
<style>
    /*@media (min-width: 1400px){*/
    /*    .\33xl\:text-\[1rem\] {*/
    /*        font-size: 1rem!important;*/
    /*    }*/
    /*}*/

</style>
{{--<nav class="bg-white border-gray-200 dark:bg-gray-900" style="position: fixed;top: 0;width: 100%;z-index: 40;">--}}
{{--    <div class="menu-top">--}}
{{--        <a href="trangchu.html">--}}
{{--            <img class="logo" src="images/logo.png">--}}
{{--        </a>--}}
{{--        <div class="flex items-center md:order-2">--}}
{{--            <a href="dangky.html" class="button-menu">--}}
{{--                Trải nghiệm 30 ngày</a>--}}
{{--            <button data-collapse-toggle="mega-menu" type="button"--}}
{{--                    class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"--}}
{{--                    aria-controls="mega-menu" aria-expanded="false">--}}
{{--                <span class="sr-only">Open main menu</span>--}}
{{--                <svg aria-hidden="true" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"--}}
{{--                     xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <path fill-rule="evenodd"--}}
{{--                          d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"--}}
{{--                          clip-rule="evenodd"></path>--}}
{{--                </svg>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--        <div id="mega-menu" class="hidden items-center justify-between w-full md:flex md:w-auto md:order-1 z-30">--}}
{{--            <ul class="flex flex-col font-medium md:flex-row md:mt-0">--}}
{{--                <li class="li-menu">--}}
{{--                    <div class="flex gap-2 items-center justify-between cursor-pointer" id="dropdownNavbarLink"--}}
{{--                         data-dropdown-toggle="dropdownNavbar">--}}
{{--                        <a class="block border-gray-100" href="vegetfly.html">Giới thiệu</a>--}}
{{--                        <img class="w-3 h-3 lg:w-2 lg:h-2" src="images/arrow-down.png">--}}
{{--                    </div>--}}
{{--                    <div id="dropdownNavbar"--}}
{{--                         class="z-30 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-96 border">--}}
{{--                        <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">--}}
{{--                            <li>--}}
{{--                                <a href="vegetfly.html" class="block p-2 hover:bg-gray-100">Về--}}
{{--                                    Getfly--}}
{{--                                    CRM</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" class="block p-2 hover:bg-gray-100">Quy--}}
{{--                                    trình--}}
{{--                                    cung cấp và sử dụng dịch vụ</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" class="block p-2 hover:bg-gray-100">Chính--}}
{{--                                    sách bảo mật</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <button class="flex gap-2 items-center justify-between w-full" id="mega-menu-dropdown-button"--}}
{{--                            data-dropdown-toggle="mega-menu-dropdown"--}}
{{--                            class="flex items-center justify-between w-full py-2 pl-3 pr-4 font-medium text-gray-900 border-b border-gray-100 md:w-auto hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-600 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-blue-500 md:dark:hover:bg-transparent dark:border-gray-700">--}}
{{--                        Sản phẩm--}}
{{--                        <img class="w-3 h-3 lg:w-2 lg:h-2" src="images/arrow-down.png">--}}
{{--                    </button>--}}
{{--                    <div id="mega-menu-dropdown" class="mega-menu absolute !top-5 z-50 flex w-full hidden">--}}
{{--                        <ul class="menu-small" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">--}}
{{--                            <li role="presentation">--}}
{{--                                <button class="mega-button" id="profile-tab" data-tabs-target="#all" type="button"--}}
{{--                                        role="tab" aria-controls="profile" aria-selected="false">--}}
{{--                                    <img src="images/All in one.png">--}}
{{--                                    <span class="hide">All in one CRM Fly</span>--}}
{{--                                </button>--}}
{{--                            </li>--}}
{{--                            <li role="presentation">--}}
{{--                                <button class="mega-button" id="dashboard-tab" data-tabs-target="#marketing"--}}
{{--                                        type="button" role="tab" aria-controls="dashboard" aria-selected="false">--}}
{{--                                    <img src="images/Marketing fly.png">--}}
{{--                                    <span class="hide">Marketing Fly</span>--}}
{{--                                </button>--}}
{{--                            </li>--}}
{{--                            <li role="presentation">--}}
{{--                                <button class="mega-button" id="settings-tab" data-tabs-target="#sales"--}}
{{--                                        type="button" role="tab" aria-controls="settings" aria-selected="false">--}}
{{--                                    <img src="images/CRM sales.png">--}}
{{--                                    <span class="hide">CRM Sales Fly</span>--}}
{{--                                </button>--}}
{{--                            </li>--}}
{{--                            <li role="presentation">--}}
{{--                                <button class="mega-button" id="contacts-tab" data-tabs-target="#extend"--}}
{{--                                        type="button" role="tab" aria-controls="contacts" aria-selected="false">--}}
{{--                                    <img src="images/modules.png">--}}
{{--                                    <span class="hide">Module mở rộng</span>--}}
{{--                                </button>--}}
{{--                            </li>--}}
{{--                            <li role="presentation">--}}
{{--                                <button class="mega-button" id="contacts-tab" data-tabs-target="#service"--}}
{{--                                        type="button" role="tab" aria-controls="contacts" aria-selected="false">--}}
{{--                                    <img src="images/DV triển khai.png">--}}
{{--                                    <span class="hide">Dịch vụ triển khai</span>--}}
{{--                                </button>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <div class="relative w-full" id="myTabContent">--}}
{{--                            <div class="hidden box-menu-small" id="all" role="tabpanel"--}}
{{--                                 aria-labelledby="profile-tab">--}}
{{--                                <div class="mega-box-detail">--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Giao tiếp nội bộ</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý tài liệu</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý chiến dịch--}}
{{--                                            marketing</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Email marketing (Tích hợp--}}
{{--                                            dịch vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Sms marketing (Tích hợp dịch--}}
{{--                                            vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Marketing automation</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Landing pages</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Webform</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý dự án công việc</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý affiliate</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý danh sách khách--}}
{{--                                            hàng</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý lịch hẹn</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý đơn hàng, doanh--}}
{{--                                            thu</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý danh mục sản phẩm</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý đơn hàng mua, bán</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý hợp đồng mua, bán</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Chính sách bán hàng</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý báo giá</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý ticket</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Đo lường KPI</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="hidden box-menu-small" id="marketing" role="tabpanel"--}}
{{--                                 aria-labelledby="dashboard-tab">--}}
{{--                                <div class="mega-box-detail">--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Giao tiếp nội bộ</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý tài liệu</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Phân loại khách hàng theo--}}
{{--                                            chiến dịch</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Email marketing (Tích hợp--}}
{{--                                            dịch vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Sms marketing (Tích hợp dịch--}}
{{--                                            vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Marketing automation</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý dự án công việc</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Mẫu email</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Đo lường KPI</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Landing Pages</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Webform</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Remarketing</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="hidden box-menu-small" id="sales" role="tabpanel"--}}
{{--                                 aria-labelledby="settings-tab">--}}
{{--                                <div class="mega-box-detail">--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Giao tiếp nội bộ</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý tài liệu</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Phân loại khách hàng theo--}}
{{--                                            chiến dịch</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Email marketing (Tích hợp--}}
{{--                                            dịch vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Sms marketing (Tích hợp dịch--}}
{{--                                            vụ thứ 3)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Marketing automation</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý affiliate</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý dự án công việc</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Phân loại khách hàng</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Bộ lọc ưu việt</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý danh sách khách--}}
{{--                                            hàng</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý đơn hàng, doanh--}}
{{--                                            thu</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý danh mục sản phẩm</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý mua, bán</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Quản lý báo giá</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Chính sách bán hàng</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Hợp đồng mua</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Hợp đồng bán</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Đo lường KPI</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="hidden box-menu-small" id="extend" role="tabpanel"--}}
{{--                                 aria-labelledby="contacts-tab">--}}
{{--                                <div class="mega-box-detail">--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý kênh social--}}
{{--                                            (fanpage, zalo OA)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý tổng đài--}}
{{--                                            VoIP</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý kho</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý nhân sự--}}
{{--                                            (HRM)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý tài chính - kế--}}
{{--                                            toán nội bộ</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module chấm công bằng GPS--}}
{{--                                            hoặc IP tĩnh</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="w-1/2 flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý bán lẻ--}}
{{--                                            (POS)</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module bảo mật nâng cao</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý bảo hành</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý lịch đi--}}
{{--                                            tuyến</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Module quản lý điểm--}}
{{--                                            thưởng</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="hidden box-menu-small" id="service" role="tabpanel"--}}
{{--                                 aria-labelledby="contacts-tab">--}}
{{--                                <div class="mega-box-detail">--}}
{{--                                    <div class="w-full flex flex-col">--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Triển khai hệ thống CRM cho--}}
{{--                                            doanh nghiệp</a>--}}
{{--                                        <a href="trangcon.html" class="mb-3 text-base">Tư vấn xây dựng quy trình--}}
{{--                                            chăm sóc tối ưu và--}}
{{--                                            triển khai cho--}}
{{--                                            doanh--}}
{{--                                            nghiệp</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="mega-journey">--}}
{{--                                <p class="uppercase text-btn-color text-base font-bold mb-2">Hành trình với Getfly--}}
{{--                                </p>--}}
{{--                                <div class="border-b mb-2 pb-2">--}}
{{--                                    <p class="font-bold flex gap-2 items-center">--}}
{{--                                        <img src="images/files.png">--}}
{{--                                        Dùng thử miễn phí--}}
{{--                                    </p>--}}
{{--                                    <p class="text-xs pl-10 pt-1">Đăng ký nhận tài khoản demo để trải nghiệm tính--}}
{{--                                        năng CRM tiêu chuẩn.--}}
{{--                                        Nếu bạn--}}
{{--                                        muốn trải nghiệm--}}
{{--                                        các module khác, vui lòng liên hệ để được trợ giúp</p>--}}
{{--                                </div>--}}
{{--                                <div class="border-b mb-2 pb-2">--}}
{{--                                    <p class="font-bold flex gap-2 items-center">--}}
{{--                                        <img src="images/support.png">--}}
{{--                                        Tư vấn trực tiếp hoặc online--}}
{{--                                    </p>--}}
{{--                                    <p class="text-xs pl-10 pt-1">Getfly sẽ dựa vào mô hình kinh doanh và nhu cầu--}}
{{--                                        quản lý của doanh--}}
{{--                                        nghiệp để--}}
{{--                                        tư vấn phương--}}
{{--                                        thức ứng dụng hiệu quả nhất</p>--}}
{{--                                </div>--}}
{{--                                <div class="border-b mb-2 pb-2">--}}
{{--                                    <p class="font-bold flex gap-2 items-center">--}}
{{--                                        <img src="images/choices.png">--}}
{{--                                        Lựa chọn gói dịch vụ--}}
{{--                                    </p>--}}
{{--                                    <p class="text-xs pl-10 pt-1">Getfly cung cấp các gói phù hợp với số lượng người--}}
{{--                                        dùng tại doanh--}}
{{--                                        nghiệp.--}}
{{--                                        Ngoài ra doanh--}}
{{--                                        nghiệp có thể lựa chọn dịch vụ triển khai để tối ưu hóa thời gian, nguồn lực--}}
{{--                                        và chi phí</p>--}}
{{--                                </div>--}}
{{--                                <div class="border-b mb-2 pb-2">--}}
{{--                                    <p class="font-bold flex gap-2 items-center">--}}
{{--                                        <img src="images/teaching.png">--}}
{{--                                        Đào tạo sử dụng--}}
{{--                                    </p>--}}
{{--                                    <p class="text-xs pl-10 pt-1">Doanh nghiệp sẽ cần chuẩn bị dữ liệu cơ bản trên--}}
{{--                                        CRM để đảm bảo việc--}}
{{--                                        đào tạo--}}
{{--                                        sử dụng được--}}
{{--                                        hiệu quả dựa trên dữ liệu thực của doanh nghiệp</p>--}}
{{--                                </div>--}}
{{--                                <div class="border-b mb-2 pb-2">--}}
{{--                                    <p class="font-bold flex gap-2 items-center">--}}
{{--                                        <img src="images/training (1).png">Đào tạo liên tục và đồng hành--}}
{{--                                    </p>--}}
{{--                                    <p class="text-xs pl-10 pt-1">Getfly cam kết hỗ trợ doanh nghiệp trong suốt thời--}}
{{--                                        gian sử dụng dịch--}}
{{--                                        vụ--}}
{{--                                        và--}}
{{--                                        cung cấp các buổi--}}
{{--                                        đào tạo sử dụng phần mềm liên tục hàng tháng để giúp doanh nghiệp ứng dụng--}}
{{--                                        CRM tốt nhất</p>--}}
{{--                                </div>--}}
{{--                                <p class="font-bold">Bảng giá dịch vụ</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <a href="baogia.html">Báo giá</a>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <a href="#">Tài liệu tham khảo</a>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <div class="flex gap-2 items-center justify-between cursor-pointer" id="dropdownNavbarLink"--}}
{{--                         data-dropdown-toggle="dropdownNavbar1">--}}
{{--                        <a class="block border-gray-100">Đối tác</a>--}}
{{--                        <img class="w-3 h-3 lg:w-2 lg:h-2" src="images/arrow-down.png">--}}
{{--                    </div>--}}
{{--                    <div id="dropdownNavbar1"--}}
{{--                         class="z-30 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-52 border">--}}
{{--                        <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">--}}
{{--                            <li>--}}
{{--                                <a href="hesinhthai.html" class="block p-2 hover:bg-gray-100 hover:rounded-lg">Hệ--}}
{{--                                    sinh thái</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="thumoidoitac.html" class="block p-2 hover:bg-gray-100 hover:rounded-lg">Thư--}}
{{--                                    mời--}}
{{--                                    hợp tác</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <div class="flex gap-2 items-center justify-between cursor-pointer" id="dropdownNavbarLink"--}}
{{--                         data-dropdown-toggle="dropdownNavbar3">--}}
{{--                        <a class="block border-gray-100">Tin tức</a>--}}
{{--                        <img class="w-3 h-3 lg:w-2 lg:h-2" src="images/arrow-down.png">--}}
{{--                    </div>--}}
{{--                    <div id="dropdownNavbar3"--}}
{{--                         class="z-30 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-52 border">--}}
{{--                        <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">--}}
{{--                            <li>--}}
{{--                                <a href="tintuc.html" class="block p-2 hover:bg-gray-100 hover:rounded-lg">Blogs</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="tintuc.html" class="block p-2 hover:bg-gray-100 hover:rounded-lg">Thông--}}
{{--                                    báo</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <a href="lienhe.html">Liên hệ</a>--}}
{{--                </li>--}}
{{--                <li class="li-menu">--}}
{{--                    <div class="flex gap-2 items-center justify-between cursor-pointer" id="dropdownNavbarLink"--}}
{{--                         data-dropdown-toggle="dropdownNavbar2">--}}
{{--                        <a>Tuyển dụng</a>--}}
{{--                        <img class="w-3 h-3 lg:w-2 lg:h-2" src="images/arrow-down.png">--}}
{{--                    </div>--}}
{{--                    <div id="dropdownNavbar2"--}}
{{--                         class="z-30 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-52 border">--}}
{{--                        <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">--}}
{{--                            <li>--}}
{{--                                <a href="tuyendung.html" class="block p-2 hover:bg-gray-100 hover:rounded-lg">Vị trí--}}
{{--                                    tuyển--}}
{{--                                    dụng</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="cuocsonggetfly.html"--}}
{{--                                   class="block p-2 hover:bg-gray-100 hover:rounded-lg">Cuộc sống--}}
{{--                                    Getfly</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}

<div id="navbar" style="margin-bottom: 84px;">
    <nav class="bg-white border-gray-200 dark:bg-gray-900" style="position: fixed;top: 0;width: 100%;z-index: 40;">
        <div class="flex-wrap menu-top py-[1rem] 2xl:px-[15rem] lg:px-[10rem] lg:px-[5rem]">
            <a href="{{url("/")}}">
                <img class="logo"  src="{{url("site/images/logo.png")}}">
            </a>
            <div class="flex items-center md:order-2 text-white">
                <a href="{{route("sites.trial")}}" class="button-menu">
                    Trải nghiệm 30 ngày</a>
                <button data-collapse-toggle="mega-menu" type="button"
                        class="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg md:hidden  focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                        aria-controls="mega-menu" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg aria-hidden="true" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                              clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div id="mega-menu" class="hidden items-center justify-between w-full md:flex md:w-auto md:order-1 z-30">
                <ul class="flex flex-col font-medium md:flex-row md:mt-0">
                    @foreach($headerListViewModel->getMenus()->getChild() as $menu)
                        @if($menu->isMega())
                            <li class="li-menu">
                                <button id="mega-menu-dropdown-button"
                                        data-dropdown-toggle="mega-menu-dropdown"
                                        class="w-full flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800"
                                >
                                    {!! $menu->getTitle() !!}
                                    <img class="w-3 h-3 lg:w-2 lg:h-2" src="{{url("site/images/arrow-down.png")}}">
                                </button>
                                <div id="mega-menu-dropdown" class="mega-menu absolute !top-5 z-50 flex w-full hidden">
                                    <ul class="menu-small" id="myTab" data-tabs-toggle="#myTabContent" role="tablist">
                                        @foreach($menu->getTabs() as $tab)
                                            <li role="presentation">
                                                <button class="mega-button"
                                                        id="{{\Illuminate\Support\Str::slug($tab->getName())}}-tab"
                                                        data-tabs-target="#tab-{{\Illuminate\Support\Str::slug($tab->getName())}}"
                                                        type="button" role="tab" aria-controls="dashboard"
                                                        aria-selected="false">
                                                    <img src="{{url($tab->getIcon())}}">
                                                    <span class="md:block hidden">{{$tab->getName()}}</span>
                                                </button>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="relative w-full" id="myTabContent">
                                        @foreach($menu->getTabs() as $tab)
                                            <div class="hidden box-menu-small"
                                                 id="tab-{{\Illuminate\Support\Str::slug($tab->getName())}}"
                                                 role="tabpanel"
                                                 aria-labelledby="dashboard-tab">
                                                <div class="mega-box-detail">
                                                    <div class="w-1/2 flex flex-col">
                                                        @foreach($tab->getItems() as $key => $item)
                                                            @if($key%2==0)
                                                                <a target="{{$item->getNewTab()?"_blank":""}}"
                                                                   href="{{url($item->getLinks())}}"
                                                                   class="mb-3 text-blue-600-important hover:text-blue-600">
                                                                    {!! $item->getTitle() !!}
                                                                </a>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="w-1/2 flex flex-col">
                                                        @foreach($tab->getItems()  as $key => $item)
                                                            @if($key%2==1)
                                                                <a
                                                                        target="{{$item->getNewTab()?"_blank":""}}"
                                                                        href="{{$item->getLinks()}}"
                                                                        class="mb-3 text-blue-600-important hover:text-blue-600">  {!! $item->getTitle() !!}</a>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="mega-journey">
                                            <p class="uppercase text-btn-color  font-bold mb-2">Hành trình với
                                                Getfly
                                            </p>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/files.png")}}">
                                                    Dùng thử miễn phí
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Đăng ký nhận tài khoản demo để trải nghiệm
                                                    tính
                                                    năng CRM tiêu chuẩn.
                                                    Nếu bạn
                                                    muốn trải nghiệm
                                                    các module khác, vui lòng liên hệ để được trợ giúp</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/support.png")}}">
                                                    Tư vấn trực tiếp hoặc online
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly sẽ dựa vào mô hình kinh doanh và
                                                    nhu
                                                    cầu
                                                    quản lý của doanh
                                                    nghiệp để
                                                    tư vấn phương
                                                    thức ứng dụng hiệu quả nhất</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/choices.png")}}">
                                                    Lựa chọn gói dịch vụ
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly cung cấp các gói phù hợp với số
                                                    lượng
                                                    người
                                                    dùng tại doanh
                                                    nghiệp.
                                                    Ngoài ra doanh
                                                    nghiệp có thể lựa chọn dịch vụ triển khai để tối ưu hóa thời gian,
                                                    nguồn
                                                    lực
                                                    và chi phí</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("site/images/teaching.png")}}">
                                                    Đào tạo sử dụng
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Doanh nghiệp sẽ cần chuẩn bị dữ liệu cơ
                                                    bản
                                                    trên
                                                    CRM để đảm bảo việc
                                                    đào tạo
                                                    sử dụng được
                                                    hiệu quả dựa trên dữ liệu thực của doanh nghiệp</p>
                                            </div>
                                            <div class="border-b mb-2 pb-2">
                                                <p class="font-bold flex gap-2 items-center">
                                                    <img src="{{url("/site/images/training (1).png")}}">Đào tạo liên
                                                    tục
                                                    và đồng
                                                    hành
                                                </p>
                                                <p class="text-xs pl-10 pt-1">Getfly cam kết hỗ trợ doanh nghiệp trong
                                                    suốt
                                                    thời
                                                    gian sử dụng dịch
                                                    vụ
                                                    và
                                                    cung cấp các buổi
                                                    đào tạo sử dụng phần mềm liên tục hàng tháng để giúp doanh nghiệp
                                                    ứng
                                                    dụng
                                                    CRM tốt nhất</p>
                                            </div>
                                            <a href="{{route("sites.pricing")}}"
                                               class="text-md font-bold text-orange text-primary-orange500 mt-2"><u>Bảng giá dịch vụ</u></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @elseif($menu->getChild()!=[])
                            <li class="li-menu">
                                <div class="flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800"
                                     id="dropdownNavbarLink"
                                     data-dropdown-toggle="{{\Illuminate\Support\Str::slug($menu->getTitle())}}">
                                    <a target="{{$menu->getNewTab()?"_blank":""}}"
                                       class="block border-gray-100"
                                       href="{{$menu->getLinks()}}">{!!$menu->getTitle()  !!}</a>
                                    <img class="w-3 h-3 lg:w-2 lg:h-2" src="{{url("site/images/arrow-down.png")}}">
                                </div>
                                <div id="{{\Illuminate\Support\Str::slug($menu->getTitle())}}"
                                     class="z-30 hidden font-normal bg-white divide-y divide-gray-100 md:w-dropdown w-96 shadow border">
                                    <ul class="text-gray-700" aria-labelledby="dropdownLargeButton">
                                        @foreach($menu->getChild() as $sub)
                                            @if($sub->getChild()!=[])
                                                <li class="peer li-menu relative">
                                                    <div class=" flex gap-2 items-center justify-between cursor-pointer hover:text-blue-800">
                                                        <a target="{{$sub->getNewTab()?"_blank":""}}"
                                                           class="block border-gray-100"
                                                           href="{{$sub->getLinks()}}">{!!$sub->getTitle()  !!}</a>
                                                        <img class="w-3 h-3 lg:w-2 lg:h-2 md:flex hidden"
                                                             src="{{url("site/images/arrow-right.png")}}">
                                                        <img class="w-3 h-3 lg:w-2 lg:h-2 md:hidden"
                                                             src="{{url("site/images/arrow-down.png")}}">
                                                    </div>
                                                    <ul class="hover-menu absolute w-96 md:w-dropdown top-0 bg-white shadow-lg rounded border"
                                                        style="right: -15rem">
                                                        @foreach($sub->getChild() as $thirdSub)
                                                            <li class="hover:text-blue-600">
                                                                <a target="{{$thirdSub->getNewTab()?"_blank":""}}"
                                                                   href="{{url($thirdSub->getLinks())}}"
                                                                   class=" block p-2 ">{!!$thirdSub->getTitle() !!}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li class="li-menu hover:text-blue-600 px-2">
                                                    <a target="{{$sub->getNewTab()?"_blank":""}}"
                                                       href="{{url($sub->getLinks())}}"
                                                       class=" block ">{!!$sub->getTitle() !!}</a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @else
                            <li class="li-menu hover:text-blue-600">
                                <a target="{{$menu->getNewTab()?"_blank":""}}"
                                   href="{{url($menu->getLinks())}}">{!! $menu->getTitle() !!}</a>
                            </li>
                        @endif
                    @endforeach

                </ul>
            </div>
        </div>
    </nav>
</div>
@push("after_scripts")
    <script>
        // $(document).ready(() => {
        //     $(".peer").hover((e) => {
        //         $(e.target).find(".hover-menu").css("display", "block")
        //     })
        // })
    </script>
@endpush