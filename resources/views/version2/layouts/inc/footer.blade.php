<div class="footer3 flex flex-col gap-4 xl:!px-[100px] md:!px-[100px] px-[10px] pt-[60px] pb-[28px]">
    <div class="w-full">
        <div class="flex gap-4 flex-col">
            <img src="{{v2_url('images/logo-footer.png')}}" class="w-[100px]">
            <div class="flex gap-4">
                <a href="https://www.facebook.com/getfly">
                    <img src="{{v2_url('images/fb.png')}}" class="w-10 h-10">
                </a>
                <a href="https://www.youtube.com/channel/UClylK-XPLzglM4olQ9TJdxQ">
                    <img src="{{v2_url('images/Youtube.png')}}" class="w-10 h-10">
                </a>
                <a href="https://www.instagram.com/getfly_crm/">
                    <img src="{{v2_url('images/insta.png')}}" class="w-10 h-10">
                </a>
            </div>
        </div>
    </div>
    <div class="flex xl:gap-24 md:gap-24 gap-4 w-full justify-between p-0 xl:flex-row md:flex-row flex-col">
        <div class="flex flex-col gap-7">
            <div class="w-full flex gap-4 flex-col">
                <p class="text-[#BCD7EF] text-base font-medium">Văn phòng Hà Nội</p>
                <div class="flex gap-2">
                    <img src="{{v2_url('images/checkin.png')}}" class="w-6 h-6">
                    <p class="text-white font-medium">Tầng 7, Tòa nhà Hoa Cương, Số 18, Ngõ 11, Phố Thái Hà, Quận Đống Đa, TP Hà Nội</p>
                </div>
                <div class="flex gap-2">
                    <img src="{{v2_url('images/phones.png')}}" class="w-6 h-6">
                    <p class="text-white font-medium">Hotline hỗ trợ: <a href="tel:02462627662">(024) 6262 7662</a></p>
                    <p class="text-white font-medium">Hotline kinh doanh: <a href="tel:0965593953">0965 593 953</a></p>
                </div>
            </div>
            <div class="w-full flex gap-4 flex-col">
                <p class="text-[#BCD7EF] text-base font-medium">Văn phòng HCM</p>
                <div class="flex gap-2">
                    <img src="{{v2_url('images/checkin.png')}}" class="w-6 h-6">
                    <p class="text-white font-medium">43D/9 Hồ Văn Huê, Phường 09, Quận Phú Nhuận, TP Hồ Chí Minh</p>
                </div>
                <div class="flex gap-2">
                    <img src="{{v2_url('images/phones.png')}}" class="w-6 h-6">
                    <p class="text-white font-medium">Hotline hỗ trợ: <a href="tel:02862856395">(028) 6285 6395</a></p>
                    <p class="text-white font-medium">Hotline kinh doanh: 0965 593 953</p>
                </div>
                <div class="flex gap-2">
                    <img src="{{v2_url('images/mail.png')}}" class="w-6 h-6">
                    <p class="text-white font-medium"><a href="mail:contact@getflycrm.com">contact@getflycrm.com</a></p>
                </div>
            </div>
        </div>
        <div class="flex flex-col">
            <div class="flex gap-8 xl:flex-row md:flex-row flex-col">
                <div class="flex flex-col gap-4 w-[194px]">
                    <p class="text-[#7EB0DC] font-medium">Giới thiệu</p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/ve-getfly.html">Về Getfly</a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/goc-bao-chi/tin-getfly/thong-bao/quy-dinh-ve-cung-cap-va-su-dung-dich-vu-n1544.html?utm_source=https://getfly.vn/">
                            Quy trình cung cấp và sử dụng dịch vụ
                        </a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/goc-bao-chitin-getfly/thong-bao/chinh-sach-bao-mat-thong-tin-n1624.html?utm_source=https://getfly.vn/">Chính sách bảo mật</a>
                    </p>
                </div>
                <div class="flex flex-col gap-4 w-[194px]">
                    <p class="text-[#7EB0DC] font-medium">Sản phẩm</p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/page/quan-ly-danh-sach-khach-hang-giup-doanh-nghiep-khai-thac-toi-da-tiem-nang-data">Quản lý khách hàng</a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/page/thiet-lap-do-luong-kpi-tu-dong-va-chinh-xac">Đo lường KPI</a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/page/nuoi-duong-khach-hang-tiem-nang-tu-dong-nho-ung-dung-marketing-automation">Marketing Automation</a>
                    </p>
                </div>
                <div class="flex flex-col gap-4 w-[194px]">
                    <p class="text-[#7EB0DC] font-medium">Tài nguyên</p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/bang-gia.html">Báo giá</a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/i41-goc-bao-chi/tin-getfly.html">Tin tức</a>
                    </p>
                    <p class="text-white font-medium">
                        <a href="https://getfly.vn/lien-he.html">Liên hệ</a>
                    </p>
                </div>
            </div>
            <div class="flex w-full pt-12 justify-between xl:flex-row md:flex-row flex-col xl:items-end md:items-end items-start">
                <img src="{{v2_url('images/bocongthuong.png')}}" class="h-10">
                <img src="{{v2_url('images/iso2.png')}}" class="w-20 h-10">
                <img src="{{v2_url('images/ukas.png')}}" class="h-16">
                <div class="flex gap-2 flex-col">
                    <p class="text-[#7EB0DC]">Tải ứng dụng này</p>
                    <div class="flex gap-2">
                        <a href="https://play.google.com/store/apps/details?id=com.getfly.crm.v6">
                            <img src="{{v2_url('images/googleplay2.png')}}">
                        </a>
                        <a href="https://apps.apple.com/vn/app/getfly-crm/id1616832691?l=vi">
                            <img src="{{v2_url('images/appstore2.png')}}">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="w-full text-left py-[10px] border-t border-[#E7E6F0]">
        <p class="text-[#A4C8EA] font-medium">© Copyright Getfly CRM 2024 - Giải pháp quản lý & chăm sóc khách hàng
            dành cho SMEs</p>
    </div>
</div>