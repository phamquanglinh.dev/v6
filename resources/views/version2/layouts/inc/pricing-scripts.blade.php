<script>
    window.addEventListener('load', function() {
        // Kéo trang lên đầu sau khi trang đã tải lại hoàn toàn
        window.scrollTo(0, 0);
    });

    // Get the button
    let mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {onScrollToShowBtn()};

    function onScrollToShowBtn() {

        if (document.body.scrollTop > 2000 || document.documentElement.scrollTop > 2000) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }


    function onScrollToTop(duration) {

        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    }

</script>
<script>
    function openPricingTab(tab_id) {
        var i;
        var x = document.getElementsByClassName("pricing-tab");
        const btnPricing1=document.getElementById("btn-1")
        const btnPricing2=document.getElementById("btn-2")
        if(tab_id=="pricing1"){
            if(!btnPricing1.classList.contains("active-pricing"))
            {
                btnPricing1.classList.add("active-pricing");
                btnPricing2.classList.remove("active-pricing");
            }

        }else{
            if(!btnPricing2.classList.contains("active-pricing"))
            {
                btnPricing2.classList.add("active-pricing");
                btnPricing1.classList.remove("active-pricing");
            }

        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        document.getElementById(tab_id).style.display = "block";
        document.getElementById(tab_id).classList.add = "active_pricing";
    }
</script>
<script>
    //PC
    const btnPay1=document.getElementById("btn-pay1")//10
    const btnPay2=document.getElementById("btn-pay2")//30
    const btnPay3=document.getElementById("btn-pay3")//50
    const tr0= document.getElementById("tr0")
    const tr1= document.getElementById("tr1")
    const tr2= document.getElementById("tr2")
    const tr3= document.getElementById("tr3")
    const tr4= document.getElementById("tr4")
    const tr5= document.getElementById("tr5")
    const tr6= document.getElementById("tr6")


    btnPay1.addEventListener("click",()=>{
        btnPay1.classList.add("active")
        btnPay2.classList.remove("active")
        btnPay3.classList.remove("active")

        animateNumber(600000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr0.textContent = formattedNumber
        })
        animateNumber(7200000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr1.textContent = formattedNumber
        })

        animateNumber(500000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr2.textContent = formattedNumber
        })

        animateNumber(1, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr3.textContent = formattedNumber
        })

        animateNumber(10000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr4.textContent = formattedNumber
        })

        animateNumber(100000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr5.textContent = formattedNumber
        })

        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr6.textContent = formattedNumber
        })
    })
    btnPay2.addEventListener("click",()=>{
        btnPay2.classList.add("active")
        btnPay1.classList.remove("active")
        btnPay3.classList.remove("active")
        animateNumber(1068000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr0.textContent = formattedNumber
        })
        animateNumber(12816000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr1.textContent = formattedNumber
        })
        animateNumber(1000000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr2.textContent = formattedNumber
        })
        animateNumber(2, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr3.textContent = formattedNumber
        })
        animateNumber(30000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr4.textContent = formattedNumber
        })
        animateNumber(50000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr5.textContent = formattedNumber
        })
        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr6.textContent = formattedNumber
        })
    })
    btnPay3.addEventListener("click",()=>{
        btnPay3.classList.add("active")
        btnPay1.classList.remove("active")
        btnPay2.classList.remove("active")
        animateNumber(1568000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr0.textContent = formattedNumber
        })
        animateNumber(18816000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr1.textContent = formattedNumber
        })
        animateNumber(1000000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr2.textContent = formattedNumber
        })
        animateNumber(2, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr3.textContent = formattedNumber
        })
        animateNumber(50000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr4.textContent = formattedNumber
        })
        animateNumber(35000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr5.textContent = formattedNumber
        })
        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            tr6.textContent = formattedNumber
        })
    })

    //MOBILE
    const mBtnPay1=document.getElementById("m-btn-pay1")//10
    const mBtnPay2=document.getElementById("m-btn-pay2")//30
    const mBtnPay3=document.getElementById("m-btn-pay3")//50
    const mTr0= document.getElementById("m-tr0")
    const mTr1= document.getElementById("m-tr1")
    const mTr2= document.getElementById("m-tr2")
    const mTr3= document.getElementById("m-tr3")
    const mTr4= document.getElementById("m-tr4")
    const mTr5= document.getElementById("m-tr5")
    const mTr6= document.getElementById("m-tr6")
    mBtnPay1.addEventListener("click",()=>{
        mBtnPay1.classList.add("active")
        mBtnPay2.classList.remove("active")
        mBtnPay3.classList.remove("active")

        animateNumber(600000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr0.textContent = formattedNumber
        })
        animateNumber(7200000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr1.textContent = formattedNumber
        })

        animateNumber(500000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr2.textContent = formattedNumber
        })

        animateNumber(1, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr3.textContent = formattedNumber
        })

        animateNumber(10000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr4.textContent = formattedNumber
        })

        // animateNumber(100000, 500, 0, function (number) {
        // const formattedNumber = number.toLocaleString()
        // mTr5.textContent = formattedNumber
        //  })

        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr5.textContent = formattedNumber
        })
    })
    mBtnPay2.addEventListener("click",()=>{
        mBtnPay2.classList.add("active")
        mBtnPay1.classList.remove("active")
        mBtnPay3.classList.remove("active")
        animateNumber(1068000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr0.textContent = formattedNumber
        })
        animateNumber(12816000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr1.textContent = formattedNumber
        })
        animateNumber(1000000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr2.textContent = formattedNumber
        })
        animateNumber(2, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr3.textContent = formattedNumber
        })
        animateNumber(30000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr4.textContent = formattedNumber
        })
        // animateNumber(50000, 500, 0, function (number) {
        // const formattedNumber = number.toLocaleString()
        // mTr5.textContent = formattedNumber
        //  })
        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr5.textContent = formattedNumber
        })
    })
    mBtnPay3.addEventListener("click",()=>{
        mBtnPay3.classList.add("active")
        btnPay1.classList.remove("active")
        mBtnPay2.classList.remove("active")
        animateNumber(1568000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr0.textContent = formattedNumber
        })
        animateNumber(18816000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr1.textContent = formattedNumber
        })
        animateNumber(1000000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr2.textContent = formattedNumber
        })
        animateNumber(2, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr3.textContent = formattedNumber
        })
        animateNumber(50000, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr4.textContent = formattedNumber
        })
        // animateNumber(35000, 500, 0, function (number) {
        // const formattedNumber = number.toLocaleString()
        // mTr5.textContent = formattedNumber
        //  })
        animateNumber(12, 500, 0, function (number) {
            const formattedNumber = number.toLocaleString()
            mTr5.textContent = formattedNumber
        })
    })


</script>
<script>
    function animateNumber(finalNumber, duration = 5000, startNumber = 0, callback) {
        let currentNumber = startNumber
        const interval = window.setInterval(updateNumber, 17)
        function updateNumber() {
            if (currentNumber >= finalNumber) {
                clearInterval(interval)
            } else {
                let inc = Math.ceil(finalNumber / (duration / 17))
                if (currentNumber + inc > finalNumber) {
                    currentNumber = finalNumber
                    clearInterval(interval)
                } else {
                    currentNumber += inc
                }
                callback(currentNumber)
            }
        }
    }

    const tblFeature=document.getElementById("table-feature");
    const btnFeature=document.getElementById("btn-feature");
    btnFeature.addEventListener("click", ()=>{
        tblFeature.classList.remove("h-[270px]")
        btnFeature.classList.add("hidden")
    });


</script>