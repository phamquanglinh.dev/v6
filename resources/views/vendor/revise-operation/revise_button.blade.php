@if ($crud->hasAccess('revise') && count($entry->revisionHistory))
    <a href="{{ url($crud->route.'/'.$entry->getKey().'/revise') }}" class="btn btn-sm btn-link"><i class="la la-history"></i> Lịch sử chỉnh sửa</a>
@endif
