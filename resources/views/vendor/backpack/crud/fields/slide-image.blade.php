{{--<input--}}
{{--        type="text"--}}
{{--        name="{{ $field['name'] }}"--}}
{{--        value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"--}}
{{--        @include('crud::fields.inc.attributes')--}}
{{-->--}}
<!-- text input -->
@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>

@include('crud::fields.inc.wrapper_end')
@push("crud_fields_scripts")
    <script>

    </script>
@endpush