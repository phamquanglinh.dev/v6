<!-- text input -->
@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
@include('crud::fields.inc.translatable_icon')

@if(isset($field['prefix']) || isset($field['suffix']))
    <div class="input-group"> @endif
        @if(isset($field['prefix']))
            <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div>
        @endif
        <input
                class="form-control"
                type="hidden"
                name="{{ $field['name'] }}"
                id="{{ $field['name'] }}"
                value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
                @include('crud::fields.inc.attributes')
        >
        <input class="youtube_val form-control">
        @if(isset($field['suffix']))
            <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div>
        @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div>
@endif
<div class="mt-2">
    <iframe id="video_{{$field['name']}}"
            class="rounded"
            src="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
            style="aspect-ratio: 16/9"></iframe>
</div>

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
    </div>
    @push("after_scripts")
        <script>
            $(document).ready(() => {
                $(".youtube_val").change((e) => {
                    getYoutubeId(e)
                })
                $(".youtube_val").keyup((e) => {
                    getYoutubeId(e)
                })
            })
            const getYoutubeId = (e) => {
                try {
                    console.log(e.target.value)
                    const url = new URL(e.target.value)
                    const id = url.searchParams.get("v")
                    if (id !== undefined) {
                        $("#video_{{$field['name']}}").attr("src", "https://www.youtube.com/embed/" + id)
                        $("#{{ $field['name'] }}").attr("value", "https://www.youtube.com/embed/" + id)
                    }
                } catch (e) {
                    console.log(e)
                }
            }
        </script>
    @endpush