@php
    use App\ViewModels\Admin\PageBuilderViewModel;
    /**
    * @var PageBuilderViewModel $pageBuilderViewModel
     */
    $page = $pageBuilderViewModel->getLandingPage();
    $forms = $pageBuilderViewModel->getForms();
@endphp
@extends("sites.shared.top_left")
@push("after_styles")
    <link href="https://unpkg.com/grapesjs/dist/css/grapes.min.css" rel="stylesheet">
    <script src="https://unpkg.com/grapesjs"></script>
    <script src="https://unpkg.com/grapesjs-preset-webpage@1.0.2"></script>
    <script src="https://unpkg.com/grapesjs-blocks-basic@1.0.1"></script>
    <script src="https://unpkg.com/grapesjs-plugin-forms@2.0.5"></script>
    <script src="https://unpkg.com/grapesjs-component-countdown@1.0.1"></script>
    <script src="https://unpkg.com/grapesjs-plugin-export@1.0.11"></script>
    <script src="https://unpkg.com/grapesjs-tabs@1.0.6"></script>
    <script src="https://unpkg.com/grapesjs-custom-code@1.0.1"></script>
    <script src="https://unpkg.com/grapesjs-touch@0.1.1"></script>
    <script src="https://unpkg.com/grapesjs-parser-postcss@1.0.1"></script>
    <script src="https://unpkg.com/grapesjs-tooltip@0.1.7"></script>
    <script src="https://unpkg.com/grapesjs-tui-image-editor@0.1.3"></script>
    <script src="https://unpkg.com/grapesjs-typed@1.0.5"></script>
    <script src="https://unpkg.com/grapesjs-style-bg@2.0.1"></script>
    <style>
        .container-fluid {
            height: 100%;
        }

        p {
            margin: 0 !important;
        }
    </style>
@endpush
@section("content")
    <div id="gjs" style="height: 100vh">

    </div>
    <script>
        {!! $page->getScript() !!}
    </script>
@endsection
@push("after_scripts")
    <script src="{{url("/admin-assets/js/i18n-vi.js")}}"></script>
    <script>
        document.title = "Page Builder :: {{$page->getTitle()}}"
    </script>
    <script src="https://use.fontawesome.com/83ac61cd0f.js"></script>
    <script type="text/javascript">
        let editor = grapesjs.init({
            allowScripts: 1,
            {{--assetManager: {--}}
                    {{--    storageType: '',--}}
                    {{--    storeOnChange: true,--}}
                    {{--    storeAfterUpload: true,--}}
                    {{--    upload: '{{route("page-builder-imageUpload")}}',--}}
                    {{--    assets: [],--}}
                    {{--    uploadFile: function (e) {--}}
                    {{--        let formData = new FormData();--}}
                    {{--        formData.append("file", e.target.files[0])--}}
                    {{--        formData.append("_token", "{{csrf_token()}}")--}}
                    {{--        console.log(formData)--}}
                    {{--        $.ajax({--}}
                    {{--            processData: false,--}}
                    {{--            contentType: false,--}}
                    {{--            dataType: 'json',--}}
                    {{--            type: "POST",--}}
                    {{--            enctype: 'multipart/form-data',--}}
                    {{--            url: "{{route("page-builder-imageUpload")}}",--}}
                    {{--            data: formData,--}}
                    {{--            success: function (data) {--}}
                    {{--                addImage(data.image)--}}
                    {{--            },--}}
                    {{--            error: function (e) {--}}

                    {{--            }--}}
                    {{--        });--}}
                    {{--    }--}}
                    {{--},--}}
            height: '100%',
            container: '#gjs',
            showOffsets: false,
            selectorManager: {componentFirst: false},
            styleManager: {
                sectors: [{
                    name: 'General',
                    properties: [
                        {
                            extend: 'float',
                            type: 'radio',
                            default: 'none',
                            options: [
                                {value: 'none', className: 'la la-times'},
                                {value: 'left', className: 'la la-align-left'},
                                {value: 'right', className: 'la la-align-right'}
                            ],
                        },
                        'display',
                        {extend: 'position', type: 'select'},
                        'top',
                        'right',
                        'left',
                        'bottom',
                    ],
                }, {
                    name: 'Dimension',
                    open: false,
                    properties: [
                        'width',
                        {
                            id: 'flex-width',
                            type: 'integer',
                            name: 'Width',
                            units: ['px', '%'],
                            property: 'flex-basis',
                            toRequire: 1,
                        },
                        'height',
                        'max-width',
                        'min-height',
                        'margin',
                        'padding'
                    ],
                }, {
                    name: 'Typography',
                    open: false,
                    properties: [
                        'font-family',
                        'font-size',
                        'font-weight',
                        'letter-spacing',
                        'color',
                        'line-height',
                        {
                            extend: 'text-align',
                            options: [
                                {id: 'left', label: 'Left', className: 'las la-align-left'},
                                {id: 'center', label: 'Center', className: 'las la-align-center'},
                                {id: 'right', label: 'Right', className: 'las la-align-right'},
                                {id: 'justify', label: 'Justify', className: 'las la-align-justify'}
                            ],
                        },
                        {
                            property: 'text-decoration',
                            type: 'radio',
                            default: 'none',
                            options: [
                                {id: 'none', label: 'None', className: 'las la-times'},
                                {id: 'underline', label: 'underline', className: 'la la-underline'},
                                {id: 'line-through', label: 'Line-through', className: 'la la-strikethrough'}
                            ],
                        },
                        'text-shadow'
                    ],
                }, {
                    name: 'Decorations',
                    open: false,
                    properties: [
                        'opacity',
                        'border-radius',
                        'border',
                        'box-shadow',
                        'background', // { id: 'background-bg', property: 'background', type: 'bg' }
                    ],
                }, {
                    name: 'Extra',
                    open: false,
                    buildProps: [
                        'transition',
                        'perspective',
                        'transform'
                    ],
                }, {
                    name: 'Flex',
                    open: false,
                    properties: [{
                        name: 'Flex Container',
                        property: 'display',
                        type: 'select',
                        defaults: 'block',
                        list: [
                            {value: 'block', name: 'Disable'},
                            {value: 'flex', name: 'Enable'}
                        ],
                    }, {
                        name: 'Flex Parent',
                        property: 'label-parent-flex',
                        type: 'integer',
                    }, {
                        name: 'Direction',
                        property: 'flex-direction',
                        type: 'radio',
                        defaults: 'row',
                        list: [{
                            value: 'row',
                            name: 'Row',
                            className: 'las la-align-justify',
                            title: 'Row',
                        }, {
                            value: 'row-reverse',
                            name: 'Row reverse',
                            className: 'icons-flex icon-dir-row-rev',
                            title: 'Row reverse',
                        }, {
                            value: 'column',
                            name: 'Column',
                            title: 'Column',
                            className: 'las la-columns',
                        }, {
                            value: 'column-reverse',
                            name: 'Column reverse',
                            title: 'Column reverse',
                            className: 'icons-flex icon-dir-col-rev',
                        }],
                    }, {
                        name: 'Justify',
                        property: 'justify-content',
                        type: 'radio',
                        defaults: 'flex-start',
                        list: [{
                            value: 'flex-start',
                            className: 'las la-align-left',
                            title: 'Start',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'las la-align-right',
                        }, {
                            value: 'space-between',
                            title: 'Space between',
                            className: 'las la-align-justify',
                        }, {
                            value: 'space-around',
                            title: 'Space around',
                            className: 'las la-align-center p-1',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'las la-align-center',
                        }],
                    }, {
                        name: 'Align',
                        property: 'align-items',
                        type: 'radio',
                        defaults: 'center',
                        list: [{
                            value: 'flex-start',
                            title: 'Start',
                            className: 'las la-align-left',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'las la-align-right',
                        }, {
                            value: 'stretch',
                            title: 'Stretch',
                            className: 'las la-align-justify',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'ilas la-align-center',
                        }],
                    }, {
                        name: 'Flex Children',
                        property: 'label-parent-flex',
                        type: 'integer',
                    }, {
                        name: 'Order',
                        property: 'order',
                        type: 'integer',
                        defaults: 0,
                        min: 0
                    }, {
                        name: 'Flex',
                        property: 'flex',
                        type: 'composite',
                        properties: [{
                            name: 'Grow',
                            property: 'flex-grow',
                            type: 'integer',
                            defaults: 0,
                            min: 0
                        }, {
                            name: 'Shrink',
                            property: 'flex-shrink',
                            type: 'integer',
                            defaults: 0,
                            min: 0
                        }, {
                            name: 'Basis',
                            property: 'flex-basis',
                            type: 'integer',
                            units: ['px', '%', ''],
                            unit: '',
                            defaults: 'auto',
                        }],
                    }, {
                        name: 'Align',
                        property: 'align-self',
                        type: 'radio',
                        defaults: 'auto',
                        list: [{
                            value: 'auto',
                            name: 'Auto',
                        }, {
                            value: 'flex-start',
                            title: 'Start',
                            className: 'icons-flex icon-al-start',
                        }, {
                            value: 'flex-end',
                            title: 'End',
                            className: 'icons-flex icon-al-end',
                        }, {
                            value: 'stretch',
                            title: 'Stretch',
                            className: 'icons-flex icon-al-str',
                        }, {
                            value: 'center',
                            title: 'Center',
                            className: 'las la-align-center',
                        }],
                    }]
                }
                ],
            },
            dragMode: 'translate',
            plugins: [
                'gjs-blocks-basic',
                'grapesjs-plugin-forms',
                'grapesjs-component-countdown',
                'grapesjs-plugin-export',
                'grapesjs-tabs',
                'grapesjs-custom-code',
                'grapesjs-touch',
                'grapesjs-parser-postcss',
                'grapesjs-tooltip',
                'grapesjs-tui-image-editor',
                'grapesjs-typed',
                'grapesjs-style-bg',
                'grapesjs-preset-webpage',
            ],
            pluginsOpts: {
                'gjs-blocks-basic': {flexGrid: true},
                'grapesjs-tui-image-editor': {
                    script: [
                        'https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.7/fabric.min.js',
                        'https://uicdn.toast.com/tui.code-snippet/v1.5.2/tui-code-snippet.min.js',
                        'https://uicdn.toast.com/tui-color-picker/v2.2.7/tui-color-picker.min.js',
                        'https://uicdn.toast.com/tui-image-editor/v3.15.2/tui-image-editor.min.js'
                    ],
                    style: [
                        'https://uicdn.toast.com/tui-color-picker/v2.2.7/tui-color-picker.min.css',
                        'https://uicdn.toast.com/tui-image-editor/v3.15.2/tui-image-editor.min.css',
                    ],
                },
                'grapesjs-tabs': {
                    tabsBlock: {category: 'Extra'}
                },
                'grapesjs-typed': {
                    block: {
                        category: 'Extra',
                        content: {
                            type: 'typed',
                            'type-speed': 40,
                            strings: [
                                'Text row one',
                                'Text row two',
                                'Text row three',
                            ],
                        }
                    }
                },
                'grapesjs-preset-webpage': {
                    modalImportTitle: 'Import Template',
                    modalImportLabel: '<div style="margin-bottom: 10px; font-size: 13px;">Paste here your HTML/CSS and click Import</div>',
                    modalImportContent: function (editor) {
                        return editor.getHtml() + '<style>' + editor.getCss() + '</style>'
                    },
                },
            },
        });
        editor.Panels.addButton('options',
            [{
                id: 'save-db',
                className: 'las la-save',
                command: 'save-db',
                attributes: {
                    title: 'Save Changes'
                }
            }]
        );
        editor.Commands.add('save-db', {
            run: function (editor, sender) {
                sender && sender.set('active', 0); // turn off the button
                editor.store();
                let htmlData = editor.getHtml().replace(/<script.*>.*<\/script>/ims, " ");
                let scriptData = editor.getJs();
                let cssData = editor.getCss();
                $.post("{{route("page-builder-save")}}", {
                    "_token": "{{csrf_token()}}",
                    "htmlData": htmlData,
                    "cssData": cssData,
                    "scriptData": scriptData,
                    "id": {{$page->getId()}},
                    success: function () {
                        setTimeout(() => {
                            window.open("{{url($page->getPageUrl())}}");
                        }, 1000)
                    },
                });
            }
        });
        editor.I18n.addMessages({
            en: vietnamTrans,
        });
        editor.BlockManager.add('list-with-check', {
            category: "Bà Hà Vlog",
            label: 'Check List',
            media: '<i class="las la-list-alt la-4x"></i>',
            content: `@include("sites.blocks.listItem")`,
        });
        editor.BlockManager.add('register-button',
            {
                label: "Nút đăng ký ngay",
                category: "Bà Hà Vlog",
                media: '<i class="las la-hand-point-right la-4x"></i>',
                content: `@include("sites.blocks.registerButton")`
            })
        editor.BlockManager.add('area-block', {
            category: "Bà Hà Vlog",
            label: "Area Block",
            media: '<i class="las la-underline la-4x"></i>',
            content: `<div style="width: 100%;min-height: 5rem"></div>`
        })
        editor.BlockManager.add("paginate-text", {
            category: "Bà Hà Vlog",
            label: "Chữ phân chia trang",
            open: true,
            media: '<i class="las la-grip-lines la-4x"></i>',
            content: `@include("sites.blocks.paginate-text")`,
        })
        editor.BlockManager.add("three-card-with-title", {
            category: "Bà Hà Vlog",
            label: "3 Card kèm tiêu đề",
            open: true,
            media: `<img src="https://devpractical.com/public/2021/div-3-columns-equal.png" style="width: 2rem">`,
            content: `@include("sites.blocks.three-card-with-title")`
        })
        editor.BlockManager.add("six-card-with-title", {
            category: "Bà Hà Vlog",
            label: "6 Card kèm tiêu đề",
            open: true,
            media: `<img src="https://devpractical.com/public/2021/div-3-columns-equal.png" style="width: 2rem">`,
            content: `@include("sites.blocks.six-card-with-title")`
        })
        editor.BlockManager.add("heading-text", {
            category: "Bà Hà Vlog",
            label: "Tiêu đề lớn ( có nền)",
            media: `<i class="las la-bars la-5x"></i>`,
            content: `@include("sites.blocks.heading-text")`
        })
        editor.BlockManager.add("heading-with-description", {
            category: "Bà Hà Vlog",
            label: "Tiêu đề + mô tả ngắn",
            media: `<i class="las la-bars la-5x"></i>`,
            content: `@include("sites.blocks.heading-with-description")`
        })
        editor.BlockManager.add("portfolio", {
            category: "Bà Hà Vlog",
            label: "Portfolio",
            media: `<i class="las la-user-alt la-5x"></i>`,
            content: `@include("sites.blocks.portfolio")`
        })
        window.onload = () => {
            window.editor = editor
            window.editor.DomComponents.clear(); // Clear components
            window.editor.CssComposer.clear();  // Clear styles
            window.editor.UndoManager.clear(); // Clear undo history
            window.editor.addComponents(`{!!  $page->getBody() !!}`)
            window.editor.addStyle(`{!!  $page->getStyle() !!}`)
            window.editor.on("component:selected", (component) => {
                component.set("resizable", true);
            });
        };
        editor.on("component:selected", (component) => {
            component.set("resizable", true);
        });
        const addImage = image => {
            window.editor.AssetManager.add(image)
        }
    </script>

@endpush