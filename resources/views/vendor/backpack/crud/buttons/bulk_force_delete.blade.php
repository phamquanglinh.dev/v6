@if ($crud->hasAccess('bulkForceDelete') && $crud->get('list.bulkActions'))
    <a href="javascript:void(0)" onclick="bulkForceDeleteEntries(this)" class="btn btn-sm btn-secondary bulk-button"><i
                class="la la-eraser"></i>Xóa vĩnh viễn</a>
@endif

@push('after_scripts')
    <script>
        if (typeof bulkForceDeleteEntries != 'function') {
            function bulkForceDeleteEntries(button) {

                if (typeof crud.checkedItems === 'undefined' || crud.checkedItems.length == 0) {
                    new Noty({
                        type: "warning",
                        text: "<strong>{!! trans('backpack::crud.bulk_no_entries_selected_title') !!}</strong><br>{!! trans('backpack::crud.bulk_no_entries_selected_message') !!}"
                    }).show();

                    return;
                }
                var button = $(this);

                swal({
                    title: "{!! trans('backpack::base.warning') !!}",
                    text: 'Xóa vĩnh viễn những bản ghi trên, bạn sẽ KHÔNG THỂ khôi phục',
                    icon: "danger",
                    buttons: {
                        cancel: {
                            text: "{!! trans('backpack::crud.cancel') !!}",
                            value: null,
                            visible: true,
                            className: "bg-secondary",
                            closeModal: true,
                        },
                        delete: {
                            text: "Xóa vĩnh viễn",
                            value: true,
                            visible: true,
                            className: "bg-danger",
                        }
                    },
                }).then((value) => {
                    if (value) {
                        var delete_route = "{{ url($crud->route) }}/bulk-force-delete";
                        $.ajax({
                            url: delete_route,
                            type: 'POST',
                            data: {entries: crud.checkedItems},
                            success: function (result) {
                                window.location.reload();
                            },
                            error: function (result) {
                                new Noty({
                                    type: "warning",
                                    text: "<strong>{!! trans('backpack::crud.bulk_delete_error_title') !!}</strong><br>{!! trans('backpack::crud.bulk_delete_error_message') !!}"
                                }).show();
                            }
                        });
                    }
                });
            }
        }
    </script>
@endpush
