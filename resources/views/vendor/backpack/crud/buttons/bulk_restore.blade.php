@if ($crud->hasAccess('bulkRestore') && $crud->get('list.bulkActions'))
    <a href="javascript:void(0)" onclick="bulkRestoreEntries(this)" class="btn btn-sm btn-secondary bulk-button"><i
                class="la la-sync-alt"></i>Khôi phục</a>
@endif

@push('after_scripts')
    <script>
        if (typeof bulkRestoreEntries != 'function') {
            function bulkRestoreEntries(button) {

                if (typeof crud.checkedItems === 'undefined' || crud.checkedItems.length == 0) {
                    new Noty({
                        type: "warning",
                        text: "<strong>{!! trans('backpack::crud.bulk_no_entries_selected_title') !!}</strong><br>{!! trans('backpack::crud.bulk_no_entries_selected_message') !!}"
                    }).show();

                    return;
                }
                var button = $(this);

                // show confirm message
                swal({
                    title: "{!! trans('backpack::base.warning') !!}",
                    text: 'Khôi phục những bản ghi trên',
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{!! trans('backpack::crud.cancel') !!}",
                            value: null,
                            visible: true,
                            className: "bg-secondary",
                            closeModal: true,
                        },
                        restore: {
                            text: "Khôi phục",
                            value: true,
                            visible: true,
                            className: "bg-success",
                        }
                    },
                }).then((value) => {
                    if (value) {
                        var delete_route = "{{ url($crud->route) }}/bulk-restore";
                        $.ajax({
                            url: delete_route,
                            type: 'POST',
                            data: {entries: crud.checkedItems},
                            success: function (result) {
                                window.location.reload();
                            },
                            error: function (result) {
                                new Noty({
                                    type: "warning",
                                    text: "<strong>{!! trans('backpack::crud.bulk_delete_error_title') !!}</strong><br>{!! trans('backpack::crud.bulk_delete_error_message') !!}"
                                }).show();
                            }
                        });
                    }
                });
            }
        }
    </script>
@endpush
