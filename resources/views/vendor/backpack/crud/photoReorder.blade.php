@php
    use App\ViewModels\Sites\TeamInfo\Object\PhotoListViewModel;
    /**
* @var PhotoListViewModel $photoListViewModel
 */
    $photos = $photoListViewModel->getPhotos();
@endphp
@extends(backpack_view("blank"))
@push("after_styles")

@endpush
@section("content")
    <div class="text-center mb-3">
        <h3>Sắp xếp album ảnh</h3>
    </div>

       <ul id="sortable" class="row" style="list-style: none">
           @foreach($photos as $photo)
               <li id="{{$photo->getId()}}" class="ui-state-default col-md-3 col-12">
                   <img class="p-3 mb-3 w-100 rounded shadow-lg" src="{{$photo->getImage()}}"/>
               </li>
           @endforeach
       </ul>
   </div>
@endsection
@push("after_scripts")
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#sortable").sortable({
                items: "> li",
                deactivate: () => {
                    $.post("{{route("photo-order")}}", {
                            _token: "{{csrf_token()}}",
                            photos: $("#sortable").sortable("toArray")
                        },
                        () => {
                            new Noty({
                                type: "success",
                                text: 'Sắp xếp thành công',
                            }).show();
                        })
                }
            });
        });
    </script>
@endpush