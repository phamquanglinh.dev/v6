@php
    use App\ViewModels\Sites\Banner\BannerListViewModel;
    /**
* @var BannerListViewModel $bannerListViewModel
 */
    $banners= $bannerListViewModel->getBanners()
@endphp
@extends(backpack_view("blank"))
@push("after_styles")

@endpush
@section("content")
    <div class="text-center mb-3">
        <h3>Sắp xếp banner</h3>
    </div>

    <ul id="sortable" class="row" style="list-style: none">
        @foreach($banners as $banner)
            <li id="{{$banner->getId()}}" class="ui-state-default col-md-3 col-12">
                <img class="p-3 mb-3 w-100 rounded shadow-lg" src="{{$banner->getImage()}}"/>
            </li>
        @endforeach
    </ul>
    </div>
@endsection
@push("after_scripts")
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#sortable").sortable({
                items: "> li",
                deactivate: () => {
                    $.post("{{route("banner-order")}}", {
                            _token: "{{csrf_token()}}",
                            banners: $("#sortable").sortable("toArray")
                        },
                        () => {
                            new Noty({
                                type: "success",
                                text: 'Sắp xếp thành công',
                            }).show();
                        })
                }
            });
        });
    </script>
@endpush