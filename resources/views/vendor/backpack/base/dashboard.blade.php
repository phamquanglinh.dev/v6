@extends(backpack_view('blank'))
@section('content')
    @if(hasAccess('manager'))
        @include("vendor.backpack.base.inc.dashboard-content")
        @include("sites.shared.shortcut")
    @endif
@endsection
