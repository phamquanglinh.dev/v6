{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i--}}
{{--                class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>--}}


@if(hasAccess("manager"))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-home"></i>Cài đặt trang chủ</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('feature') }}"><i
                            class="nav-icon la la-feather"></i>
                    Tính năng nổi bật</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('feedback') }}"><i
                            class="nav-icon la la-comment"></i>
                    Phản hồi</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('customer') }}"><i
                            class="nav-icon la la-toolbox"></i>
                    Khách hàng tiêu biểu</a></li>

        </ul>
    </li>
@endif
@if(hasAccess("manager")||hasAccess("hr"))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-newspaper-o"></i>Tin tức</a>
        <ul class="nav-dropdown-items">
            @if(hasAccess("manager"))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('category') }}"><i
                                class="nav-icon la la-list"></i> Danh
                        mục tin tức</a></li>
            @endif
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('news') }}"><i
                            class="nav-icon la la-newspaper"></i> Tin
                    tức</a></li>

        </ul>
    </li>
@endif
@if(hasAccess("manager"))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-coins"></i>Trang báo giá</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('module') }}"><i
                            class="nav-icon la la-boxes"></i> Module</a>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('pricing-of-feature') }}"><i
                            class="nav-icon la la-list"></i> Tính năng bảng giá</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('pricing') }}"><i
                            class="nav-icon la la-coins"></i>Các
                    Gói</a></li>

        </ul>
    </li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-code-branch"></i>Hệ sinh thái</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('partner-type') }}"><i
                            class="nav-icon la la-list"></i>
                    Phân loại đối tác</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('partner') }}"><i
                            class="nav-icon la la-building"></i>
                    Đại lý(Đối tác)</a></li>

        </ul>
    </li>
@endif

@if(hasAccess("hr"))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i>Tuyển dụng</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('recruitment') }}"><i
                            class="nav-icon la la-pager"></i>
                    Tuyển dụng</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('room') }}"><i
                            class="nav-icon la la-box"></i>
                    Phòng ban</a>
            </li>
        </ul>
    </li>

    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-feather-alt"></i>Cuộc sống Getfly</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('team-info/1/edit') }}"><i
                            class="nav-icon la la-info-circle"></i>
                    Thông tin</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('photo') }}"><i
                            class="nav-icon la la-images"></i>
                    Album ảnh</a></li>
        </ul>
    </li>
@endif

@if(hasAccess("admin"))

    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> Người
            dùng</a>
    </li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-cogs"></i>Cài đặt</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('menu') }}"><i
                            class="nav-icon la la-list"></i>
                    Menu</a>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('mega-menu') }}"><i
                            class="nav-icon la la-tablet-alt"></i>MegaTab</a></li>

            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('site-info/1/edit') }}"><i
                            class="nav-icon la la-cogs"></i> Thông tin trang</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('log') }}"><i
                            class="nav-icon la la-terminal"></i>Log</a>

        </ul>
    </li>
@endif
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('banner') }}"><i
                class="nav-icon la la-images"></i>
        Banner</a></li>

@if(hasAccess("manager"))
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('register-form') }}"><i
                    class="nav-icon la la-wpforms"></i>Opt-in Form</a></li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-pager"></i>Trang</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('solution') }}"><i
                            class="nav-icon la la-cog"></i> Giải
                    pháp</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('sub-page') }}"><i
                            class="nav-icon la la-newspaper-o"></i>Trang
                    con</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('page-feature') }}"><i
                            class="nav-icon las la-hand-point-right"></i> Trang tính năng</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('landing-page') }}"><i
                            class="nav-icon la la-pager"></i> Landing pages</a></li>
        </ul>
    </li>
@endif
@if(hasAccess('manager'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-pager"></i>Version 2</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('v2/pricing') }}"><i
                            class="nav-icon la la-cog"></i>Cài đặt báo giá</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('v2/home-page') }}"><i
                            class="nav-icon la la-newspaper-o"></i>Trang
                    chủ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('v2-partner') }}"><i
                            class="nav-icon la la-users"></i> KH-Đối tác</a></li>
        </ul>
    </li>

@endif