@php
    use App\ViewModels\Admin\DashboardViewModel;
    /**
* @var  DashboardViewModel $dashboardViewModel
 */
@endphp

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card border-0 text-white bg-primary">
                    <a href="{{url("admin/user")}}" class="nav-link p-0 m-0 text-white">
                        <div class="card-body">
                            <div class="text-value">{{$dashboardViewModel->getQuantity()->getUsers()}}</div>
                            <div>Người dùng hoạt động</div>
                        </div>
                    </a>

                </div>
            </div>


            <div class="col-sm-6 col-lg-3">
                <div class="card border-0 text-white bg-success">
                    <a href="{{url("admin/news")}}" class="nav-link p-0 m-0 text-white">
                        <div class="card-body">
                            <div class="text-value">{{number_format($dashboardViewModel->getQuantity()->getNews())}}</div>
                            <div>Tin tức hiển thị</div>
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-sm-6 col-lg-3">
                <div class="card border-0 text-white bg-warning">
                    <a href="{{url("admin/feedback")}}" class="nav-link p-0 m-0 text-white">
                        <div class="card-body">
                            <div class="text-value">{{$dashboardViewModel->getQuantity()->getRecruitments()}}</div>
                            <div>Khách hàng phản hổi</div>
                        </div>
                    </a>

                </div>
            </div>


            <div class="col-sm-6 col-lg-3">
                <div class="card border-0 text-white bg-dark">
                    <a href="{{url("admin/recruitment")}}" class="nav-link p-0 m-0 text-white">
                        <div class="card-body">
                            <div class="text-value">{{$dashboardViewModel->getQuantity()->getRecruitments()}}</div>
                            <div>Tin tuyển dụng</div>
                        </div>
                    </a>

                </div>
            </div>

        </div>
    </div>

