{{-- This file is used to store topbar (left) items --}}

<li class="nav-item px-3 bg-blue text-white rounded">{{backpack_user()->getUserRole()}}</li>
<li class="nav-item px-3"><a class="nav-link" href="{{url("/admin/dashboard")}}">Bảng điều khiển</a></li>
<li class="nav-item px-3"><a class="nav-link" href="{{route("backpack.auth.logout")}}">Đăng xuất</a></li>
