@extends('sites.shared.layout')


@section('title')
    Không tìm thấy trang
@endsection

@section('content')
   <div class="container-fluid flex justify-center">
       <img src="https://cdn.dribbble.com/users/1175431/screenshots/6188233/404-error-dribbble-800x600.gif" class="w-1/2">
   </div>
    <div class="text-4xl font-bold text-center w-full pb-5">
        <div>KHÔNG TÌM THẤY TRANG</div>
    </div>
@endsection
