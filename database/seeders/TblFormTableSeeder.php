<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblFormTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_form')->delete();
        
        \DB::table('tbl_form')->insert(array (
            0 => 
            array (
                'id' => 1,
            'name' => 'Form đăng ký dùng thử(HN)',
                'link' => '<script
type="text/javascript"> (function () {
var r = window.document.referrer != "" ? window.document.referrer : window.location.origin;
var f = document.createElement("iframe");
f.setAttribute("src", "https://1.getfly.vn/api/forms/viewform/?key=f9myphxCNi6I2G3oaC7er9nhIetDyUXsA5TP8AZkCnig9bnWcg&referrer=" + r);
f.style.width = "100%";
f.style.height = "500px";
f.setAttribute("frameborder", "0");
f.setAttribute("marginheight", "0");
f.setAttribute("marginwidth", "0");
var s = document.getElementById("getfly_hn");
s.appendChild(f);
})(); </script>',
                'position' => 'dang_ky_hn',
                'created_at' => NULL,
                'updated_at' => '2023-07-03 07:41:39',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Form liên hệ tư vấn',
                'link' => '<div id="getfly-optin-form-iframe-1684912451"></div>
<script
type="text/javascript">            (function () {
var r = window.document.referrer != "" ? window.document.referrer : window.location.origin;
var regex = /(https?:\\/\\/.*?)\\//g;
var furl = regex.exec(r);
r = furl ? furl[0] : r;
var f = document.createElement("iframe");
const url_string = new URLSearchParams(window.location.search);
var utm_source, utm_campaign, utm_medium, utm_content, utm_term, utm_user, utm_account;
if ((!url_string.has(\'utm_source\') || url_string.get(\'utm_source\') == \'\') && document.cookie.match(new RegExp(\'utm_source\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_source\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_source\') != null ? "&utm_source=" + url_string.get(\'utm_source\') : "";
}
if ((!url_string.has(\'utm_campaign\') || url_string.get(\'utm_campaign\') == \'\') && document.cookie.match(new RegExp(\'utm_campaign\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_campaign\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_campaign\') != null ? "&utm_campaign=" + url_string.get(\'utm_campaign\') : "";
}
if ((!url_string.has(\'utm_medium\') || url_string.get(\'utm_medium\') == \'\') && document.cookie.match(new RegExp(\'utm_medium\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_medium\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_medium\') != null ? "&utm_medium=" + url_string.get(\'utm_medium\') : "";
}
if ((!url_string.has(\'utm_content\') || url_string.get(\'utm_content\') == \'\') && document.cookie.match(new RegExp(\'utm_content\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_content\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_content\') != null ? "&utm_content=" + url_string.get(\'utm_content\') : "";
}
if ((!url_string.has(\'utm_term\') || url_string.get(\'utm_term\') == \'\') && document.cookie.match(new RegExp(\'utm_term\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_term\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_term\') != null ? "&utm_term=" + url_string.get(\'utm_term\') : "";
}
if ((!url_string.has(\'utm_user\') || url_string.get(\'utm_user\') == \'\') && document.cookie.match(new RegExp(\'utm_user\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_user\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_user\') != null ? "&utm_user=" + url_string.get(\'utm_user\') : "";
}
if ((!url_string.has(\'utm_account\') || url_string.get(\'utm_account\') == \'\') && document.cookie.match(new RegExp(\'utm_account\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_account\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_account\') != null ? "&utm_account=" + url_string.get(\'utm_account\') : "";
}
f.setAttribute("src", "https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082&referrer=" + r);
f.style.width = "100%";
f.style.height = "480px";
f.setAttribute("frameborder", "0");
f.setAttribute("marginheight", "0");
f.setAttribute("marginwidth", "0");
var s = document.getElementById("getfly-optin-form-iframe-1684912451");
s.appendChild(f);
})();        </script>',
                'position' => 'lien-he.html',
                'created_at' => NULL,
                'updated_at' => '2023-07-03 07:39:15',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Form đăng ký tuyển dụng',
                'link' => '<iframe src="https://icdt.getflycrm.com/api/forms/viewform?key=jDmOIGJOZDdAb0v5lybSAguArYOM19jFhTxE4QnZ5uqCxZMkxN" width="100%" height="350px" frameborder="0" marginheight="0" marginwidth="0">Loading ...</iframe>',
                'position' => 'tuyen-dung.html',
                'created_at' => NULL,
                'updated_at' => '2023-07-03 07:32:13',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Form đăng ký đối tác',
                'link' => '<div id="getfly-optin-form-iframe-1687235714"></div>
<script
type="text/javascript">  
(function () {
var r = window.document.referrer != "" ? window.document.referrer : window.location.origin;
var regex = /(https?:\\/\\/.*?)\\//g;
var furl = regex.exec(r);
r = furl ? furl[0] : r;
var f = document.createElement("iframe");
const url_string = new URLSearchParams(window.location.search);
var utm_source, utm_campaign, utm_medium, utm_content, utm_term, utm_user, utm_account;
if ((!url_string.has(\'utm_source\') || url_string.get(\'utm_source\') == \'\') && document.cookie.match(new RegExp(\'utm_source\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_source\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_source\') != null ? "&utm_source=" + url_string.get(\'utm_source\') : "";
}
if ((!url_string.has(\'utm_campaign\') || url_string.get(\'utm_campaign\') == \'\') && document.cookie.match(new RegExp(\'utm_campaign\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_campaign\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_campaign\') != null ? "&utm_campaign=" + url_string.get(\'utm_campaign\') : "";
}
if ((!url_string.has(\'utm_medium\') || url_string.get(\'utm_medium\') == \'\') && document.cookie.match(new RegExp(\'utm_medium\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_medium\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_medium\') != null ? "&utm_medium=" + url_string.get(\'utm_medium\') : "";
}
if ((!url_string.has(\'utm_content\') || url_string.get(\'utm_content\') == \'\') && document.cookie.match(new RegExp(\'utm_content\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_content\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_content\') != null ? "&utm_content=" + url_string.get(\'utm_content\') : "";
}
if ((!url_string.has(\'utm_term\') || url_string.get(\'utm_term\') == \'\') && document.cookie.match(new RegExp(\'utm_term\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_term\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_term\') != null ? "&utm_term=" + url_string.get(\'utm_term\') : "";
}
if ((!url_string.has(\'utm_user\') || url_string.get(\'utm_user\') == \'\') && document.cookie.match(new RegExp(\'utm_user\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_user\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_user\') != null ? "&utm_user=" + url_string.get(\'utm_user\') : "";
}
if ((!url_string.has(\'utm_account\') || url_string.get(\'utm_account\') == \'\') && document.cookie.match(new RegExp(\'utm_account\' + \'=([^;]+)\')) != null) {
r += "&" + document.cookie.match(new RegExp(\'utm_account\' + \'=([^;]+)\'))[0];
} else {
r += url_string.get(\'utm_account\') != null ? "&utm_account=" + url_string.get(\'utm_account\') : "";
}
f.setAttribute("src", "https://icdt.getflycrm.com/api/forms/viewform?key=Pwu2bPjwbj6lP7TZiRFWzsyTNgkHf2LcaRyZhO9vEBjoWCvWXR&referrer=" + r);
f.style.width = "100%";
f.style.height = "460px";
f.setAttribute("frameborder", "0");
f.setAttribute("marginheight", "0");
f.setAttribute("marginwidth", "0");
var s = document.getElementById("getfly-optin-form-iframe-1687235714");
s.appendChild(f);
})();        </script>',
                'position' => 'thu-moi-hop-tac.html',
                'created_at' => NULL,
                'updated_at' => '2023-07-03 07:19:56',
            ),
            4 => 
            array (
                'id' => 5,
            'name' => 'Form đăng ký dùng thử(TPHCM)',
                'link' => '                <script
type="text/javascript"> (function () {
var r = window.document.referrer != "" ? window.document.referrer : window.location.origin;
var f = document.createElement("iframe");
f.setAttribute("src", "https://1.getfly.vn/api/forms/viewform/?key=lTdXfl9DPaesn4AkCmms2W1I3VVeHClaCZfwnJkoMP01shCrs2&referrer=" + r);
f.style.width = "100%";
f.style.height = "500px";
f.setAttribute("frameborder", "0");
f.setAttribute("marginheight", "0");
f.setAttribute("marginwidth", "0");
var s = document.getElementById("getfly_hcm");
s.appendChild(f);
})(); </script>',
                'position' => 'dang_ky_tphcm',
                'created_at' => NULL,
                'updated_at' => '2023-07-03 07:41:39',
            ),
        ));
        
        
    }
}