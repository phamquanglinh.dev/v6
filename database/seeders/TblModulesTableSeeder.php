<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblModulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_modules')->delete();
        
        \DB::table('tbl_modules')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Module tích hợp <br> quản lý fanpage',
                'url' => '#',
                'order' => 1,
                'created_at' => '2023-05-09 11:11:12',
                'updated_at' => '2023-05-23 08:24:56',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Module tích hợp <br> tổng đài điện thoại',
                'url' => '#',
                'order' => 2,
                'created_at' => '2023-05-09 11:11:45',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Module tích hợp <br> quản lý kho',
                'url' => '#',
                'order' => 3,
                'created_at' => '2023-05-09 11:35:46',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Module quản lý <br> nhân sự HRM',
                'url' => '#',
                'order' => 4,
                'created_at' => '2023-05-09 11:36:02',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Module quản lý <br> tài chính công nợ khách hàng',
                'url' => '#',
                'order' => 5,
                'created_at' => '2023-05-09 11:36:34',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Module chấm công GPS   hoặc <br> IP tĩnh',
                'url' => '#',
                'order' => 6,
                'created_at' => '2023-05-09 11:36:53',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Module bán lẻ POS',
                'url' => '#',
                'order' => 7,
                'created_at' => '2023-05-09 11:37:07',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Nâng cấp bảo mật',
                'url' => '#',
                'order' => 8,
                'created_at' => '2023-05-09 11:37:21',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Module quản lý <br> bảo hành',
                'url' => '#',
                'order' => 9,
                'created_at' => '2023-05-09 11:37:37',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Module quản lý <br> lịch đi tuyến',
                'url' => '#',
                'order' => 10,
                'created_at' => '2023-05-09 11:38:31',
                'updated_at' => '2023-05-09 04:51:05',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Module quản lý <br> điểm thưởng',
                'url' => '#',
                'order' => 11,
                'created_at' => '2023-05-09 11:38:31',
                'updated_at' => '2023-05-09 04:51:05',
            ),
        ));
        
        
    }
}