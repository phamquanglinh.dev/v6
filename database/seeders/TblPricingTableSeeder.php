<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblPricingTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_pricing')->delete();
        
        \DB::table('tbl_pricing')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Starter',
                'price' => 417000,
                'user_price' => 139000,
                'domain_target' => '0',
                'users' => '3',
                'customers' => 10000,
                'setup_time' => NULL,
                'created_at' => '2023-05-09 02:03:49',
                'updated_at' => '2023-05-09 03:20:47',
                'order' => 9999,
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Startup',
                'price' => 600000,
                'user_price' => 60000,
                'domain_target' => '0',
                'users' => '10',
                'customers' => 10000,
                'setup_time' => '1 Buổi',
                'created_at' => '2023-05-09 02:15:51',
                'updated_at' => '2023-05-09 02:25:53',
                'order' => 9999,
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Doanh Nghiệp Nhỏ',
                'price' => 1068000,
                'user_price' => 35600,
                'domain_target' => '0',
                'users' => '30',
                'customers' => 30000,
                'setup_time' => '2 Buổi',
                'created_at' => '2023-05-09 02:26:39',
                'updated_at' => '2023-05-09 02:28:54',
                'order' => 9999,
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'Doanh Nghiệp Vừa',
                'price' => 1568000,
                'user_price' => 31360,
                'domain_target' => '1',
                'users' => '50',
                'customers' => 50000,
                'setup_time' => '3 Buổi',
                'created_at' => '2023-05-09 02:28:49',
                'updated_at' => '2023-05-09 02:28:49',
                'order' => 9999,
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'Doanh Nghiệp Lớn',
                'price' => 0,
                'user_price' => 0,
                'domain_target' => '1',
                'users' => '>50',
                'customers' => 50000,
                'setup_time' => 'Liên Hệ',
                'created_at' => '2023-05-09 02:33:06',
                'updated_at' => '2023-05-09 02:33:06',
                'order' => 9999,
            ),
        ));
        
        
    }
}