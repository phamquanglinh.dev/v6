<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 0; $i < 15; $i++) {
            DB::table("tbl_customers")->insert([
                'name' => fake()->name,
                'logo' => "https://png.pngtree.com/template/20191219/ourmid/pngtree-digital-point-technology-logo-template-designs-vector-illustration-image_341408.jpg",
            ]);
        }
    }
}
