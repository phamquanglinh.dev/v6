<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i < 30; $i++) {
            DB::table("tbl_contacts")->insert([
                'branch' => rand(0, 1),
                'company_name' => fake("vi_VN")->company(),
                'contact_name' => fake("vi_VN")->name(),
                'contact_number' => fake("vi_VN")->phoneNumber(),
                'email' => fake("vi_VN")->companyEmail(),
                'address' => fake('vi_Vn')->address(),
                'message' => 'Hi, Mình muốn được tư vấn'
            ]);
        }
    }
}
