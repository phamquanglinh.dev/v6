<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblCustomersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_customers')->delete();
        
        \DB::table('tbl_customers')->insert(array (
            0 => 
            array (
                'id' => 35,
                'name' => 'Point',
                'logo' => 'https://png.pngtree.com/template/20191219/ourmid/pngtree-digital-point-technology-logo-template-designs-vector-illustration-image_341408.jpg',
                'disable' => 0,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 37,
                'name' => 'Meta',
                'logo' => 'assets/uploads/customers/images/50_download.png_1683250149.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:29:09',
                'updated_at' => '2023-05-05 01:29:09',
            ),
            2 => 
            array (
                'id' => 38,
                'name' => 'Tweeter',
                'logo' => 'assets/uploads/customers/images/42_twitter_logo_blue.png.twimg.768.png_1683250178.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:29:38',
                'updated_at' => '2023-05-05 01:29:38',
            ),
            3 => 
            array (
                'id' => 40,
                'name' => 'Pinterest',
                'logo' => 'assets/uploads/customers/images/13_unnamed.png_1683250297.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:31:37',
                'updated_at' => '2023-05-05 01:31:37',
            ),
            4 => 
            array (
                'id' => 41,
                'name' => 'Godady',
                'logo' => 'assets/uploads/customers/images/93_0x0.png_1683250358.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:32:38',
                'updated_at' => '2023-05-05 01:32:38',
            ),
            5 => 
            array (
                'id' => 45,
                'name' => 'Visa',
                'logo' => 'assets/uploads/customers/images/52_00cc119d8cdda451535b2bb7de8def9c.png_1683250568.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:36:08',
                'updated_at' => '2023-05-05 01:36:08',
            ),
            6 => 
            array (
                'id' => 46,
                'name' => 'Redbull',
                'logo' => 'assets/uploads/customers/images/76_red_bull.png_1683251640.png',
                'disable' => 0,
                'created_at' => '2023-05-05 01:54:00',
                'updated_at' => '2023-05-05 01:54:00',
            ),
            7 => 
            array (
                'id' => 47,
                'name' => 'Krafton',
                'logo' => 'assets/uploads/customers/images/31_Krafton_Full_Logo.jpg_1683254015.jpg',
                'disable' => 0,
                'created_at' => '2023-05-05 01:55:05',
                'updated_at' => '2023-05-05 02:33:35',
            ),
            8 => 
            array (
                'id' => 48,
                'name' => 'Dingtea',
                'logo' => 'assets/uploads/customers/images/48_tmp_gcs_full_5c89e01d76ec5735d7371947-2019-03-14-050118.png_1683252139.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:02:19',
                'updated_at' => '2023-05-05 02:34:03',
            ),
            9 => 
            array (
                'id' => 49,
                'name' => 'StarBucks',
                'logo' => 'assets/uploads/customers/images/90_ee90a3b4b8e155ee8685e6a84de8e1e94781324e.png_1683254229.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:37:09',
                'updated_at' => '2023-05-05 02:37:09',
            ),
            10 => 
            array (
                'id' => 50,
                'name' => 'Adidas',
                'logo' => 'assets/uploads/customers/images/73_download.png_1683254326.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:38:46',
                'updated_at' => '2023-05-05 02:38:46',
            ),
            11 => 
            array (
                'id' => 52,
                'name' => 'Nike',
                'logo' => 'assets/uploads/customers/images/52_Nike-logo-11.webp_1683254541.webp',
                'disable' => 0,
                'created_at' => '2023-05-05 02:42:21',
                'updated_at' => '2023-05-05 02:42:21',
            ),
            12 => 
            array (
                'id' => 53,
                'name' => 'HP',
                'logo' => 'assets/uploads/customers/images/3_logo-hp.png_1683254651.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:44:11',
                'updated_at' => '2023-05-05 02:44:11',
            ),
            13 => 
            array (
                'id' => 54,
                'name' => 'Samsung',
                'logo' => 'assets/uploads/customers/images/67_samsung-logo-300x300.png_1683254701.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:45:02',
                'updated_at' => '2023-05-05 02:45:02',
            ),
            14 => 
            array (
                'id' => 55,
                'name' => 'Hadilao',
                'logo' => 'assets/uploads/customers/images/52_download.png_1683254789.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:46:29',
                'updated_at' => '2023-05-05 02:46:29',
            ),
            15 => 
            array (
                'id' => 56,
                'name' => 'Mixigaming',
                'logo' => 'assets/uploads/customers/images/97_Mixigaming-Logo.jpg_1683254860.jpg',
                'disable' => 0,
                'created_at' => '2023-05-05 02:47:40',
                'updated_at' => '2023-05-05 02:47:40',
            ),
            16 => 
            array (
                'id' => 57,
                'name' => 'Mixer',
                'logo' => 'assets/uploads/customers/images/13_307570901_132127576240837_7919344831561297456_n.jpg_1683254942.jpg',
                'disable' => 0,
                'created_at' => '2023-05-05 02:49:02',
                'updated_at' => '2023-05-05 02:49:02',
            ),
            17 => 
            array (
                'id' => 58,
                'name' => 'Spotify',
                'logo' => 'assets/uploads/customers/images/92_unnamed.png_1683255139.png',
                'disable' => 0,
                'created_at' => '2023-05-05 02:52:19',
                'updated_at' => '2023-05-05 02:52:19',
            ),
            18 => 
            array (
                'id' => 59,
                'name' => 'Uniqlo',
                'logo' => 'uploads/customers/2023052307181877218800.png',
                'disable' => 0,
                'created_at' => '2023-05-23 07:18:18',
                'updated_at' => '2023-05-23 07:18:18',
            ),
        ));
        
        
    }
}