<?php

namespace Database\Seeders;

use App\ViewModels\Sites\Feedback\Object\FeedbackObject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeedbackVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $feedbackVideos = DB::table("tbl_feedback_video")->get();
        foreach ($feedbackVideos as $feedbackVideo) {
            $feedbackObject= new FeedbackObject(
                name: "Khách hàng",
                title: $feedbackVideo->title,
                note: $feedbackVideo->description,
                link: $feedbackVideo->link,
                content: $feedbackVideo->description,
                position: "Người dùng",
                active: $feedbackVideo->active,
                priority: $feedbackVideo->priority,
                type_feedback: 2,
                avatar: "https://media.istockphoto.com/id/1016744004/vector/profile-placeholder-image-gray-silhouette-no-photo.jpg?s=612x612&w=0&k=20&c=mB6A9idhtEtsFXphs1WVwW_iPBt37S2kJp6VpPhFeoA=",
                avatar_title: "https://media.istockphoto.com/id/1016744004/vector/profile-placeholder-image-gray-silhouette-no-photo.jpg?s=612x612&w=0&k=20&c=mB6A9idhtEtsFXphs1WVwW_iPBt37S2kJp6VpPhFeoA="
            );
            DB::table("tbl_feedback")->insert($feedbackObject->toArray());
        }
    }
}
