<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(TblFeaturesTableSeeder::class);
        $this->call(TblModulesTableSeeder::class);
        $this->call(TblPricingTableSeeder::class);
        $this->call(TblRoomsTableSeeder::class);
        $this->call(TblSiteInfoTableSeeder::class);
        $this->call(TeamInfoSeeder::class);
        $this->call(TblMenuTableSeeder::class);
        $this->call(TblPartnerTypeTableSeeder::class);
        DB::table("tbl_news")->where("order", 999)->update([
            "order" => 0
        ]);
    }
}
