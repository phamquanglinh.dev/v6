<?php

namespace Database\Seeders;

use App\Models\Recruitment;
use App\Models\Room;
use App\Repository\RoomRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class RecruitmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $rooms = Room::query()->get();
        for ($i = 0; $i < 30; $i++) {
            foreach ($rooms as $room) {
                Recruitment::query()->create([
                    'job_name' => 'Nhân viên ' . $room["name"],
                    'room_id' => $room["id"],
                    'body' => "<p>Không</p>",
                    'salary' => "10-" . rand(15, 20) . "M",
                    'time' => Carbon::now(),
                    'quantity'=>rand(1,2)
                ]);
            }
        }

    }
}
