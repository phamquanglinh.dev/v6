<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblFeaturesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_features')->delete();
        
        \DB::table('tbl_features')->insert(array (
            0 => 
            array (
                'id' => 1,
                'header' => 'CRM - quản lý chăm sóc khách hàng',
                'title' => 'TÍNH NĂNG CHĂM SÓC KHÁCH HÀNG CỦA GETFLY',
                'icon' => 'assets/uploads/features/images/33_Customer management.png_1683519372.png',
                'thumbnail' => 'assets/uploads/features/images/19_feature.png_1683519372.png',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '',
                'url' => '',
                'created_at' => '2023-04-24 11:54:13',
                'updated_at' => '2023-05-08 04:16:12',
            ),
            1 => 
            array (
                'id' => 2,
                'header' => 'Marketing Automation',
                'title' => 'Marketing Automation',
                'icon' => 'assets/uploads/features/images/85_Marketing auto.png_1682395744.png',
                'thumbnail' => 'assets/uploads/features/images/10_feature.png_1682395744.png',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '<p>Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng</p>',
                'url' => '#',
                'created_at' => '2023-04-25 04:09:04',
                'updated_at' => '2023-04-25 04:09:04',
            ),
            2 => 
            array (
                'id' => 3,
                'header' => 'Thống kê đo lường KPI',
                'title' => 'Thống kê đo lường KPI',
                'icon' => 'assets/uploads/features/images/61_Statistics measure kpi.png_1682396162.png',
                'thumbnail' => 'assets/uploads/features/images/26_feature.png_1682396162.png',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '<p>Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng</p><p><br>&nbsp;</p>',
                'url' => '#',
                'created_at' => '2023-04-25 04:16:02',
                'updated_at' => '2023-04-25 04:16:02',
            ),
            3 => 
            array (
                'id' => 4,
                'header' => 'Tích hợp đa dịch vụ',
                'title' => 'Tích hợp đa dịch vụ',
                'icon' => 'assets/uploads/features/images/65_Multi-service integration.png_1682397030.png',
                'thumbnail' => 'assets/uploads/features/images/28_feature.png_1682397030.png',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '<p>Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng</p><p><br>&nbsp;</p>',
                'url' => '#',
                'created_at' => '2023-04-25 04:30:30',
                'updated_at' => '2023-04-25 04:30:30',
            ),
            4 => 
            array (
                'id' => 5,
                'header' => 'API kết nối mở',
                'title' => 'API kết nối mở',
                'icon' => 'assets/uploads/features/images/1_Statistics measure kpi.png_1682397124.png',
                'thumbnail' => 'assets/uploads/features/images/95_feature.png_1682397124.png',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '<p>Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng</p><p><br>&nbsp;</p>',
                'url' => '#',
                'created_at' => '2023-04-25 04:32:04',
                'updated_at' => '2023-04-25 04:32:04',
            ),
            5 => 
            array (
                'id' => 6,
                'header' => '11 Module mở rộng',
                'title' => '11 Module mở rộng',
                'icon' => 'assets/uploads/features/images/94_Expansion Modules.png_1682397155.png',
                'thumbnail' => 'uploads/features/11 Module mở rộng14420100.jpeg',
                'description' => 'Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng',
                'body' => '<p>Phần mềm Getfly CRM - trợ thủ đắc lực của các chủ doanh nghiệp trong quản lý và chăm sóc khách hàng chuyên nghiệp, hiệu quả. Đảm bảo 100% khách hàng được chăm sóc. Nâng cao trải nghiệm và mức độ hài lòng của khách hàng, gia tăng tỷ lệ chuyển đổi thành hợp đồng</p><p><br>&nbsp;</p>',
                'url' => '#',
                'created_at' => '2023-04-25 04:32:35',
                'updated_at' => '2023-05-23 04:35:39',
            ),
        ));
        
        
    }
}