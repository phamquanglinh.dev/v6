<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OtpInFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table("tbl_form")->delete();
        DB::table("tbl_form")->insert([
            'name' => 'Form đăng ký dùng thử',
            'link' => 'https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082',
            'position' => 'dang-ky.html'
        ]);
        DB::table("tbl_form")->insert([
            'name' => 'Form liên hệ tư vấn',
            'link' => 'https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082',
            'position' => 'lien-he.html'
        ]);
        DB::table("tbl_form")->insert([
            'name' => 'Form đăng ký tuyển dụng',
            'link' => 'https://icdt.getflycrm.com/api/forms/viewform?key=jDmOIGJOZDdAb0v5lybSAguArYOM19jFhTxE4QnZ5uqCxZMkxN',
            'position' => 'tuyen-dung.html'
        ]);
        DB::table("tbl_form")->insert([
            'name' => 'Form đăng ký đối tác',
            'link' => 'https://icdt.getflycrm.com/api/forms/viewform?key=Pwu2bPjwbj6lP7TZiRFWzsyTNgkHf2LcaRyZhO9vEBjoWCvWXR',
            'position' => 'thu-moi-hop-tac.html'
        ]);
    }
}
