<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblPartnerTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_partner_type')->delete();
        
        \DB::table('tbl_partner_type')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Chatbot',
                'created_at' => '2023-06-15 09:36:17',
                'updated_at' => '2023-06-15 09:36:17',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Fanpage',
                'created_at' => '2023-06-15 09:36:24',
                'updated_at' => '2023-06-15 09:36:24',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Zalo',
                'created_at' => '2023-06-15 09:36:45',
                'updated_at' => '2023-06-15 09:36:45',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Tổng đài',
                'created_at' => '2023-06-15 09:36:54',
                'updated_at' => '2023-06-15 09:36:54',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Tuyển dụng',
                'created_at' => '2023-06-15 09:37:00',
                'updated_at' => '2023-06-15 09:37:00',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Thẻ thành viên',
                'created_at' => '2023-06-15 09:37:08',
                'updated_at' => '2023-06-15 09:37:08',
            ),
        ));
        
        
    }
}