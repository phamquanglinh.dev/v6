<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblRoomsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_rooms')->delete();
        
        \DB::table('tbl_rooms')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hành chính nhân sự',
                'created_at' => '2023-05-24 03:45:40',
                'updated_at' => '2023-05-24 03:45:49',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Marketing',
                'created_at' => '2023-05-24 03:46:00',
                'updated_at' => '2023-05-24 03:46:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Sales',
                'created_at' => '2023-05-24 03:46:06',
                'updated_at' => '2023-05-24 03:46:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Chăm sóc khách hàng',
                'created_at' => '2023-05-24 03:46:14',
                'updated_at' => '2023-05-24 03:46:14',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Đào tạo',
                'created_at' => '2023-05-24 03:46:21',
                'updated_at' => '2023-05-24 03:46:21',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Lập trình',
                'created_at' => '2023-05-24 03:46:29',
                'updated_at' => '2023-05-24 03:46:29',
            ),
        ));
        
        
    }
}