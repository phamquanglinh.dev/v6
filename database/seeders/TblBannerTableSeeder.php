<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TblBannerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_banner')->delete();
        
        \DB::table('tbl_banner')->insert(array (
            0 => 
            array (
                'banner_id' => 1,
                'title' => 'Banner1',
                'image' => '/assets/uploads/banner/images/getfly-vn1.jpg',
                'order' => 1,
                'position' => 0,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/click-2-call-f26.html',
                'created_at' => '2015-12-19 15:12:33',
                'updated_at' => '2018-12-06 17:22:53',
                'invalid' => 1,
                'click' => 0,
            ),
            1 => 
            array (
                'banner_id' => 2,
                'title' => 'Banner2',
                'image' => '/assets/uploads/banner/images/getfly-vn2.jpg',
                'order' => 2,
                'position' => 0,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/click-2-call-f26.html',
                'created_at' => '2016-12-19 15:12:33',
                'updated_at' => '2018-12-06 17:22:53',
                'invalid' => 1,
                'click' => 0,
            ),
            2 => 
            array (
                'banner_id' => 3,
                'title' => 'Banner3',
                'image' => '/assets/uploads/banner/images/getfly-vn3.jpg',
                'order' => 3,
                'position' => 0,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/click-2-call-f26.html',
                'created_at' => '2016-12-19 15:12:33',
                'updated_at' => '2018-12-06 17:22:53',
                'invalid' => 1,
                'click' => 0,
            ),
            3 => 
            array (
                'banner_id' => 4,
                'title' => 'Ads',
                'image' => '/assets/uploads/banner/images/ads.jpg',
                'order' => 4,
                'position' => 0,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/click-2-call-f26.html',
                'created_at' => '2016-12-19 15:12:33',
                'updated_at' => '2018-12-06 17:22:53',
                'invalid' => 1,
                'click' => 0,
            ),
            4 => 
            array (
                'banner_id' => 20,
                'title' => 'Kết nối tổng đài điện thoại',
            'image' => '/assets/uploads/banner/images/ketnoitongdaidienthoai(2).png',
                'order' => 2,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/click-2-call-f26.html',
                'created_at' => '2016-12-19 15:12:33',
                'updated_at' => '2018-12-06 17:22:53',
                'invalid' => 1,
                'click' => 0,
            ),
            5 => 
            array (
                'banner_id' => 21,
                'title' => 'KPI khách hàng',
                'image' => '/assets/uploads/banner/images/kpikhachhang.png',
                'order' => 2,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/tao-dong-luc-f25.html',
                'created_at' => '2016-12-19 15:13:13',
                'updated_at' => '2018-12-06 17:22:48',
                'invalid' => 1,
                'click' => 0,
            ),
            6 => 
            array (
                'banner_id' => 22,
                'title' => 'Quản lý chiến dịch',
                'image' => '/assets/uploads/banner/images/chiendich.png',
                'order' => 3,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/maketing/chien-dich-f16.html',
                'created_at' => '2016-12-19 15:13:59',
                'updated_at' => '2018-12-06 17:22:43',
                'invalid' => 1,
                'click' => 0,
            ),
            7 => 
            array (
                'banner_id' => 23,
                'title' => 'Người bán hàng xuất sắc',
                'image' => '/assets/uploads/banner/images/nguoibanhangxs_thang.png',
                'order' => 4,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/nguoi-ban-hang-xuat-sac-f28.html',
                'created_at' => '2016-12-19 15:14:25',
                'updated_at' => '2018-12-06 17:22:38',
                'invalid' => 1,
                'click' => 0,
            ),
            8 => 
            array (
                'banner_id' => 24,
                'title' => 'Chi tiết khách hàng',
                'image' => '/assets/uploads/banner/images/chitietkhachhang.png',
                'order' => 5,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/thong-tin-360-giai-phap-thong-tin-khach-hang-f1.html',
                'created_at' => '2016-12-19 15:14:50',
                'updated_at' => '2018-12-06 17:22:31',
                'invalid' => 1,
                'click' => 0,
            ),
            9 => 
            array (
                'banner_id' => 25,
                'title' => 'Danh sách khách hàng',
                'image' => '/assets/uploads/banner/images/danhsachkh.png',
                'order' => 6,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/phan-loai-khach-hang-f5.html',
                'created_at' => '2016-12-19 15:15:24',
                'updated_at' => '2018-12-06 17:22:24',
                'invalid' => 1,
                'click' => 0,
            ),
            10 => 
            array (
                'banner_id' => 26,
                'title' => 'Pos - đơn hàng bán lẻ',
                'image' => '/assets/uploads/banner/images/pos-donhangbanle.png',
                'order' => 7,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/diem-ban-le-f7.html',
                'created_at' => '2016-12-19 15:15:48',
                'updated_at' => '2018-12-06 17:22:19',
                'invalid' => 1,
                'click' => 0,
            ),
            11 => 
            array (
                'banner_id' => 27,
                'title' => 'Kịch bản Automation',
                'image' => '/assets/uploads/banner/images/kichbanautomation.png',
                'order' => 8,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/maketing/marketing-tu-dong-f10.html',
                'created_at' => '2016-12-19 15:16:13',
                'updated_at' => '2018-12-06 17:22:13',
                'invalid' => 1,
                'click' => 0,
            ),
            12 => 
            array (
                'banner_id' => 28,
                'title' => 'Email template',
                'image' => '/assets/uploads/banner/images/emailteamplate.png',
                'order' => 9,
                'position' => 1,
                'url' => 'http://getfly.vn/giai-phap/email-marketing-s8.html',
                'created_at' => '2016-12-19 15:16:40',
                'updated_at' => '2018-12-06 17:22:06',
                'invalid' => 1,
                'click' => 0,
            ),
            13 => 
            array (
                'banner_id' => 29,
                'title' => 'Landing page',
                'image' => '/assets/uploads/banner/images/landingpages.png',
                'order' => 10,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/maketing/landing-page-f43.html',
                'created_at' => '2016-12-19 15:17:20',
                'updated_at' => '2018-12-06 17:22:00',
                'invalid' => 1,
                'click' => 0,
            ),
            14 => 
            array (
                'banner_id' => 30,
                'title' => 'Giao tiếp nội bộ',
                'image' => '/assets/uploads/banner/images/chatnoibo.png',
                'order' => 2,
                'position' => 1,
                'url' => 'http://getfly.vn/tinh-nang/ban-hang/giao-tiep-noi-bo-f30.html',
                'created_at' => '2016-12-19 15:17:55',
                'updated_at' => '2018-12-06 17:21:53',
                'invalid' => 1,
                'click' => 0,
            ),
            15 => 
            array (
                'banner_id' => 52,
                'title' => 'Telesales thiện chiến',
                'image' => '/assets/uploads/banner/images/Telesale-013.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://goo.gl/6o52x5',
                'created_at' => '2017-11-16 08:40:33',
                'updated_at' => '2018-03-12 16:05:43',
                'invalid' => 1,
                'click' => 0,
            ),
            16 => 
            array (
                'banner_id' => 53,
                'title' => 'Thông báo lịch nghỉ Tết',
                'image' => '/assets/uploads/banner/images/Banner%20web%20l%E1%BB%8Bch%20ngh%E1%BB%89%20t%E1%BA%BFt-01.jpg',
                'order' => 98,
                'position' => 1,
                'url' => 'http://#',
                'created_at' => '2018-02-08 16:04:42',
                'updated_at' => '2018-04-19 08:15:14',
                'invalid' => 1,
                'click' => 0,
            ),
            17 => 
            array (
                'banner_id' => 54,
                'title' => 'Hội thảo Tối ưu hóa quy trình CSKH trên website',
                'image' => '/assets/uploads/banner/images/Banner%20web%20h%E1%BB%99i%20th%E1%BA%A3o.png',
                'order' => 99,
                'position' => 1,
                'url' => 'http://https://1.getfly.vn/page/hoi-thao-tu-dong-hoa-quy-trinh-cskh-tu-website-owlMX',
                'created_at' => '2018-03-04 07:14:22',
                'updated_at' => '2018-04-19 08:15:19',
                'invalid' => 1,
                'click' => 0,
            ),
            18 => 
            array (
                'banner_id' => 55,
                'title' => 'Sự khác biệt trong quản lý kênh bán hàng online bằng Social CRM',
                'image' => '/assets/uploads/banner/images/Banner%20h%E1%BB%99i%20th%E1%BA%A3o%20HCM%20web.jpg',
                'order' => 99,
                'position' => 1,
                'url' => 'http://https://1.getfly.vn/page/hoi-thao-su-khac-biet-trong-quan-ly-kenh-ban-hang-online-bang-social-crm/',
                'created_at' => '2018-03-16 10:32:45',
                'updated_at' => '2018-04-19 08:15:24',
                'invalid' => 1,
                'click' => 0,
            ),
            19 => 
            array (
                'banner_id' => 56,
                'title' => 'Hội thảo Tối ưu hiệu quả Telesales trong kỷ nguyên số',
                'image' => '/assets/uploads/banner/images/banner%20web%20h%E1%BB%99i%20th%E1%BA%A3o.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://1.getfly.vn/page/hoi-thao-quan-ly-telesales-trong-ky-nguyen-so/',
                'created_at' => '2018-04-09 10:08:53',
                'updated_at' => '2018-04-26 10:01:10',
                'invalid' => 1,
                'click' => 0,
            ),
            20 => 
            array (
                'banner_id' => 57,
                'title' => 'Telesales không hiệu quả do kỹ năng hay giải pháp',
                'image' => '/assets/uploads/banner/images/H%C3%B4i%20th%E1%BA%A3o%20th%C3%A1ng%205%20HCM-02.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://1.getfly.vn/page/telesales-khong-hieu-qua-do-ki-nang-hay-giai-phap/',
                'created_at' => '2018-04-26 10:00:51',
                'updated_at' => '2018-06-06 08:45:26',
                'invalid' => 1,
                'click' => 0,
            ),
            21 => 
            array (
                'banner_id' => 58,
                'title' => 'GetFly tham gia sự kiện Việt Nam ICT COMM 2018',
                'image' => '/assets/uploads/banner/images/anh%201.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://#',
                'created_at' => '2018-06-06 08:44:13',
                'updated_at' => '2018-06-06 15:37:55',
                'invalid' => 1,
                'click' => 0,
            ),
            22 => 
            array (
                'banner_id' => 59,
                'title' => 'GetFly tham gia sự kiện Việt Nam ICT COMM 2018',
                'image' => '/assets/uploads/banner/images/anh%20post.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://#',
                'created_at' => '2018-06-06 15:38:31',
                'updated_at' => '2018-06-17 23:14:37',
                'invalid' => 1,
                'click' => 0,
            ),
            23 => 
            array (
                'banner_id' => 60,
                'title' => 'Hội thảo Công thức vận hành doanh nghiệp hiệu quả thời 4.0 23/6',
                'image' => '/assets/uploads/banner/images/C%C3%B4ng_th%E1%BB%A9c_v%E1%BA%ADn_h%C3%A0nh_doanh_nghi%E1%BB%87p.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://goo.gl/7uPD63',
                'created_at' => '2018-06-17 23:14:27',
                'updated_at' => '2018-07-23 15:22:09',
                'invalid' => 1,
                'click' => 0,
            ),
            24 => 
            array (
                'banner_id' => 62,
                'title' => 'Hội thảo: Nâng cao năng suất bán hàng cùng công nghệ 4.0',
                'image' => '/assets/uploads/banner/images/1366x588.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://1.getfly.vn/page/hoi-thao-nang-cao-nang-suat-ban-hang-cung-cong-nghe-40-18-8',
                'created_at' => '2018-08-10 15:16:15',
                'updated_at' => '2018-09-27 17:24:55',
                'invalid' => 1,
                'click' => 0,
            ),
            25 => 
            array (
                'banner_id' => 63,
                'title' => 'Sự kiện: KINH DOANH THỰC CHIẾN THỜI ĐẠI SỐ',
                'image' => '/assets/uploads/banner/images/resize-banner-getfly-1366x588.jpg',
                'order' => 2,
                'position' => 1,
                'url' => 'http://https://kinhdoanhthucchien.com.vn/?utm_account=10',
                'created_at' => '2018-09-30 23:10:12',
                'updated_at' => '2018-12-07 08:38:26',
                'invalid' => 1,
                'click' => 0,
            ),
            26 => 
            array (
                'banner_id' => 64,
                'title' => 'Getfly CRM',
                'image' => '/assets/uploads/banner/images/cover%201366x558.png',
                'order' => 3,
                'position' => 1,
                'url' => 'http://www.getfly.vn/',
                'created_at' => '2018-11-14 11:06:17',
                'updated_at' => '2019-01-09 11:56:41',
                'invalid' => 0,
                'click' => 0,
            ),
            27 => 
            array (
                'banner_id' => 65,
                'title' => 'Sự kiện Vietnam Web Summit 2018',
            'image' => '/assets/uploads/banner/images/Website%201366x588%20(1).png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://https://www.getfly.vn/',
                'created_at' => '2018-12-07 10:04:33',
                'updated_at' => '2018-12-18 10:38:57',
                'invalid' => 1,
                'click' => 0,
            ),
            28 => 
            array (
                'banner_id' => 66,
                'title' => 'Giải pháp quản lý và chăm sóc khách hàng toàn diện cho SMES Việt',
            'image' => '/assets/uploads/banner/images/Thanhs%20Branding%201366x588(2).jpg',
                'order' => 2,
                'position' => 1,
                'url' => 'http://#',
                'created_at' => '2018-12-27 15:42:37',
                'updated_at' => '2019-04-26 08:39:13',
                'invalid' => 0,
                'click' => 0,
            ),
            29 => 
            array (
                'banner_id' => 67,
                'title' => 'Giữ chặt tiền cho doanh nghiệp - Getfly CRM',
                'image' => '/assets/uploads/banner/images/web.png',
                'order' => 1,
                'position' => 1,
                'url' => 'http://#',
                'created_at' => '2019-01-09 11:53:56',
                'updated_at' => '2019-02-21 14:48:03',
                'invalid' => 1,
                'click' => 0,
            ),
            30 => 
            array (
                'banner_id' => 72,
                'title' => 'Tiêu đề banner',
                'image' => 'admin/images/banner/72/1677638046.jpg',
                'order' => 2,
                'position' => 1,
                'url' => 'url',
                'created_at' => '2023-02-28 15:37:09',
                'updated_at' => '2023-03-07 09:33:46',
                'invalid' => 0,
                'click' => 0,
            ),
            31 => 
            array (
                'banner_id' => 74,
                'title' => 'NEW BANNER',
                'image' => '',
                'order' => 99,
                'position' => 1,
                'url' => 'asd',
                'created_at' => '2023-03-10 03:11:10',
                'updated_at' => '2023-03-13 08:52:35',
                'invalid' => 0,
                'click' => 0,
            ),
            32 => 
            array (
                'banner_id' => 75,
                'title' => 'Banner test Cột phải',
                'image' => 'assets/uploads/banner/images/31_banner-news1.png_1683791592.png',
                'order' => 1,
                'position' => 2,
                'url' => '#',
                'created_at' => '2023-05-11 07:53:12',
                'updated_at' => '2023-05-11 07:53:12',
                'invalid' => 0,
                'click' => 0,
            ),
            33 => 
            array (
                'banner_id' => 76,
                'title' => 'Banner test Cột phải 2',
                'image' => 'assets/uploads/banner/images/3_banner-news2.png_1683791614.png',
                'order' => 2,
                'position' => 2,
                'url' => 'https://fb.me',
                'created_at' => '2023-05-11 07:53:34',
                'updated_at' => '2023-05-11 07:53:34',
                'invalid' => 0,
                'click' => 0,
            ),
            34 => 
            array (
                'banner_id' => 77,
                'title' => 'Banner test Cột phải 3',
                'image' => 'assets/uploads/banner/images/63_ab.jpg_1683792378.jpg',
                'order' => 3,
                'position' => 2,
                'url' => 'https://fb.me',
                'created_at' => '2023-05-11 07:53:51',
                'updated_at' => '2023-05-11 08:06:18',
                'invalid' => 0,
                'click' => 0,
            ),
        ));
        
        
    }
}