<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tbl_team_info = array(
            "id" => 1,
            "thumbnail" => "assets/uploads/team/2023060802241073537500.png",
            "title" => "Làm việc tại Getfly, bạn không chỉ có trải nghiệm đa dạng với các dự án, mà bạn còn được tham gia rất nhiều hoạt động sôi động và thú vị, nơi bạn thể hiện vô số tài lẻ của mình.",
            "created_at" => "2023-06-08 02:24:10",
            "updated_at" => "2023-06-08 02:24:10",
        );
        DB::table("tbl_team_info")->delete();
        DB::table("tbl_team_info")->insert($tbl_team_info);

    }
}
