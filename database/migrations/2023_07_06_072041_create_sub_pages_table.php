<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_sub_pages', function (Blueprint $table) {
            $table->id();
            $table->string("page_title");
            $table->string("page_description");
            $table->string("page_keywords");
            $table->string("page_slug");
            $table->longText("page_sub_title");
            $table->string("page_image_title");
            $table->longText("page_tabs");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_sub_pages');
    }
};
