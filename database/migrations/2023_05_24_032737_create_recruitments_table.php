<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_recruitments', function (Blueprint $table) {
            $table->id();
            $table->string("job_name");
            $table->string("salary");
            $table->date("time");
            $table->integer("quantity");
            $table->longText("body");
            $table->integer("room_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_recruitments');
    }
};
