<?php

use App\Models\V2Package;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_v2_packages', function (Blueprint $table) {
            $table->integer('type')->default(V2Package::ON_CLOUD_PACKAGE);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_v2_packages', function (Blueprint $table) {
            if (Schema::hasColumn('tbl_v2_packages', 'type')) {
                $table->dropColumn('type');
            }
        });
    }
};
