<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Schema::hasTable('tbl_v2_partners')) {
            Schema::create('tbl_v2_partners', function (Blueprint $table) {
                $table->id();
                $table->string('partner_name');
                $table->string('partner_logo');
                $table->integer('place_in_top_home');
                $table->integer('place_in_bottom_home');
                $table->integer('lft')->nullable();
                $table->integer('rgt')->nullable();
                $table->integer('depth')->nullable();
                $table->integer('parent_id')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_v2_partners');
    }
};
