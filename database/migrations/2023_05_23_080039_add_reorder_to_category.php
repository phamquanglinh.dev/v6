<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_categories', function (Blueprint $table) {
            $table->integer("depth")->default(1);
            $table->renameColumn("nleft", "lft");
            $table->renameColumn("nright", "rgt");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_categories', function (Blueprint $table) {
            $table->dropColumn("depth");
            $table->renameColumn("lft", "nleft");
            $table->renameColumn("rgt", "nright");
        });
    }
};
