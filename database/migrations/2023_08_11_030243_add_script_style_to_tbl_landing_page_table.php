<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_landing_page', function (Blueprint $table) {
            $table->longText("style")->nullable();
            $table->longText("script")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_landing_page', function (Blueprint $table) {
            $table->dropColumn("style");
            $table->dropColumn("script");
        });
    }
};
