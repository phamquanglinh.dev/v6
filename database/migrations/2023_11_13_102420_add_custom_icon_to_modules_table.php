<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_modules', function (Blueprint $table) {
            $table->string('icon')->default("");
            $table->integer('lft')->nullable();
            $table->integer('rgt')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('depth')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_modules', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->dropColumn('lft');
            $table->dropColumn('rgt');
            $table->dropColumn('parent_id');
            $table->dropColumn('depth');
        });
    }
};
