<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_site_info', function (Blueprint $table) {
            $table->integer("partners")->default(0);
            $table->integer("experience_years")->default(0);
            $table->integer("profession")->default(0);
            $table->longText("contact_form")->nullable();
        });
        DB::table("tbl_site_info")->where("id",1)->update([
            "contact_form"=>"https://icdt.getflycrm.com/api/forms/viewform?key=3yHqZyn3aN6XuWyDMUG5JJkpCNkn5w8lAeLePtzKPC6PJFR082",
            "experience_years"=>11,
            "profession"=>200,
            "partners"=>4000,
            'content'=>'<p>Getfly CRM mang đến giải ph&aacute;p phần mềm quản l&yacute; v&agrave; chăm s&oacute;c kh&aacute;ch h&agrave;ng to&agrave;n diện, chuy&ecirc;n nghiệp cho SMEs Việt. Quản l&yacute; kh&eacute;p k&iacute;n quy tr&igrave;nh Trước - Trong - Sau b&aacute;n hệ thống v&agrave; b&agrave;i bản, hỗ trợ quản l&yacute; tương t&aacute;c giữa c&aacute;c ph&ograve;ng ban Marketing - Sales - Chăm s&oacute;c kh&aacute;ch h&agrave;ng tr&ecirc;n một nền t&agrave;ng. Ứng dụng thống nhất quy tr&igrave;nh, tiết kiệm thời gian, tối ưu nguồn lực, đột ph&aacute;t doanh thu.</p>'
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_site_info', function (Blueprint $table) {
            $table->dropColumn("partners");
            $table->dropColumn("experience_years");
            $table->dropColumn("profession");
            $table->dropColumn("contact_form");
        });
    }
};
