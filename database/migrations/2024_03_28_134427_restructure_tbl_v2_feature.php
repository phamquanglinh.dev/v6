<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('tbl_v2_feature')) {
            Schema::table('tbl_v2_feature', function (Blueprint $table) {
                if (Schema::hasColumn('tbl_v2_feature', 'feature_link') && ! Schema::hasColumn('tbl_v2_feature', 'feature_tooltip')) {
                    $table->renameColumn('feature_link', 'feature_tooltip');
                }
                $table->integer('tab')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::hasTable('tbl_v2_feature')) {
            Schema::table('tbl_v2_feature', function (Blueprint $table) {
                if (Schema::hasColumn('tbl_v2_feature', 'feature_tooltip') && ! Schema::hasColumn('tbl_v2_feature', 'feature_link')) {
                    $table->renameColumn('feature_tooltip', 'feature_link');
                    $table->dropColumn('tab');
                }
            });
        }
    }
};
