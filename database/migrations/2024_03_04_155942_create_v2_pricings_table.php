<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Schema::hasTable('tbl_v2_pricings')) {
            Schema::create('tbl_v2_pricings', function (Blueprint $table) {
                $table->id();
                $table->string('business_size');
                $table->string('production_fee');
                $table->string('setup_fee');
                $table->string('setup_number');
                $table->longText('number_of_user');
                $table->longText('data_limit');
                $table->string('payment_cycle'); # chu kỳ thanh toán
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_v2_pricings');
    }
};
