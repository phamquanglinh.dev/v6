<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_menu', function (Blueprint $table) {
            $table->renameColumn("nleft", "lft");
            $table->renameColumn("nright", "rgt");
            $table->integer("depth")->default(1);
            $table->integer("mega")->nullable();
            $table->string("icon")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_menu', function (Blueprint $table) {
            $table->renameColumn("lft", "nleft");
            $table->renameColumn("rgt", "nright");
            $table->dropColumn("depth");
            $table->dropColumn("mega");
            $table->dropColumn("icon");
        });
    }
};
