<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_v2_pricings', function (Blueprint $table) {
            if (! Schema::hasColumn('tbl_v2_pricings', 'upgrade_fee')) {
                $table->string('upgrade_fee')->default('');
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_v2_pricings', function (Blueprint $table) {
            if (Schema::hasColumn('tbl_v2_pricings', 'upgrade_fee')) {
                $table->dropColumn('upgrade_fee');
            }
        });
    }
};
