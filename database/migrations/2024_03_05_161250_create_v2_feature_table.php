<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Schema::hasTable('tbl_v2_feature')) {
            Schema::create('tbl_v2_feature', function (Blueprint $table) {
                $table->id();
                $table->string('feature_name');
                $table->string('feature_link');
                $table->integer('startup_available');
                $table->integer('professional_available');
                $table->integer('enterprise_available');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_v2_feature');
    }
};
