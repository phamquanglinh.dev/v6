<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_feedback', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('title');
            $table->text('note')->nullable();
            $table->string('link')->nullable();
            $table->string('content')->nullable();
            $table->string('position');
            $table->tinyInteger('active');
            $table->tinyInteger('priority');
            $table->smallInteger('type_feedback');
            $table->string('avatar')->nullable();
            $table->string('avatar_title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_feedback');
    }
};
