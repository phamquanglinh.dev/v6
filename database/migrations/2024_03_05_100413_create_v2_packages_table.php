<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (! Schema::hasTable('tbl_v2_packages')) {
            Schema::create('tbl_v2_packages', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('description');
                $table->string('amount_per_month');
                $table->string('user');
                $table->string('data');
                $table->string('benefit_text');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_v2_packages');
    }
};
