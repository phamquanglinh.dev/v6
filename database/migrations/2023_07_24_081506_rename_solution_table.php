<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_solutionGetfly', function (Blueprint $table) {
            $table->rename("tbl_solution");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_solution', function (Blueprint $table) {
            $table->rename("tbl_solutionGetfly");
        });
    }
};
