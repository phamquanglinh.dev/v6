<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_sub_pages', function (Blueprint $table) {
            $table->longText("page_image_title")->nullable()->change();
            $table->longText("page_youtube_title")->nullable();
            $table->integer("page_title_type")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_sub_pages', function (Blueprint $table) {
            $table->longText("page_image_title")->nullable(false)->change();
            $table->dropColumn("page_youtube_title");
            $table->dropColumn("page_title_type");
        });
    }
};
