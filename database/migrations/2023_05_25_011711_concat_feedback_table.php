<?php

use App\Models\Feedback;
use App\ViewModels\Sites\Feedback\Object\FeedbackObject;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table("tbl_feedback", function (Blueprint $table) {
            $table->longText("content")->change();
        });
        $feedbackTexts = DB::table("tbl_feedback_text")->get();
        foreach ($feedbackTexts as $feedbackText) {
            $feedbackObject = new FeedbackObject(
                name: $feedbackText->name,
                title: $feedbackText->title,
                note: $feedbackText->note,
                link: null,
                content: $feedbackText->content,
                position: $feedbackText->position,
                active: $feedbackText->active,
                priority: $feedbackText->priority,
                type_feedback: 1,
                avatar: $feedbackText->avatar,
                avatar_title: $feedbackText->avatar_title
            );
            DB::table("tbl_feedback")->insert($feedbackObject->toArray());
        }
        $feedbackVideos = DB::table("tbl_feedback_video")->get();
        foreach ($feedbackVideos as $feedbackVideo) {
            $feedbackObject= new FeedbackObject(
                name: "Khách hàng",
                title: $feedbackVideo->title,
                note: $feedbackVideo->description,
                link: $feedbackVideo->link,
                content: $feedbackVideo->description,
                position: "Người dùng",
                active: $feedbackVideo->active,
                priority: $feedbackVideo->priority,
                type_feedback: 2,
                avatar: "https://media.istockphoto.com/id/1016744004/vector/profile-placeholder-image-gray-silhouette-no-photo.jpg?s=612x612&w=0&k=20&c=mB6A9idhtEtsFXphs1WVwW_iPBt37S2kJp6VpPhFeoA=",
                avatar_title: "https://media.istockphoto.com/id/1016744004/vector/profile-placeholder-image-gray-silhouette-no-photo.jpg?s=612x612&w=0&k=20&c=mB6A9idhtEtsFXphs1WVwW_iPBt37S2kJp6VpPhFeoA="
            );
            DB::table("tbl_feedback")->insert($feedbackObject->toArray());
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Feedback::query()->where("type_feedback", 1)->delete();
        Feedback::query()->where("type_feedback", 2)->delete();
        Schema::table("tbl_feedback", function (Blueprint $table) {
            $table->string("content")->change();
        });
    }
};
