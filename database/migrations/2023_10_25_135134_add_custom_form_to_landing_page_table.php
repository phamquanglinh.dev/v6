<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('tbl_landing_page', function (Blueprint $table) {
            $table->longText("custom_form")->nullable();
            $table->integer("enable_popup")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('tbl_landing_page', function (Blueprint $table) {
            $table->dropColumn("custom_form");
            $table->dropColumn("enable_popup");
        });
    }
};
