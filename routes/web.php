<?php

use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Sites\HomeController;
use App\Http\Controllers\Sites\JobController;
use App\Http\Controllers\Sites\NewsController;
use App\Http\Controllers\Sites\PhotoController;
use App\Http\Controllers\Sites\SolutionController;
use App\Http\Controllers\Sites\SubPageController;
use App\Http\Controllers\Sites\TeamController;
use App\ViewModels\Sites\Home\SubPageViewModel;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::group(['namespace' => 'Sites'], function () {
    Route::prefix('/')->name('sites.')->group(function () {
        //Single Url
        Route::get("/hoi-dap.html", function () {
            return 1;
        })->name("faq");
        Route::get("/thanks.html", function () {
            return view('sites.pages.thanks', []);
        })->name("thanks");
        Route::get("/GetflyCRM-Giai-phap-quan-ly-va-cham-soc-khach-hang-toan-dien-cho-SMEs-Viet", [
            HomeController::class,
            "solutionPageAction"
        ]);
        Route::get("/features.html", [HomeController::class, "solutionPageAction"]);
        Route::get('/page/{slug}', [SubPageController::class, "index"])->name("sub-page");
        Route::get('/module-mo-rong/{slug}.html', [SubPageController::class, "index"])->name("sub-page-module");
        Route::get('/tinh-nang/{folder}/{slug}.html', [
            SubPageController::class,
            "pageWithFolder"
        ])->name("sub-page-folder");
        Route::get('/tinh-nang/{slug}.html', [SubPageController::class, "index"])->name("sub-page-feature");

        Route::get('/marketing.html', [SubPageController::class, "marketing"])->name("sub-page-marketing");
        Route::get('/lien-he.html', "HomeController@contactPageAction")->name("contact");
        Route::get('/lien-he', "HomeController@contactPageAction")->name("contact");
        Route::get("/tuyen-dung.html", [JobController::class, "listJobsAction"])->name("jobs.list");
        Route::get("/chi-tiet-tuyen-dung/{recruitment_id?}", [
            JobController::class,
            "detailJobAction"
        ])->name("jobs.detail");
        Route::get('/ve-getfly.html', "HomeController@aboutPageAction")->name("about");
//        Route::get('/dang-ky.html', "HomeController@trialPageAction")->name("trial");
        Route::get('/dang-ky-dung-thu-phan-mem.html', "HomeController@trialPageAction")->name("trial");
        Route::get('/cuoc-song-getfly.html', [TeamController::class, "index"])->name("team-getfly");
        Route::get('/he-sinh-thai.html', [HomeController::class, "partnerPageAction"])->name("partner");
        Route::get('/feedback', [HomeController::class, "storyPageAction"])->name("stories");
        Route::get('/feedback/detail/{id}', [HomeController::class, "storyDetailPageAction", "id"])->name("story");
        Route::get('/crm-quan-ly-khach-hang.html/', [HomeController::class, "subHomePageAction"]);
        Route::post("/tuyen-dung", [JobController::class, "ajaxJobAction"])->name("jobs.ajax");
        Route::post("/photo/", [PhotoController::class, "getAjaxPhotoById"])->name("photo.ajax");
        Route::any("/tim-kiem", [NewsController::class, "searchNewsAction"])->name("newspapers.search");
        Route::any("/tim-kiem-them", [NewsController::class, "searchMoreNewsAction"])->name("newspapers.search-more");
        Route::get("/thu-moi-hop-tac.html", [HomeController::class, "invitePageAction"])->name("invite");
        //
        Route::get("/giai-phap/{slug}.html", [SolutionController::class, "showSolutionAction"])->name("solution");
        Route::get('/bang-gia.html', "HomeController@pricingPageAction")->name("pricing");
        Route::get('/danh-muc/{id}/{slug}.html', [NewsController::class, "listBlogAction", "id", "slug"])
            ->where("slug", "(.*)")
            ->name('blogs.list')->name("blog.list");

        Route::get('/', 'HomeController@listHomeAction')->name('home.list');

        Route::get('/i{id}-{slug}.html', [NewsController::class, "listNewspapersAction", "id", "slug"])
            ->where("slug", "(.*)")->name('newspapers.list');

        Route::any('/newspapers/more', 'NewsController@loadMoreNewspaper')->name('newspapers.more');

        Route::get('/{newspaper_data}.html', [NewsController::class, "detailNewspaperAction", "newspaper_data"])
            ->where("newspaper_data", "(.*)")
            ->name('newspaper.detail');
    });
});
Route::post("admin/login", [AuthController::class, "login"])->name("auth.login")->name("backpack.auth.login");

