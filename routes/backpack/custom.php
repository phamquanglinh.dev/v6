<?php

use App\Http\Controllers\Admin\LandingPageCrudController;
use App\Http\Controllers\Admin\V2\V2PricingCrudController;
use App\Http\Controllers\Admin\V2HomeCrudController;
use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array)config('backpack.base.web_middleware', 'web'),
        (array)config('backpack.base.middleware_key', 'admin')
    ),
    'namespace' => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('menu', 'MenuCrudController');
    Route::crud('banner', 'BannerCrudController');
    Route::get('banner/reorder', 'BannerCrudController@reorderOperation');
    Route::post('banner/order', 'BannerCrudController@orderOperation')->name("banner-order");
    Route::crud('feature', 'FeatureCrudController');
    Route::crud('feedback', 'FeedbackCrudController');
    Route::crud('customer', 'CustomerCrudController');
    Route::get('customer/reorder', 'CustomerCrudController@reorderOperation');
    Route::post('customer/order', 'CustomerCrudController@orderOperation')->name("customer-order");
    Route::crud('news', 'NewsCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('module', 'ModuleCrudController');
    Route::crud('pricing', 'PricingCrudController');
    Route::crud('site-info', 'SiteInfoCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('recruitment', 'RecruitmentCrudController');
    Route::crud('room', 'RoomCrudController');
    Route::crud('team-info', 'TeamInfoCrudController');
    Route::crud('photo', 'PhotoCrudController');
    Route::get('photo/reorder', 'PhotoCrudController@reorderOperation');
    Route::post('photo/order', 'PhotoCrudController@orderOperation')->name("photo-order");
    Route::crud('partner', 'PartnerCrudController');
    Route::crud('partner-type', 'PartnerTypeCrudController');
    Route::crud('mega-menu', 'MegaMenuCrudController');
    Route::crud('pricing-of-feature', 'PricingOfFeatureCrudController');
    Route::crud('register-form', 'RegisterFormCrudController');
    Route::crud('sub-page', 'SubPageCrudController');
    Route::crud('solution', 'SolutionCrudController');
    Route::crud('page-feature', 'PageFeatureCrudController');
    Route::crud('landing-page', 'LandingPageCrudController');
    Route::crud('page-builder', 'PageBuilderCrudController');
    Route::post("page-builder/save", [
        \App\Http\Controllers\Admin\PageBuilderCrudController::class,
        "pageBuilderSaveAction"
    ])->name("page-builder-save");
    Route::post("page-builder/upload", [
        \App\Http\Controllers\Admin\PageBuilderCrudController::class,
        "imageUpload"
    ])->name("page-builder-imageUpload");
    Route::get("page-builder/reset/{id}", [
        LandingPageCrudController::class,
        "resetContent",
        "id"
    ])->name("page-builder.reset");
    Route::get("ladipage/import", [
        LandingPageCrudController::class,
        "setupImportLadipageAction"
    ])->name("admin.ladipage");
    Route::post("ladipage/import", [
        LandingPageCrudController::class,
        "importLadipageAction"
    ])->name("admin.ladipage.import");
    Route::get('v2/pricing', [V2PricingCrudController::class, 'showV2PricingPageAction']);
    Route::get('v2/pricing/edit', [V2PricingCrudController::class, 'editV2PricingPageAction']);
    Route::post('v2/pricing/update/pricing', [V2PricingCrudController::class, 'updateV2Pricing']);
    Route::post('v2/pricing/update/feature', [V2PricingCrudController::class, 'updateV2Feature']);
    Route::post('v2/pricing/update/on-cloud-package', [V2PricingCrudController::class, 'updateV2OnCloudPackage']);
    Route::post('v2/pricing/update/on-premise-package', [V2PricingCrudController::class, 'updateV2OnPremisePackage']);
    Route::crud('v2-partner', 'V2PartnerCrudController');
    Route::get('v2/home-page', [V2HomeCrudController::class, 'index']);
    Route::post('v2/home-page/newspaper', [V2HomeCrudController::class, 'updateDelegateNew']);
});

Route::group([
    'namespace' => 'Backpack\LogManager\app\Http\Controllers',
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'prefix' => config('backpack.base.route_prefix', 'admin'),
], function () {
    Route::get('logger', 'LogController@index')->name('log.index');
    Route::get('logger/preview/{file_name}', 'LogController@preview')->name('log.show');
    Route::get('logger/download/{file_name}', 'LogController@download')->name('log.download');
    Route::delete('logger/delete/{file_name}', 'LogController@delete')->name('log.destroy');
});