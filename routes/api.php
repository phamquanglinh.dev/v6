<?php

use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Sites\HomeController;
use App\Http\Controllers\Sites\PhotoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post("/photo/", [PhotoController::class, "getAjaxPhotoById"])->name("photo.ajax");
Route::post("/upload/tinymce", [UploadController::class, "tinymce"])->name("uploads.tinymce");
Route::post('ladipage/webhook', [HomeController::class, 'webhook']);
Route::post('lala_move/webhook', [HomeController::class, 'webhookLalaMove']);