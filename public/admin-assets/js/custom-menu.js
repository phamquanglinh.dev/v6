$(document).ready(function () {
    let menu_type = 0
    const raw_link = $("#raw_link")
    const category_select = $("#category_select")
    const news_select = $("#news_select")
    const pages_select = $("#pages_select")
    $("#menu_type").change(function (e) {
        menu_type = e.target.value
        if (e.target.value === "") {
            e.target.value = 0
            menu_type = 0
        }
        console.log(menu_type)
        switch (parseInt(menu_type)) {
            case 0:
                console.log("run")
                showField(raw_link)
                hideField(category_select)
                hideField(news_select)
                hideField(pages_select)
                break
            case 1:
                showField(category_select)
                hideField(raw_link)
                hideField(news_select)
                hideField(pages_select)
                break
            case 2:
                showField(news_select)
                hideField(raw_link)
                hideField(category_select)
                hideField(pages_select)
                break
            case 3:
                hideField(news_select)
                hideField(raw_link)
                hideField(category_select)
                showField(pages_select)
                break
        }

    })


    function showField(field) {
        field.addClass("col-md-12 mb-3")
        field.removeClass("d-none")
    }

    function hideField(field) {
        field.addClass("d-none")
        field.removeClass("col-md-12 mb-3")
    }
})